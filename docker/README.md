#### docker操作

#### [docker hub](https://hub.docker.com/)

#### 拉去镜像 

docker pull ubuntu

从docker hub仓库下载 ubuntu镜像latest版本

#### 查看本机的镜像信息

docker images

#### 创建镜像

创建镜像的方法有3种：基于已有镜像的容器创建，基于本地模板导入，基于dockerfile创建

1，基于已有容器创建

docker container commit

启动一个镜像，并在其中进行修改操作 如：

docker run -it ubuntu:18.04 /bin/bash

修改镜像之后，用commit提交一个新的镜像

2，基于本地模板导入

用户也可以直接从一个操作系统模板文件导入一个镜像，主要使用docker container import命令

3，基于Dockerfile创建

基于Dockerfile创建是最常见的方式。Dockerfile是一个文本文件，利用给定的指令
描述基于某个父镜像创建新镜像的过程

Dockerfile基于debian:stretch-slim镜像创建python3环境示例：

FROM debian:stretch-slim

LABEL version="1.0" maintainer="docker user <docker_user@github>"

RUN apt-get update && apt-get install -y python3 && apt-get clean && rm -rf /var/lib/apt/lists/*

然后用docker image build -t python:3

docker images | grep python

#### 创建容器

docker create -it ubuntu:latest

#### 停止容器

启动容器并停止

docker run --name test --rm -it ubuntu bash

docker pause test

#### 终止容器

docker stop 

#### 进入容器

docker exec -it 

#### 删除容器

docker rm 

#### 删除镜像

sudo docker rmi [REPOSITORY:tag] 


#### 本地电脑上传数据到容器中

```
sudo docker cp /home/hasaki/Desktop/common [容器ID]:容器内的路径
```

### 上传镜像到阿里云

在本地commit容器成镜像之后

```
sudo docker commit -a "上传人名称" -m "上传的信息" a404c6c174a2  mymysql:v1
```

[资料](https://www.cnblogs.com/makalochen/p/14240796.html)

进入：https://cr.console.aliyun.com 阿里云镜像控制台 需要注册 用户名就是你的淘宝或者支付宝 账号名称 ，镜像控制台密码单独设置

创建命令空间

创建镜像仓库后进入仓库，按照教程实现即可


#### docker compose

[docker compose官网](https://github.com/docker/compose/releases)

```shell
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

docker-compose --version
```


#### docker与宿主机共享文件

```
docker run -it -v /home/hasaki/Desktop:/share microsoft/dotnet:latest /bin/bash
```
把宿主机的/home/hasaki/下载目录挂载到microsoft/dotnet:latest容器的/share目录下。
执行完上面命令进入Docker容器后，进入/share文件夹下，ls后就会看到原来宿主机下目录“/home/hasaki/下载”的文件。
