import java.io.{BufferedReader, InputStreamReader}
import java.net.ServerSocket

object Server {
  var port:Int=0     // socket服务的连接的端口

  def main(args: Array[String]): Unit = {
    println("start program")
    val serverSocket = new ServerSocket(port)

    val socket = serverSocket.accept()
    val bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream, "utf-8"))

    val data = bufferedReader.readLine()
    println(data)

    bufferedReader.close()

    socket.close()
  }

  // 设置端口
  def setPort(portNum:Int):Unit={
    port=portNum
  }

}
