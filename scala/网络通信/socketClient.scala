import java.net.Socket

object Client {
    var hostname:String=""
    var port:Int=8888
  def main(args: Array[String]): Unit = {
    val client = new Socket("<hostname>", <port>)

    val outputStream = client.getOutputStream

    outputStream.write("Hello Scala".getBytes("utf-8"))

    outputStream.flush()
    outputStream.close()
  }

  // 设置ip地址和端口
  def setHost(hostnameString:String):Unit={
      hostname=hostname
  }
  def setPort(portNum:Int):Unit={
      port=portNum
  }
}
