#### RDD转DateFrame

```python
import pandas as pd
from pyspark import SparkContext
hdfs = "hdfs://localhost:9000/user/hasaki/test/600000.csv"  
sc = SparkContext("spark://hasaki:8888", "hasaki")
data = sc.textFile(hdfs).cache()

# RDD转python list
type(data.collect())
>>> <class> list

# 生成df
df=pd.DataFrame(data.collect())
```

#### createDataFrame

```
from pyspark.sql import SparkSession
spark = SparkSession.builder.getOrCreate()

# 把pandas 的 DataFrame 转换成 spark 的 DataFrame
spark_df=spark.createDataFrame(df)
```