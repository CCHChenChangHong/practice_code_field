#### ubuntu sbt的安装

```shell
echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | sudo apt-key add
sudo apt-get update
sudo apt-get install sbt
```

或者官网下载安装

[官网](https://www.scala-sbt.org/download.html)
```
wget https://github.com/sbt/sbt/releases/download/v1.5.0/sbt-1.5.0.tgz
tar -zxvf sbt-1.5.0.tgz
cd sbt/bin/sbt sbtVersion

sudo mv sbt /usr/local/

sudo vim ~/.bashrc
export SBT_HOME=/usr/local/sbt
export PATH=${SBT_HOME}/bin:$PATH
source ~/.bashrc


```