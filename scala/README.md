#### 环境配置

1,java的jdk

2,scala的sdk  [scala2.13.5的deb](https://downloads.lightbend.com/scala/2.13.5/scala-2.13.5.deb)

3,IDEA

4,[清华镜像hadoop](https://mirrors.tuna.tsinghua.edu.cn/apache/hadoop/common/)

5,[清华镜像spark](https://mirrors.tuna.tsinghua.edu.cn/apache/spark/spark-3.1.1/)

#### 在IDEA上运行scala : https://blog.csdn.net/qq_24852439/article/details/103203793

简单来说就是

1,创建项目的时候选择maven模板

2,在项目的src->main下创建一个scala目录

3,右键scala目录选择make D... as->Sourses Root

4,然后就可以在scala目录中创建scala object了

#### 第三方库的官网

[Akka](https://developer.lightbend.com/guides/akka-quickstart-scala/)

[jsoup](https://jsoup.org/download)

[HttpComponents](http://hc.apache.org/downloads.cgi)

#### 在window上使用spark集群

[使用案例](http://www.voidcn.com/article/p-ambksfrf-byt.html)

#### 在centos上使用spark集群

[系统下载香港](http://mirror-hk.koddos.net/centos/8.3.2011/isos/x86_64/)

#### 在linux上安装spark和hadoop

ubuntu是默认没有安装ssh的,hadoop是用ssh的方式进行节点间通信的,要先安装ssh

```
sudo apt-get install ssh
# 验证是否成功安装ssh
which ssh

# 查看是否安装了openssh-server  
ps -e|grep ssh

# 如果只有ssh-agent那么就是没有安装openssh-server
sudo apt-get install openssh-server
```
如果安装openssl-server报错:阿里云链接不上使用清华的源
```
https://mirror.tuna.tsinghua.edu.cn/help/ubuntu/
```

[hadoop一些教程](https://blog.csdn.net/lyqdy/article/details/106604489)

```
sudo apt update
sudo apt upgrade
# 先安装java
sudo apt install openjdk-15-jre-headless
# cd /usr/lib/jvm
# sudo mv java-15-openjdk-amd64 java   # 不用修改

# -----配置系统变量  在下面

# 安装hadoop
# apache源头https://mirror-hk.koddos.net/apache/hadoop/common/hadoop-3.2.2/hadoop-3.2.2.tar.gz
wget http://mirrors.tuna.tsinghua.edu.cn/apache/hadoop/common/hadoop-3.2.2/hadoop-3.2.2.tar.gz
sudo tar -zxvf hadoop-3.2.2.tar.gz -C /usr/local
cd /usr/local
sudo mv hadoop-3.2.2 hadoop
hadoop version

# 安装spark
# apache源https://mirror-hk.koddos.net/apache/spark/spark-3.1.1/spark-3.1.1-bin-hadoop3.2.tgz
wget https://mirrors.tuna.tsinghua.edu.cn/apache/spark/spark-3.1.1/spark-3.1.1-bin-hadoop3.2.tgz 
sudo tar xvzf spark-3.1.1-bin-hadoop3.2.tgz -C /usr/local
cd /usr/local
sudo mv spark-3.1.1-bin-hadoop3.2 spark
spark-shell
```

[spark一些教程](https://www.yundongfang.com/Yun41434.html)

[spark在线远程集群](https://zhuanlan.zhihu.com/p/108008927)

[spark完全分布式](https://zhuanlan.zhihu.com/p/125107768)

```
vim ~/.bashrc

# java config 
export JAVA_HOME=/usr/lib/jvm/java-15-openjdk-amd64
export JRE_HOME=${JAVA_HOME}/jre 
export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib
export PATH=${JAVA_HOME}/bin:$PATH


# hadoop config
export HADOOP_HOME=/usr/local/hadoop
export CLASSPATH=$($HADOOP_HOME/bin/hadoop classpath):$CLASSPATH
export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native
export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin
export HADOOP_MAPRED_HOME=$HADOOP_HOME
export HADOOP_COMMON_HOME=$HADOOP_HOME
export HADOOP_HDFS_HOME=$HADOOP_HOME
export YARN_HOME=$HADOOP_HOME
export HADOOP_OPTS="-DJava.library.path=$HADOOP_HOME/lib"
export JAVA_LIBRARY_PATH=$HADOOP_HOME/lib/native:$JAVA_LIBRARY_PATH


# spark config
export SPARK_HOME=/usr/local/spark
export PATH=$PATH:$SPARK_HOME/bin:$SPARK_HOME/sbin

source ~/.bashrc
```


完全分布式设置
```
sudo su
scp -r /usr/local/spark 子节点userName@子节点ip地址:/home/子节点userName
cd /usr/local/spark/conf
sudo chmod 777 spark-env.sh
# 修改里面的SPARK_LOCAL_IP为本机ip

# 返回到master中复制spark/sbin/start-all.sh ->./start-spark.sh  用于区别hadoop的启动
```

#### spark服务启动

1,启动主节点 : 
```
start-master.sh
```

2,启动子节点
```
start-slave.sh spark://ubuntu:7707 <-这个地址是打开主节点设置的网页查看指定的路径
spark-shell --master spark://ubuntu:7707 <- 这个要使用start-all.sh才能正常启动 sudo /usr/local/spark/sbin/start-all.sh
```

3,关闭节点
```
stop-slave.sh关闭子节点   stop-master.sh关闭主节点
```

如果是基于hadoop(HDFS)上运行spark,需要先启动hadoop然后再启动spark,把下面的hadoop目录和spark目录更换成你自己的安装目录

先启动hadoop
```
$cd/usr/local/hadoop/
$./sbin/start-all.sh
```

再启动spark
```
$cd/usr/local/spark
$./sbin/start-all.sh
```

#### spark 设置

需要先把conf文件下spark-env.sh的模板复制成spark-env.sh,然后进行修改
```
export SPARK_MASTER_IP=master
export SPARK_WORKER_CORES=1
export SPARK_WORKER_MEMORY=800m
export SPARK_WORKER_INSTANCES=2
export SPARK_LOCAL_IP=本机ip    如果设置了这个,那么启动spark master的时候,终端不会显示web的链接,打开本机ip:8080即可
```
core是子节点的核数,instances是总的子节点数


#### hadoop设置
```
vim /usr/local/hadoop/etc/hadoop/hadoop-env.sh
# 增加以下
export JAVA_HOME=/usr/lib/jvm/java-15-openjdk-amd64

# 启动hadoopHDFS
sudo /usr/local/hadoop/sbin/start-dfs.sh
```

设置分布式子节点
```
cd spark/conf

cp slaves.template slaves   -> 或者是 workers.template  意思都是子节点

vi slaves

在localhost下面添加到子节点的ip地址
```


在web上启动spark-shell的可视化jobs界面 : 浏览器打开 spark运行电脑ip:4040




## 使用maven打包scala package

修改pom.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.hasaki</groupId>
    <artifactId>common</artifactId>
    <version>1.0</version>

    <properties>
        <maven.compiler.source>15</maven.compiler.source>
        <maven.compiler.target>15</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.scala-lang</groupId>
            <artifactId>scala-library</artifactId>
            <version>2.13.5</version>
        </dependency>
        <dependency>
            <groupId>org.scala-lang</groupId>
            <artifactId>scala-compiler</artifactId>
            <version>2.13.5</version>
        </dependency>
        <dependency>
            <groupId>org.scala-lang</groupId>
            <artifactId>scala-reflect</artifactId>
            <version>2.13.5</version>
        </dependency>
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.12</version>
        </dependency>
    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.scala-tools</groupId>
                <artifactId>maven-scala-plugin</artifactId>
                <version>2.15.2</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>compile</goal>
                            <goal>testCompile</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
```

在package目录下也就是src目录同级
命令行使用
```
mvn clean scala:compile compile package
```

