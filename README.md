# 练码场

#### 项目介绍
知乎轮子哥说的一个程序员的修养，除了工作之外每天多写300行代码，那么6年后就能超过100万行代码

为什么要创建那么多语言呢，也是轮子哥说的，你起码得会半打编程语言，各种各样的:

包括一种支持类抽象的语言（例如Java或者C++），一种支持函数抽象的语言（例如Lisp或者ML）,一种支持语法抽象的语言（例如Lisp）,一种支持声明式

编程的语言（例如Prolog或者C++模板），一种支持协同程序的语言（例如Icon或者Scheme）,一种支持平行并发编程的语言（例如Sial）


语言这个东西，熟悉不要超过3门，价值多在应用场景

如同《雪中悍刀行》的主角，手握两把长短刀绣冬和春雷

python和Typescript就是我的绣冬和春雷

用好两个即可，其他不重要。

![](https://gitee.com/CCHChenChangHong/practice_code_field/raw/master/golang.jpg)

![](https://gitee.com/CCHChenChangHong/practice_code_field/raw/master/typescript.jpg)


![](https://gitee.com/CCHChenChangHong/practice_code_field/raw/master/python2_0.jpg)


#### [计算机基础](https://github.com/AobingJava/JavaFamily)


git的使用：

git clone XXXX.git

修改代码

git add .

git commit -m "你想要传的消息"

git push origin master

/////////////////////////////////////

获取版本号

git branch

获取远程仓库的版本号

git branch -a

换本地仓库

git checkout -b xxxx origin/xxxx



//////////////////////////////////////////

直接下载某个tag的代码

git clone --branch v1.6.5.7 https://github.com/ManaPlus/ManaPlus.git

/////////////////////////////////////////

获取git仓库所有的tag版本
git ls-remote --tags https://github.com/ManaPlus/ManaPlus.git

########################################### 

ubuntu ssh 连接云服务
ssh -q -l 云服务系统名 -p 22 地址

############################################
云服务ubuntu安装mongodb:https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

从本地上传文件到linux云服务
sudo apt install lrzsz

然后在服务器使用rz即可

从linux服务器下载文件到本地使用sz


################################################
国内OS系统镜像下载:https://developer.aliyun.com/mirror

#################################################
linux启动mongodb数据库操作

sudo service mongod stop　　#停止服务

sudo service mongod start　　#启动服务

sudo service mongod restart #重新启动服务

sudo service mongod status #查看状态

###################################################

浏览器使用vscode, 云编程

```
docker run -it -p 0.0.0.0:8080:8080 \
  -v "$PWD:/home/coder/project" \
  -u "$(id -u):$(id -g)" \
  codercom/code-server:latest
```

config位置
```
~/.config/code-server/config.yaml
```


#### lychee图床

映射给容器的端口一定是80,初始化的时候账号密码都是lychee进去系统之后再设置个人账号密码
```
docker pull kdelfour/lychee-docker
docker run -it -d -p 9999:80 kdelfour/lychee-docker
```