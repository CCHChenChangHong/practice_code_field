
// 接口
trait Area {
    fn area(&self) -> f64;
}
 
// 具体结构体
struct Circle {
    r: f64
}
 
// impl [trait] for [struct]的结构形式 ,impl是一种实现,给外部看到的是struct
impl Area for Circle {
    fn area(&self) -> f64 {
        (3.14 * self.r) // 作为返回值 => 必须使用 () 括起来，并不能写 ;
    }
}
 
fn main()
{
    let r = Circle {r:10.5};     // 实例化,看到的是struct
    println!("area = {:?}", r.area());
}

