

/// ------------------------- 打印相关 --------------------
fn print(msg:&str){
    // rust中的简化打印
    println!(msg)
}

fn debug(msg:&str){
    // 带有时间的print
    print(msg)
}