fn main() {
    let number = 3;
    if number < 5 {
        println!("条件为 true");
    } else {
        println!("条件为 false");
    }
}

//////////////////////////////////////////////////////////
fn main() {
    let a = 12;
    let b;
    if a > 0 {
        b = 1;
    }  
    else if a < 0 {
        b = -1;
    }  
    else {
        b = 0;
    }
    println!("b is {}", b);
}

//////////////////////////////////////////////////////////
fn main() {
    let number = 3;
    if number {   // 报错，expected `bool`, found integerrustc(E0308)
        println!("Yes");
    }
}
/////////////////////////////////////////////////////////
fn main() {
    let a = 3;
    let number = if a > 0 { 1 } else { -1 };   // 三元表达式
    println!("number 为 {}", number);
}

/////////////////////////////////////////////////////////
