fn main() {
    let mut number = 1;
    while number != 4 {
        println!("{}", number);
        number += 1;
    }
    println!("EXIT");
}
///////////////////////////////////////////////
let mut i = 0;
while i < 10 {
    // 循环体
    i += 1;
}
//////////////////////////////////////////////
fn main() {
    let a = [10, 20, 30, 40, 50];
    for i in a.iter() {   // a.iter() 代表 a 的迭代器（iterator）
        println!("值为 : {}", i);
    }
}
//////////////////////////////////////
fn main() {
let a = [10, 20, 30, 40, 50];
    for i in 0..5 {     // for 循环其实是可以通过下标来访问数组的
        println!("a[{}] = {}", i, a[i]);
    }
}

//////////////////////////////////////////
// Rust 语言有原生的无限循环结构 —— loop
fn main() {
    let s = ['R', 'U', 'N', 'O', 'O', 'B'];
    let mut i = 0;
    loop {
        let ch = s[i];
        if ch == 'O' {
            break;
        }
        println!("\'{}\'", ch);
        i += 1;
    }
}

// break
fn main() {
    let s = ['R', 'U', 'N', 'O', 'O', 'B'];
    let mut i = 0;
    let location = loop {
        let ch = s[i];
        if ch == 'O' {
            break i;     // rust的break可以返回值
        }
        i += 1;
    };
    println!(" \'O\' 的索引为 {}", location);
}