// base for rust

fn main() {
    let a=1;
    let mut b="hasaki";
    println!("{} ,{}!",a,b);

    let a=123;
    b="gugu";
    println!("{} ,{}!",a,b);

    // illgal
    //const a: i32 = 123;
    //let a = 456;
    
    // about shadowing 重影与可变变量的赋值不是一个概念，重影是指用同一个名字重新代表另一个变量实体，其类型、可变属性和值都可以变化。但可变变量赋值仅能发生值的变化。
    let x=5;
    let x=x+1;
    println!("{} ,{} {}!",a,b,x);

    let mut sum = 5 + 10; // 加
    let difference = 95.5 - 4.3; // 减
    let product = 4 * 30; // 乘
    let quotient = 56.7 / 32.2; // 除
    let remainder = 43 % 5; // 求余
    sum+=1;
    println!("{} ,{} {} {}!{}",sum,difference,product,quotient,remainder);


    // 复合类型
    let tup: (i32, f64, u8) = (500, 6.4, 1);
    // tup.0 等于 500
    // tup.1 等于 6.4
    // tup.2 等于 1
    let (x, y, z) = tup;
    // y 等于 6.4

    println!("{} ,{} {} ",x,y,z);

    let a = [1, 2, 3, 4, 5];
    // a 是一个长度为 5 的整型数组

    let b = ["January", "February", "March"];
    // b 是一个长度为 3 的字符串数组

    let c: [i32; 5] = [1, 2, 3, 4, 5];
    // c 是一个长度为 5 的 i32 数组

    let d = [3; 5];
    // 等同于 let d = [3, 3, 3, 3, 3];

    let first = a[0];
    let second = a[1];
    // 数组访问

    a[0] = 123; // 错误：数组 a 不可变
    let mut a = [1, 2, 3];
    a[0] = 4; // 正确
}

