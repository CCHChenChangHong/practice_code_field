// 一个结构体
struct Site {
    domain: String,
    name: String,
    nation: String,
    found: u32
}

// 实例
let runoob = Site {
    domain: String::from("www.hasaki.com"),
    name: String::from("HASAKI"),
    nation: String::from("China"),
    found: 2023
};

// 等同于
let domain = String::from("www.hasaki.com"),
let name = String::from("HASAKI"),
let runoob = Site {
    domain,  // 等同于 domain : domain,
    name,    // 等同于 name : name,
    nation: String::from("China"),
    traffic: 2023
};
