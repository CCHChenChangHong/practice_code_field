// 这段代码是不会通过 Rust 编译器的，原因是 r 所引用的值已经在使用之前被释放。
{
    let r;

    {
        let x = 5;
        r = &x;
    }

    println!("r: {}", r);
}



&i32        // 常规引用
&'a i32     // 含有生命周期注释的引用
&'a mut i32 // 可变型含有生命周期注释的引用


/*

fn main() {
    let r;
    {
        let s1 = "rust";
        let s2 = "ecmascript";
        r = longer(s1, s2);
    }
    println!("{} is longer", r);
}

// 这里会运行错误,返回值引用可能会返回过期的引用
fn longer(s1: &str, s2: &str) -> &str {
    if s2.len() > s1.len() {
        s2
    } else {
        s1
    }
}
*/

/*所以上面需要改成

fn main() {
    let r;
    {
        let s1 = "rust";
        let s2 = "ecmascript";
        r = longer(s1, s2);
    }
    println!("{} is longer", r);
}

fn longer<'a>(s1:&'a str, s2: &'a str) -> &'a str {
    if s2.len() > s1.len() {
        s2
    } else {
        s1
    }
}
*/