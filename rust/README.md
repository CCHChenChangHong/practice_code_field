## Rust

####　安装

linux 安装:
```shell
curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
```

[window安装](https://www.rust-lang.org/zh-CN/tools/install)

#### [官方教程](https://doc.rust-lang.org/stable/rust-by-example/index.html)

#### rust的pip-->cargo

```shell
cargo --version
```

使用cargo初始化项目
```shell
cargo new projectName
```

不用编译先快速检查代码能否通过
```shell
cargo check
```

编译
```shell
cargo build
```

类似go run 的先运行
```shell
cargo run 
```

#### cargo 使用第三方库

在Cargo.toml中
```toml
[package]
name = "rt"
version = "0.1.0"
edition = "2018"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

# 就在https://crates.io/ 中找到对应的库,然后在dependencies下表明就行了
[dependencies]
rand = "0.8.0"
```


### 相关学习项目

[操作系统](https://github.com/rustcc/writing-an-os-in-rust)

[rCore操作系统](https://gitee.com/CCHChenChangHong/practice_code_field/tree/master/%E6%93%8D%E4%BD%9C%E7%B3%BB%E7%BB%9F#https://rcore-os.github.io/rCore-Tutorial-Book-v3/)

[deno](https://github.com/denoland/deno)

[高性能储存](https://github.com/datenlord/datenlord)

[tock OS](https://github.com/tock/tock)


[B站rust写操作系统讲解](https://space.bilibili.com/1286472/favlist?fid=966774072&ftype=create)

[writing an OS in Rust](https://github.com/phil-opp/blog_os)

[redox](https://github.com/redox-os/redox)      [redox官网](https://gitlab.redox-os.org/redox-os/redox)

[zCore](https://github.com/rcore-os/zCore)

[PingCAP rust课程](https://university.pingcap.com/talent-plan/)

[Rust 宏](https://danielkeep.github.io/tlborm/book/mbe-syn-macros-in-the-ast.html)

[UnSafe Rust](https://doc.rust-lang.org/nomicon/index.html)

[知乎Rust Study RoadMap](https://zhuanlan.zhihu.com/p/146472398)