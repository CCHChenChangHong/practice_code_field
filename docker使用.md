#### docker-ce源代码

https://github.com/docker/docker-ce


#### [《自己动手写docker》一书源代码](https://github.com/xianlubird/mydocker)

#### 安装docker 

[阿里云安装方式](https://developer.aliyun.com/mirror/docker-ce?spm=a2c6h.13651102.0.0.3e221b11LHVJL7)

资料来源：《Docker技术入门与实践》(第三版)

sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common

添加gpg密钥源
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - 

确认指纹
sudo apt-key fingerprint 0EBFCD88

获取当前系统代号
lsb_release -cs

ubuntu18后系统代号是bionic

添加官方稳定镜像源
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

开始安装docker 
sudo apt-get install -y docker-ce

#### 查看镜像信息

sudo docker images

查询镜像的详细信息

sudo docker inspect [ images id ]

查看镜像历史

sudo docker history [ images id ]

查看本机所有镜像

sudo docker ps -a

#### 搜索镜像

sudo docker search tensorflow     搜索tensorflow的镜像

#### 拉取镜像

sudo docker pull python

#### 删除镜像，清理镜像

sudo docker rmi [ images id ]

清理镜像

sudo docker prune -f 

暂停容器 ： sudo docker pause CONTAINER ID

终止容器 ： sudo docker stop CONTAINER ID

要完全删除镜像，先删除容器，使用docker rm CONTAINER ID之后，再docker ps查看镜像也没有了

#### 一键删除已经停止运行的容器   docker rm $(docker ps -aq)

#### 创建镜像

sudo docker run -it [ images id ] /bin/bash

提交已经修改的镜像

sudo docker commit

example:
```
docker commit -a "镜像的作者" -m "提交的信息" [CONTAINER ID]  [REPOSITORY[:TAG]]
```

#### 进入容器

sudo docker run -it imagesID /bin/bash


#### 上传镜像

打tag

sudo docker tag images ID hasaki_python (默认为:latest)

sudo docker tag hasaki_python hasakichen/hasaki_python

上传镜像到公共仓库

sudo docker push hasakichen/hasaki_python:latest

#### 宿主机与容器的数据交互

将宿主机的www文件夹复制到容器中

docker cp /www/ 96f7f14e99ab:/www/


把宿主机的/home/hasaki/桌面/testDockerFile目录下的所有文件映射到容器中的/home/hasaki下
```shell
sudo docker run -it -v /home/hasaki/桌面/testDockerFile:/home/hasaki registry.cn-guangzhou.aliyuncs.com/hasakichen/base_python3:v2 /bin/bash
```


映射端口到宿主机

```shell
# 把容器内部的端口8080映射到宿主机的6666端口
sudo docker run -it -p 8080:6666 python3:v2 /bin/bash
```

#### Dockerfile使用

简单运行本地的python镜像,并且在容器内运行pip3 list,运行结束之后,容器会自动被删除,同时会基于registry.cn-guangzhou.aliyuncs.com/hasakichen/base_python3
产生一个新的registry.cn-guangzhou.aliyuncs.com/hasakichen/base_python3:v2镜像
```Dockerfile
FROM registry.cn-guangzhou.aliyuncs.com/hasakichen/base_python3:v2
RUN pip3 list
```

启动,在Dockerfile同一个目录下运行
```
sudo docker build -t registry.cn-guangzhou.aliyuncs.com/hasakichen/base_python3:v2 .
```



### docker-compose安装

[官方资料](https://docs.docker.com/compose/install/)

docker-compose依赖python3的
```shell
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
pip3 install docker-compose
```


启动docker-compose
docker-compose.yml
```yml
version: "2.1"
services:
        LA_python:
                image: registry.cn-guangzhou.aliyuncs.com/hasakichen/base_python3:v2.1.1
```

docker-compose通过Dockerfile启动容器
```
version: "2.1"
services:
  la_python:
    build: .


# Dockerfile内容:
FROM registry.cn-guangzhou.aliyuncs.com/hasakichen/base_python3:v2.1.1
COPY ./main.py /home/
CMD ["python3","/home/main.py"]

```


通过docker-compose映射本机目录到容器 (volumnes命令)   [docker-compose在容器内操作命令 (command)](https://blog.csdn.net/whatday/article/details/108863389)

ports就是把容器内的端口映射到宿主机,就好像docker run 的-p一样
```yml
version: "2.1"
services:
  la_python:
    image: registry.cn-guangzhou.aliyuncs.com/hasakichen/base_python3:v2.1.1
    volumes:
        - /home/hasaki/桌面/common:/home
    ports:
        - 8080:8080
    command:
        - sh
        - -c
        - |
              pip3 list
              ls /home
              rm -rf /home/common.iml
  task1:
    image: registry.cn-guangzhou.aliyuncs.com/hasakichen/base_python3:v2.1.1
    command:
        - sh
        - -c
        - |
              cd /home
              pip3 list
              python3
```



```
# 需要在当前的目录下有docker-compose.yml这个文件
sudo docker-compose up
# 然后就会启动LA_python这个镜像,然后又关闭
```





#### 阿里云的仓库

https://cr.console.aliyun.com/cn-guangzhou/instances