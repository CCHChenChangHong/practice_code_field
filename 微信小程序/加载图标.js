var global_text=''
Page({
  data:{
  show_picture:false,
  show_text:false,
  return_text:"没有从服务器返回数据",
        },
  hasaki_tap:function(){
    this.setData({
      show_picture: true
    })
  },

  tap_text:function(){
    // 渲染加载图标
    wx.showLoading({
      title: '等待服务器返回消息',
    })

    wx.request({
      url: 'https://127.0.0.1:6666',
      data: '',
      header: {},
      method: 'POST',
      dataType: 'json',
      responseType: 'text',
      success: function (res) {
        global_text = res.data + "正常返回"
      },
      fail: function (res) {
        global_text = res.data + "错误返回",
          console.log("失败返回" + res)
      },
      complete: function (res) {
        global_text = res.data + "完成返回"
      },
    })

    console.log('进入了wx.request'+global_text)

    
    this.setData({
      return_text:global_text,
      show_text: true,
    })
    
    wx.hideLoading()      // 隐藏加载图标
  },

});