
[链接：](https://www.zhihu.com/question/50907897/answer/128494332)


1：官方工具：https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html

2：简易教程：https://mp.weixin.qq.com/debug/wxadoc/dev/?t=1476434677599

3：设计指南：https://mp.weixin.qq.com/debug/wxadoc/design/index.html

4：设计资源下载：https://mp.weixin.qq.com/debug/wxadoc/design/#资源下载

5：微信小程序公测接入指南：http://www.wxapp-union.com/portal.php?mod=view&amp;amp;aid=259

6：微信小程序支付文档：https://pay.weixin.qq.com/wiki/d ... pi.php?chapter=3_1#

7：新手入门宝典：http://www.wxapp-union.com/forum.php?mod=viewthread&amp;amp;tid=1989

8：免费视频：http://www.wxapp-union.com/forum.php?mod=forumdisplay&amp;amp;fid=37&amp;amp;filter=typeid&amp;amp;typeid=7

9：实战宝典：http://www.wxapp-union.com/special/solution.html

10：从注册到上线系列：http://www.wxapp-union.com/forum.php?mod=viewthread&amp;amp;tid=4375

echart微信小程序版: https://github.com/ecomfe/echarts-for-weixin

