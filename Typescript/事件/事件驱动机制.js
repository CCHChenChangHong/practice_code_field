// 事件驱动机制

file_path='C:/Users/cchyp/Desktop/日志.txt'
var fs=require('fs');
var events=require('events');
// 创建eventEmitter实例
var eventEmitter=new events.EventEmitter();
// 创建事件处理程序
var connectHandler=function connected(){
  console.log('connect success!')
  // 出发data-received事件
  eventEmitter.emit('data-received');
}

// 绑定connection事件处理程序
eventEmitter.on('connection',connectHandler);

// 使用匿名函数绑定data-received事件
eventEmitter.on('data-received',function(){
  console.log('hasaki data-received');
})

// 触发connection事件
eventEmitter.emit('connection');
console.log('here is the buttom of the code ')