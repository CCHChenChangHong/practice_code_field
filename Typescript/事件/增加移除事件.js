var event=require('events')
var eventEmitter=new event.EventEmitter();

// 事件一
var hasa1=function hasa1(){
  console.log('111111111');
}

// 事件二
var hasa2=function hasa2(){
  console.log('222222222222222');
}

// 绑定事件触发器
eventEmitter.on('hasaki',hasa1)

// 触发事件
eventEmitter.emit('hasaki')

// 增加绑定事件
eventEmitter.addListener('hasaki',hasa2)
console.log('add new listenner')
// 再触发一次事件
eventEmitter.emit('hasaki')
console.log('remove old listener')
// 移除事件
eventEmitter.removeListener('hasaki',hasa1)

// 再触发一次事件
eventEmitter.emit('hasaki')


/*
结果：
111111111
add new listenner
111111111
222222222222222
remove old listener
222222222222222
*/