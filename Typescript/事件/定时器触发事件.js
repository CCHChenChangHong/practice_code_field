// 定时器触发事件，两秒后自动触发事件
var event=require('events')
var eventEmitter=new event.EventEmitter();

eventEmitter.on('hasaki',function(){
  console.log('hasaki');
})
setTimeout(function(){
  eventEmitter.emit('hasaki');
},2000);