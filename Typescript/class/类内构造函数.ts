
// backtestEngine.ts
class BacktestEngine{
    // 类内要这样赋值变量
    constructor(
        public initData:string[],
        public testData:string[]
    ){}

    init(initData:string[],testData:string[]):void{
        this.initData=initData
        this.testData=testData
    }


}

export {BacktestEngine}


/*
import {BacktestEngine} from './backtestEngine'     // 加载backtestEngine.ts

let testList=['hasaki','gugu']
let engine=new BacktestEngine(testList,testList)     // 构造函数的变量要在new的时候传进去

engine.init(testList,testList)
*/
