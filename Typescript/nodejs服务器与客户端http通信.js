//服务器
var http=require("http");
var server=new http.Server();

server.on("request",function(req,res){
    res.writeHead(200,{
        "content-type":"text/plain"
    });
    res.write("连接成功");
    res.end();
    if (req!=null){
        console.log("有数据到");
    };
    req.on('data',function(chunk){
        console.log(chunk.toString());
    });
});
server.listen(6666);

/*客户端
var http=require("http");

var options={
    hostname:"127.0.0.1",
    port:6666,
    method:'POST'
}

var req=http.request(options,function(res){
    res.setEncoding("utf-8");
    res.on("data",function(chunk){
        console.log(chunk.toString())
    });
    console.log(res.statusCode);
});
req.on("error",function(err){
    console.log(err.message);
});
req.write("接受到信息了吗？服务器");
req.end();

*/