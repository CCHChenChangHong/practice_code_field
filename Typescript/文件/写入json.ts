// typescript 写入文件
let fs=require('fs')

// 写入文件内容（如果文件不存在会创建一个文件）
// 传递了追加参数 { 'flag': 'a' }
fs.writeFile('C:/Users/cchyp/Desktop/hasaki.json', JSON.stringify({"a":1}) , { 'flag': 'w' }, function(err) {
    if (err) {
        throw err;
    }
});

/*
参数说明
第一个参数是文件保存路径
第二个参数是文件内容
第三个参数是写入类型   写入或者是追加写入
第四个参数是状态回调
*/