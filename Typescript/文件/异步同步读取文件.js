file_path='C:/Users/cchyp/Desktop/日志.txt'

/*
nodejs文件系统有异步和同步两个版本
fs.readFile()和fs.readFileSync()
异步的方法函数最后一个参数为回调函数，回调函数的第一个参数包含了错误信息
*/

var fs=require('fs');

// 异步读取
fs.readFile(file_path,function(err,data){
   if (err){
      return console.error(err);
   }
   console.log('hasaki no sai'+data.toString());
});

// 同步读取
var data=fs.readFileSync(file_path);
console.log('hasaki sai'+data.toString());
console.log('in the code end')       // 这句话并不是在最后而是在中间