//html代码
/*
<input id="upload_input" class="hide" type="file" accept=".csv" onchange="onUpload(this)" />
*/
function onUpload(input) {
      //	校验文件
      var file = input.files[0];
      var reader = new FileReader();
      if(typeof reader == 'undefined') {
        alert("您的浏览器暂不支持该功能", false);
        return;
      }
      var fileName = file.name;
      var pointIndex = fileName.lastIndexOf(".");
      var fileSuffix = fileName.substr(pointIndex);
      if (fileSuffix == ".CSV" || fileSuffix == ".csv") {
        try {
          reader.readAsText(file, 'utf-8');
          reader.onload = function(f) {
            if (this.result) {
              uploadContent = this.result;
              console.log(uploadContent)
              //submit();
            } else {
              alert("上传的文件为空！", false);
            }
            input.value='';
          }
        } catch (e) {
          alert("读取文件失败！", false);
          console.log(e);
        }
      } else {
        alert("您选择的文件不是CSV格式", false);
      }
    }