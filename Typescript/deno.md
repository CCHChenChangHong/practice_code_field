#### 官方文档

https://deno.land/manual/introduction



#### 安装

curl -fsSL https://deno.land/x/install/install.sh | sh


iwr https://deno.land/x/install/install.ps1 -useb | iex

#### 启动

/home/ubuntu/.deno/bin/deno

deno

#### 直接运行网络上的脚本文件

deno run http://www.gudastudio.top/hasaki.ts

```javascript
console.log('hasaki')
```

```shell
Download http://www.gudastudio.top/hasaki.ts
Check http://www.gudastudio.top/hasaki.ts
hasaki
```
#### 基本命令

版本号:
```
deno --version
>
deno 1.3.0
v8 8.6.334
typescript 3.9.7
```

#### 运行脚本

```
deno run xxx.ts
```

权限运行
```
deno --allow-net --allow-read run xxx.ts
```

#### 打包成二进制

```
deno install --root /UwantFile/ yourScript.ts
```

权限
```
deno install --allow-net --allow-read --root /UwantFile/ yourScript.ts
```

重命名
```
deno install --allow-net --allow-read --root /UwantFile/  -n hasaki yourScript.ts
```


重载cache
```
deno cache --reload my_module.ts

// 重载指定cache
deno cache --reload=https://deno.land/std@0.100.0/fs/copy.ts,https://deno.land/std@0.100.0/fmt/colors.ts my_module.ts
```


#### deno调用npm

```typescript
import { createRequire } from "https://deno.land/std/node/module.ts";

const require = createRequire(import.meta.url);

const cheerio = require('cheerio');
const $ = cheerio.load('<h2 class="title">Hello world</h2>');

$("h2.title").text("Hello Deno!");
$("h2").addClass("deno");

console.log($.html());
```

```shell
deno run --unstable --allow-net --allow-read .\pic.ts
```


#### request请求

[doc](https://github.com/keroxp/deno-request)

```typescript
const json = fetch("https://stock.xueqiu.com/v5/stock/realtime/quotec.json?symbol=SH600519,SH603288,SH600887,SH600872,SZ000858,SH600600,SZ002302&_=0");

json.then((response) => {
  return response.json();
}).then((jsonData) => {
  console.log(jsonData);
});



import {request} from "https://denopkg.com/keroxp/deno-request@v0.2.0/request.ts"
// POST
const {status, headers, body} = await request({
    url: "http://httpbin.org/post",
    method: "POST",
    body: new TextEncoder().encode("wayway");,
    headers: new Headers({
        "Content-Type": "application/json"
    })
});
const buf = new Deno.Buffer();
await Deno.copy(buf, body);
const json = JSON.parse(buf.toString())
json["data"]; // wayway


// 下载文件
import {request} from "https://denopkg.com/keroxp/deno-request@v0.2.0/request.ts"
const {body} = await request("http://httpbin.org/get?deno=land");
const f = await Deno.open("out.json")
await Deno.copy(f, body)
f.close()
```