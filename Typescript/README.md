#### Typescript安装

先安装nodejs包

>npm install -g typescript   // 安装了这个之后，就可以使用tsc来把ts文件转换成js文件运行

>npm install -g ts-node

在项目根目录命令行使用

>tsc --init   就会生成tsconfig.json的配置文件，配置里面的outdir用于ts编译出来的js保存地方

然后在vscode中编辑菜单栏点"终端"-"运行任务"-选择tsc监视，那么ts每次修改都会自动编译到js

#### 使用淘宝镜像

npm install -g cnpm --registry=https://registry.npm.taobao.org

使用淘宝镜像安装vue框架:cnpm install vue



#### [Deno](https://gitee.com/CCHChenChangHong/practice_code_field/blob/master/Typescript/deno.md)



#### 一些库的安装

cnpm install mysql       安装mysql数据库

cnpm install mongodb     安装mongodb数据库

cnpm install ws           安装node 的websocket库

cnpm install -g @angular/cli   安装 angular 2 框架

cnpm install -g @vue/cli 安装vue3 相关文档:https://cn.vuejs.org/v2/guide/typescript.html
                                          https://cli.vuejs.org/zh/guide/creating-a-project.html#vue-create

cnpm install npm install vue@next  相关文档 : https://v3.cn.vuejs.org/guide/installation.html#cdn

cnpm install kafka-node 安装kafka消息队列组件

cnpm install request --save    安装request,一般都已经安装了这个库


#### Jupyter notebook for ts / js

```typescript
sudo -H npm install -g itypescript

its --install=local
```

```javascript
npm config set prefix $HOME
npm install -g ijavascript
ijsinstall
```

#### MDN web docs

https://developer.mozilla.org/en-US/docs/Web/API/Request

#### nodejs免安装版本(windows)

[官网](https://nodejs.org/en/download/releases/)

```
点击download后选择系统对应的版本即可,下载解压可用
```


#### 壳的框架

[vue3 vite tenplate](https://geekqiaqia.github.io/vue3.0-template-admin/#/login?redirect=/home)

[antV](https://antv.vision/zh)

[layui](https://www.layui.com/doc/element/panel.html)