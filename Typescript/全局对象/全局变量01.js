

/*
js全局对象
在浏览器中，通常window是全局对象
以下变量是全局变量：
1，在最外层定义的变量
2，全局对象的属性
3，隐式定义的变量（未定义直接赋值的变量）
*/
console.log(__filename); // 当前执行脚本的绝对位置
console.log(__dirname); // 当前执行脚本的目录

// 定时器，两秒后执行程序
setTimeout(function() {
   console.log('hasaki') 
}, 2000);

// 清除定时器
// clearTimeout(t)   t是setTimeout对象

