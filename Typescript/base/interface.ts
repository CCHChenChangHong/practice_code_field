// typescript的接口类型,接口当然能存函数
interface hasakiFace{
    a:number,
    b:string,
    func:()=>void
}

let interFunc:hasakiFace={
    a:814,
    b:'hasaki',
    func:():void=>{console.log('interface test !!')}
}

console.log(interFunc.a)
console.log(interFunc.b)
interFunc.func()


// 联合接口
interface multiFace1{
    a:string|number
}

interface nultiFace2{
    a:string|(()=>void)     // 联合函数要包个括号
}

interface pythonFace{
    [index:number]:number|string|boolean
}

let pyFace:pythonFace
pyFace=[814,'hasaki']
console.log('python :',pyFace[0])