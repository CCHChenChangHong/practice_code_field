// typescript的名命空间
// 大型项目中解决重名的问题
namespace Hasaki{
    export interface nameFace{
        hasakiFace()  // 函数接口
    }

    export class nameClass{
        
    }
}

/*另一个文件A.ts中
namespace Hasaki{
    export class hasaki1 implements nameFace{
        pubilc hasakiFace(){
            console.log('这是A')
        }
    }
}

另一个文件B.ts中
namespace Hasaki{
    export class hasaki2 implements nameFace{
        pubilc hasakiFace(){
            console.log('这是B')
        }
    }
}

测试文件test.ts中
function test(hasaki:Hasaki.nameFace){
    hasaki.hasakiFace()
}

test(new Hasaki.hasaki1())      // 通过这种方式实例化
test(new Hasaki.hasaki2())
*/