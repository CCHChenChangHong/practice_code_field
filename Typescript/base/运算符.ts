// 逻辑运算符
let x=10
let y=810
// 并操作
if (x>y && y==0){}

// 或运算
if (x==10 || y>10){}

// 否运算  !x==y会报错的，因为==不是boolean,所以!要加在外面
if (!(x==y)){}

if (!true){}      // 这种加在括号里面是可以的