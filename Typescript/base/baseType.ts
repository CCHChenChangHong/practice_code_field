// typescript 的基本类型
// 注意。ts和js都没有整型  变量声明不能有除了_和$以外的其他特殊字符，不能以数字开头

// 数值类型 双精度64位浮点值，可以用来表示整数和分数
let binaryLiteral: number=0b1010   // 二进制
let octalLiteral: number=0o744   // 八进制
let decLiteral: number=6    // 十进制
let hexLiteral: number=0xf00d   // 十六进制


// 字符串类型，可以是单引号也可以是双引号 ``表示多行文字和内嵌表达式
let name_:string="hasaki"
let hasaki:string='hasaki'
let word:string=`我爱 ${hasaki} `


// 布尔类型
let em:boolean=true


///////////////    容器类型    /////////////
// 元组类型
let x:[string,number]
x=['hasaki',24]        //  正常运行
//x=[1,'hasaki']         // 报错
console.log(x[0])

// 数组
let array:number[]=[1,2,3,4,5]
let array2:Array<number>=[1,2,3,4,5]    // 泛型

// 枚举
enum Mypal {'羽裴泠','余佩玲'}
let c:Mypal=Mypal.余佩玲    // 屌炸！！！中文属性！！！！
console.log(c)      // 输出1

// void 用于标识函数返回值的类型，表示该方法没有返回值
function bala():void{
    alert('hasaki')
}


// 缺失值
null
// 没定义值
undefined

// 声明所有变量  任意类型
let cc:any=1

// 代表从不会出现的值
let nev:never

