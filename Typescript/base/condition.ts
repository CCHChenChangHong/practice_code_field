// typescript条件语句

if (1){
    console.log('1')
}else if(0){
    console.log('0')
}else{
    console.log('else')
}

let expression:number   // 这里switch的传参要在外面声明好
switch(expression){
    case 1:
        console.log('case 1 ')
    case 2:
        console.log('case 2')
    default:
        console.log('default')
}

