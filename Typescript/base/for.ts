// typescript的循环
//let i :number     // 见鬼，for循环的条件居然也要外面声明...switch也是
for (let i=0;i<5;i++){
    // 用let 就可以声明啦哈哈哈
    console.log('C类循环',i)  // 0 1 2 3 4
}

// for ... in 循环  循环的是位数
let a:number[]=[1,2,3,4,5,6,7]
for (let j in a){
    console.log('类python循环',j)  // 0 1 2 3 4 5 6
}

// for ... of 循环  循环的是容器里面面的内容
for (let k of a){
    console.log('带of的循环',k)  // 1 2 3 4 5 6 7
}

// forEach循环
// every循环