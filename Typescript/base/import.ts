// typescript中的模块加载
// 如果编译成js报错:ReferenceError: define is not defined
// 有可能是底层的错误，代码没法解决，用vscode 的快捷运行依旧可以运行

// importTestA.ts
// 一个typescript文件,被其他文件调用
export class Print{
    print(content:string) {
        console.log(content)
    }
}

// importB.ts
// 用来调用importA.ts的函数
import {Print} from './importTestA'
let P=new Print()
P.print('hasaki')

// Ctrl + Alt + N 运行
// 输出:hasaki