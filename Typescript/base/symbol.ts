// typescript Symbol类型的用法  此类型和文档教程的不符合，没办法重现官方文档的效果
let symbol1=Symbol();
/* 'Symbol' only refers to a type, but is being used as a value here. 
Do you need to change your target library? Try changing the `lib` compiler option to es2015 or later.ts(2585)*/
let symbol2=Symbol('hasaki')
symbol1===symbol2