/*
js只有字符串类型，但是再进行TCP文件流操作的时候，需要二进制的格式，
这个时候就需要用到buffer类，用来创建一个专门存放二进制数据的缓存区
*/
var str='余佩玲爱陈常鸿';
console.log(str.length)    // 7
var str2='hasaki';
console.log(str2.length)   // 6