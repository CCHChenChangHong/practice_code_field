/*
js只有字符串类型，但是再进行TCP文件流操作的时候，需要二进制的格式，
这个时候就需要用到buffer类，用来创建一个专门存放二进制数据的缓存区
*/
var buf=Buffer.from('hasaki','ascii');

console.log(buf.toString('hex'));   // 686173616b69

console.log(buf.toString('base64'));    // aGFzYWtp

console.log(buf.toString());   // hasaki

// 中文的情况
var buf=Buffer.from('啦啦啦','ascii');

console.log(buf.toString('hex'));   // 666666

console.log(buf.toString('base64'));    //  ZmZm

console.log(buf.toString());   // fff

// utf8中文情况
var buf=Buffer.from('啦啦啦','utf8');

console.log(buf.toString('hex'));   // e595a6e595a6e595a6

console.log(buf.toString('base64'));    // 5ZWm5ZWm5ZWm

console.log(buf.toString());   // 啦啦啦


// 另一种utf8输出中文结果方式
var str='余佩玲爱陈常鸿';
console.log(str.toString('utf8'))