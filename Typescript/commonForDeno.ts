/*
基于deno的typescript 的 common function
创建时间 : 2021-7-6
最后修改时间 : 2021-7-15
TODO : 需要把deno的标准库另外保存在自己的库里,这样可以防止官网的库不可访问
*/

// github对应的链接https://raw.githubusercontent.com/denoland/deno_std/0.100.0/io/mod.ts
import { readerFromStreamReader } from "https://deno.land/std@0.100.0/io/mod.ts"; 
import { format,parse } from "https://deno.land/std@0.100.0/datetime/mod.ts";
// @deno-types="https://cdn.jsdelivr.net/gh/justjavac/deno_cheerio/cheerio.d.ts"
import cheerio from "https://dev.jspm.io/cheerio/index.js";


// ------------------------  打印  ---------------------------
export function print(msg:string):void{
    console.log(msg)
}

export function debug(msg:string):void{
    let info:string=nowString()+" Debug Info :"+msg
    print(info)
}

// ------------------------  时间  ---------------------------
export function nowString():string{
    // 获取当前的时间日期的字符串
    // return example : 2019-01-20 16:34:23
    return format(new Date(), "yyyy-MM-dd HH:mm:ss");
}

export function str2Date(strDate:string):Date{
    // 时间格式的转换
    // 转换格式为:2021-07-06 15:11:44
    return parse(strDate, "yyyy-MM-dd HH:mm:ss")
}

export function sleep(second:number):void{
    // function : 自封装的睡眠函数
    // param : 睡眠的秒数
    let d=second*1000
    for(var t = Date.now();Date.now() - t <= d;);
}

// ------------------------- 文件 -----------------------------
export function isEmpty(path:string):boolean{
    // function : 检查文件是否为空
    return false
}

export function isExist(path:string):boolean{
    // function : 判断文件是否存在
    return false
}

export async function createDir(path:string):Promise<void>{
    // function : 创建一个文件夹
    // param path example : /home/hasaki/gugu
    // need --allow-read --allow-write
    // 如果文件不为空那么就会报错
    try{
        await Deno.mkdir(path)
    }catch (error){
        debug(path+"has already exists")
        console.debug(error)
    }
    
}

export async function currentPath():Promise<string>{
    // function : 返回当前程序运行的文件目录,类似python的os.cwd()
    // need --allow-read 
    let path:string=await Deno.cwd()
    return path
}

// ------------------------- 网络 -----------------------------
export async function saveJPG(localPath:string,jpgLink:string):Promise<number>{
    // function : 保存网络上的图片到本地 ,调用的时候前面要加上await才能正常使用,不使用的话也能用,但是是一次性保存所有图片
    // param localPath : as its name ,example: D:\pic\1.jpg
    // param jpgLink : picture s original link,example : http://www.xxx.com/1.jpg
    const res = await fetch(jpgLink);
    if (res.status!=200){
        return res.status
    }
    const file = await Deno.open(localPath, { create: true, write: true });
    const reader = readerFromStreamReader(res.body!.getReader());
    await Deno.copy(reader, file);
    file.close();
    return res.status
}

export async function getHTML(url:string):Promise<string> {
    // function : 返回网页的html
    const text = fetch(url);
    return text.then((response) => {
    return response.text();
    })
}

export async function parseHTML(url:string,evalContent:string):Promise<any>{
    // function : 简单解析解析HTML内容
    // param url: 网页链接
    // param avalContent : 可执行的命令行列表,注意需要在这个列表中加入return
    // example : let a=await parseHTML('https://azsxx.vip:5560/luyifa/2020/0405/7418.html','')
    // console.log(a("img")['1'].attribs.src)
    // if (a("img")['7'].attribs.alt==undefined){
    //     console.log("hasaki")
    // }
    let HTMLcontent:string = await getHTML(url)
    const $ = cheerio.load(HTMLcontent);
    return $    //$(evalContent).text()   $("img")
}

export async function getJSON(url:string,header:any):Promise<any>{
    // function : http请求获取json数据
    // return 返回json object
    // let url:string='https://stock.xueqiu.com/v5/stock/realtime/quotec.json?symbol=SH600519,SH603288,SH600887,SH600872,SZ000858,SH600600,00388,MO,RLX,PM=0'
    // let result=await getJSON(url,'')
    // console.log(result['data'][0]['current'])
    let json=fetch(url)
    return json.then((response) => {return response.json()})
}


export async function httpGet(url:string,header:any):Promise<void>{
    // get 请求
}

export async function httpPost(url:string,header:any,body:any):Promise<void>{
    // post请求
}