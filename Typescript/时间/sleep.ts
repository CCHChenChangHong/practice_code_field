/*
javascript中是没有封装好的sleep函数...

这里实现一个其他语言中的sleep函数
todo : 做成异步
*/

export function sleep(second:number):void{
    // function : 自封装的睡眠函数
    // param : 睡眠的秒数
    let d=second*1000
    for(var t = Date.now();Date.now() - t <= d;);
}