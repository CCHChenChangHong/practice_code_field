// write by hashaki
// first edit on 2018/12/04
// last change on 2018/12/04
const child_process=require('child_process');

var python_path=__dirname+'/examples/Backtesting/Multi_Strategy/Multi_Trend1_Strategy/runMultiBacktesing_1.py';
var cash=1000000.0;               // 初始金额
var start_time='20120101';        // 开始K线时间
var end_time='20181201';         // K线结束时间
var initDay =10;                  // 加载前多少天的数据
var save=true;                  //  是否保存回测结果
var plot=true;                  // 是否打印结果图
// 设置涉及品种的交易参数
var symParam=JSON.stringify({"bitfinex_btc_usd":
{"size": 1, "priceTick": 0.2, "slip_fixed": 0.0,"slip_rate": 0.001, "fee_fixed": 0.0,
"fee_rate": 0.0005, "marginRate": 0.1},
"bitfinex_eth_usd":
{"size": 1, "priceTick": 0.2, "slip_fixed": 0.0,
 "slip_rate": 0.001, "fee_fixed": 0.0,
 "fee_rate": 0.0005, "marginRate": 0.1}
});
// 策略相关参数
var strategy=JSON.stringify({'s1':{"ClassName": "DoubleMA1Strategy", "market": "crypto",
"symbol":"bitfinex_btc_usd", "frequency": "1hour","d":{"n1": 20, "n2": 90, "posRate": 0.05}},'s2':{"ClassName": "BollBasic1Strategy", "market": "crypto",
"symbol":"bitfinex_eth_usd", "frequency": "30min","d":{"n1": 40, "n2": 2.0, "n3": 120, "posRate": 0.1}}});

var python=child_process.spawn('python',[python_path,cash,start_time,end_time,initDay,save,plot,symParam,strategy]);
python.stdout.on('data',function(data){
    console.log('输出：'+data);
});
python.stderr.on('data',function(data){
    console.log("hashaki"+data);
}
);
python.on('data',function(data){
    console.log(data)
});

/*
# -*- coding: utf-8 -*-
"""
@Date   : 2018/11/21 17:22
@Author : Jack
@Content: 多策略回测执行程序
策略py文件需要放在trader.BacktestModule.Strategy下面

修改：适应node.js调用
"""
import os
import sys
sys.path.append(os.getcwd())

import time
from trader.BacktestModule.ctaBacktesting_multi import BacktestingEngineMulti
import cProfile
import json

if __name__ == '__main__':
    t1 = time.time()
    # -------------------------------------------------------------------
    # 1、创建回测引擎
    engine = BacktestingEngineMulti()

    # -------------------------------------------------------------------
    # 2、设置基本的变量
    # 设置回测模式
    engine.setBacktestingMode(engine.BAR_MODE, False)
    # 设置数据库
    engine.setDatabase(address="192.168.101.88", host=27017)   # 京琛的数据库
    #engine.setDatabase(address="localhost", host=27017)  # 本地的数据库
    # 设置初始资金
    engine.setInitCapital(float(sys.argv[1]))  # 初始资金为100w元
    # 设置策略开始和结束时间
    engine.setStartDate(sys.argv[2],initDays=int(sys.argv[4]))  # 策略开始日期, 加载多少天的数据
    engine.setEndDate(sys.argv[3])                # 策略结束日期

    # -------------------------------------------------------------------
    # 3、设置交易的策略
    settingList = []    # 储存列表
    s=json.loads(sys.argv[8])
    for i in list(s.values()):
          settingList.append(i)

    # # (2) 双均线策略_比特币_30分钟
    # s2 = {"ClassName": "DoubleMA1Strategy", "market": "crypto",
    #       "symbol":"bitfinex_btc_usd", "frequency": "30min"}
    # s2.update({"d": {"n1": 20, "n2": 90, "n3": 120, "posRate": 0.05}}) # 策略参数
    # settingList.append(s2)

    # 传入引擎中
    engine.setStrategySetting(settingList)

    # -------------------------------------------------------------------
    # 4、设置涉及品种的交易参数(可以不用设置，使用数据库默认的参数)
    symParam = json.loads(sys.argv[7])
    engine.setSymParam(symParam)

    # -------------------------------------------------------------------
    # 5、执行回测
    engine.runBacktesting()

    # -------------------------------------------------------------------
    # 6、策略评价
    engine.runEvaluating(saving=sys.argv[5], plotting=sys.argv[6])


    print(u"回测耗时:{:.2f}s".format(time.time()-t1))

*/