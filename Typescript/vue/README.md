#### vue项目创建

1,全局安装：npm install --global vue-cli    或   cnpm install -g @vue/cli
 
2,初始化项目 : vue init webpack project-name

3,运行项目，在项目的根目录下命令行: npm run dev


[教程](https://cli.vuejs.org/zh/guide/prototyping.html)

#### 创建一个项目

vue create hello-world

#### 使用图形化界面

vue ui

#### vue中使用axios

[资料](https://www.runoob.com/vue3/vue3-ajax-axios.html)

#### 直接在html里加载vue.js

通过一个链接就可以不用nodejs就可以运行vue.js的代码
```html
<!doctype html>
<html lang="zn">
  <head>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  </head>

  <body>
    <div id="app">
      {{message}}
    </div>

    <script type="text/javascript">
      var app=new Vue({
        el: '#app',
        data: {
            message: "Hasaki"
        }
      });
    </script>
  </body>
</html>
```



#### vue+axios例子

```html
<div id='app'>
    <p> {{count}}</p>
</div>

<script src="https://unpkg.com/vue@next"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
const app = Vue.createApp({
    data() {
        return { count: "hasaki",gr:"hasaki",info:"jiji"}
    },
    mounted () {
    axios
      .get('https://www.runoob.com/try/ajax/json_demo.json',{
            headers: { Authorization: this.tokens ,'Access-Control-Allow-Origin':'*'},
            
            }
      )
    .then(response => (this.info = response)).catch(function (error) { 
        // 请求失败处理
        alert(error);
            });
        }
    })
    
    const vm = app.mount('#app')

    // 修改 vm.count 的值也会更新 $data.count
    // vm.count = "gugu"
    // console.log(vm.$data.count) // => 5
    
    // axios({
    // method: 'get',
    // url: 'https://stock.xueqiu.com/v5/stock/realtime/quotec.json?symbol=SH600519,SH603288,SH600887,SH600872,SZ000858,SH600600,00388,RLX,PM&_=0',
    
    // }).then(function (response){
    //     vm.gr=response
    //     alert(response)
    // })

</script>

```