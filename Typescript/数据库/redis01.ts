const redis = require('redis')

// 创建redis客户端
const redisClient = redis.createClient(6379, '127.0.0.1')
redisClient.on('error', err => {
  console.error(err)
  return
})

// 测试——设置redis记录
redisClient.set('myname', 'Frank', redis.print)
// 测试——查询redis记录
redisClient.get('myname', (err, val) => {
  if (err) {
    console.error(err)
    return
  }
  console.log(val)

  // 退出
  redisClient.quit()
})