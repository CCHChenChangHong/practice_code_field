// 定时获取股票行情
/*
import {getJSON,sleep,nowString} from 'https://gitee.com/CCHChenChangHong/practice_code_field/raw/master/Typescript/commonForDeno.ts'

interface quote{
    '贵州茅台':string,
    '五粮液':string,
    '青岛啤酒':string,
    '中炬高新':string,
    '伊利股份':string,
    '香港交易所':string,
    '莫里斯':string,
    '悦刻':string,
    '海天味业':string
}


while (true){
    let q:quote={
        '贵州茅台':'',
        '五粮液':'',
        '青岛啤酒':'',
        '中炬高新':'',
        '伊利股份':'',
        '香港交易所':'',
        '莫里斯':'',
        '悦刻':'',
        '海天味业':''
    }
    let result=await getJSON('https://stock.xueqiu.com/v5/stock/realtime/quotec.json?symbol=SH600519,SH603288,SH600887,SH600872,SZ000858,SH600600,00388,RLX,PM&_=0','')
    // let data = result['data']
    for (let k of result['data']){
        if (k['symbol']=='SH600519'){
            q.贵州茅台=k['current']
        }else if(k['symbol']=='PM'){
            q.莫里斯=k['current']
        }else if(k['symbol']=='RLX'){
            q.悦刻=k['current']
        }else if(k['symbol']=='SH600600'){
            q.青岛啤酒=k['current']
        }else if(k['symbol']=='00388'){
            q.香港交易所=k['current']
        }else if(k['symbol']=='SH600872'){
            q.中炬高新=k['current']
        }else if(k['symbol']=='SH600887'){
            q.伊利股份=k['current']
        }else if(k['symbol']=='SZ000858'){
            q.五粮液=k['current']
        }else if(k['symbol']=='SH603288'){
            q.海天味业=k['current']
        }
    }
    console.log(nowString())
    console.log(q)
    sleep(5)
    console.log('\n')
}

*/

/*
import {nowString,sleep,saveJPG,parseHTML,currentPath,createDir} from 'https://gitee.com/CCHChenChangHong/practice_code_field/raw/master/Typescript/commonForDeno.ts'

// 获取美女图片并保存到当前运行的目录
let cp:string=await currentPath()
let basePath:string=cp+'/COS-Pic'
let tampSavaPath:string=""
let pageCounter:number=1
createDir(basePath)
basePath=basePath+'/'

// 网址列表 console.log(htmlList[0][1])->13
let htmlList:Array<[string,number,string]>=[
    ['https://imoemei.com/cos',32,'COS'],
    ["https://imoemei.com/meinv",11,"写真"],
    ['https://imoemei.com/zipai',11,'自拍'],
    ['https://imoemei.com/gif',1,'gif'],
    ['https://imoemei.com/2cy',1,'二次元'],
    ['https://imoemei.com/hentai',10,'绅士']
]

async function getHref(resultHTML:any):Promise<void>{
    // 获取所有的图片页面链接
    let htmlLink:string=""
    for (let a=0;a<=100;a++){
        
        try{
            if (resultHTML('a')[a.toString()].attribs==undefined){
                break
            }
        }catch (error){
            console.log(error)
            break
        }
        
        if (resultHTML('a')[a.toString()].attribs.class!='thumb-link'){
            continue
        }
        htmlLink=resultHTML('a')[a.toString()].attribs.href
        let tampSavaPath_:string=tampSavaPath+pageCounter.toString()+'/'
        createDir(tampSavaPath_)
        await parasePicHTML(htmlLink,tampSavaPath_)
        pageCounter+=1
    }
}

async function parasePicHTML(picPageLink:any,localPath:string):Promise<void>{
    // 传入具体的图片页面返回具体的图片jpg链接或者gif连接
    // 提取出图片
    let counter:number=20  // 单页图片最大数量
    let picPageResult:any=await parseHTML(picPageLink,"")
    console.log("page link ------> ",picPageLink,"localPath ----->",localPath)
    for (let singlePagePic=0;singlePagePic<=counter;singlePagePic++){
        try{
            let picLink:string=picPageResult('img')[singlePagePic.toString()].attribs.src
            if (picLink.split(".")[2]=='png'){
                continue
            }
            if (picLink.split(".")[2]=='bdstatic'){
                continue
            }
            console.log("pic link : ",picLink,"---->",picLink.split(".")[2])
            // 保存图片
            if (picLink.split(".")[1]=='gif'){
                await saveJPG(localPath+singlePagePic.toString()+'.gif',picLink)
            }else{
                await saveJPG(localPath+singlePagePic.toString()+'.jpg',picLink)
            }
            
        }catch (error){
            console.log(error)
            break
        }
    }
}

// 遍历所有主链接的内容,包括html/总页数/名称
for (let mainHTMLSet of htmlList){
    // 创建新的文件夹
    console.log(nowString()+' '+mainHTMLSet[2])
    createDir(basePath+mainHTMLSet[2])
    let savePath:string=basePath+mainHTMLSet[2]+'/'     // 单个组的目录,组下面是具体图片集池的目录
    tampSavaPath=savePath
    // 遍历
    for (let pageNum=1;pageNum<=mainHTMLSet[1];pageNum++){
        console.log(nowString()+'  '+mainHTMLSet[2]+'  页数 : '+pageNum.toString())
        
        if (pageNum==1){
            let resultHTML=await parseHTML(mainHTMLSet[0],'')
            await getHref(resultHTML)
            
        }else{
            let tempURL:string=mainHTMLSet[0]+'/page/'+pageNum.toString()
            let resultHTML=await parseHTML(tempURL,'')
            await getHref(resultHTML)
        }
        sleep(4)
    }
    // 初始化页面计数器
    pageCounter=1
    sleep(4)
}

*/
import {nowString,sleep,parseHTML,currentPath,createDir,getHTML} from 'https://gitee.com/CCHChenChangHong/practice_code_field/raw/master/Typescript/commonForDeno.ts'
import { readerFromStreamReader } from "https://deno.land/std@0.100.0/io/mod.ts"
// 获取美女图片并保存到当前运行的目录
let cp:string=await currentPath()
let basePath:string=cp+'/situ-Pic'
let tampSavaPath:string=""
let picSavePath:string=""
let pageCounter:number=1
createDir(basePath)
basePath=basePath+'/'

let basePageLink:string="http://situdao.com/portal/list/index/id/4/p/"
let totalPage:number=986


async function saveJPG(localPath:string,jpgLink:string):Promise<void>{
    // function : 保存网络上的图片到本地 ,调用的时候前面要加上await才能正常使用,不使用的话也能用,但是是一次性保存所有图片
    // param localPath : as its name ,example: D:\pic\1.jpg
    // param jpgLink : picture s original link,example : http://www.xxx.com/1.jpg
    const res = await fetch(jpgLink);
    if (res.status!=200){
        return
    }
    const file = await Deno.open(localPath, { create: true, write: true });
    const reader = readerFromStreamReader(res.body!.getReader());
    await Deno.copy(reader, file);
    file.close();
}

function parsePicLink(link:string):string{
    // 处理0.jpg的连接,获取基础的链接
    // example : https://tjg.hywly.com/a/1/45404/0.jpg 
    // return : https://tjg.hywly.com/a/1/45404/
    let baseLink:string=link.split("0.jpg")[0]
    console.log("base link------>",baseLink)
    return baseLink
}

// 总页数的循环
for (let pageNum=1;pageNum<totalPage+1;pageNum++){
    console.log(nowString()+' 当前页数 '+pageNum)
    // 按照页数来创建目录一个page number就是保存一个页数的下包括子页面内的图片
    createDir(basePath+pageNum.toString())
    tampSavaPath=basePath+pageNum.toString()+"/"
    // 获取整个页面的链接和名称,名称用来创建page number下的子目录
    let nowPageHTML:string=basePageLink+pageNum.toString()+".html"
    // 获取html
    let pageResult:any=await parseHTML(nowPageHTML,"")
    // 获取每个图片集的入口link和名称
    for (let setNum=1;setNum<25;setNum++){
        // 获取图片集的名称
        let fileName:string=pageResult("img")[setNum.toString()].attribs.alt
        console.log(nowString()+" 当前的写真名称 :"+fileName)
        // 获取图片集里面的图片的链接
        let picLinks:string=pageResult("img")[setNum.toString()].attribs.src
        let basePicLink:string=parsePicLink(picLinks) // 获取base link

        // 创建单个page下每个图片集的目录
        createDir(tampSavaPath+fileName)
        picSavePath=tampSavaPath+fileName+"/"

        for (let picCounter=1;picCounter<99;picCounter++){
            // 假设每个集合的图片都有99张
            try{
                await saveJPG(picSavePath+picCounter.toString()+'.jpg',basePicLink+picCounter.toString()+'.jpg')
            }catch (error){
                console.log("--------->",error)
                break
            }
        }
        sleep(1)
    }
    // 每个界面暂停一段时间
    sleep(3)
}
