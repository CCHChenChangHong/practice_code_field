/*
匿名函数的使用
一般的函数是先定义，再传递，js可以直接在函数括号中定义和传递这个函数
*/

function two(some_function,function_input){
    some_function(function_input);
};

two(function(input_something){
    console.log(input_something);
},'hasaki');