/*
js里面的函数
一个函数可以作为另一个函数的参数，可以先定义一个函数，然后传递
也可以传递参数的地方直接定义函数
和Python的万物皆为对象类似，都是动态语言，差不多尿性
*/
function one(input_something){
    console.log(input_something);
};

function two(some_function,function_input){
    some_function(function_input);
};

two(one,'hasaki');