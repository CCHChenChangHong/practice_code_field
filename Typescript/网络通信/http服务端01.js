
/*
js实现http服务
*/
var http=require('http');

// 传进去一个函数
http.createServer(function(request,response){
    response.writeHead(200,{'hasaki':'gugu'});
    response.write('I really love the lady YPL');
    response.end();
}).listen(8888);

// 以下是兼容上面的方式
function onRequest(request,response){
    response.writeHead(200,{'hasaki':'gugu'});
    response.write('I really love the lady YPL');
    response.end();
}
http.createServer(onRequest).listen(8888);
//////////////////////////////////////////////