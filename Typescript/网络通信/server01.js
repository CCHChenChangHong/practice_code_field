/*
js中 的路由
路由提供URL需要的GET以及POST参数，
*/
var http =require('http');
var url=require('url');

function start(){
    function onRequest(request,response){
        var pathname=url.parse(request.url).pathname;
        console.log('Request for'+pathname+'received');
        response.writeHead(200,{'Content-Type':'text/plain'});
        response.write('You are the best Hasaki');
        response.end();
    }

    http.createServer(onRequest).listen(6666);
    console.log('Hasaki Server has started')
}

exports.start=start;


/*
其他脚本调用服务程序
var server=require('./server01');
server.start()
*/