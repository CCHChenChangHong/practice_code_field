let savePath:string='D:/gogo/pic.jpg'    //保存图片的本地文件路径
let picLink:string='http://www.hangxun100.com/data/attachment/forum/201904/02/141716qakbeybaboobaoa9.jpg'   // 网络图片的链接


import { readerFromStreamReader } from "https://deno.land/std@0.100.0/io/mod.ts";
const res = await fetch(picLink);
const file = await Deno.open(savePath, { create: true, write: true });

const reader = readerFromStreamReader(res.body!.getReader());
await Deno.copy(reader, file);
file.close();