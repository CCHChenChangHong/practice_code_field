// websocket 测试
declare const require: any
let webSocket=require('ws')

let wss=new webSocket('wss://fx-ws-testnet.gateio.ws/v4/ws/btc')

function send():void{
    wss.send('{"time" : 123456, "channel" : "futures.trades", "event": "subscribe", "payload" : ["BTC_USD"]}')
}

function recv(data:any):void{
    console.log('接收到交易所的数据',data)
}

// 发送消息
wss.on('open',send)

// 接收消息
wss.on('message',recv)