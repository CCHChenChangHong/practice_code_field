var http = require('https')

let options={
    method:'GET',
    hostname:'data.gateio.life',      // 这里得网址前面是没有http和https的，后面的路径也是没有的
    headers: { 'User-Agent' : '' },
    path:'/api2/1/ticker/eos_usdt'    // 网址后面的路径放在path这里
}
let req=http.request(options,function (res) {
    console.log('状态码 : ',res.statusCode)
    res.on('data',chunk=>{console.log(chunk)})    // 这里的chunk是buffer数据，转字符串用toString(),转json用JSON.parse
})
req.on('error', (e) => {
    console.error(e)
  });
req.end()