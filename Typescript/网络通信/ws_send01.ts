// node的websocket库
let websocket=require('ws')

let wss=new websocket('wss://fx-ws-testnet.gateio.ws/v4/ws/btc')

// 发送消息
wss.on('open', function open() {
    wss.send('{"time" : 123456, "channel" : "futures.trades", "event": "subscribe", "payload" : ["BTC_USD"]}');
  });

// 接收消息
wss.on('message', function incoming(data) {
    console.log(data);
  });