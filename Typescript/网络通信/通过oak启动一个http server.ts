import { Application } from "https://deno.land/x/oak/mod.ts";

const app = new Application();

app.use((ctx) => {
  ctx.response.body = "Hello Oak!";
});

console.log(`🦕 oak server running at http://127.0.0.1:8888/ 🦕`)

await app.listen("127.0.0.1:8888");

// http://127.0.0.1:8888