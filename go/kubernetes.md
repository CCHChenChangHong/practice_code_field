## kubernetes

#### 一整套流程

原地址：https://phoenixnap.com/kb/install-minikube-on-ubuntu

```
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install curl
sudo apt-get install apt-transport-https
sudo apt install virtualbox virtualbox-ext-pack

wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo cp minikube-linux-amd64 /usr/local/bin/minikube
sudo chmod 755 /usr/local/bin/minikube
minikube version

curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl

chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
kubectl version -o json
minikube start
# 可能会出现虚拟机不支持虚拟化 可以minikube delete删除原来的minikube之后再使用minikube start --driver=docker再重新启动

kubectl config view
kubectl cluster-info
kubectl get nodes
kubectl get pod
minikube ssh

```

#### 中文官网

[中文官网](https://kubernetes.io/zh/docs/tutorials/kubernetes-basics/)

#### 安装kubernetes

```
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"

echo "$(<kubectl.sha256) kubectl" | sha256sum --check

sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

kubectl version --client
```

```
用原生包管理工具安装

<!--
1. Update the `apt` package index and install packages needed to use the Kubernetes `apt` repository:
-->
1. 更新 `apt` 包索引，并安装使用 Kubernetes `apt` 仓库锁需要的包：


   sudo apt-get update
   sudo apt-get install -y apt-transport-https ca-certificates curl

<!--
2. Download the Google Cloud public signing key:
-->
2. 下载 Google Cloud 公开签名秘钥：

   sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

<!--
3. Add the Kubernetes `apt` repository:
-->
3. 添加 Kubernetes `apt` 仓库：

 
   echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list


<!--
4. Update `apt` package index with the new repository and install kubectl:
-->
4. 更新 `apt` 包索引，使之包含新的仓库并安装 kubectl：


   sudo apt-get update
   sudo apt-get install -y kubectl

```

#### 安装minikube

[使用minikube搭建k8s](https://www.cnblogs.com/arvinhuang/p/14783448.html)

```
下载minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
安装
sudo install minikube-linux-amd64 /usr/local/bin/minikube

# 启动minikube
minikube start



# 出现Exiting due to DRV_AS_ROOT: The "docker" driver should not be used with root privileges.的情况
->提示docker 驱动不能与管理员权限一起使用

sudo groupadd docker    #添加docker用户组
sudo gpasswd -a $USER docker #将登陆用户加入到docker用户组中
newgrp docker  #更新用户组
minikube start  # 继续运行

```

#### 把宿主机目录弄进minikube集群容器中

使用minikube ssh进入集群容器终端
然后使用linux的ssh命令把远程的目录下载到容器内
```shell
scp sudo scp hasaki@170.20.6.183:/home/hasaki/main.py /home/hasaki/main.py (后面的这个路径是容器内的目录)
```


#### kubernets基本命令

```shell
1,查看集群状态
kubectl version --short=true 查看客户端及服务端程序版本信息
kubectl cluster-info 查看集群信息

2,创建资源对象
kubectl run name --image=(镜像名) --replicas=(备份数) --port=(容器要暴露的端口) --labels=(设定自定义标签)
kubectl create -f **.yaml  陈述式对象配置管理方式
kubectl apply -f **.yaml  声明式对象配置管理方式（也适用于更新等）

3,查看资源对象
kubectl get namespace 查看命名空间
kubectl get pods,services -o wide (-o 输出格式 wide表示plain-text)
kubectl get pod -l "key=value,key=value" -n kube-system (-l 标签选择器(多个的话是与逻辑)，-n 指定命名空间，不指定默认default)
kubectl get pod -l "key1 in (val1,val2),!key2" -L key (-l 基于集合的标签选择器, -L查询结果显示标签) 注意：为了避免和shell解释器解析!,必须要为此类表达式使用单引号
kubectl get pod -w(-w 监视资源变动信息)

4,打印容器信息
kubectl logs name -f -c container_name -n kube-system (-f 持续监控，-c如果pod中只有一个容器不用加)

5,在容器中执行命令
kubectl exec name -c container_name -n kube-system -- 具体命令
kubectl exec -it pod_name /bin/sh 进入容器的交互式shell

6,删除资源对象
kubectl delete [pods/services/deployments/...] name 删除指定资源对象
kubectl delete [pods/services/deployments/...] -l key=value -n kube-system  删除kube-system下指定标签的资源对象
kubectl delete [pods/services/deployments/...] --all -n kube-system 删除kube-system下所有资源对象
kubectl delete [pods/services/deployments/...] source_name --force --grace-period=0 -n kube-system 强制删除Terminating的资源对象
kubectl delete -f xx.yaml
kubectl apply -f xx.yaml --prune -l <labels>(一般不用这种方式删除)
kubectl delete rs rs_name --cascade=fale(默认删除控制器会同时删除其管控的所有Pod对象，加上cascade=false就只删除rs)

7,更新资源对象
kubectl replace -f xx.yaml --force(--force 如果需要基于此前的配置文件进行替换，需要加上force)

8,服务暴露出去
kubectl expose deployments/deployment_name --type="NodePort" --port=(要暴露的容器端口) --name=(Service对象名字)

9,扩容和缩容
kubectl scale deployment/deployment_name --replicas=N
kubectl scale deployment/deployment_name --replicas=N --current-replicas=M 只有当前副本数等于M时才会执行扩容或者缩容

10,查看API版本
kubectl api-versions

11,在本地主机上为API Server启动一个代理网关
kubectl proxy --port=8080
之后就可以通过curl来对此套字节发起访问请求
curl localhost:8080/api/v1/namespaces/ | jq .items[].metadata.name (jq可以对json进行过滤)

12,当定义资源配置文件时，不知道怎么定义的时候，可以查看某类型资源的配置字段解释
kubectl explain pods/deployments/...(二级对象可用类似于pods.spec这种方式查看)

13,查看某资源对象的配置文件
kubectl get source_type source_name -o yaml --export(--export表示省略由系统生成的信息) 后面加 > file.yaml就可以快速生成一个配置文件了


14,标签管理相关命令
kubectl label pods/pod_name key=value 添加标签,如果是修改的话需要后面添加--overwrite
kubectl label nodes node_name key=value 给工作节点添加标签，后续可以使用nodeSelector来指定pod被调度到指定的工作节点上运行

15,注解管理相关命令
kubectl annotate pods pod_name key=value

16,patch修改Deployment控制器进行控制器升级
kubectl patch deployment deployment-demo -p '{"spec": {"minReadySeconds": 5}}'(-p 以补丁形式更新补丁形式默认是json)
kubectl set image deployments deployment-demo myapp=ikubernetes/myapp:v2 修改depolyment中的镜像文件
kubectl rollout status deployment deployment-demo 打印滚动更新过程中的状态信息
kubectl get deployments deployment-demo --watch 监控deployment的更新过程
kubectl kubectl rollout pause deployments deployment-demo 暂停更新
kubectl rollout resume deployments deployment-demo 继续更新
kubectl rollout history deployments deployment-demo 查看历史版本(能查到具体的历史需要在apply的时候加上--record参数)
kubectl rollout undo deployments deployment-demo --to-revision=2 回滚到指定版本，不加--to-version则回滚到上一个版本

17,快速生成yaml模板文件
kubectl create deployment nginx --image=nginx -o yaml --dry-run > deployment.yaml
kubectl expose deployment nginx --port=80 --type=NodePort -o yaml --dry-run > svc.yaml
```



#### yaml格式

[参考资料](https://www.cnblogs.com/-wenli/p/13632474.html)

```yaml
apiVersion: v1       #必填，版本号，例如v1
kind: Depolyment     #必填
metadata:       #必填，元数据
  name: string       #必填，Pod名称
  namespace: string    #必填，Pod所属的命名空间
  labels:      #自定义标签
    - name: string     #自定义标签名字<key: value>
  annotations:       #自定义注释列表
    - name: string
spec:         #必填，部署的详细定义
  selector: 
    matchLabels:
      name: string #必填，通过此标签匹配对应pod<key: value>
  replicas: number #必填，副本数量
  template: #必填，应用容器模版定义
    metadata: 
      labels: 
        name: string #必填，遇上面matchLabels的标签相同
    spec: 
      containers:      #必填，定义容器列表
      - name: string     #必填，容器名称
        image: string    #必填，容器的镜像名称
        imagePullPolicy: [Always | Never | IfNotPresent] #获取镜像的策略 Alawys表示下载镜像 IfnotPresent表示优先使用本地镜像，否则下载镜像，Nerver表示仅使用本地镜像
        command: [string]    #容器的启动命令列表，如不指定，使用打包时使用的启动命令
        args: [string]     #容器的启动命令参数列表
        workingDir: string     #选填，容器的工作目录
        env:       #容器运行前需设置的环境变量列表
        - name: string     #环境变量名称
          value: string    #环境变量的值
        ports:       #需要暴露的端口库号列表
        - name: string     #选填，端口号名称
          containerPort: int   #容器需要监听的端口号
          hostPort: int    #选填，容器所在主机需要监听的端口号，默认与Container相同
          protocol: string     #选填，端口协议，支持TCP和UDP，默认TCP
        resources:       #资源限制和请求的设置
          limits:      #资源限制的设置
            cpu: string    #Cpu的限制，单位为core数，将用于docker run --cpu-shares参数
            memory: string     #内存限制，单位可以为Mib/Gib，将用于docker run --memory参数
          requests:      #资源请求的设置
            cpu: string    #Cpu请求，容器启动的初始可用数量
            memory: string     #内存清楚，容器启动的初始可用数量
        volumeMounts:    #挂载到容器内部的存储卷配置
        - name: string     #引用pod定义的共享存储卷的名称，需用volumes[]部分定义的的卷名
          mountPath: string    #存储卷在容器内mount的绝对路径，应少于512字符
          readOnly: boolean    #是否为只读模式
        livenessProbe:     #对Pod内个容器健康检查的设置，当探测无响应几次后将自动重启该容器，检查方法有exec、httpGet和tcpSocket，对一个容器只需设置其中一种方法即可
          exec:      #对Pod容器内检查方式设置为exec方式
            command: [string]  #exec方式需要制定的命令或脚本
          httpGet:       #对Pod内个容器健康检查方法设置为HttpGet，需要制定Path、port
            path: string
            port: number
            host: string
            scheme: string
            HttpHeaders:
            - name: string
              value: string
          tcpSocket:     #对Pod内个容器健康检查方式设置为tcpSocket方式
            port: number
          initialDelaySeconds: 0  #容器启动完成后首次探测的时间，单位为秒
          timeoutSeconds: 0   #对容器健康检查探测等待响应的超时时间，单位秒，默认1秒
          periodSeconds: 0    #对容器监控检查的定期探测时间设置，单位秒，默认10秒一次
          successThreshold: 0
          failureThreshold: 0
          securityContext:
            privileged:false
        #Pod的重启策略，Always表示一旦不管以何种方式终止运行，kubelet都将重启，OnFailure表示只有Pod以非0退出码退出才重启，Nerver表示不再重启该Pod
        restartPolicy: [Always | Never | OnFailure]
        nodeSelector: obeject  #设置NodeSelector表示将该Pod调度到包含这个label的node上，以key：value的格式指定
        imagePullSecrets:    #Pull镜像时使用的secret名称，以key：secretkey格式指定
        - name: string
        hostNetwork:false      #是否使用主机网络模式，默认为false，如果设置为true，表示使用宿主机网络
        volumes:       #在该pod上定义共享存储卷列表
        - name: string     #共享存储卷名称 （volumes类型有很多种）
          emptyDir: {}     #类型为emtyDir的存储卷，与Pod同生命周期的一个临时目录。为空值
          hostPath: string     #类型为hostPath的存储卷，表示挂载Pod所在宿主机的目录
            path: string     #Pod所在宿主机的目录，将被用于同期中mount的目录
        - name: string     #共享存储卷名称
          secret:      #类型为secret的存储卷，挂载集群与定义的secre对象到容器内部
            scretname: string  
            items:     
            - key: string     #选择secrets定义的某个key
              path: string    #文件内容路径
        - name: string     #共享存储卷名称
          configMap:     #类型为configMap的存储卷，挂载预定义的configMap对象到容器内部
            name: string
            items:
            - key: string     #选择configmap定义的某个key
              path: string     #文件内容路径
        - name: string     #共享存储卷名称
          persistentVolumeClaim:
            claimName: string     #类型为PVC的持久化存储卷
```


### yaml示例

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: la-k8s
  labels:
    app: backtest
spec:
  containers:
    - name: task1
      image: registry.cn-guangzhou.aliyuncs.com/hasakichen/base_python3:v2.1.1
      imagePullPolicy : Never
      volumeMounts : 
      - mountPath : /home
        name : hasaki
      command : ["sh","-c","python3 /home/main.py"]
    - name: task2
      image: registry.cn-guangzhou.aliyuncs.com/hasakichen/base_python3:v2.1.1
      imagePullPolicy : Never
      volumeMounts : 
      - mountPath : /home
        name : hasaki
      command : ["sh","-c","python3 /home/main.py"]

  volumes :
    - name : hasaki
      hostPath : 
        path : /home/hasaki
  
  # never restart pod
  restartPolicy: Never

```