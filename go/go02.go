package main
import "fmt"

func main(){
	fmt.Println("hasaki,中文也可以吗")
	var a int
	var b bool
	var s string
	a,b,s=ha1()
	fmt.Println(a,b,s)
	ha2()
	ha3(1,2)
}

// 赋值
func ha1() (int,bool,string){
	a := 1
	b := true
	s := "hasaki"
	return a,b,s
}

// 测试循环
func ha2(){
	a := [...]int{1,2,3,4,5,6,7,8,9,10}
	for _,h :=range a{
		fmt.Println(h)
	}
}

// 测试函数参数
func ha3(a,b int){
	fmt.Println(a,b)
}