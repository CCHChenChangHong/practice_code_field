package main

import (
	"fmt"
	"os"
    "path/filepath"
    "strings"
)

func main()  {
	dir,_:=filepath.Abs(filepath.Dir(os.Args[0]))
    fmt.Println("当前文件运行的路径：",dir)
    // 当前文件运行的路径： c:\Users\cchyp\Desktop\go  这个是该运行文件的所在文件夹
    needPath:=strings.Split(dir,"\\")
    fmt.Println("文件地址切割之后",needPath[:3]) //文件地址切割之后 [c: Users cchyp]
}