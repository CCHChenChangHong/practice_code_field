### linux安装golang 

sudo apt-get install goland  这个并不会安装到最新版本

snap安装

sudo snap install goland --classic


```shell
sudo wget -c https://golang.google.cn/dl/go1.16.6.linux-amd64.tar.gz -O - | sudo tar -xz -C /usr/local
export PATH=$PATH:/usr/local/go/bin
source ~/.profile
go version
```

#### golang学习资料

[Go语言圣经](https://books.studygolang.com/gopl-zh/)

[mojotv 进阶系列](https://mojotv.cn/404#Golang)

[跟煎鱼学GO](https://eddycjy.gitbook.io/golang/)

[Uber 编程规范](https://www.infoq.cn/article/G6c95VyU5telNXXCC9yO)   这个是一篇文章,把它拉下来到本地仓库




#### golang标准库：https://studygolang.com/static/pkgdoc/


#### http协议中间件 : go get github.com/gin-contrib/cors

#### grpc : go get -u google.golang.org/grpc

#### JSON 解析 : go get github.com/buger/jsonparser     go get github.com/json-iterator/go

#### beego : go get github.com/astaxie/beego

#### http : go get github.com/julienschmidt/httprouter    go get github.com/valyala/fasthttp     go get github.com/gorilla/mux

#### elastic : go get github.com/olivere/elastic

#### 网络frp : go get github.com/fatedier/frp

#### Service Mesh istio: go get github.com/istio/istio

#### goquery中文文档：https://www.flysnow.org/2018/01/20/golang-goquery-examples-selector.html

#### goquery官方文档: https://godoc.org/github.com/PuerkitoBio/goquery#pkg-examples

#### goquery : go get github.com/PuerkitoBio/goquery

#### 下载redis数据库: go get github.com/garyburd/redigo/redis    go get -u github.com/go-redis/redis

#### 下载mongoDB数据库:go get github.com/go-mgo/mgo  参考文档 ：https://www.jianshu.com/p/f52298dad0e4

#### golang后台框架gin:go get -u github.com/gin-gonic/gin 参考文档：https://www.jianshu.com/p/93b5cdd458b4

#### 数据库抽象gorm:go get -u github.com/jinzhu/gorm

#### golang实现以太坊：https://github.com/ethereum/go-ethereum

#### golang消息引擎kafka：go get github.com/Shopify/sarama

#### golang websocket服务 : go get github.com/gorilla/websocket

#### influxDB : go get github.com/influxdata/influxdb1-client/v2

#### 如果go get 被墙导致无法获取golang.org/net/x/可使用(ubuntu):

sudo mkdir -p $GOPATH/src/golang.org/x/

cd $GOPATH/src/golang.org/x/

sudo git clone https://github.com/golang/net.git net

sudo go install net

还有一些库需要git clone https://github.com/golang/sys.git 同样在golang.org/x目录下


#### go代理库

https://goproxy.io/zh/

### 使用goproxy

[参考资料](https://blog.51cto.com/u_3732370/2420936)

#### 1.16版本后的库管理

go 1.13之后,go的包管理是通过在工作目录中使用
```shell
go mod init main
```
可以自动创建
然后再在该目录中直接go get就可以了

使用go env,GO111MODULE=""

然后

```
go mod init XXX //xxx代表文件夹名
然后
go get xxx
```


```
export GOPROXY=https://mirrors.aliyun.com/goproxy/
go get xxx
```

```
# 启用 Go Modules 功能
go env -w GO111MODULE=on

# 配置 GOPROXY 环境变量，以下三选一

# 1. 七牛 CDN
go env -w  GOPROXY=https://goproxy.cn,direct

# 2. 阿里云
go env -w GOPROXY=https://mirrors.aliyun.com/goproxy/,direct

# 3. 官方
go env -w  GOPROXY=https://goproxy.io,direct

# 测试
go env | grep GOPROXY
```

```
# window
# 启用 Go Modules 功能
$env:GO111MODULE="on"

# 配置 GOPROXY 环境变量，以下三选一

# 1. 七牛 CDN
$env:GOPROXY="https://goproxy.cn,direct"

# 2. 阿里云
$env:GOPROXY="https://mirrors.aliyun.com/goproxy/,direct"

# 3. 官方
$env:GOPROXY="https://goproxy.io,direct"

# 测试
time go get golang.org/x/tour
```

私有模块
```
# Go version >= 1.13
go env -w GOPROXY=https://goproxy.cn,direct
# 设置不走 proxy 的私有仓库，多个用逗号相隔
go env -w GOPRIVATE=*.corp.example.com

```



#### jupyter notebook使用golang

sudo apt-get install libzmq3-dev

sudo apt-get install pkg-config

go get github.com/yunabe/lgo/cmd/lgo && go get -d github.com/yunabe/lgo/cmd/lgo-internal

sudo vim /etc/profile

export LGOPATH="/home/ubuntu/go/lgo"

export PATH=$PATH:$LGOPATH

Python $(go env GOPATH)/src/github.com/yunabe/lgo/bin/install_kernel