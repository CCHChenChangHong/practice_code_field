package main

import "database/sql"
import _ "github.com/go-sql-driver/mysql"

var sqlDB *sql.DB  // 数据库的类型

func main(){
    // 连接，kunlun2020是数据库名
    mysql,err:=sql.Open("mysql","root:kunlun2020@tcp(0.0.0.0:3306)/kunlun_quote?charset=utf8")
    
    // 其他操作
    sqlDB=mysql

    // 查询数据
    rows, err := sqlDB.Query("select * from tableName")
    if err!=nil{
		fmt.Println("查询mysql数据库报错 : ",err)
	}else{
		for rows.Next(){
			var time string
			var vol float32
			var open float32
			var high float32
			var low float32
			var close float32
			var id interface{}
			var amount interface{}
			var count interface{}
			err:=rows.Scan(&id,&amount,&count,&open,&high,&low,&close,&vol,&time)
			fmt.Println(id,amount,count,open,high,low,close,vol,time)
		}
}