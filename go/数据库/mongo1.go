package main


// 增删查改
import "fmt"
import "gopkg.in/mgo.v2"

func main(){
	var address string = "127.0.0.1:27017"

	// 连接数据库
	session,err:=mgo.Dial(address)
	if err!=nil{
		fmt.Println("连接数据库报错",err)
	}

	// 选择表,test数据库的hasakiTestTable表
	c:=session.DB("test").C("hasakiTestTable")

	// 增
	c.Insert(map[string]interface{}{"hasaki":814,"id":"hasaki"})

	// 查全部
	var result []map[string]interface{}

	c.Find(nil).All(&result)
	fmt.Println(result)

    // 倒序一百条数据
    c.Find(nil).Sort("-date").Limit(100).All(&result)

    // 删除所有某字段的数据
    c.RemoveAll(bson.M{"date":"2020-1-13"})

	// objid := bson.ObjectIdHex("55b97a2e16bc6197ad9cad59")

	// c.RemoveId(objid) //删除

	// c.UpdateId(objid, map[string]interface{}{"id": 8, "name": "aaaaa", "age": 30}) //改

	// var one map[string]interface{}
	// c.FindId(objid).One(&one) //查询符合条件的一行数据
	// fmt.Println(one)
}
/*
[map[_id:ObjectIdHex("5e0171c154f430d331a8458b") hasaki:814 id:hasaki] map[_id:ObjectIdHex("5e01728454f430d331a84590") hasaki:814 id:hasaki]]
*/