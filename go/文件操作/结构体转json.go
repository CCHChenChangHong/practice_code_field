package main

import (
	"fmt"
	"encoding/json"
)

// 首字母一定要为大写，不然json编码出来只有一个空的数据{}，结构后面的tag可以改变输出json的键值
type jsonStruct struct{
	Time string `json:"time"`
	Volume float32 `json:"volume"`
	High float32 `json:"high"`
	Open float32 `json:"open"`
	Low float32  `json:"low"`
	Close float32 `json:"close"`
	Symbol string `json:"symbol"`
}

func main(){
	var js jsonStruct
	js.Time="2019-6-3"
	js.Volume=1230.0
	js.Open=1.0
	js.High=2.0
	js.Low=3.0
	js.Close=4.0
	js.Symbol="btc_usdt"
	fmt.Println("-------------",js)
	if data,err:=json.Marshal(js);err==nil{
		fmt.Println(string(data),err)
	}else{
		fmt.Println("结果错误",err)
	}
	
}
/*
------------- {2019-6-3 1230 2 1 3 4 btc_usdt}
{"Time":"2019-6-3","Volume":1230,"High":2,"Open":1,"Low":3,"Close":4,"Symbol":"btc_usdt"} <nil>
结构带tag之后：
{"time":"2019-6-3","volume":1230,"high":2,"open":1,"low":3,"close":4,"symbol":"btc_usdt"}
*/