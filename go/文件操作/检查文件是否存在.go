func pathExist(path string) bool{
	// function : 检查文件或者文件夹是否存在,出现异常在这个函数内解决
	// return : 1,true,nil:文件存在 2,false,nil:文件不存在 3,false,error:查询出现错误,不确定文件是否存在
	_, err := os.Stat("D:/gogo/pic")
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}