package main
// 读写json文件
import (
	"fmt"
	"io/ioutil"
	"encoding/json"
)

func main(){
	jsonInPath:="C:/Users/cchyp/Desktop/hasaki/quote/last_week/2019-6-1.json"
	//jsonOutPath:=""

	fmt.Println("读取json")
	byteValue, err := ioutil.ReadFile(jsonInPath)
	if err!=nil{
		fmt.Println("报错1")
	}
	//fmt.Println("原始码输出",byteValue)

	var result []map[string]interface{}
	err = json.Unmarshal(byteValue, &result)
	if err != nil {
		fmt.Println("报错2",err)
	}
	fmt.Println("修改json",result[:3],"\n",result[0]["high"])

	//err = ioutil.WriteFile(jsonOutPath, byteValue, 0644)
	fmt.Println("写入json")
}

/*读取的json
[
    {
        "time": "2019-06-02 01:25:00",
        "open": 8.2647,
        "close": 8.15,
        "high": 8.33,
        "low": 7.9928,
        "volume": 12699.126066684497
    },
    ...
]




程序运行的结果：
读取json
修改json [map[low:7.9928 volume:12699.126066684497 time:2019-06-02 01:25:00 open:8.2647 close:8.15 high:8.33] map[time:2019-06-02 05:25:00 open:8.2647 close:8.3 high:8.3858 low:7.7941 volume:20381.228699520554] map[low:7.453 volume:47952.42198025403 time:2019-06-02 09:25:00 open:8.2647 close:7.8206 high:8.3858]] 
 8.33
写入json

*/