package main

import (
	"fmt"
	"encoding/json"
)

// 首字母一定要为大写，不然json编码出来只有一个空的数据{}
type jsonStruct struct{
	Time string
	Volume float32
	High float32
	Open float32
	Low float32 
	Close float32
	Symbol string
}

func main(){
	var js jsonStruct
	js.Time="2019-6-3"
	js.Volume=1230.0
	js.Open=1.0
	js.High=2.0
	js.Low=3.0
	js.Close=4.0
	js.Symbol="btc_usdt"
	fmt.Println("-------------",js)
	if data,err:=json.MarshalIndent(js,"","    ");err==nil{
		fmt.Println(string(data),err)
	}else{
		fmt.Println("结果错误",err)
	}
	
}
/*
    "Time": "2019-6-3",
    "Volume": 1230,
    "High": 2,
    "Open": 1,
    "Low": 3,
    "Close": 4,
    "Symbol": "btc_usdt"
} <nil>
*/