package main
// 读写json文件
import (
	"fmt"
	"io/ioutil"
	"encoding/json"
)

func main(){
	jsonInPath:="C:/Users/cchyp/Desktop/strategy/CTA_setting.json"
	jsonOutPath:="C:/Users/cchyp/Desktop/strategy/cta_json.json"    // 文件夹中不存在这个文件，运行之后，会自动生成

	fmt.Println("读取json")
	byteValue, err := ioutil.ReadFile(jsonInPath)
	if err!=nil{
		fmt.Println("报错1")
	}
	//fmt.Println("原始码输出",byteValue)

	var result []map[string]interface{}
	err = json.Unmarshal(byteValue, &result)
	if err != nil {
		fmt.Println("报错2",err)
	}
	fmt.Println("修改json",result[:3],"\n",result[0]["high"])
	// map[] map[]] <nil>  因为这个json没有"high"这个字段
	err = ioutil.WriteFile(jsonOutPath, byteValue, 0644)
	fmt.Println("写入json")
}
