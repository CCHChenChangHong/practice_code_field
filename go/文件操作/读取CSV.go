package main  
  
import (  
    "encoding/csv"  
    "fmt"  
    "io"  
    "os"  
)  
  
func main() {  
    file, err := os.Open("C:/hashaki/work/run/Init.csv")  
    if err != nil {  
        fmt.Println("Error:", err)  
        return  
    }  
    // 这个方法体执行完成后，关闭文件  
    defer file.Close()  
  
    reader := csv.NewReader(file)  
    for {  
        // Read返回的是一个数组，它已经帮我们分割了，  
        record, err := reader.Read()  
        // 如果读到文件的结尾，EOF的优先级居然比nil还高！  
        if err == io.EOF {  
            break  
        } else if err != nil {  
            fmt.Println("记录集错误:", err)  
            return  
        }  
        for i := 0; i < len(record); i++ {  
            fmt.Print(record[i] + " | ")  
        }  
        fmt.Print("\n")  
    }  
}
/*
_id | amount | close | end_time | frequency | high | low | open | openInterest | start_time | symbol | volume | 
1082583788451532800 | 2147483647 | 235.2 | 2015-09-26T00:00:00Z | 1d | 239.99 | 234.81 | 239.99 | 0 | 2015-09-25T00:00:00Z | XBTUSD | 190090 | 
1082583788619304960 | 2147483647 | 234.51 | 2015-09-27T00:00:00Z | 1d | 235.36 | 232.91 | 235.2 | 0 | 2015-09-26T00:00:00Z | XBTUSD | 258488 | 
1082583788711579648 | 2147483647 | 233.29 | 2015-09-28T00:00:00Z | 1d | 234.76 | 233.03 | 234.51 | 0 | 2015-09-27T00:00:00Z | XBTUSD | 161013 | 
1082583788829020160 | 2147483647 | 240.07 | 2015-09-29T00:00:00Z | 1d | 243.19 | 232.9 | 233.29 | 0 | 2015-09-28T00:00:00Z | XBTUSD | 319143 | 
1082583788925489152 | 2147483647 | 237 | 2015-09-30T00:00:00Z | 1d | 241.5 | 235.81 | 240.07 | 0 | 2015-09-29T00:00:00Z | XBTUSD | 368771 | 
1082583789038735360 | 2147483647 | 236.55 | 2015-10-01T00:00:00Z | 1d | 238.2 | 235.56 | 237 | 0 | 2015-09-30T00:00:00Z | XBTUSD | 294736 | 
1082583789151981568 | 2147483647 | 238.1 | 2015-10-02T00:00:00Z | 1d | 239.53 | 234.63 | 236.55 | 0 | 2015-10-01T00:00:00Z | XBTUSD | 180356 | 
*/