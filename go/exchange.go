/*
开发者：陈常鸿
创建时间：2019-6-7
最后修改时间：2019-6-27
模拟交易所
模块功能：
1，撮合交易
2，接收投资者发单
3，投资者账户信息
4，行情广播发送
5，合成大周期K线

说明：
本来打算是做广播分发行情，但想想在本地跑为什么还用广播
于是模拟交易所只做一个一条连接即可，在客户端再做行情和下单的聚合
因为每个策略都是有用户id，所以在客户端那边再做一个分发
*/
package main

import (
	"fmt"
	"encoding/json"
	"net/http"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"reflect"
	"sync"
	"math"

	"github.com/gorilla/websocket"
)

//全局变量
var upgrader=websocket.Upgrader{}
var goSync sync.WaitGroup                // 声明一个WaitGroup变量
var quoteChan chan map[string]interface{}    // 行情的管道

type PriceAndVolume struct{
	Price int `json:"price"`     // type : int
	Volume int `json:"volume"`   // type : int
	Stack []int `json:"stack"`     // type : []
}

// 深度行情数据结构
type Depth struct{
	Time string `json:"time"`
	Symbol string `json:"symbol"`

	Ask10 PriceAndVolume `json:"ask10"`
	Ask9 PriceAndVolume `json:"ask9"`
	Ask8 PriceAndVolume `json:"ask8"`
	Ask7 PriceAndVolume `json:"ask7"`
	Ask6 PriceAndVolume `json:"ask6"`
	Ask5 PriceAndVolume `json:"ask5"`
	Ask4 PriceAndVolume `json:"ask4"`
	Ask3 PriceAndVolume `json:"ask3"`
	Ask2 PriceAndVolume `json:"ask2"`
	Ask1 PriceAndVolume `json:"ask1"`

	Bid1 PriceAndVolume `json:"bid1"`
	Bid2 PriceAndVolume `json:"bid2"`
	Bid3 PriceAndVolume `json:"bid3"`
	Bid4 PriceAndVolume `json:"bid4"`
	Bid5 PriceAndVolume `json:"bid5"`
	Bid6 PriceAndVolume `json:"bid6"`
	Bid7 PriceAndVolume `json:"bid7"`
	Bid8 PriceAndVolume `json:"bid8"`
	Bid9 PriceAndVolume `json:"bid9"`
	Bid10 PriceAndVolume `json:"bid10"`
}

// tick行情数据结构，合并了深度数据
type Quote struct{
	Time interface{} `json:"time"`       // type : string
	Symbol interface{} `json:"symbol"`   // type : string

	control Control
	// stack是一个档位的堆栈，先到这个档位的单先消耗
	Ask10 map[string]interface{} `json:"ask10"`    // type : {"price":int,"volume":int,"stack":[]map[string]int}
	Ask9 map[string]interface{} `json:"ask9"`
	Ask8 map[string]interface{} `json:"ask8"`
	Ask7 map[string]interface{} `json:"ask7"`
	Ask6 map[string]interface{} `json:"ask6"`
	Ask5 map[string]interface{} `json:"ask5"`
	Ask4 map[string]interface{} `json:"ask4"`
	Ask3 map[string]interface{} `json:"ask3"`
	Ask2 map[string]interface{} `json:"ask2"`
	Ask1 map[string]interface{} `json:"ask1"`

	Bid1 map[string]interface{} `json:"bid1"`
	Bid2 map[string]interface{} `json:"bid2"`
	Bid3 map[string]interface{} `json:"bid3"`
	Bid4 map[string]interface{} `json:"bid4"`
	Bid5 map[string]interface{} `json:"bid5"`
	Bid6 map[string]interface{} `json:"bid6"`
	Bid7 map[string]interface{} `json:"bid7"`
	Bid8 map[string]interface{} `json:"bid8"`
	Bid9 map[string]interface{} `json:"bid9"`
	Bid10 map[string]interface{} `json:"bid10"`
}

// K线数据
type Kline struct{
	Time string `json:"time"`
	Volume float32 `json:"volume"`
	Open float32 `json:"open"`
	High float32 `json:"high"`
	Low float32 `json:"low"`
	Close float32 `json:"close"`
	Frequency string `json:"frequency"`
	Symbol string `json:"symbol"`
}

// 用户order的数据结构
type UserOrder struct{
	Symbol string `json:"symbol"`
	Type string `json:"type"`
	Price int `json:"price"`
	Volume int `json:"volume"`
	Direction string `json:"direction"`
	UserID int `json:"userID"`
}

// 投资者账号信息
type User struct{
	Cash int `json:"cash"`
	FreezeCash int `json:"freezeCash"`
	FreezePos int `json:"freezePos"`
	Position int `json:"position"`
}

// 用于控制流程的结构体
type Control struct{
	readTheLocalQuote bool     // 刚启动模拟交易所，第一次读取本地行情快照的开关
}

// 客户端结构体
type Client struct{
	hub *Hub
	conn *websocket.Conn
	send chan []byte
}

// 总设置
type Hub struct{
	clients map[*Client]bool    // 客户端登陆
	broadcast chan []byte       // 广播的消息
	register chan *Client       // 客户端的登陆信息
	unregister chan *Client     // 客户端退出登陆
}

// 主函数
func main(){
	url:=getDataFilePath()+"users.json"
	jsonData:=readJson(url)
	fmt.Println("jsonData",jsonData)
	//writeJson(url,jsonData)
	http.HandleFunc("/",route)    // 接收到的信息
	log.Fatal(http.ListenAndServe("127.0.0.1:6666",nil))   // 监听端口
}

// 读取本地json文件
func readJson(localPath string) []map[string]interface{} {
	byteValue, err := ioutil.ReadFile(localPath)
	if err!=nil{
		fmt.Println("读取json文件报错，读取的文件地址：",localPath)
	}

	var result []map[string]interface{}
	err = json.Unmarshal(byteValue, &result)
	if err != nil {
		fmt.Println("解析json 错误",err)
	}
	
	return result
}

// 写入本地json文件
func writeJson(localPath string,mapData interface{}){
	if jsonData,err:=json.Marshal(mapData);err==nil{
		err := ioutil.WriteFile(localPath, jsonData, 0644)
		fmt.Println("写入json",err)
	}else{
		fmt.Println("结果错误",err)
	}
}

// 发送信息到客户端
func send(w http.ResponseWriter,r *http.Request){
	c,_:=upgrader.Upgrade(w,r,nil)
	defer c.Close()
	s:=new(PriceAndVolume)
	s.Price=1
	s.Volume=2
	data:=structToJson(s)
	for {
		// 获取行情
		c.WriteMessage(1,append([]byte(data)))
	}
}

// float64转int
func float64ToInt(f float64) int {
	return int(math.Floor(f))
}

// 结构体转json
func structToJson(s interface{}) string {
	var data string
	if dataU,err:=json.MarshalIndent(s,"","    ");err==nil{
		// fmt.Println(fmt.Sprintf("%T",dataU)) []uint8
		data=string(dataU)
	}else{
		log.Fatal("结构体转json结果错误",err)
	}
	return data
}

// uint8转字符串
func uint8ToStr(uint8Data []uint8) string{
	//fmt.Println("uint8转字符串",string(uint8Data))
	return string(uint8Data)
}

// json转map
func jsonToMap(jsonStr string) map[string]interface{}{
	var returnData map[string]interface{}
	err:=json.Unmarshal([]byte(jsonStr),&returnData)
	if err!=nil{
		log.Fatal("json转map报错",jsonStr,"错误原因:",err)
	}
	return returnData
}

// uint8转map
func uint8ToMap(uint8Data []uint8) interface{}{
	var returnData map[string]interface{}
	err:=json.Unmarshal(uint8Data,&returnData)
	if err!=nil{
		log.Fatal("json转map报错",uint8Data,err)
	}
	return returnData
}

// map 转 byte数组
func mapToByte(mapData map[string]interface{}) []byte {
	mJson,_:=json.Marshal(mapData)
	mJsonStr:=string(mJson)
	mByte:=[]byte(mJsonStr)
	return mByte
}

// 接收客户端发来的请求
func recv(w http.ResponseWriter,r *http.Request){
	c,_:=upgrader.Upgrade(w,r,nil)
	defer c.Close()
	for {  // 收到send后的处理
		mt,message,_:=c.ReadMessage()
		c.WriteMessage(mt,append([]byte("发送"),message[:]...))
	}
}

// byte类型转map
func byteToMap(byteData []byte) map[string]interface{}{
	// TODO :具体实现转换
	var defaultReturn map[string]interface{}
	defaultReturn["default"]="default"
	return defaultReturn
}

// 把一个map[string]interface{}里面的float64类型转换成另一个数值为int类型的map[string]interface{}
func float64MapToIntMap(float64Map map[string]interface{},IntMap *map[string]interface{}){
	// 这是个定制函数，不是通用函数,目前order里面包含了字符串类型userID,type,direction,float64类型price和volume
	//var tempOriginMap map[string]interface{}
	var tempAfterMap map[string]interface{}
	tempAfterMap=*IntMap
	// *IntMap["userID"]=float64Map["userID"]
	// *IntMap["type"]=float64Map["type"]
	// *IntMap["direction"]=float64Map["direction"]
	// *IntMap["price"]=float64ToInt(float64Map["price"])
	// *IntMap["volume"]=float64ToInt(float64Map["volume"])
	tempAfterMap["userID"]=float64Map["userID"]
	tempAfterMap["type"]=float64Map["type"]
	tempAfterMap["direction"]=float64Map["direction"]
	tempAfterMap["price"]=float64ToInt(float64Map["price"].(float64))
	tempAfterMap["volume"]=float64ToInt(float64Map["volume"].(float64))

}

// 订单map转结构体
func (o *UserOrder)mapToStruct(order map[string]interface{}){
	o.Symbol=order["symbol"].(string)
	o.Type=order["type"].(string)
	o.Price=float64ToInt(order["price"].(float64))
	o.Volume=float64ToInt(order["volume"].(float64))
	o.Direction=order["direction"].(string)
	o.UserID=float64ToInt(order["userID"].(float64))
}

// 行情结构体转Map类型
func (q *Quote)quoteStructToMap() map[string]interface{} {
	var tempMap map[string]interface{}
	tempMap=make(map[string]interface{})
	tempMap["symbol"]=q.Symbol
	tempMap["time"]=q.Time
	tempMap["ask10"]=q.Ask10
	tempMap["ask9"]=q.Ask9
	tempMap["ask8"]=q.Ask8
	tempMap["ask7"]=q.Ask7
	tempMap["ask6"]=q.Ask6
	tempMap["ask5"]=q.Ask5
	tempMap["ask4"]=q.Ask4
	tempMap["ask3"]=q.Ask3
	tempMap["ask2"]=q.Ask2
	tempMap["ask1"]=q.Ask1
	tempMap["bid1"]=q.Bid1
	tempMap["bid2"]=q.Bid2
	tempMap["bid3"]=q.Bid3
	tempMap["bid4"]=q.Bid4
	tempMap["bid5"]=q.Bid5
	tempMap["bid6"]=q.Bid6
	tempMap["bid7"]=q.Bid7
	tempMap["bid8"]=q.Bid8
	tempMap["bid9"]=q.Bid9
	tempMap["bid10"]=q.Bid10
	return tempMap
}

// 交易所路由,收到的消息推到接收模块，接收行情模块的数据推送
func route(w http.ResponseWriter,r *http.Request){
	quoteStruct:=new(Quote)         // 行情结构体
	quoteStruct.initQuoteMap()                // 一次性实例化Quote结构体里面的map类型
	quoteStruct.control.readTheLocalQuote=false   // 第一次运行，初始化从本地读取行情快照
	orderStruct:=new(UserOrder)          // 订单结构体
	orderChan:=make(chan UserOrder)
	quoteChan=make(chan map[string]interface{})
	c,_:=upgrader.Upgrade(w,r,nil)

	defer c.Close()
	for {  // 收到send后的处理
		mt,message,_:=c.ReadMessage()   // message是byte类型
		checkTheWebsocketRecv(message)  // 测试检查用
		// 接收用户的发单
		jsonStr:=uint8ToStr(message)
		fmt.Println("从客户端收到的信息",jsonStr)
		if jsonStr==""{
			fmt.Println("客户端收到的信息为空")
		}else{
			mapData:=jsonToMap(jsonStr)
			fmt.Println("mapData",mapData)
			orderStruct.mapToStruct(mapData)
			goSync.Add(1)
			go quoteStruct.recvClientMsg(orderChan,quoteChan,&goSync)
			orderChan<-*orderStruct
			//go sendQuote(quoteChan,&goSync)     // 暂时没用
			finalQuote:=mapToByte(<-quoteChan)
			c.WriteMessage(mt,append(finalQuote))
		}
		c.WriteMessage(mt,append(message[:]))
		
	}
}

// 获取交易所数据保存的文件路径
func getDataFilePath() string {
	var dataFilePath string

	defer func(){
		if p:=recover();p!=nil{
			fmt.Println("panic错误：",p)
		}
	}()
	// 获取交易所运行路径
	dir,err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil{
		log.Fatal("获取exchange运行目录报错--getDataFilePath")
	}
	fmt.Println("获取到的文件路径：",dir)
	dirList:=strings.Split(dir,"\\")
	dataFilePathList:=dirList[:len(dirList)-1]
	for _,fileName:=range dataFilePathList{
		// 注意这种方法裁剪后的路径第一个字符是 ‘\’,所以要把第一个字符去掉再返回
		dataFilePath+="\\"+fileName
	}
	// 切割文件路径
	return dataFilePath[1:]+"\\data\\"
}

// 读取投资者信息,返回所有投资者的账号Id,持仓，现金，冻结信息
func readUserJson() interface{} {
	fmt.Println("---------------------------------------")
	userDataFile:=getDataFilePath()+"exchangeUserJson.json"
	fmt.Println("获取到交易所本地保存的地址：",userDataFile)
	userData:=readJson(userDataFile)
	return userData
}

// 合成大周期K线
func makeKLine(order map[string]interface{}) interface{} {
	switch order["frequency"] {
	case "1min":
		fmt.Println("合成1分钟K线")
	case "5min":
		fmt.Println("合成5分钟K线")
	case "15min":
		fmt.Println("合成15分钟K线")
	case "30min":
		fmt.Println("合成30分钟K线")
	case "1hour":
		fmt.Println("合成1小时K线")
	case "4hour":
		fmt.Println("合成4小时K线")
	case "1day":
		fmt.Println("合成日线")
	default:
		log.Fatal("没有对应的K线",order["frequency"])
	}
	return 0
}

// 遍历某一个方向的深度，二分法遍历，外加成交撮合判断计算
func (q *Quote) loopTheDepth(order UserOrder,quoteChan chan map[string]interface{}){
	// 首先判断下单方向
	// 再判断下单方向是否在对手方向，在对手方向则撮合，不再对手方向则合并到同方向
	// 因为不知道怎么遍历指定map的键值，所以用二分法来做
	if order.Direction=="bid"{
		// bid档位
		for i:=1;i<=10;i++{
			if order.Price>q.Bid1["price"].(int){
				fmt.Println("买入价格大于买一档的情况")
				if order.Price<q.Ask1["price"].(int){
					// 买单价格大于买一档且小于卖一档，那么这个买单就会变为买一档
					q.depthPosChange("bid",1)
					q.Bid1["volume"],q.Bid1["price"]=order.Volume,order.Price
					fmt.Println("---------检查行情数据是否有改变--------",q.Ask1["volume"],q.Ask1["price"])
				}else if order.Price >= q.Ask1["price"].(int){
					// 买单价格大于买一档且大于卖一档，那么就会进行撮合，,如果吃完第一档后，买单仍有剩余
					// 则判断买单价格是否超过买二档，超过则继续吃量，吃不完继续往上吃，直到吃完所有量或者
					// 小于卖单价格，然后剩余的单成为买一档
					q.crossTrade("bid",order.Price,order.Volume)
					fmt.Println("---------11111111--------",q.Bid1["volume"],q.Bid1["price"])
				}
			}else if order.Price<=q.Bid1["price"].(int) && order.Price>q.Bid5["price"].(int){
				fmt.Println("买入价格在买一与买五之间")
				if order.Price==q.Bid1["price"]{
					// 买单等于买一价
					temp:=order.Volume+q.Bid1["volume"].(int)
					q.Bid1["volume"]=temp
				}else if order.Price>q.Bid3["price"].(int){
					// 买单价格在买一买三之间
					if order.Price==q.Bid2["price"].(int){
						// 买单价格等于买二的情况
						temp:=order.Volume+q.Bid2["volume"].(int)
						q.Bid2["volume"]=temp
					}else if order.Price>q.Bid2["price"].(int){
						// 买单价格在买一和买二之间
					}else{
						// 买单价格在买二和买三之间
					}
				}else if order.Price<q.Bid3["price"].(int){
					// 买单价格在买三买五之间
					if order.Price==q.Bid4["price"].(int){
						// 买单价格等于买四的情况
						temp:=order.Volume+q.Bid4["volume"].(int)
						q.Bid4["volume"]=temp
					}else if order.Price>q.Bid4["price"].(int){
						// 买单价格在买三和买四之间
					}else{
						// 买单价格在买四和买五之间
					}
				}else{
					// 买单价格等于买三价
					temp:=order.Volume+q.Bid3["volume"].(int)
					q.Bid3["volume"]=temp
				}
			}else if order.Price<=q.Bid5["price"].(int) && order.Price>q.Bid10["price"].(int){
				fmt.Println("买入价格在买五和买10之间")
				if order.Price==q.Bid5["price"].(int){
					// 买单价格等于买五价
					temp:=order.Volume+q.Bid5["volume"].(int)
					q.Bid5["volume"]=temp
				}else if order.Price>q.Bid7["price"].(int){
					// 买单价格在买五和买七之间
					if order.Price==q.Bid6["price"].(int){
						// 买单价格等于买六的情况
						temp:=order.Volume+q.Bid6["volume"].(int)
						q.Bid6["volume"]=temp
					}else if order.Price >q.Bid6["price"].(int){
						// 买单价格在买五和买六之间
					}else{
						// 买单价格在买六和买七之间
					}
				}else if order.Price < q.Bid7["price"].(int){
					// 买单价格在买七和买10之间
					if order.Price==q.Bid8["price"].(int){
						// 买单价格等于买八的情况
						temp:=order.Volume+q.Bid8["volume"].(int)
						q.Bid8["volume"]=temp
					}else if order.Price>q.Bid8["price"].(int){
						// 买单价格在买七和买八之间
					}else{
						// 买单价格在买八和买九之间
					}
					// 买单价格等于买九的情况
					if order.Price==q.Bid9["price"].(int){
						temp:=order.Volume+q.Bid9["volume"].(int)
						q.Bid9["volume"]=temp
					}else{
						// 买单价格在买九与买十之间，把原第十档合到该价格
					}
				}else{
					// 买单价格等于买七价格
					temp:=order.Volume+q.Bid7["volume"].(int)
					q.Bid7["volume"]=temp
				}
			}else{
				fmt.Println("买入价格低于10档，归结到第十档内")
				temp:=order.Volume+q.Bid10["volume"].(int)
				q.Bid10["volume"]=temp
			}
			// 返回已更改的行情
			quoteChan<-q.quoteStructToMap()
		}
	}else if order.Direction=="ask"{
		// ask档位
		for j:=1;j<=10;j++{
			if order.Price<q.Ask1["price"].(int){
				fmt.Println("卖出价格小于卖一档的情况")
				if order.Price>q.Bid1["price"].(int){
					// 卖单价格小于卖一档且大于买一档，那么这个卖单就会变为卖一档
				}else if order.Price <= q.Bid1["price"].(int){
					// 卖单价格小于卖一档且小于买一档，那么就会进行撮合 ,如果吃完第一档后，卖单仍有剩余
					// 则判断卖单价格是否超过买二档，超过则继续吃量，吃不完继续往下吃，直到吃完所有量或者
					// 大于买单价格，然后剩余的单成为卖一档
				}
			}else if order.Price>=q.Ask1["price"].(int) && order.Price<q.Ask5["price"].(int){
				fmt.Println("卖出价格在卖一与卖五之间")
				if order.Price==q.Ask1["price"]{
					// 卖单价格等于卖一价
					temp:=order.Volume+q.Ask1["volume"].(int)
					q.Ask1["volume"]=temp
				}else if order.Price<q.Ask3["price"].(int){
					// 卖单价格在卖一卖三之间
					if order.Price==q.Ask2["price"].(int){
						// 卖单价格等于卖二的情况
						temp:=order.Volume+q.Ask2["volume"].(int)
						q.Ask2["volume"]=temp
					}else if order.Price<q.Ask2["price"].(int){
						// 卖单价格在卖一和卖二之间
					}else{
						// 卖单价格在卖二和卖三之间
					}
				}else if order.Price>q.Ask3["price"].(int){
					// 卖单价格在卖三卖五之间
					if order.Price==q.Ask4["price"].(int){
						// 卖单价格等于卖四的情况
						temp:=order.Volume+q.Ask4["volume"].(int)
						q.Ask4["volume"]=temp
					}else if order.Price<q.Ask4["price"].(int){
						// 卖单价格在卖三和卖四之间
					}else{
						// 卖单价格在卖四和卖五之间
					}
				}else{
					// 卖单价格等于卖三价
					temp:=order.Volume+q.Ask3["volume"].(int)
					q.Ask3["volume"]=temp
				}
			}else if order.Price>=q.Ask5["price"].(int) && order.Price<q.Ask10["price"].(int){
				fmt.Println("卖出价格在卖五和卖10之间")
				if order.Price==q.Ask5["price"].(int){
					// 买单价格等于买五价
					temp:=order.Volume+q.Ask5["volume"].(int)
					q.Ask5["volume"]=temp
				}else if order.Price<q.Ask7["price"].(int){
					// 卖单价格在卖五和卖七之间
					if order.Price==q.Ask6["price"].(int){
						// 卖单价格等于卖六的情况
						temp:=order.Volume+q.Ask6["volume"].(int)
						q.Ask6["volume"]=temp
					}else if order.Price<q.Ask6["price"].(int){
						// 卖单价格在卖五和卖六之间
					}else{
						// 卖单价格在卖六和卖七之间
					}
				}else if order.Price>q.Ask7["price"].(int){
					// 卖单价格在卖七和卖10之间
					if order.Price==q.Ask8["price"].(int){
						// 卖单价格等于卖八的情况
						temp:=order.Volume+q.Ask8["volume"].(int)
						q.Ask8["volume"]=temp
					}else if order.Price<q.Ask8["price"].(int){
						// 卖单价格在卖七和卖八之间
					}else{
						// 卖单价格在卖八和卖九之间
					}
					if order.Price==q.Ask9["price"].(int){
						// 卖单价格等于九档价格的情况
						temp:=order.Volume+q.Ask9["volume"].(int)
						q.Ask9["volume"]=temp
					}else{
						// 卖单价格在九档和十档之间
					}
				}else{
					// 卖单价格等于卖7价格
					temp:=order.Volume+q.Ask7["volume"].(int)
					q.Ask7["volume"]=temp
				}
			}else{
				fmt.Println("卖出价格高于10档，归结到第十档内")
				temp:=order.Volume+q.Ask10["volume"].(int)
				q.Ask10["volume"]=temp
			}
			// 返回已经更新的行情快照
			quoteChan<-q.quoteStructToMap()
		}
	}else if order.Direction=="cancel"{
		// 撤单的情况，并返回更新后的行情
		quoteChan<-q.quoteStructToMap()
	}
}

// 某个方向，某个位置往后的档位全部上升一位，最后一档行情合并
func (q *Quote) depthPosChange(direction string ,position int){
	// 档位往后推移的时候，先从最后一档开始，如果前方档位被吃，那么后面的档位就往前排
	if direction=="bid"{
		// 买方情况
		if position==1{
			// 成为多方的第一档行情，第十档合并到第九档，然后原来的第九档成为第十档
			q.Bid10["volume"]=q.Bid10["volume"].(int) + q.Bid9["volume"].(int)
			q.Bid10["price"]=q.Bid9["price"].(int)
			q.Bid9["price"],q.Bid9["volume"]=0,0      // 初始化原来的第九档
			q.Bid9["volume"],q.Bid9["price"]=q.Bid8["volume"].(int),q.Bid8["price"].(int)
			q.Bid8["volume"],q.Bid8["price"]=0,0
			q.Bid8["volume"],q.Bid8["price"]=q.Bid7["volume"].(int),q.Bid7["price"].(int)
			q.Bid7["volume"],q.Bid7["price"]=0,0
			q.Bid7["volume"],q.Bid7["price"]=q.Bid6["volume"].(int),q.Bid6["price"].(int)
			q.Bid6["volume"],q.Bid6["price"]=0,0
			q.Bid6["volume"],q.Bid6["price"]=q.Bid5["volume"].(int),q.Bid5["price"].(int)
			q.Bid5["volume"],q.Bid5["price"]=0,0
			q.Bid5["volume"],q.Bid5["price"]=q.Bid4["volume"].(int),q.Bid4["price"].(int)
			q.Bid4["volume"],q.Bid4["price"]=0,0
			q.Bid4["volume"],q.Bid4["price"]=q.Bid3["volume"].(int),q.Bid3["price"].(int)
			q.Bid3["volume"],q.Bid3["price"]=0,0
			q.Bid3["volume"],q.Bid3["price"]=q.Bid2["volume"].(int),q.Bid2["price"].(int)
			q.Bid2["volume"],q.Bid2["price"]=0,0
			q.Bid2["volume"],q.Bid2["price"]=q.Bid1["volume"].(int),q.Bid1["price"].(int)
			q.Bid1["volume"],q.Bid1["price"]=0,0
		}else if position==2{
			// 新单成为多方的第二档行情
			q.Bid10["volume"]=q.Bid10["volume"].(int) + q.Bid9["volume"].(int)
			q.Bid10["price"]=q.Bid9["price"].(int)
			q.Bid9["price"],q.Bid9["volume"]=0,0      // 初始化原来的第九档
			q.Bid9["volume"],q.Bid9["price"]=q.Bid8["volume"].(int),q.Bid8["price"].(int)
			q.Bid8["volume"],q.Bid8["price"]=0,0
			q.Bid8["volume"],q.Bid8["price"]=q.Bid7["volume"].(int),q.Bid7["price"].(int)
			q.Bid7["volume"],q.Bid7["price"]=0,0
			q.Bid7["volume"],q.Bid7["price"]=q.Bid6["volume"].(int),q.Bid6["price"].(int)
			q.Bid6["volume"],q.Bid6["price"]=0,0
			q.Bid6["volume"],q.Bid6["price"]=q.Bid5["volume"].(int),q.Bid5["price"].(int)
			q.Bid5["volume"],q.Bid5["price"]=0,0
			q.Bid5["volume"],q.Bid5["price"]=q.Bid4["volume"].(int),q.Bid4["price"].(int)
			q.Bid4["volume"],q.Bid4["price"]=0,0
			q.Bid4["volume"],q.Bid4["price"]=q.Bid3["volume"].(int),q.Bid3["price"].(int)
			q.Bid3["volume"],q.Bid3["price"]=0,0
			q.Bid3["volume"],q.Bid3["price"]=q.Bid2["volume"].(int),q.Bid2["price"].(int)
			q.Bid2["volume"],q.Bid2["price"]=0,0
		}else if position==3{
			q.Bid10["volume"]=q.Bid10["volume"].(int) + q.Bid9["volume"].(int)
			q.Bid10["price"]=q.Bid9["price"].(int)
			q.Bid9["price"],q.Bid9["volume"]=0,0      // 初始化原来的第九档
			q.Bid9["volume"],q.Bid9["price"]=q.Bid8["volume"].(int),q.Bid8["price"].(int)
			q.Bid8["volume"],q.Bid8["price"]=0,0
			q.Bid8["volume"],q.Bid8["price"]=q.Bid7["volume"].(int),q.Bid7["price"].(int)
			q.Bid7["volume"],q.Bid7["price"]=0,0
			q.Bid7["volume"],q.Bid7["price"]=q.Bid6["volume"].(int),q.Bid6["price"].(int)
			q.Bid6["volume"],q.Bid6["price"]=0,0
			q.Bid6["volume"],q.Bid6["price"]=q.Bid5["volume"].(int),q.Bid5["price"].(int)
			q.Bid5["volume"],q.Bid5["price"]=0,0
			q.Bid5["volume"],q.Bid5["price"]=q.Bid4["volume"].(int),q.Bid4["price"].(int)
			q.Bid4["volume"],q.Bid4["price"]=0,0
			q.Bid4["volume"],q.Bid4["price"]=q.Bid3["volume"].(int),q.Bid3["price"].(int)
			q.Bid3["volume"],q.Bid3["price"]=0,0
		}else if position==4{
			q.Bid10["volume"]=q.Bid10["volume"].(int) + q.Bid9["volume"].(int)
			q.Bid10["price"]=q.Bid9["price"].(int)
			q.Bid9["price"],q.Bid9["volume"]=0,0      // 初始化原来的第九档
			q.Bid9["volume"],q.Bid9["price"]=q.Bid8["volume"].(int),q.Bid8["price"].(int)
			q.Bid8["volume"],q.Bid8["price"]=0,0
			q.Bid8["volume"],q.Bid8["price"]=q.Bid7["volume"].(int),q.Bid7["price"].(int)
			q.Bid7["volume"],q.Bid7["price"]=0,0
			q.Bid7["volume"],q.Bid7["price"]=q.Bid6["volume"].(int),q.Bid6["price"].(int)
			q.Bid6["volume"],q.Bid6["price"]=0,0
			q.Bid6["volume"],q.Bid6["price"]=q.Bid5["volume"].(int),q.Bid5["price"].(int)
			q.Bid5["volume"],q.Bid5["price"]=0,0
			q.Bid5["volume"],q.Bid5["price"]=q.Bid4["volume"].(int),q.Bid4["price"].(int)
			q.Bid4["volume"],q.Bid4["price"]=0,0
		}else if position==5{
			q.Bid10["volume"]=q.Bid10["volume"].(int) + q.Bid9["volume"].(int)
			q.Bid10["price"]=q.Bid9["price"].(int)
			q.Bid9["price"],q.Bid9["volume"]=0,0      // 初始化原来的第九档
			q.Bid9["volume"],q.Bid9["price"]=q.Bid8["volume"].(int),q.Bid8["price"].(int)
			q.Bid8["volume"],q.Bid8["price"]=0,0
			q.Bid8["volume"],q.Bid8["price"]=q.Bid7["volume"].(int),q.Bid7["price"].(int)
			q.Bid7["volume"],q.Bid7["price"]=0,0
			q.Bid7["volume"],q.Bid7["price"]=q.Bid6["volume"].(int),q.Bid6["price"].(int)
			q.Bid6["volume"],q.Bid6["price"]=0,0
			q.Bid6["volume"],q.Bid6["price"]=q.Bid5["volume"].(int),q.Bid5["price"].(int)
			q.Bid5["volume"],q.Bid5["price"]=0,0
		}else if position==6{
			q.Bid10["volume"]=q.Bid10["volume"].(int) + q.Bid9["volume"].(int)
			q.Bid10["price"]=q.Bid9["price"].(int)
			q.Bid9["price"],q.Bid9["volume"]=0,0      // 初始化原来的第九档
			q.Bid9["volume"],q.Bid9["price"]=q.Bid8["volume"].(int),q.Bid8["price"].(int)
			q.Bid8["volume"],q.Bid8["price"]=0,0
			q.Bid8["volume"],q.Bid8["price"]=q.Bid7["volume"].(int),q.Bid7["price"].(int)
			q.Bid7["volume"],q.Bid7["price"]=0,0
			q.Bid7["volume"],q.Bid7["price"]=q.Bid6["volume"].(int),q.Bid6["price"].(int)
			q.Bid6["volume"],q.Bid6["price"]=0,0
		}else if position==7{
			q.Bid10["volume"]=q.Bid10["volume"].(int) + q.Bid9["volume"].(int)
			q.Bid10["price"]=q.Bid9["price"].(int)
			q.Bid9["price"],q.Bid9["volume"]=0,0      // 初始化原来的第九档
			q.Bid9["volume"],q.Bid9["price"]=q.Bid8["volume"].(int),q.Bid8["price"].(int)
			q.Bid8["volume"],q.Bid8["price"]=0,0
			q.Bid8["volume"],q.Bid8["price"]=q.Bid7["volume"].(int),q.Bid7["price"].(int)
			q.Bid7["volume"],q.Bid7["price"]=0,0
		}else if position==8{
			q.Bid10["volume"]=q.Bid10["volume"].(int) + q.Bid9["volume"].(int)
			q.Bid10["price"]=q.Bid9["price"].(int)
			q.Bid9["price"],q.Bid9["volume"]=0,0      // 初始化原来的第九档
			q.Bid9["volume"],q.Bid9["price"]=q.Bid8["volume"].(int),q.Bid8["price"].(int)
			q.Bid8["volume"],q.Bid8["price"]=0,0
		}else if position==9{
			q.Bid10["volume"]=q.Bid10["volume"].(int) + q.Bid9["volume"].(int)
			q.Bid10["price"]=q.Bid9["price"].(int)
			q.Bid9["price"],q.Bid9["volume"]=0,0      // 初始化原来的第九档
		}
	}else{
		// 卖方情况
		if position==1{
			q.Ask10["volume"]=q.Ask10["volume"].(int) + q.Ask9["volume"].(int)
			q.Ask10["price"]=q.Ask9["price"].(int)
			q.Ask9["price"],q.Ask9["volume"]=0,0      // 初始化原来的第九档
			q.Ask9["volume"],q.Ask9["price"]=q.Ask8["volume"].(int),q.Ask8["price"].(int)
			q.Ask8["volume"],q.Ask8["price"]=0,0
			q.Ask8["volume"],q.Ask8["price"]=q.Ask7["volume"].(int),q.Ask7["price"].(int)
			q.Ask7["volume"],q.Ask7["price"]=0,0
			q.Ask7["volume"],q.Ask7["price"]=q.Ask6["volume"].(int),q.Ask6["price"].(int)
			q.Ask6["volume"],q.Ask6["price"]=0,0
			q.Ask6["volume"],q.Ask6["price"]=q.Ask5["volume"].(int),q.Ask5["price"].(int)
			q.Ask5["volume"],q.Ask5["price"]=0,0
			q.Ask5["volume"],q.Ask5["price"]=q.Ask4["volume"].(int),q.Ask4["price"].(int)
			q.Ask4["volume"],q.Ask4["price"]=0,0
			q.Ask4["volume"],q.Ask4["price"]=q.Ask3["volume"].(int),q.Ask3["price"].(int)
			q.Ask3["volume"],q.Ask3["price"]=0,0
			q.Ask3["volume"],q.Ask3["price"]=q.Ask2["volume"].(int),q.Ask2["price"].(int)
			q.Ask2["volume"],q.Ask2["price"]=0,0
			q.Ask2["volume"],q.Ask2["price"]=q.Ask1["volume"].(int),q.Ask1["price"].(int)
			q.Ask1["volume"],q.Ask1["price"]=0,0
		}else if position==2{
			q.Ask10["volume"]=q.Ask10["volume"].(int) + q.Ask9["volume"].(int)
			q.Ask10["price"]=q.Ask9["price"].(int)
			q.Ask9["price"],q.Ask9["volume"]=0,0      // 初始化原来的第九档
			q.Ask9["volume"],q.Ask9["price"]=q.Ask8["volume"].(int),q.Ask8["price"].(int)
			q.Ask8["volume"],q.Ask8["price"]=0,0
			q.Ask8["volume"],q.Ask8["price"]=q.Ask7["volume"].(int),q.Ask7["price"].(int)
			q.Ask7["volume"],q.Ask7["price"]=0,0
			q.Ask7["volume"],q.Ask7["price"]=q.Ask6["volume"].(int),q.Ask6["price"].(int)
			q.Ask6["volume"],q.Ask6["price"]=0,0
			q.Ask6["volume"],q.Ask6["price"]=q.Ask5["volume"].(int),q.Ask5["price"].(int)
			q.Ask5["volume"],q.Ask5["price"]=0,0
			q.Ask5["volume"],q.Ask5["price"]=q.Ask4["volume"].(int),q.Ask4["price"].(int)
			q.Ask4["volume"],q.Ask4["price"]=0,0
			q.Ask4["volume"],q.Ask4["price"]=q.Ask3["volume"].(int),q.Ask3["price"].(int)
			q.Ask3["volume"],q.Ask3["price"]=0,0
			q.Ask3["volume"],q.Ask3["price"]=q.Ask2["volume"].(int),q.Ask2["price"].(int)
			q.Ask2["volume"],q.Ask2["price"]=0,0
		}else if position==3{
			q.Ask10["volume"]=q.Ask10["volume"].(int) + q.Ask9["volume"].(int)
			q.Ask10["price"]=q.Ask9["price"].(int)
			q.Ask9["price"],q.Ask9["volume"]=0,0      // 初始化原来的第九档
			q.Ask9["volume"],q.Ask9["price"]=q.Ask8["volume"].(int),q.Ask8["price"].(int)
			q.Ask8["volume"],q.Ask8["price"]=0,0
			q.Ask8["volume"],q.Ask8["price"]=q.Ask7["volume"].(int),q.Ask7["price"].(int)
			q.Ask7["volume"],q.Ask7["price"]=0,0
			q.Ask7["volume"],q.Ask7["price"]=q.Ask6["volume"].(int),q.Ask6["price"].(int)
			q.Ask6["volume"],q.Ask6["price"]=0,0
			q.Ask6["volume"],q.Ask6["price"]=q.Ask5["volume"].(int),q.Ask5["price"].(int)
			q.Ask5["volume"],q.Ask5["price"]=0,0
			q.Ask5["volume"],q.Ask5["price"]=q.Ask4["volume"].(int),q.Ask4["price"].(int)
			q.Ask4["volume"],q.Ask4["price"]=0,0
			q.Ask4["volume"],q.Ask4["price"]=q.Ask3["volume"].(int),q.Ask3["price"].(int)
			q.Ask3["volume"],q.Ask3["price"]=0,0
		}else if position==4{
			q.Ask10["volume"]=q.Ask10["volume"].(int) + q.Ask9["volume"].(int)
			q.Ask10["price"]=q.Ask9["price"].(int)
			q.Ask9["price"],q.Ask9["volume"]=0,0      // 初始化原来的第九档
			q.Ask9["volume"],q.Ask9["price"]=q.Ask8["volume"].(int),q.Ask8["price"].(int)
			q.Ask8["volume"],q.Ask8["price"]=0,0
			q.Ask8["volume"],q.Ask8["price"]=q.Ask7["volume"].(int),q.Ask7["price"].(int)
			q.Ask7["volume"],q.Ask7["price"]=0,0
			q.Ask7["volume"],q.Ask7["price"]=q.Ask6["volume"].(int),q.Ask6["price"].(int)
			q.Ask6["volume"],q.Ask6["price"]=0,0
			q.Ask6["volume"],q.Ask6["price"]=q.Ask5["volume"].(int),q.Ask5["price"].(int)
			q.Ask5["volume"],q.Ask5["price"]=0,0
			q.Ask5["volume"],q.Ask5["price"]=q.Ask4["volume"].(int),q.Ask4["price"].(int)
			q.Ask4["volume"],q.Ask4["price"]=0,0
		}else if position==5{
			q.Ask10["volume"]=q.Ask10["volume"].(int) + q.Ask9["volume"].(int)
			q.Ask10["price"]=q.Ask9["price"].(int)
			q.Ask9["price"],q.Ask9["volume"]=0,0      // 初始化原来的第九档
			q.Ask9["volume"],q.Ask9["price"]=q.Ask8["volume"].(int),q.Ask8["price"].(int)
			q.Ask8["volume"],q.Ask8["price"]=0,0
			q.Ask8["volume"],q.Ask8["price"]=q.Ask7["volume"].(int),q.Ask7["price"].(int)
			q.Ask7["volume"],q.Ask7["price"]=0,0
			q.Ask7["volume"],q.Ask7["price"]=q.Ask6["volume"].(int),q.Ask6["price"].(int)
			q.Ask6["volume"],q.Ask6["price"]=0,0
			q.Ask6["volume"],q.Ask6["price"]=q.Ask5["volume"].(int),q.Ask5["price"].(int)
			q.Ask5["volume"],q.Ask5["price"]=0,0
		}else if position==6{
			q.Ask10["volume"]=q.Ask10["volume"].(int) + q.Ask9["volume"].(int)
			q.Ask10["price"]=q.Ask9["price"].(int)
			q.Ask9["price"],q.Ask9["volume"]=0,0      // 初始化原来的第九档
			q.Ask9["volume"],q.Ask9["price"]=q.Ask8["volume"].(int),q.Ask8["price"].(int)
			q.Ask8["volume"],q.Ask8["price"]=0,0
			q.Ask8["volume"],q.Ask8["price"]=q.Ask7["volume"].(int),q.Ask7["price"].(int)
			q.Ask7["volume"],q.Ask7["price"]=0,0
			q.Ask7["volume"],q.Ask7["price"]=q.Ask6["volume"].(int),q.Ask6["price"].(int)
			q.Ask6["volume"],q.Ask6["price"]=0,0
		}else if position==7{
			q.Ask10["volume"]=q.Ask10["volume"].(int) + q.Ask9["volume"].(int)
			q.Ask10["price"]=q.Ask9["price"].(int)
			q.Ask9["price"],q.Ask9["volume"]=0,0      // 初始化原来的第九档
			q.Ask9["volume"],q.Ask9["price"]=q.Ask8["volume"].(int),q.Ask8["price"].(int)
			q.Ask8["volume"],q.Ask8["price"]=0,0
			q.Ask8["volume"],q.Ask8["price"]=q.Ask7["volume"].(int),q.Ask7["price"].(int)
			q.Ask7["volume"],q.Ask7["price"]=0,0
		}else if position==8{
			q.Ask10["volume"]=q.Ask10["volume"].(int) + q.Ask9["volume"].(int)
			q.Ask10["price"]=q.Ask9["price"].(int)
			q.Ask9["price"],q.Ask9["volume"]=0,0      // 初始化原来的第九档
			q.Ask9["volume"],q.Ask9["price"]=q.Ask8["volume"].(int),q.Ask8["price"].(int)
			q.Ask8["volume"],q.Ask8["price"]=0,0
		}else if position==9{
			q.Ask10["volume"]=q.Ask10["volume"].(int) + q.Ask9["volume"].(int)
			q.Ask10["price"]=q.Ask9["price"].(int)
			q.Ask9["price"],q.Ask9["volume"]=0,0      // 初始化原来的第九档
		}
	}
}

// 成交撮合的时候，前面的档位给吃掉了，后面的档位往前推，后面的的档位用最后一个档位的价格
func (q *Quote) crossPosChange(direction string,position int){
	// 被吃的永远是第一档
	if direction=="bid"{
		// 买方情况
		if position==1{
			// 卖方一档被吃掉的情况
			q.Ask1["volume"],q.Ask1["price"]=q.Ask2["volume"].(int),q.Ask2["price"].(int)
			q.Ask2["volume"],q.Ask2["price"]=q.Ask3["volume"].(int),q.Ask3["price"].(int)
			q.Ask3["volume"],q.Ask3["price"]=q.Ask4["volume"].(int),q.Ask4["price"].(int)
			q.Ask4["volume"],q.Ask4["price"]=q.Ask5["volume"].(int),q.Ask5["price"].(int)
			q.Ask5["volume"],q.Ask5["price"]=q.Ask6["volume"].(int),q.Ask6["price"].(int)
			q.Ask6["volume"],q.Ask6["price"]=q.Ask7["volume"].(int),q.Ask7["price"].(int)
			q.Ask7["volume"],q.Ask7["price"]=q.Ask8["volume"].(int),q.Ask8["price"].(int)
			q.Ask8["volume"],q.Ask8["price"]=q.Ask9["volume"].(int),q.Ask9["price"].(int)
			q.Ask9["volume"],q.Ask9["price"]=q.Ask10["volume"].(int),q.Ask10["price"].(int)
			q.Ask10["volume"]=0
		}
	}else{
		// 卖方情况
		q.Bid1["volume"],q.Bid1["price"]=q.Bid2["volume"].(int),q.Bid2["price"].(int)
		q.Bid2["volume"],q.Bid2["price"]=q.Bid3["volume"].(int),q.Bid3["price"].(int)
		q.Bid3["volume"],q.Bid3["price"]=q.Bid4["volume"].(int),q.Bid4["price"].(int)
		q.Bid4["volume"],q.Bid4["price"]=q.Bid5["volume"].(int),q.Bid5["price"].(int)
		q.Bid5["volume"],q.Bid5["price"]=q.Bid6["volume"].(int),q.Bid6["price"].(int)
		q.Bid6["volume"],q.Bid6["price"]=q.Bid7["volume"].(int),q.Bid7["price"].(int)
		q.Bid7["volume"],q.Bid7["price"]=q.Bid8["volume"].(int),q.Bid8["price"].(int)
		q.Bid8["volume"],q.Bid8["price"]=q.Bid9["volume"].(int),q.Bid9["price"].(int)
		q.Bid9["volume"],q.Bid9["price"]=q.Bid10["volume"].(int),q.Bid10["price"].(int)
		q.Bid10["volume"]=0
	}
}

// 下单超过对手方向，撮合成交
func (q *Quote) crossTrade(direction string,price int,volume int){
	if direction=="bid"{
		// 买方吃空方的深度情况
		for volume!=0{
			if q.Ask1["volume"].(int)>volume{
				// 下单的量不足以吃掉对手一档的情况
				q.Ask1["volume"]=q.Ask1["volume"].(int)-volume
			}else{
				// 下单量完全吃掉对手一档的情况
				volume-=q.Ask1["volume"].(int)
				if volume==0{
					// 下单量刚好等于一档的量
					q.crossPosChange("bid",1)
				}else{
					// 这里不应该有判断，应该要在外层用循环，不断消耗掉买单直到一档成交量大于买单并
					// 并全部撮合完
					// 如果是第一次启动的情况，那么对手档位的量全为0，所以当吃完所有深度之后，这个买单
					// 就会成为买方的第一档
					q.Bid1["price"],q.Bid1["volume"]=price,volume
					fmt.Println("+++++++++++++++++++++",q.Bid1["price"],q.Bid1["volume"])
					break
				}
			}
		}
		fmt.Print("{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}")
	}else{
		// 空方往下吃买方的深度情况
		for volume!=0{
			if q.Bid1["volume"].(int)>volume{
				// 下单量不足以吃掉对手一档的情况
				q.Bid1["volume"]=q.Bid1["volume"].(int)-volume
			}else{
				// 下单量完全吃掉对手一档的情况
				volume-=q.Bid1["volume"].(int)
				if volume==0{
					// 下单量刚好等于一档的量
					q.crossPosChange("ask",1)
				}
			}
		}
		
	}
}

// 深度改变的时候，挂单量的改变实际上是对应着挂单量的堆栈改变
func (q *Quote) stackVolumeChange(direction string,position int,upOrEnd string){
	// 参数解析：direction是买卖方向，position是指第几档改变，upOrEnd是表示档位是向哪个方向移动up向档位深处移动
	// 每次深度改变都要改变用户挂单量堆栈先进先出
	// 从前方堆栈移动过来的在堆栈前面，从后面堆栈过来的用户挂单堆栈堆后面
	if direction=="bid"{
		// 买方深度改变的情况
		if position==1{
			// 买方一档的深度堆栈移动情况
			if upOrEnd=="up"{
				// 买方一档移动到原买方二档的位置
				for _,stackList:=range q.Bid1["stack"].([]map[string]int){
					q.Bid2["stack"]=append(q.Bid2["stack"].([]map[string]int),stackList)
				}
			}else{
				// 一档不能向下移动
				fmt.Println("买一档堆栈移动不能向下移动了")
			}
		}else if position==2{
			// 买方二档的深度堆栈移动情况
			if upOrEnd=="up"{
				// 买方二档移动到原买方三档的位置
				for _,stackList:=range q.Bid2["stack"].([]map[string]int){
					q.Bid3["stack"]=append(q.Bid3["stack"].([]map[string]int),stackList)
				}
			}else{
				// 买方二档移动到原买方一档的位置
				for _,stackList:=range q.Bid2["stack"].([]map[string]int){
					q.Bid1["stack"]=append(q.Bid1["stack"].([]map[string]int),stackList)
				}
			}
		}else if position==3{
			// 买方三档的深度堆栈移动情况
			if upOrEnd=="up"{
				// 买方三档移动到原买方四档的位置
				for _,stackList:=range q.Bid3["stack"].([]map[string]int){
					q.Bid4["stack"]=append(q.Bid4["stack"].([]map[string]int),stackList)
				}
			}else{
				// 买方三档移动到原买方二档的位置
				for _,stackList:=range q.Bid3["stack"].([]map[string]int){
					q.Bid2["stack"]=append(q.Bid2["stack"].([]map[string]int),stackList)
				}
			}
		}else if position==4{
			// 买方四档的深度堆栈移动情况
			if upOrEnd=="up"{
				// 买方四档移动到原买方五档的位置
				for _,stackList:=range q.Bid4["stack"].([]map[string]int){
					q.Bid4["stack"]=append(q.Bid4["stack"].([]map[string]int),stackList)
				}
			}else{
				// 买方四档移动到原买方三档的位置
				for _,stackList:=range q.Bid4["stack"].([]map[string]int){
					q.Bid3["stack"]=append(q.Bid3["stack"].([]map[string]int),stackList)
				}
			}
		}else if position==5{
			// 买方五档的深度堆栈移动情况
			if upOrEnd=="up"{
				// 买方五档移动到原买方六档的位置
				for _,stackList:=range q.Bid5["stack"].([]map[string]int){
					q.Bid6["stack"]=append(q.Bid6["stack"].([]map[string]int),stackList)
				}
			}else{
				// 买方五档移动到原来买方四档的位置
				for _,stackList:=range q.Bid5["stack"].([]map[string]int){
					q.Bid4["stack"]=append(q.Bid4["stack"].([]map[string]int),stackList)
				}
			}
		}else if position==6{
			// 买方六档的深度堆栈移动情况
			if upOrEnd=="up"{
				// 买方六档移动到原买方7档的位置上
				for _,stackList:=range q.Bid6["stack"].([]map[string]int){
					q.Bid7["stack"]=append(q.Bid7["stack"].([]map[string]int),stackList)
				}
			}else{
				// 买方六档移动到原买方5档的位置上
				for _,stackList:=range q.Bid6["stack"].([]map[string]int){
					q.Bid5["stack"]=append(q.Bid5["stack"].([]map[string]int),stackList)
				}
			}
		}else if position==7{
			// 买方七档的深度堆栈移动情况
			if upOrEnd=="up"{
				// 买方7档移动到原买方8档的位置上
				for _,stackList:=range q.Bid7["stack"].([]map[string]int){
					q.Bid8["stack"]=append(q.Bid8["stack"].([]map[string]int),stackList)
				}
			}else{
				// 买方7档移动到原买方6档的位置上
				for _,stackList:=range q.Bid7["stack"].([]map[string]int){
					q.Bid6["stack"]=append(q.Bid6["stack"].([]map[string]int),stackList)
				}
			}
		}else if position==8{
			// 买方八档的深度堆栈移动情况
			if upOrEnd=="up"{
				// 买方8档移动到原买方9档的位置上
				for _,stackList:=range q.Bid8["stack"].([]map[string]int){
					q.Bid9["stack"]=append(q.Bid9["stack"].([]map[string]int),stackList)
				}
			}else{
				// 买方8档移动到原买方7档的位置上
				for _,stackList:=range q.Bid8["stack"].([]map[string]int){
					q.Bid7["stack"]=append(q.Bid7["stack"].([]map[string]int),stackList)
				}
			}
		}else if position==9{
			// 买方九档的深度堆栈移动情况
			if upOrEnd=="up"{
				// 买方9档移动到原买方10档的位置上
				for _,stackList:=range q.Bid9["stack"].([]map[string]int){
					q.Bid10["stack"]=append(q.Bid10["stack"].([]map[string]int),stackList)
				}
			}else{
				// 买方9档移动到原买方8档的位置上
				for _,stackList:=range q.Bid9["stack"].([]map[string]int){
					q.Bid8["stack"]=append(q.Bid8["stack"].([]map[string]int),stackList)
				}
			}
		}else{
			// 最后就是10档深度堆栈移动情况
			if upOrEnd=="up"{
				// 买10档不能再向上移动了
				fmt.Println("买十堆栈不能往后移动了")
			}else{
				// 买方10档移动到原买方9档的位置上
				for _,stackList:=range q.Bid10["stack"].([]map[string]int){
					q.Bid9["stack"]=append(q.Bid9["stack"].([]map[string]int),stackList)
				}
			}
		}
	}else{
		// 卖方深度改变的情况
		if position==1{
			// 卖方一档的深度堆栈移动情况
			if upOrEnd=="up"{
				for _,stackList:=range q.Ask1["stack"].([]map[string]int){
					q.Ask2["stack"]=append(q.Ask2["stack"].([]map[string]int),stackList)
				}
			}else{
				fmt.Println("卖单不能再往前移动了")
			}
		}else if position==2{
			// 卖方二档的深度堆栈移动情况
			if upOrEnd=="up"{
				for _,stackList:=range q.Ask2["stack"].([]map[string]int){
					q.Ask3["stack"]=append(q.Ask3["stack"].([]map[string]int),stackList)
				}
				}else{
					for _,stackList:=range q.Ask2["stack"].([]map[string]int){
						q.Ask2["stack"]=append(q.Ask2["stack"].([]map[string]int),stackList)
					}
				}
		}else if position==3{
			// 卖方三档的深度堆栈移动情况
			if upOrEnd=="up"{
				for _,stackList:=range q.Ask3["stack"].([]map[string]int){
					q.Ask4["stack"]=append(q.Ask4["stack"].([]map[string]int),stackList)
				}
				}else{
					for _,stackList:=range q.Ask3["stack"].([]map[string]int){
						q.Ask2["stack"]=append(q.Ask2["stack"].([]map[string]int),stackList)
					}
				}
		}else if position==4{
			// 卖方四档的深度堆栈移动情况
			if upOrEnd=="up"{
				for _,stackList:=range q.Ask4["stack"].([]map[string]int){
					q.Ask5["stack"]=append(q.Ask5["stack"].([]map[string]int),stackList)
				}
				}else{
					for _,stackList:=range q.Ask4["stack"].([]map[string]int){
						q.Ask3["stack"]=append(q.Ask3["stack"].([]map[string]int),stackList)
					}
				}
		}else if position==5{
			// 卖方五档的深度堆栈移动情况
			if upOrEnd=="up"{
				for _,stackList:=range q.Ask5["stack"].([]map[string]int){
					q.Ask6["stack"]=append(q.Ask6["stack"].([]map[string]int),stackList)
				}
				}else{
					for _,stackList:=range q.Ask5["stack"].([]map[string]int){
						q.Ask4["stack"]=append(q.Ask4["stack"].([]map[string]int),stackList)
					}
				}
		}else if position==6{
			// 卖方六档的深度堆栈移动情况
			if upOrEnd=="up"{
				for _,stackList:=range q.Ask6["stack"].([]map[string]int){
					q.Ask7["stack"]=append(q.Ask7["stack"].([]map[string]int),stackList)
				}
				}else{
					for _,stackList:=range q.Ask6["stack"].([]map[string]int){
						q.Ask5["stack"]=append(q.Ask5["stack"].([]map[string]int),stackList)
					}
				}
		}else if position==7{
			// 卖方七档的深度堆栈移动情况
			if upOrEnd=="up"{
				for _,stackList:=range q.Ask7["stack"].([]map[string]int){
					q.Ask8["stack"]=append(q.Ask8["stack"].([]map[string]int),stackList)
				}
				}else{
					for _,stackList:=range q.Ask7["stack"].([]map[string]int){
						q.Ask6["stack"]=append(q.Ask6["stack"].([]map[string]int),stackList)
					}
				}
		}else if position==8{
			// 卖方8档的深度堆栈移动情况
			if upOrEnd=="up"{
				for _,stackList:=range q.Ask8["stack"].([]map[string]int){
					q.Ask9["stack"]=append(q.Ask9["stack"].([]map[string]int),stackList)
				}
				}else{
					for _,stackList:=range q.Ask8["stack"].([]map[string]int){
						q.Ask7["stack"]=append(q.Ask7["stack"].([]map[string]int),stackList)
					}
				}
		}else if position==9{
			// 卖方9档的深度堆栈移动情况
			if upOrEnd=="up"{
				for _,stackList:=range q.Ask9["stack"].([]map[string]int){
					q.Ask10["stack"]=append(q.Ask10["stack"].([]map[string]int),stackList)
				}
				}else{
					for _,stackList:=range q.Ask9["stack"].([]map[string]int){
						q.Ask8["stack"]=append(q.Ask8["stack"].([]map[string]int),stackList)
					}
				}
		}else{
			// 卖方10档的深度堆栈移动情况
			if upOrEnd=="up"{
					fmt.Println("卖单堆栈不能再往后移动了")
				}else{
					for _,stackList:=range q.Ask10["stack"].([]map[string]int){
						q.Ask9["stack"]=append(q.Ask9["stack"].([]map[string]int),stackList)
					}
				}
		}
	}
}

// 传进两个Int,不管int是什么值，最后都返回两个0
func initPriceAndVolume(price int,volume int )(int,int){
	return 0,0
}

// ---------------------------流程函数-----------------------------------
// 把实时行情的map类型一次性make一下
func (q *Quote) initQuoteMap(){
	q.Ask10 =make(map[string]interface{})
	q.Ask10["price"]=int(0)
	q.Ask10["volume"]=int(0)
	q.Ask9 =make(map[string]interface{})
	q.Ask9["price"]=int(0)
	q.Ask9["volume"]=int(0)
	q.Ask8 =make(map[string]interface{})
	q.Ask8["price"]=int(0)
	q.Ask8["volume"]=int(0)
	q.Ask7 =make(map[string]interface{})
	q.Ask7["price"]=int(0)
	q.Ask7["volume"]=int(0)
	q.Ask6 =make(map[string]interface{})
	q.Ask6["price"]=int(0)
	q.Ask6["volume"]=int(0)
	q.Ask5 =make(map[string]interface{})
	q.Ask5["price"]=int(0)
	q.Ask5["volume"]=int(0)
	q.Ask4=make(map[string]interface{})
	q.Ask4["price"]=int(0)
	q.Ask4["volume"]=int(0)
	q.Ask3 =make(map[string]interface{})
	q.Ask3["price"]=int(0)
	q.Ask3["volume"]=int(0)
	q.Ask2 =make(map[string]interface{})
	q.Ask2["price"]=int(0)
	q.Ask2["volume"]=int(0)
	q.Ask1 =make(map[string]interface{})
	q.Ask1["price"]=int(0)
	q.Ask1["volume"]=int(0)

	q.Bid1 =make(map[string]interface{})
	q.Bid1["price"]=int(0)
	q.Bid1["volume"]=int(0)
	q.Bid2 =make(map[string]interface{})
	q.Bid2["price"]=int(0)
	q.Bid2["volume"]=int(0)
	q.Bid3 =make(map[string]interface{})
	q.Bid3["price"]=int(0)
	q.Bid3["volume"]=int(0)
	q.Bid4 =make(map[string]interface{})
	q.Bid4["price"]=int(0)
	q.Bid4["volume"]=int(0)
	q.Bid5 =make(map[string]interface{})
	q.Bid5["price"]=int(0)
	q.Bid5["volume"]=int(0)
	q.Bid6 =make(map[string]interface{})
	q.Bid6["price"]=int(0)
	q.Bid6["volume"]=int(0)
	q.Bid7 =make(map[string]interface{})
	q.Bid7["price"]=int(0)
	q.Bid7["volume"]=int(0)
	q.Bid8 =make(map[string]interface{})
	q.Bid8["price"]=int(0)
	q.Bid8["volume"]=int(0)
	q.Bid9=make(map[string]interface{})
	q.Bid9["price"]=int(0)
	q.Bid9["volume"]=int(0)
	q.Bid10 =make(map[string]interface{})
	q.Bid10["price"]=int(0)
	q.Bid10["volume"]=int(0)
	fmt.Print("检查行情map的初始化情况",q)
	// 初始化行情的堆栈结构，更新用户id时候，还要初始化stack的map[string]int
	q.Ask10["stack"]=make([]map[string]int,100)
	q.Ask9["stack"]=make([]map[string]int,100)
	q.Ask8["stack"]=make([]map[string]int,100)
	q.Ask7["stack"]=make([]map[string]int,100)
	q.Ask6["stack"]=make([]map[string]int,100)
	q.Ask5["stack"]=make([]map[string]int,100)
	q.Ask4["stack"]=make([]map[string]int,100)
	q.Ask3["stack"]=make([]map[string]int,100)
	q.Ask2["stack"]=make([]map[string]int,100)
	q.Ask1["stack"]=make([]map[string]int,100)

	q.Bid1["stack"]=make([]map[string]int,100)
	q.Bid2["stack"]=make([]map[string]int,100)
	q.Bid3["stack"]=make([]map[string]int,100)
	q.Bid4["stack"]=make([]map[string]int,100)
	q.Bid5["stack"]=make([]map[string]int,100)
	q.Bid6["stack"]=make([]map[string]int,100)
	q.Bid7["stack"]=make([]map[string]int,100)
	q.Bid8["stack"]=make([]map[string]int,100)
	q.Bid9["stack"]=make([]map[string]int,100)
	q.Bid10["stack"]=make([]map[string]int,100)
}

// 读取本地用户json文件，获取用户Id,持有现金，冻结资产，持仓
func getUserJson(fileName string) []map[string]interface{}{
	allPath:=getDataFilePath()+fileName
	userJsonData:=readJson(allPath)  // typeof:[]map
	return userJsonData
}

// 读取硬盘上上一次的行情快照
func (q *Quote) getHistoryQuoteSnapShot(){
	allPath:=getDataFilePath()+"lastQuote.json"
	snapShotJson:=readJson(allPath)[0]  // typeof: []map
	q.Time=snapShotJson["time"]
	q.Symbol=snapShotJson["symbol"]
	//var tempMap map[string]map[string]interface{}
	tempMap:=make(map[string]interface{})
	
	// {"price":int,"volume":int,"symbol":string}
	tempMap=snapShotJson["ask10"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["ask10"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["ask10"].(map[string]interface{})["volume"].(float64))
	q.Ask10=tempMap
	tempMap=snapShotJson["ask9"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["ask9"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["ask9"].(map[string]interface{})["volume"].(float64))
	q.Ask9=tempMap
	tempMap=snapShotJson["ask8"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["ask8"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["ask8"].(map[string]interface{})["volume"].(float64))
	q.Ask8=tempMap
	tempMap=snapShotJson["ask7"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["ask7"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["ask7"].(map[string]interface{})["volume"].(float64))
	q.Ask7=tempMap
	tempMap=snapShotJson["ask6"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["ask6"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["ask6"].(map[string]interface{})["volume"].(float64))
	q.Ask6=tempMap
	tempMap=snapShotJson["ask5"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["ask5"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["ask5"].(map[string]interface{})["volume"].(float64))
	q.Ask5=tempMap
	tempMap=snapShotJson["ask4"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["ask4"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["ask4"].(map[string]interface{})["volume"].(float64))
	q.Ask4=tempMap
	tempMap=snapShotJson["ask3"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["ask3"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["ask3"].(map[string]interface{})["volume"].(float64))
	q.Ask3=tempMap
	tempMap=snapShotJson["ask2"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["ask2"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["ask2"].(map[string]interface{})["volume"].(float64))
	q.Ask2=tempMap
	tempMap=snapShotJson["ask1"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["ask1"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["ask1"].(map[string]interface{})["volume"].(float64))
	q.Ask1=tempMap

	tempMap=snapShotJson["bid1"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["bid1"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["bid1"].(map[string]interface{})["volume"].(float64))
	q.Bid1=tempMap
	tempMap=snapShotJson["bid2"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["bid2"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["bid2"].(map[string]interface{})["volume"].(float64))
	q.Bid2=tempMap
	tempMap=snapShotJson["bid3"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["bid3"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["bid3"].(map[string]interface{})["volume"].(float64))
	q.Bid3=tempMap
	tempMap=snapShotJson["bid4"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["bid4"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["bid4"].(map[string]interface{})["volume"].(float64))
	q.Bid4=tempMap
	tempMap=snapShotJson["bid5"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["bid5"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["bid5"].(map[string]interface{})["volume"].(float64))
	q.Bid5=tempMap
	tempMap=snapShotJson["bid6"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["bid6"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["bid6"].(map[string]interface{})["volume"].(float64))
	q.Bid6=tempMap
	tempMap=snapShotJson["bid7"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["bid7"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["bid7"].(map[string]interface{})["volume"].(float64))
	q.Bid7=tempMap
	tempMap=snapShotJson["bid8"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["bid8"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["bid8"].(map[string]interface{})["volume"].(float64))
	q.Bid8=tempMap
	tempMap=snapShotJson["bid9"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["bid9"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["bid9"].(map[string]interface{})["volume"].(float64))
	q.Bid9=tempMap
	tempMap=snapShotJson["bid10"].(map[string]interface{})
	tempMap["price"]=float64ToInt(snapShotJson["bid10"].(map[string]interface{})["price"].(float64))
	tempMap["volume"]=float64ToInt(snapShotJson["bid10"].(map[string]interface{})["volume"].(float64))
	q.Bid10=tempMap
	fmt.Println("读取硬盘中的历史行情数据:",snapShotJson)
}

// 分发行情,quoteIn是要交易所处理过的最新行情，quoteOut是一个接收行情的管道
func sendQuote(quoteOut chan map[string]interface{},wg *sync.WaitGroup){
	defer wg.Done()
	var testData =make(map[string]interface{})
	testData["hasaki"]=123
	quoteOut<-testData
	wg.Wait()
}

// 接收投资者发单 处理后返回单的信息
func (q *Quote) recvClientMsg(order chan UserOrder,quoteChanFator chan map[string]interface{},wg *sync.WaitGroup) {
	orderData:=<-order
	fmt.Println("收到用户的信息：",orderData)
	defer wg.Done()
	switch orderData.Type{
	case "login":
		fmt.Println("新用户登陆")
		//loginNew(orderData)
	case "vip":
		fmt.Println("老用户登陆")
		//loginVIP(orderData)
	case "order":
		fmt.Println("用户下单")
		accountError:=checkUserAccount(orderData)
		if accountError!=0{
			fmt.Println("账户余额报错",orderData)
			// TODO : 这里要加一个返回委托回报给客户端
		}
		q.updateSnapShot(orderData,quoteChanFator)
		//quoteChanFator<-quote
	default:
		log.Fatal("投资者发过来的信息有毛病，这是投资者发过来的内容：",orderData)
	}
	
	wg.Wait()
}

// 更新快照，撮合完之后，再通过管道返回，另外再传去合成K线
func (q *Quote) updateSnapShot(order UserOrder,quoteChanFator chan map[string]interface{}) {
	// 分为上下各十档行情，先根据下单方向，作分流，如果下单方向在同方向中，则把下单插进深度行情，
	// 如果插进的行情迫使第十档行情后推，那么就把原本第十和第九的深度合成在一起，共同形成第十档，
	// 如果下单方向不在同方向的深度，那么就按照反方向的深度顺序，直到消耗完下单的量
	if q.control.readTheLocalQuote==false{
		q.getHistoryQuoteSnapShot()
		//fmt.Println("读取到的本地数据",localHistoryQuoteData)
		q.control.readTheLocalQuote=true
	}
	switch order.Direction{
	case "bid":
		fmt.Println("挂一个买单")
		q.loopTheDepth(order,quoteChanFator)
	case "ask":
		fmt.Println("挂一个卖单")
		q.loopTheDepth(order,quoteChanFator)
	case "camcel":
		fmt.Println("撤单")
		q.loopTheDepth(order,quoteChanFator)
	}
	//return_:=makeKLine(order)   // TODO : 暂时先不做合成K先
	//fmt.Println("返回的大周期K线",return_)
	//return order
}

// 收到下单后修改用户资产状态
func recvOrder(order map[string]interface{}){
	// 收到用户下单
	// 判断下单类型，然后判断下单是否与用户资产匹配
}

// 新用户登陆
func loginNew(userData map[string]interface{}) {
	fmt.Println("新用户登陆：",userData["userid"],"现金：",userData["cash"],"持仓：",userData["positions"])
	// 用户信息保存文件
	filePath:=getDataFilePath()+"users.json"
	writeJson(filePath,userData)
}

// 老用户登陆
func loginVIP(userData map[string]interface{}){
	// 更新用户账户信息
}

// 收到用户的下单后，先检查用户的账户余额是否正确，够不够资产下单，如果没有就返回错误
func checkUserAccount(order UserOrder) int {
	// 如果账户没有错误则返回0，账户不正确的情况则返回1
	userData:=getUserJson("users.json")         // type : []map[string]interface{}
	fmt.Println("用户数据为:",userData)
	for _,i:=range userData{
		//fmt.Println("---------------",i)
		tempUserID:=float64ToInt(i["userID"].(float64))
		if tempUserID==order.UserID{
			//fmt.Println("---------------",i)
			// TODO : 这里要检查账户的资金与下单的资金的比较
			// 先判断方向
			if order.Direction == "bid"{
				// 买单
				tempCash:=float64ToInt(i["cash"].(float64))
				if tempCash>=order.Price * order.Volume{
					// 下单价格不能超过账户余额
					fmt.Println("该账户现金余额为:",tempCash,"下单金额为:",order.Price * order.Volume)
					return 0
				}else{
					// 不够钱
					return 1
				}
			}else{
				tempPosition:=float64ToInt(i["cash"].(float64))
				// 卖单
				if tempPosition>=order.Volume{
					// 卖出仓位不能大于持有仓位
					return 0
				}else{
					// 持仓不够
					return 1
				}
			}
		}else{
			return 1
		}
	}
	return 1
}

// ---------------------------K线合成------------------------------------
// 合成1分钟K线,参数分别为每tick 的价格，成交量，成交序列号，用于保存tick
func make1MinBar(price int,volume int,time int,tickCacheList *interface{}) interface{}{
	return 0
}

// 合成5分钟K线
func make5MinBar(bar1Min interface{},barCacheList *interface{}) interface{}{
	return 0
}

// 合成15分钟K线
func make15MinBar(bar5Min interface{},barCacheList *interface{}) interface{}{
	return 0
}

// 合成30分钟K线
func make30MinBar(bar15Min interface{},barCacheList *interface{}) interface{}{
	return 0
}

// 合成1小时K线
func make1HourBar(bar30Min interface{},barCacheList *interface{}) interface{}{
	return 0
}

// 合成4小时K线
func make4HourBar(bar1Hour interface{},barCacheList *interface{}) interface{}{
	return 0
}

// 合成日线K线
func make1DayBar(bar4Hour interface{},barCacheList *interface{}) interface{}{
	return 0
}

// ------------------------------测试用函数---------------------------------
func checkTheWebsocketRecv(data []uint8){
	// 检测从客户端接受到的是什么类型，这里测试后是[]uint8类型
	fmt.Println(reflect.TypeOf(data),string(data))
}