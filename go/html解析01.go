package main
import "fmt"
import "net/http"
import "github.com/PuerkitoBio/goquery"
// 使用go来编写爬虫

func main(){
	fmt.Println("这是个爬虫")
	resp,err:=http.Get("http://metalsucks.net")
	if err != nil{
		fmt.Println("出现了错误",err)
		return
	}

	doc,err:=goquery.NewDocumentFromReader(resp.Body)
	if err!=nil{
		fmt.Println("啦啦啦啦，解析错误",err)
	}

	doc.Find(".sidebar-reviews article .content-block").Each(func(i int,s *goquery.Selection){
		band := s.Find("a").Text()
    	title := s.Find("i").Text()
    	fmt.Printf("Review %d: %s - %s\n", i, band, title)
	})
}