package main
import "fmt"
import "net/http"
import "io/ioutil"
// 使用go来编写爬虫

func main(){
	fmt.Println("这是个爬虫")
	resp,err:=http.Get("https://zhuanlan.zhihu.com/p/55039990")
	if err != nil{
		fmt.Println("出现了错误",err)
		return
	}

	body,err:=ioutil.ReadAll(resp.Body)
	if err!=nil{
		fmt.Println("读取错误",err)
		return
	}
	fmt.Println(string(body))
}
/*
        <span class="nx">fmt</span><span class="p">.</span><span class="nx">Println</span><span class="p">(</span><span class="s">"parse url"</span><span class="p">,</span> <span class="nx">link</span><span class="p">)</span>
        <span class="k">go</span> <span class="kd">func</span><span class="p">()</span> <span class="p">{</span>
            <span class="nx">queue</span> <span class="o">&lt;-</span> <span class="nx">link</span>
        <span class="p">}()</span>
    <span class="p">}</span>
<span class="p">}</span>
</code></pre></div><p>现在的流程是main有一个for循环读取来自名为queue的通道，download下载网页和链接解析，将发现的链接放入main使用的同一队列中，并再开启一个新的goroutine去抓取形成无限循环。</p><p>这里对于新手来说真的不好理解，涉及到Golang的两个比较重要的东西：goroutine和channels，这个我也不大懂，这里也不多讲了，以后有机会细说。</p><ul><li>官方：A <i>goroutine</i> is a lightweight thread managed by the Go runtime。翻译过来就是：Goroutine是由Go运行时管理的轻量级线程。</li><li>channels是连接并发goroutine的管道，可以理解为goroutine通信的管道。 可以将值从一个goroutine发送到通道，并将这些值接收到另一个goroutine中。对这部分有兴趣的可以去看文档。</li></ul><p>好了，到这里爬虫基本上已经完成了，但是还有两个问题：去重、链接是否有效。</p><h2>链接转为绝对路径</h2><div class="highlight"><pre><code class="language-go"><span></span><span class="kn">package</span> <span class="nx">main</span>
*/