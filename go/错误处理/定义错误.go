/*
类似Python Exception("hasaki error")
golang error 是个内置类型，创建自己的错误信息使用errors库
*/
package main

import "fmt"
import "errors"

func main(){
    var hasakiError error = errors.New("hasaki error")
    fmt.Println(hasakiError)
}
// hasaki error