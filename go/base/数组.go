// 数组的应用
package main 
import "fmt"


func main(){
    var a [3]string
    a[0]="ha"
    a[1]="sa"
    a[2]="ki"
	// 或者var team = [...]string{"hammer", "soldier", "mum"}
    for i,j := range a{
        fmt.Println(i,j)
    }
}