// golang接口的实现
package main 
import "fmt"

type hasaki interface{
  hasakiFuncion(data interface{}) error
}

type kasaki struct{

}

// 实现接口具体的函数，这个d就可以在函数里面使用结构体kasaki的变量了
func (d *kasaki) hasakiFuncion(data interface{}) error {
  // 模拟写入数据
  fmt.Println("收到数据:", data)
  return nil
}

func main(){
  // 实例化结构体
  f := new(kasaki)

  // 声明一个DataWriter的接口
  var ha hasaki

  // 将接口赋值f，也就是*file类型
  ha = f

  // 使用DataWriter接口进行数据写入
  ha.hasakiFuncion("data")
}