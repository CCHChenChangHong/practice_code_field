package main

import (
    "fmt"
)

func main()  {
   a:=make(chan []string)
   fmt.Println("111111111111")
   //a<-[]string{"hasaki","jijiji"}
   go change(a)
   go print(a)
   fmt.Println("44444444444")
   for {}
}

func print(data chan []string){
    fmt.Println("22222222")
    fmt.Println(<-data)
}

func change(data chan []string){
    fmt.Println("333333333")
    data <- []string{"gugugu"}
}
/*
结果：
注释掉for{}的情况
111111111111
44444444444

有for{}不让程序结束的情况
111111111111
44444444444
333333333
22222222
[gugugu]
Failed to get threads - Process 8220 has exited with status 0
也就是说如果不延长点时间让管道传输，主程序就直接结束了

如果在主进程里面传值给管道，还会导致死锁
*/