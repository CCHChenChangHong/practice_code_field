package main 
import "fmt"
// []int使用的是 即使函数内部得到的是 slice 的值拷贝，但依旧会更新 slice 的原始数据（底层 array）

///*
func main(){
    a:=[]int{1,2,3}
    func (arr []int){
        arr[0]=6
        fmt.Println("arr的数组",arr)     // [6 2 3]
    }(a)
    fmt.Println("外面的数组",a)        // [6 2 3]

}
//*/
/*
func main() {
    x := [3]int{1,2,3}

    func(arr [3]int) {
        arr[0] = 7
        fmt.Println(arr)    // [7 2 3]
    }(x)
    fmt.Println(x)            // [1 2 3]    // 并不是你以为的 [7 2 3]
}
*/