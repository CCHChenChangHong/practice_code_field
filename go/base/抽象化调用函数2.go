package main

import "fmt"

// 函数列表结构体
type List struct{
    funcList []func()
}

func a(){
    fmt.Println("a")
}

func b(){
    fmt.Println("b")
}

func main(){
    var l = new(List)
    l.funcList=append(l.funcList,a)
    l.funcList=append(l.funcList,b)
    
    for i,j:=range l.funcList{
        fmt.Println("...",i)
        // 抽象函数的用法
        j()  
    }
}
/*
... 0
a
... 1
b
*/