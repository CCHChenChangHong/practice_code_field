// 这个和python 不一样，go的空不是一个指定类型
package main 
import "fmt"


func main(){
    var a [3]string
    a[0]="ha"
    a[1]="sa"
    a[2]="ki"
    var null=a[0:0]
    if null==nil{
        fmt.Println(a[0:1]) // 输出  [ha]
    }else{
        fmt.Println("hasaki",null)
    }
}