package main
import (
    "fmt"
)
//对 defer 延迟执行的函数，它的参数会在声明时候就会求出具体值，而不是在执行时才求值
func main(){
    i:=1
	defer fmt.Println("defer输出：",func()int{return i*2}())  //defer输出： 2
	i++
}