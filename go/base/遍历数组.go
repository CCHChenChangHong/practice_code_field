package main

import (
    "fmt"
)

func main(){
    test1()
    test2()
    test3()
}

func test1(){
    data :=[]int{1,2,3}
    for _,v:=range data{
        v*=10
    }
    fmt.Println("test1 data:",data)
}

func test2(){
    data:=[]int{1,2,3}
    for i,v:=range data{
        data[i]=v*10
    }
    fmt.Println("test2 data:",data)
}

func test3(){
    data:=[]*struct{num int}{{1},{2},{3}}
    for _,v:=range data{
        v.num*=10
    }
    fmt.Println("test3 data:",data[0],data[1],data[2])
}