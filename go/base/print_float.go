// 输出指定位数的浮点型
package main
import (
	"fmt"
)

func main(){
	fmt.Printf("%f\n",0.1231)
	fmt.Printf("%.2f\n",1.213123)
}
//0.123100
//1.21
