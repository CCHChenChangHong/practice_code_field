/*
golang和python，js这些个能随意加载文件代码的不一样。

python的包管理pip以及js的npm对用户非常友好，管理第三方库非常方便，但是golang的包管理就比较麻烦。

安装golang后，如果是默认安装，那么在系统盘的根目录会有一个Go文件夹，这个是默认的GOROOT文件夹，里面就是golang的源代码。除此之外，在系统盘的根目录user文件下，也会生成一个go文件夹，那个就是默认的工作区，然后在环境变量中，把user目录下的go文件夹加进到系统文件变量中GOPATH中。

user目录下的go文件夹，又有3个文件夹：bin/pkg/src，golang下载的第三方库就会下载在src这个目录下，比如golang的机器学习库gorgonia，下载后就会在src这个目录下有一个gorgonia.org的文件夹。
比如，我自己写的一个方法库的package 为 hasaki 调用第三方库方式：

import "Hasaki"
​
func lalala(){
  Hasaki.hasaki()  //hasaki为package下的一个方法


但是与Python不一样的是，在同一个package下，不需要显式加载其他函数即可直接调用不在同一文件在同一package的方法：
在同一个package下，有两个文件，一个是a.go，另一个是b.go，两个文件均在同一个package下，a.go里面有一个

func a(){
  }

那么b.go中不需要去import 这个a，直接在代码中使用

func b(){
  a()
 }

这样就能在b.go文件中使用a.go的方法，这个比python和js都要方便
*/