/*
golang的接口实现class的效果
interface+struct实现类的效果，interface是一个没有实例化的class,但是interface里面只有方法func
实现类内变量则需要再struct里面定义，最后使用struct实例化interface相当于python里面的实例化类

接口只有函数，如果要获取或修改结构体的变量(如类内的变量)，则通过函数的方式修改变量
本质上，把实例化接口作为函数参数传递，都是传实例化的结构体，直接传结构体也是可以的，通过接口的通信更加易读
*/
package main

import "fmt"

type engine interface{
	hasaki(msg string)
}

type strategy interface{
	gugu(hFunc engine)
}

// 用来实例化引擎接口
type Engine struct{

}

// 用来实例化策略接口
type Strategy struct{
	Hasaki  string `json:"hasaki"`
}

// 函数的实现
func (e *Engine) hasaki(msg string){
	fmt.Println(msg)
}

func (s *Strategy) gugu(hFunc engine){
	fmt.Println("strategy")
	hFunc.hasaki(s.Hasaki)
}

func main(){
	var vEngine engine          // 声明engine接口
	var vStartegy strategy      // 声明strategy接口
	var s=new(Strategy)         // 先实例化结构体，然后再用这个实例化后的结构体再去实例化接口

	vEngine=new(Engine)         // 实例化engine接口
	vStartegy=s                 // 实例化strategy接口

	s.Hasaki="Hasaki"
	vStartegy.gugu(vEngine)     // 把engine接口传进另一个接口的函数里面
}
/*
strategy
Hasaki
*/