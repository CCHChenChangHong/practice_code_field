package main

import (
    "fmt"
    "time"
)


func main()  {
  var a=make(chan int,10)
  var b=make(chan string,10)
  go func (){
      for i:=0;i<9;i++{
      a<-1
      b<-"gugu"
      }
  }()
 
    select{
    case c:=<-a:
      fmt.Println("hasaki",c)
    case d:=<-b:
      fmt.Println("kasaki",d)
    }
    for{
    time.Sleep(time.Second*1)}

}

// 输出：hasaki 1