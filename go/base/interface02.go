package main

import "fmt"

// 接口
type interfaceTest interface{
	haInterface() float32
}

// 用来实现接口的结构体
type Ha struct{
	A string
	B int
	C float32
}

// 接口实现1
func (h Ha) haInterface() float32 {
	return h.C
}

// 接口实现2
func haInterface() float32 {
	return 2.2
}

func main(){
	s:=haInterface()
	fmt.Println("第一个打印",s)

	haStruct:=new(Ha)
	haStruct.C=1.1       // 结构体只要赋值到那个用到的即可
	fmt.Println("第二个打印",haStruct)
	//第一个打印 2.2
	//第二个打印 &{ 0 1.1}
}