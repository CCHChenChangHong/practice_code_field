package main

import (
    "fmt"
)

func main()  {
    a:=[]string{"hasali","lalala"}
    change(&a)
    print(a)
}

func print(data []string){
    for _,i:=range data{
        fmt.Println("print输出",i)
    }
}

func change(d *[]string){
    *d=append(*d,"gugu")
}
/*
用指针最终输出：
print输出 hasali
print输出 lalala
print输出 gugu

不用指针最终输出
print输出 hasali
print输出 lalala
*/