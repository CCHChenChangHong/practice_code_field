package main

import "fmt"

type H struct{

}

type J struct{

}

type lala interface{
	test()
}

// 在实例化的时候，是声明接口的方式，所以，结构体实现接口对应的对象必须一样，这里
// 就不能修改函数的形式，比如：test(str string){
// fmt.Println(str)},否则在main()实例化中会报错
func (h *H)test(){
	fmt.Println("hasaki")
}


func (j *J)test(){
	fmt.Println("JJJJ")
}

func main(){
	var la lala
	la=new(H)
	la.test()

	la=new(J)
	la.test()
}
/*

*/