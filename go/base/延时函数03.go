package main

import (
	"fmt"
)

func main()  {
    for i:=0;i<5;i++{
        defer func (n int){
            fmt.Println("输出：",n)
        }(i)
    }
}
/*
同一个外围函数中每一个defer语句在执行的时候，针对其延迟函数的调用表达式都会被压在同一个栈
延迟函数执行的时候是先进后出的
输出： 4
输出： 3
输出： 2
输出： 1
输出： 0
*/