package main

import "fmt"

type H struct{

}

// 即使把interface的整个声明都注释掉，也不会影响函数的使用
type lala interface{
	test()
	//test2()
}

func (h *H)test(){
	fmt.Println("hasaki")
}

// 即使没有在接口中声明这个函数，也能在运行的时候登记这个函数
func (h *H)test2(str string ){
	fmt.Println(str)
}

func test(str string){
	fmt.Println(str)
}

func main(){
	hasaki:=new(H)
	hasaki.test()
	hasaki.test2("gugu")
	test("bala")
}
/*
hasaki
gugu
bala
*/