package main 
import "fmt"
// []int使用的是 即使函数内部得到的是 slice 的值拷贝，但依旧会更新 slice 的原始数据（底层 array）

/*
func main(){
   x:=map[string]string{"a":"1","b":"hasaki"}
   if i,j:=x["hasaki"];i==""{
       fmt.Println("第一个参数：",i,"第二个参数：",j)
       // 第一个参数：  第二个参数： false
   }
}
*/
func main(){
    x:=map[string]interface{}{"a":"1","b":"hasaki"}
    if i,j:=x["hasaki"];i==""{
        fmt.Println("第一个参数：",i,"第二个参数：",j)
    }else{
        fmt.Println("没有触发判断")
        // 没有触发判断
    }
 }