package main
import "fmt"

type hasaki struct{
    H string `json:h`
}

func (this *hasaki)la(){
    this.H="la"
}

func (this hasaki)gu(){
    this.H="gugu"
}

func main() {
    hasa:=new(hasaki)
    hasa.la()
    fmt.Println("指针函数输出：",hasa.H)
    hasa.gu()
    fmt.Println("不带指针结构体输出",hasa.H)
}
/*
指针函数输出： la
不带指针结构体输出 la
*/