// 字典
package main 
import "fmt"

type hasaki struct{
  a int
  b float32
  c string
}

func main(){
  var a=new(hasaki)
  a.a=1
  a.b=2.0
  a.c="hasaki"

  fmt.Println(a.a,a.b,a.c)
}