package main 
import "fmt"
import "time"
//import "sync"
// 只有在数据被 receiver 处理时，sender 才会阻塞。因运行环境而异，在 sender 发送完数据后，receiver 的 goroutine 可能没有足够的时间处理下一个数据
func main(){
    ch:=make(chan string)

    go func(){
        for m:=range ch{
            fmt.Println("字符串输出",m)
            time.Sleep(time.Second *1)
        }
    }()

    ch<-"hasaki1"
    ch<-"hasaki2"
 }
// "hasaki1"马上出来 ， "hasaki2"过了一秒才打印