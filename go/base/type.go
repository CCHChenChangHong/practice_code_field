// 类型别名与类型定义
package main 
import "fmt"
type a int
type b=int

func main(){
	// 将jj声明为a类型
    var jj a
    // 查看a的类型名
    fmt.Printf("jj type: %T\n", jj)

    // 将a2声明为b类型
    var a2 b
    // 查看a2的类型名
    fmt.Printf("a2 type: %T\n", a2)
}