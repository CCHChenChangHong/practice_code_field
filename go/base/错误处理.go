package main

import (
    "errors"
    "fmt"
)

func main()  {
    defer func(){
        if p:=recover();p!=nil{
            fmt.Println("recover函数的输出：",p)
        }
    }()

    panic(errors.New("hasaki"))
}
/*
recover函数应该和defer函数一起使用
结果输出：
recover函数的输出： hasaki
*/