// 爬取中国政府官网的新闻滚动
package main

import "net/http"
import "fmt"
import "strings"
import "strconv"
import "sync"
import "time"

import "github.com/PuerkitoBio/goquery"
import "gopkg.in/mgo.v2"

var govNewsCrollAddress string = "http://sousuo.gov.cn/column/30611/"     // 257.htm
var govNewsCrollLastDate string = "0"                                     // 最后一篇文章的时间，如果没有则从数据库读取
var allPages int = 5712
var savePath string = ""
var go_sync sync.WaitGroup

// 保存成json每一个数据的格式
type dataType struct{
	Date string `json:"date"`
	Title string `json:"title"`
	Content string `json:"content"`
	From int `json:"from"`
	Id string
}

func govNewsCrollHTMLString(address string) (*http.Response ,error){
	// function : 获取html的代码
	// param address : 网址地址
	// return : 返回html代码 类型：http.bodyEOFSignal
	resp,err:=http.Get(address)
	if err != nil{
		fmt.Println("获取中华人民共和国人民网回应失败 :",err)
	}

	return resp,err
}

func govNewsCrollTile(resp *http.Response)(string,error){
	// function : 传入resp的Body内容，然后获取文章的题目和时间
	// param respBody : HTML对象
	// return : 文章列表和时间列表
	var title string
	doc,err:=goquery.NewDocumentFromReader(resp.Body)
	if err!=nil{
		fmt.Println("解析中华人民共和国人民网HTML错误 文章标题",err)
		return "",err
	}
	
	// 爬取文章的题目和时间
	doc.Find("div").Each(func(i int,s *goquery.Selection){
		tempTitle := s.Find("h1").Text()
		if len(tempTitle)!=0{
			title=string([]byte(tempTitle[9:]))
		}
	})
	fmt.Println("新闻标题 : ",title)
    return title,err
}

func govNewsCrollHrefContent(resp *http.Response)([]string){
	// function : 具体内容的href链接
	// param resp : 请求的返回
	// return : 返回链接的字符串列表
	var hrefList []string
	doc,err:=goquery.NewDocumentFromReader(resp.Body)
	if err!=nil{
		fmt.Println("解析中华人民共和国人民网HTML错误",err)
	}

	// 爬取文章的链接地址
    doc.Find("a").Each(func(i int,s *goquery.Selection){
        href,isExist := s.Attr("href")
        if isExist==true{
            if "javascript:void(0)"==href || "http://www.gov.cn"==href || strings.Index(href,"http://sousuo.gov.cn/column")!=-1{
                return
            }
			//fmt.Printf("网址 : %s\n",href)
			hrefList=append(hrefList,href)
        }
	})

	return hrefList
}

func govNewsCrollContent(resp *http.Response)(string,string,error){
	// function : 获取具体的文章内容
	// param address : 具体文章地址链接的HTML对象
	// return : 文章内容string，这部分和前面的标题都要存进数据库
	var content string
	var date string
	doc,err:=goquery.NewDocumentFromReader(resp.Body)
	if err!=nil{
		fmt.Println("解析中华人民共和国人民网 新闻滚动 HTML错误",err)
		return "","",err
	}
	
	// 爬取文章的内容和时间
	doc.Find(".pages_content").Each(func(i int,s *goquery.Selection){
		// 爬取逻辑
		title := s.Find("p").Text()
		// 检查无效打印
        if strings.Index(title,"下一页")!=-1 || strings.Index(title,"上一页")!=-1{
            return
        }
		content=title
	})
	
	// 文章的时间
	doc.Find("div[class=pages-date]").Each(func(i int,s *goquery.Selection){
		date=string([]byte(s.Text()[:16]))
	})
	if err!=nil{
		fmt.Println("错误出现 : ",date,content)
	}
	return date,content,err
}

func saveAsMongoDB(session *mgo.Session ,title string,content string ,time string ,dataFrom int,id string){
	// function : 保存数据到mongo数据库
	// 读表
	c:=session.DB("crawl").C("govNews")
	c.Insert(map[string]interface{}{"title":title,"content":content,"date":time,"id":id,"from":dataFrom})   // 插入
	
}

func main(){
	// function : 总运行启动函数
	fmt.Println("开始爬取中华人民共和国新闻滚动")
	// session,err:=mgo.Dial("127.0.0.1:27017")
	// if err!=nil{
	// 	fmt.Println("连接数据库报错 : ",err)
	// }
	for i:=0;i<2;i++{
		var tempAddress string = strings.Join([]string{govNewsCrollAddress,".htm"},strconv.Itoa(i))
		go_sync.Add(1)
		go func(tempAddress string,i int,wg *sync.WaitGroup){
			defer wg.Done()
			respAll ,err:= govNewsCrollHTMLString(tempAddress)
			if err!=nil{
				return
			}
			hrefList := govNewsCrollHrefContent(respAll)

			for _,href := range hrefList{
				respOne ,err:= govNewsCrollHTMLString(href)
				if err!=nil{
					continue
				}
				_,_=govNewsCrollTile(respOne)
				// newsDate,newsContent,err := govNewsCrollContent(respOne)
				// // 过滤无效消息
				// if len(newsContent)<4 || err!=nil{
				// 	continue
				// }
				// saveAsMongoDB(session,"",newsContent,newsDate,6,strconv.Itoa(i))
			}
		}(tempAddress,i,&go_sync)

		time.Sleep(time.Second * 1)
		fmt.Println("准备关闭线程",i)
		
	}
	go_sync.Wait()
	fmt.Println("爬取中华人民共和国新闻滚动结束")
}