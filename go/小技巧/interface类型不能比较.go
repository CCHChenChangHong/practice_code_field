/*
interface{}之间不能用于比较

如果是确定的数据类型，就用  .(类型)  来比较

如 a type map[string]interface{}
   b type map[string]interface{}

   if a["hasaki"] > b["hasaki"]{
       ...
       }  // 这样就会报错，

如果确定interface的类型就可以这样：
if a["hasaki"].(int) > b["hasaki"].(int){
       ...
       }    // 已知interface的类型
*/