package main

import (
	"fmt"
	"sync"
)

func A(aChan chan int,data int ,wait *sync.WaitGroup){
	aChan <- data
	fmt.Println("A   ------ :  ",data)
	wait.Done()
}

func B(bChan chan int ,wait *sync.WaitGroup){
	a:=<-bChan
	fmt.Println("B ----- : ",a)
	wait.Done()
}

func main(){
	dataChan:=make(chan int ,100)        // 通讯通道
	var wg sync.WaitGroup

	for i:=0;i<10;i++{
		go A(dataChan,i,&wg)    // 生产数据
		wg.Add(1)
	}

	for j:=0;j<10;j++{
		go B(dataChan,&wg)      // 消费数据
		wg.Add(1)
	}

	wg.Wait()
}