package main

import "log"
import "os/exec"
import "strings"

func main(){
    log.Println("golang使用子进程shell进行操作python执行命令")
    cmd:=exec.Command("python","C:\\Projects\\Research3\\dockerPY\\pythonClient.py")
    cmd.Stdin = strings.NewReader("some input")   // 用来输入系统密码,有时候权限需要
	log.Println("----->>",cmd.String())
	err:= cmd.Run()
	if (err!=nil){
		log.Println(err)
	}
}

/*
import requests

url:str='http://192.168.209.128:8080/LADockerServer'

r=requests.get(url)
print(r.text)
*/