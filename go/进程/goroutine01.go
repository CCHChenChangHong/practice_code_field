package main

import "fmt"
import "runtime"
import "time"
import "sync"

func a(a ,b int){
	/*
	defer func(){
		err:=recover()
		if err!=nil{
			fmt.Println("错误情况")
		}
	}()*/
	c:=a+b
	fmt.Println("-------",c)
}

func main(){
	var go_sync sync.WaitGroup     // 声明一个WaitGroup变量
	num:=runtime.NumCPU()
	fmt.Println(num)
	for i:=0;i<10;i++{
		go_sync.Add(1)
		go a(i,i+1)
	}
	time.Sleep(time.Second * 1)
	go_sync.Wait()             // 等待所有协程完成
}