package main

import "fmt"
import "github.com/gin-gonic/gin"

func frontServer(context *gin.Context){
	// function : 前端服务接口服务函数
	fmt.Println("前端服务接口启动")
	context.JSON(200,gin.H{
		"code":200,
		"success":true,
	})
}

func dataServer(context *gin.Context){
	// function : 数据请求服务
	fmt.Println("数据请求")
	context.JSON(200,gin.H{
		"code":200,
		"success":true,
	})
}

func main(){
	fmt.Println("启动数据服务")
	// Engin指针
    router := gin.Default()
    //router := gin.New()  gin.Default()和gin.New()都可以
    
    // 启动路由
	router.GET("/frontServer", frontServer)
	router.GET("/dataServer",dataServer)
    // 指定地址和端口号
    router.Run("localhost:8888")
}