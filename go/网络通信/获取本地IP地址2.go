package main

import "net"
import "fmt"
import "strings"

func GetOutBoundIP()(ip string, err error)  {
    conn, err := net.Dial("udp", "8.8.8.8:53")
    if err != nil {
        fmt.Println(err)
        return "",err
    }
    localAddr := conn.LocalAddr().(*net.UDPAddr)
    fmt.Println(localAddr.String())
    ip = strings.Split(localAddr.String(), ":")[0]
    return ip,nil
}
func main() {
    ip, err := GetOutBoundIP()
    if err != nil {
        fmt.Println(err)
    }
    fmt.Println(ip)
}

/*
运行结果
170.20.6.182:38551
170.20.6.182
*/