package main
import "fmt"
import "net/http"
import "io/ioutil"

func main() {
   url :="https://www.baidu.com"
   res,err:=http.Get(url)
   defer res.Body.Close()
   if err!=nil{
       fmt.Println("请求网址返回报错",err)
   }

   body,err:=ioutil.ReadAll(res.Body)
   fmt.Println("请求正确返回",string(body))
}
/*
请求正确返回 <html>
<head>
	<script>
		location.replace(location.href.replace("https://","http://"));
	</script>
</head>
<body>
	<noscript><meta http-equiv="refresh" content="0;url=http://www.baidu.com/"></noscript>
</body>
</html>
*/
