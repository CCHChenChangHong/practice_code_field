package main

import "net"
import "fmt"

func main() {
    addrs, err := net.InterfaceAddrs()
    if err != nil {
        fmt.Println(err)
        return
    }
    for _, address := range addrs {
        // 检查ip地址判断是否回环地址
        if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
            if ipnet.IP.To4() != nil {
                fmt.Println(ipnet.IP.String())
            }
        }
    }
}


/*
运行结果
170.20.6.182
172.17.0.1
192.168.49.1
172.18.0.1
*/