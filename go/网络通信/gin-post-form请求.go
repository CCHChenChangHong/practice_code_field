package main

import "log"
import "net/http"
import "github.com/gin-gonic/gin"

func main() {
	log.Println("start server")
	r := gin.Default()

	r.POST("/hasakipost",hasakiPost)
	r.Run() // 监听并在 0.0.0.0:8080 上启动服务
}



func hasakiPost(c *gin.Context){
	id := c.Query("id")      // 这个是要在url中传参数的
	page := c.DefaultQuery("page", "0")
	name := c.PostForm("name")
	message := c.PostForm("message")

	log.Printf("id: %s; page: %s; name: %s; message: %s", id, page, name, message)

	c.JSON(200, gin.H{
		"status":  "posted",
		"message": message,
		"name":    name,
	})
	
}
/*
服务收到请求之后的打印
2021/08/01 15:27:09 id: 233; page: 222; name: hasaki; message: dodo
*/


/*
python 请求
import requests

url= 'http://127.0.0.1:8080/hasakipost?id=233&page=222'
data= {'name':'hasaki','message':'dodo'}
r=requests.post(url,data)
print(r.content)
*/