package main

import "log"

import "github.com/gin-gonic/gin"

func main() {
	log.Println("start server")
	r := gin.Default()

	r.POST("/uploadFile",saveFile)
	r.Run() // 监听并在 0.0.0.0:8080 上启动服务
}


func saveFile(c *gin.Context){
	// 接收post文件,并保存
	// 单文件
	file, _ := c.FormFile("file")
	log.Println(file.Filename)

	// 上传文件到指定的路径
	c.SaveUploadedFile(file, "D:/pratice/0.jpg")
	log.Println("uploaded!")
}

/*
python的客户端
import requests

files = {'file': open('D:/pratice/hasakiPic/Mimipigeon/0.jpg', 'rb')}
url='http://127.0.0.1:8080/uploadFile'
r=requests.post(url,files=files)
print(r.content)
*/