package main

import "fmt"
import "github.com/gin-gonic/gin"

func test(c *gin.Context){
    // function : gin框架post请求测试
    defalutURL:=c.DefaultQuery("page","814")    // 如果url后面的参数为空就会有一个默认值
    fromParam:=c.PostForm("formData")          // post请求中的body参数
    fmt.Println("收到参数 : ",defalutURL,fromParam)
    c.JSON(200,gin.H{
        "status":"post",
        "defalutURL":defalutURL,
        "formParam":fromParam,
    })
}

func main(){
	// Engin指针
    router := gin.Default()

    router.POST("/",test)
    router.Run("127.0.0.1:8888")
}