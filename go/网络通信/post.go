package main
import "log"
import "net/http"
import "io/ioutil"
import "bytes"
import "encoding/json"

func main(){
    var url string="http://192.168.209.128:8080/paramPostServer?"+quantity
	data:=map[string]string{"execFilePath":"aaa","resultPath":"bbb"}
	var result string=Post(url,data,"application/json")

	log.Println(result)
}

func Post(url string, data interface{}, contentType string) string {
    // 超时时间：5秒
    client := &http.Client{Timeout: 5 * time.Second}
    jsonStr, _ := json.Marshal(data)
    resp, err := client.Post(url, contentType, bytes.NewBuffer(jsonStr))
    if err != nil {
        panic(err)
    }
    defer resp.Body.Close()

    result, _ := ioutil.ReadAll(resp.Body)
    return string(result)
}