package main
import "fmt"
import "net/http"
import "io/ioutil"

func main() {
   res,err:=http.NewRequest("GET","https://www.baidu.com",nil)
   // 运行这个回跳转到panic.go的源代码
   defer res.Body.Close()
   if err!=nil{
       fmt.Println("GET报错",err)
   }
   
   res.Body.Close()
   body,err:=ioutil.ReadAll(res.Body)
   if err!=nil{
       fmt.Println("解析body出错",err)
   }
   fmt.Println("最后输出",string(body))
}