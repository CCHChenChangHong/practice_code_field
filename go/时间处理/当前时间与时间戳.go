package main

import "fmt"
import "time"

func main(){
	var time1 string = "2019-12-22 11:11:11"
	var cuo int64 = time.Now().Unix()           // 时间戳 int64类型

	// 注意 ： 这里的Format必须是"2006-01-02 15:04:05"，否则结果肯定是错的，垃圾！
	fmt.Println("time1 ： ",time1,"time2 : ",time.Now().Format("2006-01-02 15:04:05"),"time3 : ",cuo)
}
/*
time1 ：  2019-12-22 11:11:11 time2 :  2019-12-22 11:13:17 time3 :  1576984397
*/