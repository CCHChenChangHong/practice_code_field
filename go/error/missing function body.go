/*
函数：
func function()
{

}
上面的函数就会报错：missing function body

又或者
func function(ha map){
	// 参数类型不明确也会报missing function body
}
*/
package error