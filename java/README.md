# Java

这个仓是为了做鸿蒙而学的,所以这里学的java是针对鸿蒙系统的

#### 相关资料

[鸿蒙IDE链接](https://developer.harmonyos.com/cn/develop/deveco-studio#download·)

[学习资料](https://www.runoob.com/java/java-object-classes.html)


有一个.zip文件是免安装的文件

[jdk下载](https://www.oracle.com/java/technologies/javase-downloads.html) 

[清华镜像OpenJDK](https://mirrors.tuna.tsinghua.edu.cn/AdoptOpenJDK/11/jdk/x64/windows/)

#### XML教程

[XML教程](https://www.runoob.com/xml/xml-syntax.html)

#### hadoop相关

[window安装的时缺少winutils执行文件,导致hadoop上构建应用失败](https://github.com/steveloughran/winutils/tree/master/hadoop-3.0.0/bin)


Hadoop配置
```
sudo vim /usr/local/hadoop/etc/hadoop/hadoop-env.sh
export JAVA_HOME=/usr/lib/jvm/java

```


启动hdfs
```
保证无密码ssh
ssh localhost

ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
chmod 0600 ~/.ssh/authorized_keys

ssh root@localhost如果需要密码则
sudo vim /etc/ssh/sshd_config
>>>PermitRootLogin:yes
# 重启ssh服务
sudo service ssh restart
# 实现无密码登录
ssh-keygen -t rsa -P ""
cat $HOME/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys




设置NameNode
vim /usr/local/hadoop/etc/hadoop/core-site.xml
<configuration>
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://localhost:9000</value>
    </property>
</configuration>

vim /usr/local/hadoop/etc/hadoop/hdfs-site.xml
<configuration>
    <property>
        <name>dfs.replication</name>
        <value>3</value>
    </property>
    <property>
        <name>dfs.namenode.name.dir</name>
        <value>file:/usr/local/hadoop/hadoop_data/hdfs/namenode</value>
    </property>
    <property>
        <name>dfs.datanode.data.dir</name>
        <value>file:/usr/local/hadoop/hadoop_data/hdfs/datanode</value>
    </property>
</configuration>

vim /usr/local/hadoop/etc/hadoop/yarn-site.xml
<configuration>
    <property>
        <name>yarn.nodemanager.aux-services</name>
        <value>mapreduce_shuffle</value>
    </property>
    <property>
        <name>yarn.nodemanager.aux-services.mapreduce.shuffle.class</name>
        <value>org.apache.hadoop.mapred.ShuffleHandler</value>
    </property>
</configuration>


创建hdfs数据保存目录
sudo mkdir -p /usr/local/hadoop/hadoop_data/hdfs/datanode  
sudo mkdir -p /usr/local/hadoop/hadoop_data/hdfs/namenode

格式化namenode HDFS目录
sudo /usr/local/hadoop/bin/hdfs namenode -format

如果出现错误:
Starting namenodes on [localhost]
ERROR: Attempting to operate on hdfs namenode as root
ERROR: but there is no HDFS_NAMENODE_USER defined. Aborting operation.
Starting datanodes
ERROR: Attempting to operate on hdfs datanode as root
ERROR: but there is no HDFS_DATANODE_USER defined. Aborting operation.
Starting secondary namenodes [hasaki-master]
ERROR: Attempting to operate on hdfs secondarynamenode as root
ERROR: but there is no HDFS_SECONDARYNAMENODE_USER defined. Aborting operation.
Starting resourcemanager
ERROR: Attempting to operate on yarn resourcemanager as root
ERROR: but there is no YARN_RESOURCEMANAGER_USER defined. Aborting operation.
Starting nodemanagers
ERROR: Attempting to operate on yarn nodemanager as root
ERROR: but there is no YARN_NODEMANAGER_USER defined. Aborting operation.

修改:
vim /usr/local/hadoop/sbin/start-dfs.sh和stop-dfs.sh 在文件顶部增加
HDFS_DATANODE_USER=root
HADOOP_SECURE_DN_USER=hdfs
HDFS_NAMENODE_USER=root
HDFS_SECONDARYNAMENODE_USER=root

另:
vim start-yarn.sh,stop-yarn.sh
YARN_RESOURCEMANAGER_USER=root
HADOOP_SECURE_DN_USER=yarn
YARN_NODEMANAGER_USER=root

启动HDFS
sudo /usr/local/hadoop/sbin/start-dfs.sh

打开浏览器http://localhost:9870 查看hadoop NameNode 界面
```