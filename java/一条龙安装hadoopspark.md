# Hadoop + Spark + python环境搭建 (Ubuntu 20.10 Desktop)

## update

```
sudo apt update
sudo apt-get update
sudo apt upgrade
sudo apt install vim
```

## 配置ssh

```
sudo apt-get install ssh
sudo apt-get install rsync
ssh-keygen -t dsa -P '' -f ~/.ssh/id_rsa
ll ~/.ssh
cat ~/.ssh/id_dsa.pub >> ~/.ssh/authorized_keys

# 哪个账户下没法进行ssh localhost就去哪个账户下
cd ~/.ssh/
ssh-keygen -t rsa -P ""
cat $HOME/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys

ssh root@localhost如果需要密码则
sudo vim /etc/ssh/sshd_config
>>>PermitRootLogin:yes
# 重启ssh服务
sudo service ssh restart
# 实现无密码登录
ssh-keygen -t rsa -P ""
cat $HOME/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys
```

## 安装java

hadoop和spark运行基于java,所以要安装java的环境

```
sudo apt install openjdk-15-jre-headless
sudo apt install openjdk-15-jdk-headless
```

## 下载hadoop+spark

```
# 安装hadoop
# apache源头https://mirror-hk.koddos.net/apache/hadoop/common/hadoop-3.2.2/hadoop-3.2.2.tar.gz
wget http://mirrors.tuna.tsinghua.edu.cn/apache/hadoop/common/hadoop-3.2.2/hadoop-3.2.2.tar.gz
sudo tar -zxvf hadoop-3.2.2.tar.gz -C /usr/local
cd /usr/local
sudo mv hadoop-3.2.2 hadoop
hadoop version

# 安装spark
# apache源https://mirror-hk.koddos.net/apache/spark/spark-3.1.1/spark-3.1.1-bin-hadoop3.2.tgz
wget https://mirrors.tuna.tsinghua.edu.cn/apache/spark/spark-3.1.1/spark-3.1.1-bin-hadoop3.2.tgz 
sudo tar xvzf spark-3.1.1-bin-hadoop3.2.tgz -C /usr/local
cd /usr/local
sudo mv spark-3.1.1-bin-hadoop3.2 spark
spark-shell
```

## 配置hadoop spark

#### hadoop配置

```
# hadoop的java路径设置
vim /usr/local/hadoop/etc/hadoop/hadoop-env.sh
# 增加以下
export JAVA_HOME=/usr/lib/jvm/java-15-openjdk-amd64


设置NameNode
vim /usr/local/hadoop/etc/hadoop/core-site.xml
<configuration>
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://localhost:9000</value>
    </property>
</configuration>

vim /usr/local/hadoop/etc/hadoop/hdfs-site.xml
<configuration>
    <property>
        <name>dfs.replication</name>
        <value>3</value>
    </property>
    <property>
        <name>dfs.namenode.name.dir</name>
        <value>file:/usr/local/hadoop/hadoop_data/hdfs/namenode</value>
    </property>
    <property>
        <name>dfs.datanode.data.dir</name>
        <value>file:/usr/local/hadoop/hadoop_data/hdfs/datanode</value>
    </property>
</configuration>

vim /usr/local/hadoop/etc/hadoop/yarn-site.xml
<configuration>
    <property>
        <name>yarn.nodemanager.aux-services</name>
        <value>mapreduce_shuffle</value>
    </property>
    <property>
        <name>yarn.nodemanager.aux-services.mapreduce.shuffle.class</name>
        <value>org.apache.hadoop.mapred.ShuffleHandler</value>
    </property>
</configuration>


创建hdfs数据保存目录
sudo mkdir -p /usr/local/hadoop/hadoop_data/hdfs/datanode  
sudo mkdir -p /usr/local/hadoop/hadoop_data/hdfs/namenode

格式化namenode HDFS目录
sudo /usr/local/hadoop/bin/hdfs namenode -format

如果出现错误:
Starting namenodes on [localhost]
ERROR: Attempting to operate on hdfs namenode as root
ERROR: but there is no HDFS_NAMENODE_USER defined. Aborting operation.
Starting datanodes
ERROR: Attempting to operate on hdfs datanode as root
ERROR: but there is no HDFS_DATANODE_USER defined. Aborting operation.
Starting secondary namenodes [hasaki-master]
ERROR: Attempting to operate on hdfs secondarynamenode as root
ERROR: but there is no HDFS_SECONDARYNAMENODE_USER defined. Aborting operation.
Starting resourcemanager
ERROR: Attempting to operate on yarn resourcemanager as root
ERROR: but there is no YARN_RESOURCEMANAGER_USER defined. Aborting operation.
Starting nodemanagers
ERROR: Attempting to operate on yarn nodemanager as root
ERROR: but there is no YARN_NODEMANAGER_USER defined. Aborting operation.

修改:
vim /usr/local/hadoop/sbin/start-dfs.sh和stop-dfs.sh 在文件顶部增加
HDFS_DATANODE_USER=root
HADOOP_SECURE_DN_USER=hdfs
HDFS_NAMENODE_USER=root
HDFS_SECONDARYNAMENODE_USER=root

另:
vim start-yarn.sh,stop-yarn.sh
YARN_RESOURCEMANAGER_USER=root
HADOOP_SECURE_DN_USER=yarn
YARN_NODEMANAGER_USER=root
```

#### spark配置

```
# 需要先把spark/conf文件下spark-env.sh的模板复制成spark-env.sh,然后进行修改
sudo vim /usr/local/spark/conf/spark-env.sh
export SPARK_MASTER_IP=master
export SPARK_WORKER_CORES=1
export SPARK_WORKER_MEMORY=800m
export SPARK_WORKER_INSTANCES=2
export SPARK_EXECUTOR_MEMORY=256m 这个不能大于单个工作节点的内存不然回报错TaskSchedulerImpl: Initial job has not accepted any resources
export SPARK_LOCAL_IP=本机ip    如果设置了这个,那么启动spark master的时候,终端不会显示web的链接,打开本机ip:8080即可
```

#### ~/.bashrc配置

```
vim ~/.bashrc

# java config 
export JAVA_HOME=/usr/lib/jvm/java-15-openjdk-amd64
export JRE_HOME=${JAVA_HOME}/jre 
export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib
export PATH=${JAVA_HOME}/bin:$PATH


# hadoop config
export HADOOP_HOME=/usr/local/hadoop
export CLASSPATH=$($HADOOP_HOME/bin/hadoop classpath):$CLASSPATH
export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native
export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin
export HADOOP_MAPRED_HOME=$HADOOP_HOME
export HADOOP_COMMON_HOME=$HADOOP_HOME
export HADOOP_HDFS_HOME=$HADOOP_HOME
export YARN_HOME=$HADOOP_HOME
export HADOOP_OPTS="-DJava.library.path=$HADOOP_HOME/lib"
export JAVA_LIBRARY_PATH=$HADOOP_HOME/lib/native:$JAVA_LIBRARY_PATH


# spark config
export SPARK_HOME=/usr/local/spark
export PATH=$PATH:$SPARK_HOME/bin:$SPARK_HOME/sbin

source ~/.bashrc
```

## Hadoop使用

#### 启动HDFS
```
# sudo /usr/local/hadoop/sbin/start-dfs.sh   启动HDFS
sudo /usr/local/hadoop/sbin/start-all.sh
```

#### 创建HDFS文件和显示

```
# 创建HDFS目录
sudo /usr/local/hadoop/bin/hadoop fs -mkdir /user
sudo /usr/local/hadoop/bin/hadoop fs -mkdir /user/hasaki   # 创建一个用户目录
sudo /usr/local/hadoop/bin/hadoop fs -mkdir /user/hasaki/test
hadoop fs -ls   # 查看之前创建HDFS目录
hadoop fs -ls /  # 查看HDFS根目录
hadoop fs -ls /user # 查看HDFS的/user目录


# 从本地计算机复制文件到HDFS
sudo /usr/local/hadoop/bin/hadoop fs -copyFromLocal /usr/local/hadoop/README.txt  /user/hasaki/test
sudo /usr/local/hadoop/bin/hadoop fs -copyFromLocal /usr/local/hadoop/README.txt  /user/hasaki/test/test1.txt
sudo /usr/local/hadoop/bin/hadoop fs -ls /user/hasaki/test

# Hadoop HDFS Web
# 浏览器打开 : http://localhost:9870/explorer.html#/   可以看到分布式文件
```

## spark使用

#### 启动spark

```
sudo /usr/local/spark/sbin/start-all.sh
# sudo /usr/local/spark/sbin/start-master.sh 启动主节点
```

#### 浏览器上查看spark工作情况

```
ifconfig -a 获取本机ip

浏览器打开 http://localhost:8080
```