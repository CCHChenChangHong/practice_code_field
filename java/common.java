/*
java for common 因为scala可以直接调用java文件,所以common由java来写的话就能一套多用
java 15的标准库甲骨文文档 : https://docs.oracle.com/en/java/javase/15/docs/api/index.html
 */

import java.io.*;
import java.net.MalformedURLException;
import java.time.LocalDateTime;
import java.net.HttpURLConnection;
import java.net.URL;

public class common {
    public static boolean debug = false;

    public static String getHtml(String URL){
        // 获取网页的html
        return "";
    }

    // -------------------  网络通信 ------------------------------
    public static String get(String URLs) throws IOException {
        // java 简单的http get请求
        // return 返回 response
        HttpURLConnection connection = null;
        InputStream is = null;
        BufferedReader br = null;
        String result = null;// 返回结果字符串
        try {
        URL url=new URL(URLs);     // 创建远程对象
        connection = (HttpURLConnection) url.openConnection();    // 打开一个链接
        // 设置连接方式：get
        connection.setRequestMethod("GET");
        // 设置连接主机服务器的超时时间：15000毫秒
        connection.setConnectTimeout(15000);
        // 设置读取远程返回的数据时间：60000毫秒
        connection.setReadTimeout(60000);
        // 发送请求
        connection.connect();
        // 通过connection连接，获取输入流
        if (connection.getResponseCode() == 200) {
            is = connection.getInputStream();
            // 封装输入流is，并指定字符集
            br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            // 存放数据
            StringBuffer sbf = new StringBuffer();
            String temp = null;
            while ((temp = br.readLine()) != null) {
                sbf.append(temp);
                sbf.append("\r\n");
            }
            result = sbf.toString();
        }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException error) {
            error.printStackTrace();
        } finally {
            // 关闭资源
            if (null != br) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        connection.disconnect();// 关闭远程连接
        }
        return result;

    }

    // ---------------------  文件操作 ---------------------------
    public static void writeTxt(String path,String content){
        // 写入文件
        try{
            PrintWriter writer = new PrintWriter(new File(path));
            writer.write(content);
            writer.close();
        }catch (FileNotFoundException error) {
            print("write txt function can`t find the file " + error.toString());
        }
    }

    // --------------- 时间处理 ---------------------
    public static String now(){
        // 返回字符串时间
        String date=LocalDateTime.now().toString();
        return date;
    }

    // ------------------ 日志输出 -----------------------
    public static void debugPrint(String msg){
        if (debug){
            printByJava(msg);
        }
    }

    public static void printByJava(String msg){
        System.out.println(msg);
    }

    private static void print(String msg){
        // 用于兼容内部打印的程序输出
        printByJava(msg);
    }

}
