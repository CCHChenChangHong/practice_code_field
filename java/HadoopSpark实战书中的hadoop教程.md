## 先

```
sudo apt update
sudo apt-get update
sudo apt upgrade
```

## 安装java

```
sudo apt-get install default-jdk    
# 如果使用java,那么提示是sudo apt install openjdk-15-jre-headless
# 如果使用jps,那么提示是 sudo apt install openjdk-15-jdk-headless
```

## SSH无密码安装

```
sudo apt-get install ssh
sudo apt-get install rsync
ssh-keygen -t dsa -P '' -f ~/.ssh/id_rsa
ll ~/.ssh
cat ~/.ssh/id_dsa.pub >> ~/.ssh/authorized_keys

# 哪个账户下没法进行ssh localhost就去哪个账户下
cd ~/.ssh/
ssh-keygen -t rsa -P ""
cat $HOME/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys
```

## 安装hadoop

[链接](https://gitee.com/CCHChenChangHong/practice_code_field/tree/master/scala#https://blog.csdn.net/lyqdy/article/details/106604489)

## 设置hadoop环境

```
sudo vim ~/.bashrc

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64

# hadoop config
export HADOOP_HOME=/usr/local/hadoop
export CLASSPATH=$($HADOOP_HOME/bin/hadoop classpath):$CLASSPATH
export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native
export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin
export HADOOP_MAPRED_HOME=$HADOOP_HOME
export HADOOP_COMMON_HOME=$HADOOP_HOME
export HADOOP_HDFS_HOME=$HADOOP_HOME
export YARN_HOME=$HADOOP_HOME
export HADOOP_OPTS="-DJava.library.path=$HADOOP_HOME/lib"
export JAVA_LIBRARY_PATH=$HADOOP_HOME/lib/native:$JAVA_LIBRARY_PATH

source ~/.bashrc
```

编辑Hadoop-env.sh
```
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
```

设置core-site.xml yarn-site.xml   hdfs-site.xml

[链接](https://gitee.com/CCHChenChangHong/practice_code_field/tree/master/java)

## 启动HDFS

```
# /usr/local/hadoop/sbin/start-dfs.sh   启动HDFS
/usr/local/hadoop/sbin/start-all.sh
```

## 创建HDFS目录

```
# 创建HDFS目录
sudo /usr/local/hadoop/bin/hadoop fs -mkdir /user
sudo /usr/local/hadoop/bin/hadoop fs -mkdir /user/hasaki   # 创建一个用户目录
sudo /usr/local/hadoop/bin/hadoop fs -mkdir /user/hasaki/test
hadoop fs -ls   # 查看之前创建HDFS目录
hadoop fs -ls /  # 查看HDFS根目录
hadoop fs -ls /user # 查看HDFS的/user目录
```

## 从本地计算机复制文件到HDFS

```
sudo /usr/local/hadoop/bin/hadoop fs -copyFromLocal /usr/local/hadoop/README.txt  /user/hasaki/test
sudo /usr/local/hadoop/bin/hadoop fs -copyFromLocal /usr/local/hadoop/README.txt  /user/hasaki/test/test1.txt
sudo /usr/local/hadoop/bin/hadoop fs -ls /user/hasaki/test
>>>-rw-r--r--   3 root supergroup       1361 2021-04-06 14:20 /user/hasaki/test/README.txt
>>>-rw-r--r--   3 root supergroup       1361 2021-04-06 14:21 /user/hasaki/test/test1.txt
```


## spark-shell中使用hdfs
```
# hdfs://localhost:9000为core-site.xml设置的路径
val textFile=sc.textFile("hdfs://localhost:9000/user/hasaki/test/test1.txt")  
textFile.count
```

## Hadoop HDFS Web

```
浏览器打开 : http://localhost:9870/explorer.html#/   可以看到分布式文件
```