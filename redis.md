#### ubuntu 安装redis

sudo apt-get install redis-server

#### ubuntu 启动redis

>redis-server

检查redis是否启动

>redis-cli

#### 检查redis链接是否还正常

>ping
pong

#### window启动redis

>.\redis-server.exe .\redis.windows.conf

进入redis命令行

> .\redis-cli.exe -h 170.20.6.114 -p 6379

#### 局域网访问redis(window)

修改redis.windows.conf
```
# 把blind后面改成ipconfig的ip address
bind 170.20.6.114

# 然后把模式改成
protected-mode no
```

有可能还需要关闭电脑的防火墙