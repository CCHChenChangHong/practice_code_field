#include<iostream>
#include<cmath>
#include<string>
#include<vector>
#include<array>
using namespace std;

float hashaki()
{
	float a=2.0;
	return a*a;  // return后面可以接运算 
}

char char_type()
{
	char a='hashaki';     // 字符类型学习 
	cout<<sizeof a;
	return a;
}

char know_about_const(){
	const char *concon="hashaki";   // 为什么用指针呢，数组也可以啊 
	cout<<"\n";
	for(int i=0;i<7;++i){
		cout<<*(concon+i);
	}
	
}

char about_string(){
	string a="hashaki";     // 这个就比较熟悉 
	string b="cch_";
	cout<<"\n";
	cout<<b<<a;
}

int about_array(){
	int a[4]={1,2,3,4};     // 数组的学习 
	cout<<"\n"<<"数组的学习";
	for(int i=0;i<4;++i){
		cout<<a[i];
	}
}

void about_struct(){         // 结构体的学习 

	struct myHashaki{
		string name;
		int age;
		bool loveMe;
		float HowMuchLove;
	};
	
	myHashaki littileMe{
	"hashaki",24,true,1.0
	};
	cout<<"\n";
	cout<<littileMe.name<<"   "<<littileMe.age<<"   "<<littileMe.loveMe<<"  "<<littileMe.HowMuchLove<<"结构体的学习";
	
	cout<<"\n"<<"结构数组";
	myHashaki arrayMe[2]{
	{"CCH",24,true,1.1},
	{"YPL",24,true,0.8}
	};
	cout<<arrayMe[1].name<<"  "<<" LOVE"<<"   "<<arrayMe[0].name;
}

void about_enum()   // 使用enum可以实现python for列表那种效果 
{
	cout<<"\n";
	enum hashaki{YPL,CCH,OUR,US};
	hashaki a=YPL;              // 非常奇怪的方式，hashaki是类型，但是a只是hashaki的其中一个，a不是hashaki全部 
	cout<<"枚举测试"<<"   "<<a;
}

void about_pointer()
{
	struct hashaki{
		string name;
		int age;
	};
	cout<<"\n";
	float a;
	float *p_a=&a;
	*p_a=6.66;
	
	hashaki *p_struct=new hashaki;         // 结构体指针，越来越丰富了 
	*p_struct={"CCH",24};
	cout<<"指针测试"<<"  "<<*p_a<<"   "<<p_struct->name;     // 为什么p_struct前面不用加 * 呢 ，它不是个指针么？ 
	delete p_struct;
	//cout<<"\n"<<p_struct->name;                              // 没有p_struct的内存，所以运行时会出错 
}

void about_pointer_calculate(){
	int a[5]={1,2,3,4,5};
	int *P_a=a;
	cout<<"\n";
	for(int i=0;i<4;++i){
		cout<<"  "<<*P_a+i;
	}
}

void about_vector(){
	cout<<"\n";
	cout<<"关于向量的操作！"<<"\n";
	vector<float> a(5);
	for(int i=0;i<5;++i){
		a[i]=i;
		cout<<a[i];
	}
	
}
int main(){
	float a=2;
	float ha;
	float cbs;
	
	float hasha=hashaki();
	ha=cos(a);
	cbs=abs(ha);
	cout<<"hashaki"<<"\n"<<ha<<"\n"<<cbs<<"\n"<<hasha<<endl; //后面的endl是类似结束分行的意思 
	
	cout<<"\n"<<"打印类型"<<"\n";
	char cha_=char_type();
	
	know_about_const();
	about_array();
	about_string();
	about_struct();
	about_enum();
	about_pointer();
	about_pointer_calculate();
}
