#include<iostream>
#include<stdio.h>
#include<string>
#include<cmath>

using namespace std;

// 结构体
struct GuGu{
    float a;
    int age;
    string c;
};

class collections{
    public:// 没有public就不能调用函数
    float list(float a){
        return 0.0;
    }

    void dict(string key,float value){
        //print(key); 类内还不能调用外面的函数，这个在python内也是一样的啦
    }

    private:
    struct hashaki{
        int INT;
        float FLOAT;
        string STR;
    };
};

// 
void print(string str){
    cout<<str;
}


// 类的尝试
class hashaki{
    public: 
    float add(float a,float b){
        float c;
        c=a+b;
        return c;
    }

    float minus(float a,float b){
        float c;
        c=a-b;
        return c;
    }

    float mul(float a,float b){
        float c;
        c=a*b;
        return c;
    }
};

//////////////////////////////////////////////////////////////////////////////////////
int i=810;

int main(int argc, char const *argv[])
{
    print("hashaki");
    hashaki a;
    float c;
    c=a.add(1,1);
    float b;
    b=a.minus(2,1);
    cout<<'\n'<<c<<'\n'<<b<<'\n';
    int i = 0;
    int j=i;
    cout<<i<<'\t'<<j;
    for (i;i!=10;++i){
        j++;
    }
    cout<<'\n'<<i<<'\n'<<j<<'\t';
    const int bb=i;
    i=12;
    cout<<bb<<'\t'<<i<<'\n';
    return 0;
}