// exeTry.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include <cmath>

float makeArray() {
	// 生成需要训练的数据
	float _x[10] = { 1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0 };
	float _y[10] = { 2.2,3.3,4.2,2.4,3.5,3.6,1.7,2.4,3.4,2.1 };
	float *x = _x;
	float *y = _y;
	return *x, *y;
}

float calculateMeanSqrtError(float w,float *x,float b,float *y) {
	// 计算均方差,sqrt((y-(w*x)+b)^2)/n
	float sum=0;
	float mean;
	for (int i = 0; i < 10; ++i) {
		sum += sqrt(pow(*(y + i) - w* *(x+i) - b, 2));
	}
	mean = sum / 10;
	return mean;
}


int main()
{
	float *x=0;
	float *y=0;
	float w = 1.1;
	float b = 1.1;
	*x, *y = makeArray();
	float mean = calculateMeanSqrtError(w,*x,b,*y);
	std::cout << "   " << mean;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
