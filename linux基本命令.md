## linux基本命令

#### 文件操作

cp xxx/a.txt yyy/  把xxx目录下的a.txt移动到yyy目录下

#### 文档编辑

#### 文件传输

#### 磁盘管理

#### 磁盘维护

#### 网络通信

#### 系统管理

#### 系统设置

#### 备份压缩

解压：unzip filename.zip

压缩：zip -r filename.zip dirname

解包：tar zxvf filename.tar

打包：tar czvf filename.tar dirname

#### 文件安装

ubuntu安装influx : 

下载包 : wget https://dl.influxdata.com/influxdb/releases/influxdb-1.7.5.x86_64.rpm

解压rpm包 : sudo apt-get install alien

sudo alien influxdb-1.7.5.x86_64.rpm

安装.deb文件 : sudo dpkg -i influxdb_1.7.5-2_amd64.deb

