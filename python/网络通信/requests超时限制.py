# 为防止服务器不能及时响应，大部分发至外部服务器的请求都应该带着 
# timeout 参数。在默认情况下，除非显式指定了 timeout 值，requests 是不会自动进行超时处理的。
# 如果没有 timeout，你的代码可能会挂起若干分钟甚至更长时间。
import requests
# 一个单一的值作为 timeout 
try:
    requests.get('https://github.com', timeout=5)
except Exception as error:
    print(error)
'''
HTTPSConnectionPool(host='www.google.com', port=443): 
Max retries exceeded with url: / (Caused by ConnectTimeoutError
(<urllib3.connection.VerifiedHTTPSConnection object at 0x00000202C6392FD0>, 
'Connection to www.google.com timed out. (connect timeout=1)'))
'''