# 类中的__enter__和__exit__都是为了python 中的with而生，代表了with的开始与结束调用，其中__exit__参数要有3个
class H:
    def __init__(self):
        print('类初始化成功')
    def __enter__(self):
        print('这里是enter')  # 这里要return,才会有执行
    def __exit__(self,exc_type,exc_val,exc_tb):
        print('这里是exit,以及接收的参数为:{},{},{}'.format(exc_type,exc_val,exc_tb))

h=H()
>>>类初始化成功
with h as a:
    print(a)
>>>类初始化成功
>>>None
>>>这里是exit,以及接收的参数为:None,None,None