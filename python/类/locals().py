# locals()函数，返回当前位置的属性，以字典的方式返回
def func():
    arg_a, arg_b = 'a', 'b'

    def func_a():
        pass

    def func_b():
        pass

    def print_value():
        print(arg_a, arg_b)

    return locals()


if __name__ == '__main__':

    args = func()
    print(type(args))
    print(args)
'''
<class 'dict'>
{'func_a': <function func.<locals>.func_a at 0x10d8f71e0>, 'arg_b': 'b', 'arg_a': 'a', 'print_value': 
<function func.<locals>.print_value at 0x10d8f7378>,
 'func_b': <function func.<locals>.func_b at 0x10d8f72f0>}
'''