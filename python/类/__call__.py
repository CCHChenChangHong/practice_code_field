'''
__call__方法可以让类的实例的行为表现的像函数一样

'''
# 示例一
class Entity:
'''调用实体来改变实体的位置。'''

def __init__(self, size, x, y):
    self.x, self.y = x, y
    self.size = size

def __call__(self, x, y):
    '''改变实体的位置'''
    self.x, self.y = x, y

e = Entity(1, 2, 3) # 创建实例
e(4, 5) #实例可以象函数那样执行，并传入x y值，修改对象的x y 

# 示例二   __getattr__和__call__搭配使用

class A:
    def __init__(self):
        self.a
    
    def __call__(self,method,**kawargs):
        print('-------',method,kawargs)
    
    def __getattr__(self,method):
        return lambda **kawargs:self(method,**kawargs)

a=A()
a.hasaki(**locals())
'''  运行结果
------- hasaki {'__name__': '__main__', '__doc__': None, '__package__': '', '__loader__': None, '__spec__': None, 
'__file__': 'c:\\Users\\cchyp\\Desktop\\strategy\\jqdatasdk-1.6.5\\jqdatasdk-1.6.5\\jqdatasdk\\hasaki_test.py', 
    '__cached__': None, '__builtins__': {'__name__': 'builtins', '__doc__': 
    ...
    ...
    ...
'''