# 类可以作为参数传，传不同函数，都是引用
class hashaki:
    def __init__(self):
        self.a=''
    def jj(self,string):
        self.a=string
    def printf(self):
        print(self.a)

def GG(hashaki,string):
    hashaki.jj(string)
    print("In GG")
    hashaki.printf()

def GuGu(hashaki):
    hashaki.printf()
    print("In GuGu")

if __name__=='__main__':
    a=hashaki()
    GG(a,'hashaki')
    GuGu(a)