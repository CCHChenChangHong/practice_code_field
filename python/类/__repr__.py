# repr会返回类得属性，与__str__相似，不同的事，__str__面向用户，__repr__面向开发者
class H:
    def __init__(self):
        self.a='a'
    def __repr__(self):
        return "hasaki"

h=H()
print(h)
>>>hasaki
h
>>>hasaki

# 不管是print还是直接使用类本身都是可以拿到返回值，而不是返回得内存地址