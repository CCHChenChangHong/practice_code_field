class A(object):
    bar = 1
    def one(self):   # self,表示实例化类后的地址id
        print ('one') 
    @classmethod
    def two(cls):
        print ('two')
        print (cls.bar)
        cls().one()   # 调用 foo 方法

    def three():
        print("three")
 
A.one()               # 会报错，
A.two()               # 不需要实例化
A.three()             # 同样不需要实例化，但这样不规范

# ------------------------------------------------------------------------------
# build in python 3.5
# write by hashaki first edit on 20181023 last change on 20181025

class hashaki:
    def __init__(self,num):
        self.jj='????'
        self.num=num
    
    def one(self):
        print(self.num)

    @classmethod
    def two(cls):
        num=cls(3) # 看出来了吗，这是实例化
        return num # 一定要return,否则a.one()会报：AttributeError: 'NoneType' object has no attribute 'one'

if __name__=='__main__':
    a=hashaki.two()
    a.one()


#-----------------------------------------------------------------------------------------------------
# 例子3
class hashaki:
    def __init__(self,a):
        self.a=a
    
    @classmethod
    def printf(cls,num):
        a=cls(num)        # 在类内实例化类，再打印类的属性，真抽象
        print('???',a.a)

hashaki.printf(2)  # ??? 2