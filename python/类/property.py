# 如果 c 是 C 的实例化, c.x 将触发 getter,c.x = value 将触发 setter ， del c.x 触发 deleter。
class C(object):
    def __init__(self):
        self._x = None
 
    def getx(self):
        return self._x
 
    def setx(self, value):
        self._x = value
 
    def delx(self):
        del self._x
 
    x = property(getx, setx, delx, "I'm the 'x' property.")

# 修饰器用法

class C(object):
    def __init__(self):
        self._x = None
 
    @property
    def x(self):
        """I'm the 'x' property."""
        return self._x
 
    @x.setter
    def x(self, value):
        self._x = value
 
    @x.deleter
    def x(self):
        del self._x

# 使用property把内部函数当作常数来用
class hashaki:
    def __init__(self):
        self.a=1
    @property
    def ha(self):
        return self.a
    
    def jj(self):
        print(sele.ha)   # 1
        if self.ha>10:
            print("yes")
        else:
            print("no")  # no


# class中函数当作dict来使用

class T:
    def __inti__(self):
        pass
    
    @property
    def get(self)-> dict:
        return {'a':1,'b':2}
    

if __name__=='__main__':
    t=T()
    print(t.get['a'])
    # >>> 1