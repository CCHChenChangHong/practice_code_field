# build in python 3.5
# write by hashaki first edit on 20181023 last change on 20181023

from abc import ABC,abstractmethod

class hashaki(ABC):
    @abstractmethod
    def ha(self):
        '''类似C#中的，interface,继承的类必须实现该函数'''
        pass
    
    def jj(self):
        pass

class GuGu(hashaki):
    def jj(self):
        print("jj")

class domo(hashaki):
    def ha(self):
        print("hashaki")

if __name__=='__main__':
    b=domo()
    b.ha()
    a=GuGu()  # TypeError: Can't instantiate abstract class GuGu with abstract methods ha
    a.jj()