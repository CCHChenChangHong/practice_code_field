#如果访问的属性存在，就不会调用__getattr__方法,如果属性不存在，则返回__getattr__的属性
class ClassA:

    x = 'a'

    def __init__(self):
        self.y = 'b'

    def __getattr__(self, item):
        return '__getattr__'

if __name__ == '__main__':
    a = ClassA()
    # 输出结果 a
    print(a.x)

    # 使用实例直接访问实例存在的实例属性时,不会调用__getattr__方法
    # 输出结果 b
    print(a.y)
    
    # 使用实例直接访问实例不存在的实例属性时,会调用__getattr__方法
    # 输出结果 __getattr__
    print(a.z)