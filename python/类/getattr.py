# getattr() 函数用于返回一个对象属性值
class hasaki:
    def __init__(self):
        self.a=1

a=hasaki()
getattr(a,'a')   # 1
getattr(a,'b',123)  # 123