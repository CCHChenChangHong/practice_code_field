# 不用super也是可以继承，super方便多继承，py2中super(className,self).__init__()
# py3就不需要super中填东西,super().__init__()即可
class A:
    def __init__(self):
        self.a='A'
    def inClassA(self):
        print('inClassA')
    def forB(self):
        pass
class B(A):
    def __init__(self):
        super().__init__()
        self.b='B'
    def forB(self):
        print('welcome to B')

if __name__=='__main__':
    b=B()
    b.forB()
    b.inClassA()