# 使用python便准库中的types.MethodType为一个类添加新的方法，注意不是给类的实例添加方法，而是给未实例化的类添加方法
# 作为对比，这里还有一个只为实例化后的类添加函数的方法作为对比
import types


# 用来展示没有任何内容的的class
class Hasaki:
        pass
    
# 用来添加在Hasaki类的函数，     注意！ 这里函数不是在class Hasaki里面的，而是外面,并且函数参数要有self
def func1(self,a):
        self.a=a

# 用来展示的函数2                注意！，如果没有self,是无法绑定的
def func2(self,b):
        self.b=b



# 开始演示，展示演示两种方法绑定函数到类

# 1,先实例化
f1=Hasaki()                           # Hasaki第一个实例化
f2=Hasaki()                           # Hasaki第二个实例化

# 上面那两个实例化是空白的现在就给类进行添加函数
f1.func1=types.MethodType(func1,f1)            # 绑定函数到实例化类，注意这里是绑定到已经实例化的类，参数1，外部函数，参数2，实例化的类
f2.func2=types.MethodType(func1,f2)            # 如上

# 2，给没有实例化的类添加函数,使用func2作为例子，与上面做了对比
Hasaki=types.MethodType(func2,Hasaki)          # 参数1，外部函数，参数2，还没实例化的类

# 展示结果
f1.func1("f1 func1")
f2.func1("f2 func2")

Hasaki.func2("Hasaki func2")

# 下面两个变量是实例化之后赋值的
print(f1.a)                     # f1 func1
print(f2.a)                     # f2 func1
# 这两个变量是没有实例化类的赋值的
print(f1.b)                     # Hasaki func2
print(f2.b)                     # Hasaki func2