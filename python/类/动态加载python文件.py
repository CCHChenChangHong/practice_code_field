# 第一种方法
import imp
import os
strategy_path=os.getcwd()+'/wequant-quant-engine/vnpy_wq/trader/TradeModule/app/ctaStrategy/strategy/strategyBitmexMA.pyc'
module=imp.load_compiled('module',strategy_path)
CTA_json=module.CTA_json


# 第二种方法
import importlib
strategy_json_path='vnpy_wq.trader.TradeModule.app.ctaStrategy.strategy.strategyBitmexMA'
module=importlib.import_module(strategy_json_path)
CTA_json=module.CTA_json
