class one(object):
    def printf(self):
        print("one")

class two:
    def printf(self):
        print("two")

def makeChoice(one_two=False):
    if one_two:
        return one()
    else:
        return two()

if __name__=='__main__':
    hashaki=makeChoice(True)
    hashaki.printf()