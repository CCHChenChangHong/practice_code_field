# 类里的变量放dict里的用法
from collections import OrderedDict

class hashaki:
    def __init__(self):
        self.a=1
        self.b=2
        self.c=3

a=hashaki()
d=OrderedDict()
key="hashaki"
key2="haha"
d[key]=a
d[key2]=a
for i in d.items():
    print(i[0],'\n',i[1].a,'\t',i[1].b) 
'''
hashaki
 1       2
haha
 1       2
 '''

 # as the same result
 for i,k in d.items():
    print(k.a,'\t',k.b,'\t',k.c)
    print(i)