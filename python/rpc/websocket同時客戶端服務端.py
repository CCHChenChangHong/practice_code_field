import websockets
from websocket import create_connection
import asyncio
from time import sleep

a=create_connection('ws://127.0.0.1:7777')
b=create_connection('ws://127.0.0.1:7778')

async def hello(websocket,path):
    while 1:
        data1=a.recv()
        data2=b.recv()
        await websocket.send(data1)
        print('发送了data1')
        sleep(1)
        await websocket.send(data2)
        print('發送了data2')
        
start_server = websockets.serve(hello,'127.0.0.1',7779)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

'''
服務端1
import os
import sys
sys.path.append(os.getcwd())
import websockets
import asyncio
from time import sleep
import json

data='hasaki'
async def hello(websocket,path):
    i=0
    while 1:
        await websocket.send(data)
        print('发送了hasaki')

    
start_server = websockets.serve(hello,'127.0.0.1',7778)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

'''

'''
服務端2
import os
import sys
sys.path.append(os.getcwd())
import websockets
import asyncio
from time import sleep
import json

data='sudhaohoasjdpiajspidaipdj'
async def hello(websocket,path):
    i=0
    while 1:
        await websocket.send(data)
        print('发送了asdasd')

    
start_server = websockets.serve(hello,'127.0.0.1',7777)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
'''

'''
客戶端
from websocket import create_connection

a=create_connection('ws://127.0.0.1:7779')
while 1:
    print(a.recv())
'''