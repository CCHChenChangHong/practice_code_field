# 服务端
from xmlrpc.server import SimpleXMLRPCServer
s=SimpleXMLRPCServer(('127.0.0.1',6666))
def test(string):
    return string

s.register_function(test)
s.serve_forever()

'''
客户端
import xmlrpc.client
s=xmlrpc.client.ServerProxy('http://127.0.0.1:6666')
print(s.test('hasaki'))
'''