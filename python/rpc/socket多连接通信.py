import socket
from time import sleep
import multiprocessing

def A():
    s1=socket.socket()
    s2=socket.socket()
    s1.bind(('127.0.0.1',6666))
    s2.bind(('127.0.0.1',8888))
    s1.listen(1)
    s2.listen(1)
    client1,addr1=s1.accept()
    client2,addr2=s2.accept()
    while 1:
        client1.sendall('服务端6666发出去的'.encode())
        client2.sendall('服务端8888发出去的'.encode())
        data1=client1.recv(2000).decode()
        if len(data1)==0:
            pass
        else:    
            print(data1)
        
# ---------------------------------------------------
def B():
    c1=socket.socket()
    c1.connect(('127.0.0.1',6666))
    while 1:
        data=c1.recv(2000).decode()
        if len(data)==0:
            pass
        else:
            print(data)
        c1.send("客户端再发送".encode())
        sleep(0.5)

def C():
    c2=socket.socket()
    c2.connect(('127.0.0.1',8888))
    while 1:
        data=c2.recv(2000).decode()
        if len(data)==0:
            pass
        else:
            print('只接受数据的客户端',data)
        sleep(0.7)

if __name__=='__main__':
    p1=multiprocessing.Process(target=A)
    p2=multiprocessing.Process(target=B)
    p3=multiprocessing.Process(target=C)
    p1.start()
    p2.start()
    p3.start()