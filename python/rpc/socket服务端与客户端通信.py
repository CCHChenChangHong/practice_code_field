import socket
from time import sleep 
import multiprocessing
def A():
    s1=socket.socket()
    s1.bind(('127.0.0.1',6666))
    s1.listen(2)
    client,addr=s1.accept()
    while 1:
        client.sendall('服务端发出去的'.encode())
        sleep(1)
        data=client.recv(2000).decode()
        if len(data)==0:
            pass
        else:    
            print(data)
        
# ---------------------------------------------------
def B():
    s2=socket.socket()
    s2.connect(('127.0.0.1',6666))
    while 1:
        data=s2.recv(2000).decode()
        if len(data)==0:
            pass
        else:
            print(data)
        s2.send("客户端再发送".encode())

if __name__=='__main__':
    p1=multiprocessing.Process(target=A)
    p2=multiprocessing.Process(target=B)
    p1.start()
    p2.start()