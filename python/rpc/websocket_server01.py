import os
import sys
sys.path.append(os.getcwd())
import websockets
import asyncio
from time import sleep
import json
trade_dict={
    "table":"trade",
    "action":"insert",
    "data":[
        {
            "timestamp":"2018-12-28T08:18:47.523Z",
            "symbol":"XBTUSD",
            "side":"Sell",
            "size":10,
            "price":3605,
            "tickDirection":"ZeroMinusTick",
            "trdMatchID":"92b3e28b-e42d-9a04-92e0-13d23a2663dc",
            "grossValue":277390,
            "homeNotional":0.0027739,
            "foreignNotional":10
        }
    ]
}
orderBook10_dict={
    "table":"orderBook10",
    "action":"update",
    "data":[
        {
            "symbol":"XBTUSD",
            "asks":[
                [
                    3606.5,
                    3528349
                ],
                [
                    3607,
                    247616
                ],
                [
                    3607.5,
                    91766
                ],
                [
                    3608,
                    272552
                ],
                [
                    3608.5,
                    57527
                ],
                [
                    3609,
                    365700
                ],
                [
                    3609.5,
                    28709
                ],
                [
                    3610,
                    172901
                ],
                [
                    3610.5,
                    56020
                ],
                [
                    3611,
                    82525
                ]
            ],
            "timestamp":"2018-12-28T08:22:41.733Z",
            "bids":[
                [
                    3606,
                    701294
                ],
                [
                    3605.5,
                    344466
                ],
                [
                    3605,
                    131327
                ],
                [
                    3604.5,
                    41713
                ],
                [
                    3604,
                    46201
                ],
                [
                    3603.5,
                    20561
                ],
                [
                    3603,
                    34191
                ],
                [
                    3602.5,
                    50171
                ],
                [
                    3602,
                    135324
                ],
                [
                    3601.5,
                    64680
                ]
            ]
        }
    ]
} 
contract={
    "table":"instrument",
    "action":"update",
    "data":[
  {
    "symbol": "XBTUSD",
    "rootSymbol": "string",
    "state": "string",
    "typ": "string",
    "listing": "2019-01-07T03:55:37.921Z",
    "front": "2019-01-07T03:55:37.921Z",
    "expiry": "2019-01-07T03:55:37.921Z",
    "settle": "2019-01-07T03:55:37.921Z",
    "relistInterval": "2019-01-07T03:55:37.921Z",
    "inverseLeg": "string",
    "sellLeg": "string",
    "buyLeg": "string",
    "optionStrikePcnt": 0,
    "optionStrikeRound": 0,
    "optionStrikePrice": 0,
    "optionMultiplier": 0,
    "positionCurrency": "string",
    "underlying": "string",
    "quoteCurrency": "string",
    "underlyingSymbol": "string",
    "reference": "string",
    "referenceSymbol": "string",
    "calcInterval": "2019-01-07T03:55:37.921Z",
    "publishInterval": "2019-01-07T03:55:37.921Z",
    "publishTime": "2019-01-07T03:55:37.921Z",
    "maxOrderQty": 0,
    "maxPrice": 0,
    "lotSize": 1,
    "tickSize": 0.001,
    "multiplier": 0,
    "settlCurrency": "string",
    "underlyingToPositionMultiplier": 0,
    "underlyingToSettleMultiplier": 0,
    "quoteToSettleMultiplier": 0,
    "isQuanto": True,
    "isInverse": True,
    "initMargin": 0.1,
    "maintMargin": 0.1,
    "riskLimit": 0,
    "riskStep": 0,
    "limit": 0,
    "capped": True,
    "taxed": True,
    "deleverage": True,
    "makerFee": 0,
    "takerFee": 0,
    "settlementFee": 0,
    "insuranceFee": 0,
    "fundingBaseSymbol": "string",
    "fundingQuoteSymbol": "string",
    "fundingPremiumSymbol": "string",
    "fundingTimestamp": "2019-01-07T03:55:37.921Z",
    "fundingInterval": "2019-01-07T03:55:37.921Z",
    "fundingRate": 0,
    "indicativeFundingRate": 0,
    "rebalanceTimestamp": "2019-01-07T03:55:37.921Z",
    "rebalanceInterval": "2019-01-07T03:55:37.921Z",
    "openingTimestamp": "2019-01-07T03:55:37.921Z",
    "closingTimestamp": "2019-01-07T03:55:37.921Z",
    "sessionInterval": "2019-01-07T03:55:37.921Z",
    "prevClosePrice": 0,
    "limitDownPrice": 0,
    "limitUpPrice": 0,
    "bankruptLimitDownPrice": 0,
    "bankruptLimitUpPrice": 0,
    "prevTotalVolume": 0,
    "totalVolume": 0,
    "volume": 0,
    "volume24h": 0,
    "prevTotalTurnover": 0,
    "totalTurnover": 0,
    "turnover": 0,
    "turnover24h": 0,
    "homeNotional24h": 0,
    "foreignNotional24h": 0,
    "prevPrice24h": 0,
    "vwap": 0,
    "highPrice": 0,
    "lowPrice": 0,
    "lastPrice": 0,
    "lastPriceProtected": 0,
    "lastTickDirection": "string",
    "lastChangePcnt": 0,
    "bidPrice": 0,
    "midPrice": 0,
    "askPrice": 0,
    "impactBidPrice": 0,
    "impactMidPrice": 0,
    "impactAskPrice": 0,
    "hasLiquidity": True,
    "openInterest": 0,
    "openValue": 0,
    "fairMethod": "string",
    "fairBasisRate": 0,
    "fairBasis": 0,
    "fairPrice": 0,
    "markMethod": "string",
    "markPrice": 0,
    "indicativeTaxRate": 0,
    "indicativeSettlePrice": 0,
    "optionUnderlyingPrice": 0,
    "settledPrice": 0,
    "timestamp": "2019-01-07T03:55:37.921Z"
  }
]
}
trade=json.dumps(trade_dict)
orderBook10=json.dumps(orderBook10_dict)
contract_j=json.dumps(contract)

async def hello(websocket,path):
    i=0
    while 1:
        if websocket.recv()=='trade':
            await websocket.send(trade)
            print('发送了trade')

        elif websocket.recv()=='orderBook10':
            await websocket.send(orderBook10)
            print('发送了orderBook10')

        await websocket.send(trade)
        print('发送了trade')

        sleep(1)

        await websocket.send(orderBook10)
        print('发送了orderBook10')

        if i==0:
            await websocket.send(contract_j)
            i+=1
        
    
start_server = websockets.serve(hello,'127.0.0.1',7777)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
