import os
# 进程的环境名字
print(os.environ.get('SHELL'))
# /bin/bash

# 获取当前系统环境变量
print(os.environ)
print(os.environ["PATH"])
print(os.environ.keys())

# 创建临时系统变量
os.environ["TEMP"]=os.getcwd()
'''
os.environ['HOMEPATH']:当前用户主目录。
os.environ['TEMP']:临时目录路径。
os.environ[PATHEXT']:可执行文件。
os.environ['SYSTEMROOT']:系统主目录。
os.environ['LOGONSERVER']:机器名。
os.environ['PROMPT']:设置提示符。
'''

'''
os.environ['HOMEPATH']:当前用户主目录。
os.environ['TEMP']:临时目录路径。
os.environ[PATHEXT']:可执行文件。
os.environ['SYSTEMROOT']:系统主目录。
os.environ['LOGONSERVER']:机器名。
os.environ['PROMPT']:设置提示符。
'''