# build in python 3.5
# write by hashaki first edit on 20181023 last change on 20181023
# OS.path模块的使用  http://www.runoob.com/python3/python3-os-path.html
import os

path=os.getcwd()
# 取文件的属性
a=os.path.abspath(path)            # 返回绝对路径
b=os.path.basename(path)           # 返回文件名
c=os.path.dirname(path)            # 返回文件路径   os.path.dirname(__file__)返回当前文件的目录，這個方法可以放在啓動文件去獲取系統環境變量
d=os.path.exists(path)             # 路径存在则返回True,路径损坏返回False
e=os.path.lexists(path)            # 路径存在则返回True,路径损坏也返回True
f=os.path.getatime(path)           # 返回最近访问时间（浮点型秒数）
g=os.path.getmtime(path)           # 返回最近文件修改时间
h=os.path.getctime(path)           # 返回文件 path 创建时间
i=os.path.getsize(path)            # 返回文件大小，如果文件不存在就返回错误
j=os.path.isabs(path)              # 判断是否为绝对路径
k=os.path.realpath(path)           # 返回path的真实路径
l=os.path.normcase(path)           # 规范化路径，就是window下把 \转成\\
print(a,b,c,d,e,f,g,h,i,j,k)
#/home/hashaki/下载/vnpy_crypto vnpy_crypto /home/hashaki/下载 True True 1540276961.262741 1540276957.494868 1540276957.494868 4096 True /home/hashaki/下载/vnpy_crypto