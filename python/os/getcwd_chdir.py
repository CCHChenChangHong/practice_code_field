import os

file='/home/hashaki/桌面'

# 查看当前目录，不是運行文件的目錄，而是python啓動二進制文件的目錄(命令行運行的情況下)
a=os.getcwd()
print(a)             # /home/hashaki/下载/vnpy_crypto

# 改变当前工作目录到指定的路径
os.chdir(file)

a=os.getcwd()
print(a)            # /home/hashaki/桌面