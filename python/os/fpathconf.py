import os

file='/home/hashaki/桌面/RL.pdf'

# 用于返回一个打开的文件的系统配置信息,Unix 平台下可用
# 打开文件
fd = os.open( "file", os.O_RDWR|os.O_CREAT )

print ("%s" % os.pathconf_names)

# 获取文件最大连接数
no = os.fpathconf(fd, 'PC_LINK_MAX')
print ("Maximum number of links to the file. :%d" % no)  # 65000

# 获取文件名最大长度
no = os.fpathconf(fd, 'PC_NAME_MAX')
print ("Maximum length of a filename :%d" % no)      # 255

# 关闭文件
os.close( fd)