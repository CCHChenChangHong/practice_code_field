# 实现功能：在列表中放入100个元素作为总数据，然后每次往队列放入10个数据，供engine函数使用，使用完再请求feed继续放入数据

from queue import Queue

database=[]
for i in range(100):
    database.append(i)

def feed(q,start,end):
        data=database[start:end]
        q.put(data)
        print("已放入数据")

def engine(n,q):
    while n:
        if q.empty():
            print("没有数据了")
            return True
        data=q.get()
        for i in data:
            print(i)
        print("读完了数据")

def manager():
    q=Queue()
    len_=len(database)
    start=0
    while 1:
        n=True
        end=start+10
        if end >len_:
            print("读完数据")
            break
        feed(q,start,end)
        engine(n,q)
        start=end

manager()