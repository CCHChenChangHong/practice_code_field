from queue import Queue
from time import sleep

class hashaki:
    def __init__(self):
        self.active=True
        self.q=Queue()
        self.counter=0
    
    def into(self):
        while self.active:
            sleep(1)
            self.q.put("hashaki")
            self.out()

    def out(self):
        while self.active:
            print(self.q.get())
            self.counter+=1
            if self.counter==5:
                self.active=False
            if self.q.empty:
                break
            
a=hashaki()
a.into()
