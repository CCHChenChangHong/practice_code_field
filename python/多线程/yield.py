# yield的用法类似return，但是与return不一样，return不会再执行下一步，直接退出函数，yield则会继续执行下一步
import time

def a():
        print('a')
        while True:
                yield 'hasaki'
                print('balala')

for i in a():
        print(i)
        time.sleep(1)
'''
结果：
a
hasaki
--------这里睡一秒
balala
hasaki
--------这里睡一秒
balala
hasaki
.
.
.
'''