from queue import Queue

q=Queue()
print(q.empty())  # True
print(q.full())   # False
q.put("hashaki")
print(q.qsize())  # 1
print(q.get())    # hashaki