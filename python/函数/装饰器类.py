class Hasaki:
    def __init__(self):
        self.a=1
    
    def __call__(self,func):     # 当然是依靠__call__
        print('__call__打印',self.a)
        def insideCall():
            func(self.a)
        return insideCall

    def hasaki(self):
        print('hsaki')

@Hasaki()
def b(num):
    print('b函数打印',num)

b()
>>>__call__打印 1
b函数打印 1
