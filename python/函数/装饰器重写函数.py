from functools import wraps

def a(func):
    @wraps(func)
    def insideA():
        print('inside A')
        func()
    return insideA

@a
def b():
    print('这是b函数')

b=a(b)

# 不加wraps
print(a.__name__)   # a
print(b.__name__)   # insideA  它已经不是b了，而是insideA()

# 加wraps
print(a.__name__)   # a
print(b.__name__)   # b