def a(func):
    def insdideA():
        print('insideA 1')
        func()
        print('insideA 2')
    return insdideA

def b(func):
    def insideB():
        print('insideB 1')
        func()
        print('insideB 2')
    return insideB

@b
@a
def c():
    print('hasaki')

c()
>>>
insideB 1
insideA 1
hasaki
insideA 2
insideB 2