def a(func):
    def insideA():
        print('inside A')
        func()
    return insideA

def b():
    print('这是b函数')

b=a(b)

b()     # 它已经不是b了，而是insideA()
>>>insideA
>>>这是b函数

del a

b()
>>>insideA
>>>这是b函数