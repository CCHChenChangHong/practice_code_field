def a():      # 随便定义一个函数
    print('hasaki')

a()
>>>hasaki
b=a           # 指针传递

del a         # 删除a

try:
    a()
except Exception as error:
    print(error)
>>> name 'a' is not defined
b()           # b依旧存在
>>>hasaki