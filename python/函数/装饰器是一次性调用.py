class Hasaki:
    def __init__(self):
        self.a=[1]
    
    def __call__(self,func):     # 当然是依靠__call__
        print('__call__打印',self.a)
        def insideCall():
            func(self.a)
        return insideCall

    def hasaki(self):
        print('hsaki',self.a)

@Hasaki()
def b(num):
    Hasaki().a.append(2)
    print('b函数打印',num)
    Hasaki().hasaki()       # self.a并没有改变，也就是说，装饰器类是一次性调用

b()
>>>__call__打印 [1]
b函数打印 [1]
hsaki [1]

