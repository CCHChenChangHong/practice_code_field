# 从  不是用装饰器实现装饰器的功能.py扩展 
def a(func):
    def insideA():
        print('inside A')
        func()
    return insideA

# 这里的 @a 表示：b=a(b)
@a
def b():
    print('这是b函数')

b()
>>>inside A
>>>这是b函数