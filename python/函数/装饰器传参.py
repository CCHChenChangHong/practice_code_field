def hasaki(name='hasaki'):
    def a(func):
        def insideA():
            func(name)
        return insideA
    return a

@hasaki('陈常鸿')
def b(string):
    print('b函数打印',string)

b()  # 如果b函数传参string，会报错：TypeError: insideA() takes 0 positional arguments but 1 was given
>>>b函数打印 陈常鸿