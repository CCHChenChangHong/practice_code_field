# build in python3.6
# 作者：陈常鸿
# 常鸿量化机，为快乐而生 0.1v纪念
from gateway.gategateway import GateGateway
from gateway.zbgateway import ZBGateway
from gateway.huobigateway import huobigateway
from gateway.okexgateway import Okexgateway
from examples.lifeforfun import LiveForFun
import time
from multiprocessing import Process

#--------------------------------------------------------------------
# 统一买进
def buy(COIN_NAME):
    pass

#--------------------------------------------------------------------
def sell(COIN_NAME):
    '''统一卖出'''
    pass

#--------------------------------------------------------------------
def cancelOrder():
    '''统一取消交易'''
    pass

#--------------------------------------------------------------------
def depth(choice,COIN_NAME,depthNum):
    '''统一深度数据'''
    if choice==1:
        print("连接gate")
        api_gate = GateGateway()
        api_gate.connect()
        while 1:
            api_gate.onMarketDepth(COIN_NAME,depthNum)
            time.sleep(3)
    elif choice==2:
        print("连接zb")
        api_zb = ZBGateway()
        api_zb.connect()
        while 1:
            api_zb.qryMarketDepth(COIN_NAME,depthNum)
            time.sleep(3)

#--------------------------------------------------------------------
def Kline(COIN_NAME):
    '''统一K线数据'''
    pass

if __name__=='__main__':
    print("************************************************")
    print("\n \n \n")
    print("           常鸿量化机，为快乐而生")
    print("\n \n \n")
    print("************************************************")
    gateProcess=Process(target=depth,args=(1,'btc_usdt',10))
    zbProcess=Process(target=depth,args=(2,'btc_qc',10))
    gateProcess.start()
    zbProcess.start()
    stopOrder=input("是否终止程序：[y/n]")
    if stopOrder=='y':
        if gateProcess.is_alive:
            gateProcess.terminate()
            print("gate停止")
            gateProcess.join()
        elif zbProcess.is_alive:
            zbProcess.terminate()
            print("zb停止")
            zbProcess.join()