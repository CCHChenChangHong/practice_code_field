from api.okex.OkcoinSpotAPI import OKCoinSpot
from api.okex.OkcoinFutureAPI import OKCoinFuture

class Okexgateway:
    def __init__(self):
        self.okcoinRESTURL = 'www.okcoin.com'   #请求注意：国内账号需要 修改为 www.okcoin.cn
        self.qryEnable = False
        self.filePath_write=''

    #-----------------------------------------------------------------------------------------
    def connet(self):
        '''连接交易所'''
        self.API_KEY=''
        self.SCRETE_KEY=''
        self.api_Spot=OKCoinSpot(self.okcoinRESTURL,self.API_KEY,self.SCRETE_KEY)
        self.api_Future=OKCoinFuture(self.okcoinRESTURL,self.API_KEY,self.SCRETE_KEY)

    # -----------------------------------------------------------------------------------------
    def writeLog(self, log, FILE_NAME):
        '''日志写入'''
        GATE_LOG_PATH = FILE_NAME  # 这里应该统一把变量放到一个文件内
        f = open(self.filePath_write + GATE_LOG_PATH, 'a')
        f.write(log)
        f.close()

    #------------------------------------------------------------------------------------------
    def loadData(self, FILE_NAME, data):
        '''保存数据到指定文件夹'''
        f = open(self.filePath_write + FILE_NAME, 'a')
        f.write(data)
        f.close()

    #------------------------------------------------------------------------------------------
    def readData(self,FILE_NAME):
        '''从数据储存位置读取数据'''
        f = open(self.filePath_write + FILE_NAME, 'r')
        f.read()  # 这里存疑，记得验证
        f.close()

    #------------------------------------------------------------------------------------------
    def qryDepth(self,COIN_NAME):
        '''深度信息，OK的深度查询没有深度层数的选择'''
        return self.api_Spot.depth(COIN_NAME)

    #-----------------------------------------------------------------------------------------
    def qryKLine(self,COIN_NAME):
        '''K线信息'''
        return self.api_Spot.ticker(COIN_NAME)

    #------------------------------------------------------------------------------------------
    def buy(self,oneIsSpot_twoIsFuture):
        '''买进'''
        if oneIsSpot_twoIsFuture==1:
            print("现货买进")
        elif oneIsSpot_twoIsFuture==2:
            print("期货买进")
        else:
            print("输入参数不正确，现货输入1，期货输入2")
            return 0

    #------------------------------------------------------------------------------------------
    def sell(self,oneIsSpot_twoIsFuture):
        '''卖出'''
        if oneIsSpot_twoIsFuture==1:
            print("现货卖出")
        elif oneIsSpot_twoIsFuture==2:
            print("期货卖出")
        else:
            print("输入参数不正确，现货输入1，期货输入2")
            return 0

    #------------------------------------------------------------------------------------------
    def cancelOrder(self):
        '''撤单'''
        pass