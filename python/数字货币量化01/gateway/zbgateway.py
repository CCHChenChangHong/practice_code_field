from api.zb.RestAPI import ZbAPI

class ZBGateway:
    def __init__(self):
        self.qryEnable = False  # 开启循环查询
        self.filePath_write = ''  # 保存文件的路径
        self.filePath_read = ''  # 文件读取路径

    #---------------------------------------------------------------------------------
    def writeLog(self,log,FILE_NAME):
        '''日志写入'''
        GATE_LOG_PATH=FILE_NAME      # 这里应该统一把变量放到一个文件内
        f=open(self.filePath_write+GATE_LOG_PATH,'a')
        f.write(log)
        f.close()

    #--------------------------------------------------------------------------------
    def connect(self,API_KEY=None,SCRETE_KEY=None):
        '''连接交易所'''
        self.API_KEY = API_KEY
        self.SCRETE_KEY = SCRETE_KEY
        self.api=ZbAPI(self.API_KEY,self.SCRETE_KEY)
        try:
            print(self.api.market('btc_qc'))
            print("成功连接到ZB！")
        except Exception as error:
            print("出现错误：",error)

    #----------------------------------------------------------------------------------
    def qryMarketDepth(self,COIN_NAME,numberDepth):
        '''获取市场深度'''
        print(self.api.depth(COIN_NAME,str(numberDepth)))

    #----------------------------------------------------------------------------------
    def qryKline(self,COIN_NAME):
        '''获取K线'''
        print(self.api.K_line(COIN_NAME))