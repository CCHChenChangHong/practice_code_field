from api.gate.gateAPI import GateIO
import time

class GateGateway:
    def __init__(self):
        self.qryEnable=False        # 开启循环查询
        self.filePath_write=''      # 保存文件的路径
        self.filePath_read=''       # 文件读取路径
        self.haveKey=False          # 判断有没有Key
        self.API_QUERY_URL = 'data.gateio.io'
        self.API_TRADE_URL = 'api.gateio.io'

    #-------------------------------------------------------------------------------------------
    def connect(self,API_KEY=None,SCRETE_KEY=None):
        '''连接交易所,一定要先调用这个函数才使用其他查询功能，在这个函数中输入KEY的'''
        if API_KEY is None or SCRETE_KEY is None:
            self.api = GateIO(self.API_QUERY_URL,'','')
            self.haveKey=False
            try:
                print(self.api.pairs()[0])
            except Exception as error:
                print("链接失败，原因：",error)    # 让交易所返回

        elif API_KEY is not None and SCRETE_KEY is not None:
            self.api=GateIO(self.API_TRADE_URL,API_KEY,SCRETE_KEY)
            try:
                print("账户连接成功!可以使用所有功能")
                print(self.api.balances())  # 获取账号余额，判断是否KEY正确
                self.API_KEY=API_KEY
                self.SCRETE_KEY=SCRETE_KEY
                self.haveKey=True
            except Exception as error:
                print("账号没有连接成功，出错原因：", error)

    #-------------------------------------------------------------------------------------------
    def initQuery(self):
        '''初始化查询功能'''
        if self.qryEnable:
            self.qryCount = 0  # 查询触发倒计时
            self.qryTrigger = 1  # 查询触发点
            self.qryNextFunction = 0  # 上次运行的查询函数索引

    #-----------------------------------------------------------------------------------------
    def writeLog(self,log,FILE_NAME):
        '''日志写入'''
        GATE_LOG_PATH=FILE_NAME      # 这里应该统一把变量放到一个文件内
        f=open(self.filePath_write+GATE_LOG_PATH,'a')
        f.write(log)
        f.close()

    #------------------------------------------------------------------------------------------
    def onMarketDepth(self,COIN_NAME,depthNum):
        '''市场深度'''
        result=self.api.orderBooks()[COIN_NAME]
        if result['result']=='true':
            print("市场深度获取成功")
        else:
            print("市场深度获取失败")
            return 0

        # 展示深度,因为传来的数据是从最后一个买卖单开始的，所以需要reverse()
        bids=result['bids']
        asks=result['asks']
        asks.reverse()          # .reverse()不能赋值，否则会出现TypeError
        # bids=bids.reverse()   # ask默认是从大到小，符合要求不需要颠倒
        # 返回数据形式：[价格 数量]
        print("买单:",bids[:depthNum])
        print("卖单：",asks[:depthNum])

    #------------------------------------------------------------------------------------------
    def qryTime(self):
        '''每次查询的间隔'''
        time.sleep(1)

    #------------------------------------------------------------------------------------------
    def qryKLine(self,COIN_NAME,hour=None,sec=None):
        '''K线查询,K线返回的时间戳是毫秒级的'''
        result=self.api.kLine(COIN_NAME,sec,hour)
        if result['result']=='true':
            print("成功获取数据到K线")
        else:
            print("没有成功获取到K线数据")
            return 0
        print(result['data'])
        # return result['data']

    #-----------------------------------------------------------------------------------------
    def loadData(self,FILE_NAME,data):
        '''保存数据到指定文件夹'''
        f = open(self.filePath_write + FILE_NAME, 'a')
        f.write(data)
        f.close()

    #-----------------------------------------------------------------------------------------
    def readData(self,FILE_NAME):
        '''从数据储存位置读取数据'''
        f = open(self.filePath_write + FILE_NAME, 'r')
        f.read()        # 这里存疑，记得验证
        f.close()

    # 查询基本信息
    #-----------------------------------------------------------------------------------------
    def qryBaseInfo(self):
        if self.haveKey:
            print(self.api.balances())
        else:
            print("没有在connet()中输入KEY，所以该方法不可用")

    #-----------------------------------------------------------------------------------------
    def buy(self):
        '''买进'''
        pass

    #-----------------------------------------------------------------------------------------
    def sell(self):
        '''卖出去'''
        pass

    #-----------------------------------------------------------------------------------------
    def cancelOrder(self):
        '''撤单'''
        pass