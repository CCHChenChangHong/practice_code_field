# 所有设置加载的接口，比如交易所的API_KEY
import os
import json

class loadUserSetting:
    def __init__(self):
        print("加载用户数据")

    #--------------------------------------------------------------------
    def loadJsonSetting(self,EXCHANGE_NAME):
        '''从json文件加载用户信息'''
        f=open('UserSetting.json',encoding='utf-8')
        setting=json.load(f)
        exchangeAPI=setting[EXCHANGE_NAME]
        API_KEY=exchangeAPI['API_KEY']
        SCRETE_KEY=exchangeAPI['SCRETE_KEY']
        return API_KEY,SCRETE_KEY