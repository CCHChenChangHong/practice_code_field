# build in python 3.6
# 作者：陈常鸿
# 0.1v,常鸿量化机，为快乐而生
# 第一层交易所API，第二层gateway,第三层把gateway封装起来供最上层统一调用
from gateway.gategateway import GateGateway
from gateway.zbgateway import ZBGateway
from gateway.huobigateway import huobigateway
from gateway.okexgateway import Okexgateway

class LiveForFun:
    def __init__(self):
        self.api_gate=GateGateway()
        self.api_gate.connect()
        self.api_zb=ZBGateway()
        self.api_huobi=huobigateway()
        self.api_ok=Okexgateway()

    #------------------------------------------------------------------
    def showDepth(self,COIN_NAME):
        '''查看所有交易所的深度数据'''
        self.api_gate.onMarketDepth(COIN_NAME,10)
        self.api_zb.qryMarketDepth(COIN_NAME,10)
        pass

    #------------------------------------------------------------------
    def buy(self):
        '''买买买'''
        pass

    #------------------------------------------------------------------
    def sell(self):
        '''抛抛抛'''
        pass

