#  直接用python-binance库
#  该库有完整的文档：https://python-binance.readthedocs.io/en/latest/index.html
#  支持python2.7,3.4,3.5,3.6
#  调用频率：每分钟1200次requests,每秒10orders，每天100000次orders
from binance.client import Client
from binance.enums import *
API_KEY='nxJuh20zUxNCsggYQ31kMnf2iKMTmzijc3a8B3yiq8BhsDLDylIvgu2KVZ4hz8UG'
API_SECRET='x9NTlrwQFHd5OKLahmkhdzSA64ckmy9M19Eik0XOiyxPUNbaerruIkijZfnkLsY6'
# 初始化client
client=Client(API_KEY,API_SECRET)

class GeneralEndpoint:
    def __init__(self,COIN_NAME):
        self.COIN_NAME=COIN_NAME

    def pingTheServer(self):
        return client.ping()

    def getTheServerTime(self):
        return client.get_server_time()

    def getSystemStatus(self):
        return client.get_system_status()

    def getExchangeInfo(self):
        return client.get_exchange_info()

    def getSymbolInfo(self):
        return client.get_symbol_info(self.COIN_NAME)

class DataEndpoint:
    def __init__(self,COIN_NAME,interval):
        self.COIN_NAME=COIN_NAME
        self.interval=interval        # 获取K线的数据间隔

    def getMarketDepth(self):
        return client.get_order_book(symbol=self.COIN_NAME)

    def getRecentTrades(self):
        return client.get_recent_trades(symbol=self.COIN_NAME)

    def getHistoricalTrades(self):
        return client.get_historical_trades(symbol=self.COIN_NAME)

    def getAggregateTrades(self):
        return client.get_aggregate_trades(symbol=self.COIN_NAME)

    def getKline(self):
        return client.get_klines(symbol=self.COIN_NAME, interval=self.interval)

    def getHistoricalKline(self):
        return client.get_historical_klines(self.COIN_NAME,self.interval,"1 day ago UTC")
        #return client.get_historical_klines(self.COIN_NAME, self.interval, "1 Jan, 2017")
        #return client.get_historical_klines(self.COIN_NAME,self.interval,"1 Dec, 2017", "1 Jan, 2018")

    # 用生成器获得历史K线
    def getHistoricalKlineGenerator(self):
        for kline in client.get_historical_klines_generator(self.COIN_NAME, self.COIN_NAME, "1 day ago UTC"):
            print(kline)
            # do something with the kline

    def get24hTicker(self):
        return client.get_ticker()

    # Get last price for all markets.
    def getAllPrices(self):
        return client.get_all_tickers()

    # Get first bid and ask entry in the order book for all markets.
    def getOrderTickers(self):
        return client.get_orderbook_tickers()

class Orders:
    def __init__(self,COIN_NAME):
        self.COIN_NAME=COIN_NAME
        self.amount=0.000234234   # 官方文档用这两个数来format
        self.precision=5

    # 获取所有订单
    def fetchAllOrders(self,limit):
        return client.get_all_orders(symbol=self.COIN_NAME, limit=limit)

    # 下单
    def placeAnOrder(self,SIDE_BUY,ORDER_TYPE_LIMIT,TIME_IN_FORCE_GTC):
        return client.create_order(symbol='BNBBTC',side=SIDE_BUY,type=ORDER_TYPE_LIMIT,timeInForce=TIME_IN_FORCE_GTC,quantity=100,price='0.00001')

    # 参数示例：symbol='BNBBTC',quantity=100,price='0.00001'
    def limitOrder(self,order,quantity,price):
        if order=='buy':
            return client.order_limit_buy(symbol=self.COIN_NAME,quantity=quantity,price=price)

        if order=='sell':
            return client.order_limit_sell(symbol=self.COIN_NAME,quantity=quantity,price=price)

        else:
            print("%s 参数有误,要输入字符buy或者sell" % order)

    # 参数示例：symbol='BNBBTC',quantity=100
    def marketOrder(self,order,quantity):
        if order=='buy':
            return client.order_market_buy(symbol=self.COIN_NAME,quantity=quantity)

        if order == 'sell':
            return client.order_market_sell(symbol=self.COIN_NAME, quantity=quantity)

        else:
            print("%s 参数有误,要输入字符buy或者sell" % order)

    # 参数示例：symbol='BNBBTC',side=SIDE_BUY,type=ORDER_TYPE_LIMIT,timeInForce=TIME_IN_FORCE_GTC,quantity=100,price='0.00001'
    def testOrder(self):
        return client.create_test_order(symbol='BNBBTC',side=SIDE_BUY,type=ORDER_TYPE_LIMIT,timeInForce=TIME_IN_FORCE_GTC,quantity=100,price='0.00001')

    # 参数示例：symbol='BNBBTC',orderId='orderId'
    def checkOrderStatus(self,orderId):
        return client.get_order(symbol=self.COIN_NAME,orderId=orderId)

    # 取消订单
    def cancelOrder(self,id):
        return client.cancel_order(symbol=self.COIN_NAME,orderId=id)

    #
    def allOpenOrder(self):
        return client.get_open_orders(symbol=self.COIN_NAME)

    def allOrder(self):
        return client.get_all_orders(symbol=self.COIN_NAME)


class Account:
    def __init__(self,COIN_NAME):
        self.COIN_NAME=COIN_NAME

    def accountInfo(self):
        return client.get_account()

    # 参数示例：asset='BTC'
    def assetBalance(self,asset):
        return client.get_asset_balance(asset=asset)

    def accountStatus(self):
        return client.get_account_status()

    def getTrades(self):
        return client.get_my_trades(symbol=self.COIN_NAME)

    def tradesFees(self,trades=None):
        if trades is None:
            return client.get_trade_fee()
        if trades is not None:
            return client.get_trade_fee(symbol=self.COIN_NAME)

    def assetDetials(self):
        return client.get_asset_details()

    def dustLog(self):
        return client.get_dust_log()
