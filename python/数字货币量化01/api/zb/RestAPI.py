# build in python3.5.2
# 作者：陈常鸿
# zb.com里虚拟币的行情获取
# 1.单个IP限制每分钟1000次访问，超过1000次将被锁定1小时，一小时后自动解锁
# 2.单个用户限制每秒钟30次访问，一秒钟内30次以上的请求，将会视作无效
# 3.K线接口每秒只能请求一次数据
# zb.com官方API文档：https://www.zb.cn/i/developer/restApi#trade
import requests

class ZbAPI:
    def __init__(self,API_KEY,SCRETE_KEY):
        self.API_KEY=API_KEY
        self.SCRETE_KEY=SCRETE_KEY
        self.__URL='http://api.zb.cn/data/v1/'

    # priceScale : 价格小数位数
    # amountScale : 数量小数位数
    # 参数说明：token='btc_qc'
    def market(self,token):
        return requests.post(self.__URL+'markets').json()[token]

    # 全币种行情
    # 第二参数：代币符号qc or 代币符号usdt
    # high : 最高价
    # low : 最低价
    # buy : 买一价
    # sell : 卖一价
    # last : 最新成交价
    # vol : 成交量(最近的24小时)
    # 参数说明：token="btcqc"
    def allTicker(self,token):
        return requests.post(self.__URL+'allTicker').json()[token]

    # 单币种行情
    # 第二参数：代币符号_qc or 代币符号_usdt
    # high : 最高价
    # low : 最低价
    # buy : 买一价
    # sell : 卖一价
    # last : 最新成交价
    # vol : 成交量(最近的24小时)
    # 参数说明：token='btc_qc'
    def ticker(self,token):
        return requests.post(self.__URL+'ticker?market=' + token).json()

    # 市场深度
    # 第二参数为代币符号_qc，第三参数为深度大小size='n'
    # asks : 卖方深度
    # bids : 买方深度
    # timestamp : 此次深度的产生时间戳
    def depth(self,token,size):
        return requests.post(self.__URL+'depth?market=' + token + '&' + 'size=' + size).json()

    # 历史成交
    # 第二参数为代币符号_qc
    # date : 交易时间(时间戳)
    # price : 交易价格
    # amount : 交易数量
    # tid : 交易生成ID
    # type : 交易类型，buy(买)/sell(卖)
    # trade_type : 委托类型，ask(卖)/bid(买)
    def trades(self,token):
        return requests.post(self.__URL+'trades?market=' + token).json()

    # K线
    # 第二参数：代币符号_qc
    # data : K线内容 返回dict类型
    # moneyType : 买入货币
    # symbol : 卖出货币
    # data : 内容说明
    # [
    # 1417536000000, 时间戳
    # 2370.16, 开
    # 2380, 高
    # 2352, 低
    # 2367.37, 收
    # 17259.83 交易量
    # ]
    def K_line(self,token):
        return requests.post(self.__URL+'kline?market=' + token).json()