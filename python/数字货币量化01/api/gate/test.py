from api.gate.gateAPI import GateIO

## 填写 apiKey APISECRET
apiKey = ''
secretKey = ''
## address
#btcAddress = 'your btc address'

## Provide constants
API_QUERY_URL = 'data.gateio.io'
API_TRADE_URL = 'api.gateio.io'

## Create a gate class instance
gate_query = GateIO(API_QUERY_URL, apiKey, secretKey)
gate_trade = GateIO(API_TRADE_URL, apiKey, secretKey)

# Trading Pairs
#print(gate_query.pairs()[2])

## Below, use general methods that query the exchange

#  市场信息，小数点后面位数，最小数额，手续费 大字典'pairs',返回'result':'true'
#print(gate_query.marketinfo()['result'])

# 在交易所中排名下来的详细市场信息，比如总量，买卖量，排第几位，涨跌幅度，现价，锚定什么法币
#print(gate_query.marketlist()['data'][0])

# 交易数据，24小时最高价，最近价，24小时最低价，涨跌幅，还有一些baseVolume和quoteVolume不知道是什么
#print(gate_query.tickers()['eos_usdt'])

#深度,bids卖单，asks买单,返回的是深度还有挂单量
a=gate_query.orderBooks()['eos_usdt']['bids']
a.reverse()
print(a[:5])
print(gate_query.orderBooks())

# orders 这个需要key
#print(gate_query.openOrders())

# Ticker就是指定币种的Tickers
#print(gate_query.ticker('btc_usdt'))

# Market depth of pair
# print(gate_query.orderBook('btc_usdt'))

# Trade History
#print(gate_query.tradeHistory('eos_usdt'))

# K线 这是默认时间线参数的
#print(gate_query.kLine('btc_usdt'))

## 以下方法用到用户的KEY
# 账户余额和冻结余额
# print(gate_trade.balances())

# get new address
# print(gate_trade.depositAddres('btc'))

# get deposit withdrawal history
# print(gate_trade.depositsWithdrawals('1469092370', '1569092370'))

# Place order sell
# print(gate_trade.buy('etc_btc', '0.001', '123'))

# Place order sell
# print(gate_trade.sell('etc_btc', '0.001', '123'))

# Cancel order
# print(gate_trade.cancelOrder('267040896', 'etc_btc'))

# Cancel all orders
# print(gate_trade.cancelAllOrders('0', 'etc_btc'))

# Get order status
# print(gate_trade.getOrder('267040896', 'eth_btc'))

# Get my last 24h trades
# print(gate_trade.mytradeHistory('etc_btc', '267040896'))

# withdraw
# print(gate_trade.withdraw('btc', '88', btcAddress))