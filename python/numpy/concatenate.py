# numpy concatenate的用法，就是数组扩展的高性能用法
# python中数组扩展使用的是extend函数，但是对于多数组扩展性能并不可行
import numpy as np

a=[1,2,3]    # np.array也是可以的
b=[4,5,6]
c=[7,8,9]
np.concatenate((a,b,c),axis=0)    # axis是可以不用传进去的
>>>array([1, 2, 3, 4, 5, 6, 7, 8, 9])