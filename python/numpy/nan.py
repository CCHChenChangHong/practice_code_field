import numpy as np

np.nan # >>> nan
np.isnan(np.nan)  # >>> True
~np.isnan(np.nan) # >>> False

~True    # >>> -2    ???为什么是-2
~False   # >>> -1    ???-1又是为什么

# 尽管np.isnan(np.nan)是True,对True取反是False，但是直接对True取反却不是Faslse