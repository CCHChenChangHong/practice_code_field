import numpy as np

a=[1,2,3]
b=[3,4,5]

np.isin(a,b)
>>>
array[False,False,True]

np.isin(a,b).all()
>>>
False