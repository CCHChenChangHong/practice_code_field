import numpy as np

# 演示数组
showArray1=np.arange(10)  # >>> array([0,1,2,3,4,5,6,7,8,9])
showArray2=None

# bool矩阵
np.full((3,3),True,dtype=bool)
np.ones((3,3),dtype=bool)
# array([[ True,  True,  True],
#        [ True,  True,  True],
#        [ True,  True,  True]])

a=showArray1
# 从一维数组找到指定的元素
# 提取奇数
a[a%2==0]   # >>> array([0, 2, 4, 6, 8])

# 把数组中的奇数换成指定的数值
a[a%2==0]=810  # >>> array([810,   1, 810,   3, 810,   5, 810,   7, 810,   9])   这里a本身会改变

# 把数组中的奇数换成指定的数值而不改变原数组
np.where(a%2==1,-1,a)