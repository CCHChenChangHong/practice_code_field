# 这个和print差不多功能，只是pprint适合打印复杂的数据结构，pprint自动换行什么的
import pprint

data = ("test", [1, 2, 3,'test', 4, 5], "This is a string!",
        {'age':23, 'gender':'F'})
print(data)
>>>('test', [1, 2, 3, 'test', 4, 5], 'This is a string!', {'age': 23, 'gender': 'F'})
pprint.pprint(data)
>>>('test',
 [1, 2, 3, 'test', 4, 5],
 'This is a string!',
 {'age': 23, 'gender': 'F'})