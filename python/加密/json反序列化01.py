import json
dict = {'name':'zhangsan', 'age':33, 'address':'红星路'}
print('未序列化前的数据类型为:', type(dict))
print('为序列化前的数据：', dict)
#对dict进行序列化的处理
dict_xu = json.dumps(dict,ensure_ascii=False)    #添加ensure_ascii=False进行序列化
print('序列化后的数据类型为：', type(dict_xu))
print('序列化后的数据为：', dict_xu)
#对dict_xu进行反序列化处理
dict_fan = json.loads(dict_xu)
print('反序列化后的数据类型为：', type(dict_fan))
print('反序列化后的数据为: ', dict_fan)
#----------------------------------------------------------------------
# 未序列化前的数据类型为: <class 'dict'>
# 为序列化前的数据： {'name': 'zhangsan', 'age': 33, 'address': '红星路'}
# 序列化后的数据类型为： <class 'str'>
# 序列化后的数据为： {"name": "zhangsan", "age": 33, "address": "红星路"}
# 反序列化后的数据类型为： <class 'dict'>
# 反序列化后的数据为:  {'name': 'zhangsan', 'age': 33, 'address': '红星路'}