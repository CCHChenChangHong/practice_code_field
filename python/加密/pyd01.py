# 首先，系统环境必须有cython
'''
需要加密的python脚本
use_to_test.py:
print('hasaki')

在use_to_test.py当前目录下，创建setup.py

pyd不能直接用python去执行，要Import去执行
位数不一致报错 ImportError: DLL load failed: %1 不是有效的 Win32 应用程序
'''
# setup.py
import os
import sys
sys.path.append(os.getcwd())
from distutils.core import setup
from Cython.Build import cythonize
setup(ext_modules=cythonize('C:/hashaki/work/run/基于vnpy版本/use_to_test.py'))
# 生成的C文件，会和py文件在同一个目录下

'''
进入命令行模式：
输入：
python setup.py build_ext --inplace
python setup.py install
注意：setup.py是不能接收传参的比如：
python setup.py 'C:/hashaki/工作交接/工作交接/bianyipyc/main.py' build_ext --inplace

出现错误：invalid command name 'C:/hashaki/工作交接/工作交接/bianyipyc/main.py'

use_to_test.cp36-win_amd64.pyd文件生成在python的Lib\site-packages下
'''