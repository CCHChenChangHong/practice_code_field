import os
import sys
sys.path.append(os.getcwd())
from distutils.core import setup
from Cython.Build import cythonize
import json
file_path='C:/hashaki/work/run/基于vnpy版本/hasaki.json'
python_path=''
with open(file_path,'r',encoding='utf-8') as json_file:
    python_path=json.load(json_file)['path']

setup(ext_modules=cythonize(python_path))
