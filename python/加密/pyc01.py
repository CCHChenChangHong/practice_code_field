# 当在一个程序中import其他程序，那么打包那个被调用的程序，成pyc就可以被调用
# 编译
#! /usr/bin/env python  
# -*- coding: utf-8 -*-
import os
import py_compile
import sys
sys.path.append(os.getcwd())

def bianyi(fpath):
    py_compile.compile(fpath)


bianyi("C:\hashaki\工作交接\工作交接/bianyipyc/be_test.py")

# 编译之后就会在__pycache__有一个be_test.cythonpy36.py的文件，把中间那段字符去掉，值要be_test.pyc
# 然后把be_test.pyc放在调用它的那个程序同一个目录即可