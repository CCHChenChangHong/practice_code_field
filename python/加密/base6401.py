import base64
base64.b64encode('binary\x00string')  # 'YmluYXJ5AHN0cmluZw=='
base64.b64decode('YmluYXJ5AHN0cmluZw==')  # 'binary\x00string'