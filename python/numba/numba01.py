# numba学习
from numba import jit
import random

@jit(nopython=True)
def text(nsample):
    acc=0
    for i in range(nsample):
        x=random.random()
        y=random.random()
        if (x ** 2 + y ** 2) < 1.0:
            acc+=1
    return 4.0*acc/nsample

if __name__=='__main__':
    result=text(10)
    print(result)     # 3.6