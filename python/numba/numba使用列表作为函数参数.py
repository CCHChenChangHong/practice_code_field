import numba

@numba.njit
def hasaki(listData) -> list:
    for i in listData:
        print('hasalkio',i)
    return listData

if __name__=='__main__':
    # 如果不使用numba内置的lies()，那么运行的时候就会出现warning
    listData=numba.typed.List()
    [listData.append(i) for i in [1,2,3,4,5,6,7,8,9]]
    result=hasaki(listData)
    print(result)

>>>
hasalkio 1
hasalkio 2
hasalkio 3
hasalkio 4
hasalkio 5
hasalkio 6
hasalkio 7
hasalkio 8
hasalkio 9
[1, 2, 3, 4, 5, 6, 7, 8, 9]