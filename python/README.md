#### TypeError: parse() got an unexpected keyword argument 'transport_encoding'

conda install -c anaconda html5lib

#### pip使用清华镜像

```
pip install -i https://pypi.tuna.tsinghua.edu.cn/simple tensorflow
```

#### 使用豆瓣镜像

```
pip3 install tensorflow -i https://pypi.douban.com/simple --trusted-host pypi.doubban.com
```

#### 远程使用服务器jupyter notebook

```
sudo pip3 install ipython

sudo pip3 install jupyter notebook

jupyter notebook --generate-config

# 终端输入ipython增加密码
from notebook.auth import passwd
passwd()

out>>xxxxxx保存这个密钥

# 修改配置文件
vim ~/.jupyter/jupyter_notebook_config.py
c.NotebookApp.ip='*'  # 就是设置所有ip皆可访问
c.NotebookApp.password = u'sha1:05aacc8528d2:3a95c7fae662922c25c1531cd7f9b1712d4a795a' #前边保存的密码信息
c.NotebookApp.open_browser = False #禁止自动打开浏览器
c.NotebookApp.port =8888 #随便指定一个端口，默认为8888

# 终端输入
jupyter notebook

# 然后在浏览器登录即可
http://服务器ip:8888
```

# Linux上删除pip3
```
apt-get remove python3-pip
```

# linux安装指定版本的python
```
[来源](https://www.jianshu.com/p/df5ca2eaf906)

查看当前的python版本
ls -l /usr/bin | grep python

在python官网找到python版本的地址：https://www.python.org/ftp/python/3.7.1/Python-3.7.1.tgz

下载具体版本:
wget https://www.python.org/ftp/python/3.7.4/Python-3.7.4.tgz

解压包
tar -zxvf Python-3.7.4.tgz

切换到解压后的目录下
cd Python-3.7.4

配置
./configure --prefix=/usr/local/python3.7.4
或者
./configure

编译make
make

测试make test
make test

安装sudo make install
sudo make install


```