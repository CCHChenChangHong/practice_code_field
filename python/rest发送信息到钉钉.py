# 参考文章：https://segmentfault.com/a/1190000015774630
def sendmessage():
    url = 'https://oapi.dingtalk.com/robot/send?access_token=xxx' #这里填写你自定义机器人的webhook地址
    HEADERS = {
        "Content-Type": "application/json ;charset=utf-8 "
    }
    message = "周五晚上踢球，大家记得带装备呀~"
    String_textMsg = { \
        "msgtype": "text",
        "text": {"content": message},
        "at": {
            "atMobiles": [
                "130xxxxxxxx"                                    #如果需要@某人，这里写他的手机号
            ],
            "isAtAll": 1                                         #如果需要@所有人，这些写1
        }
    }
    String_textMsg = json.dumps(String_textMsg)
    res = requests.post(url, data=String_textMsg, headers=HEADERS)
    print(res.text)

if __name__ == '__main__':
    sendmessage()