from websocket import create_connection
import gzip
import time
ws = create_connection("wss://api.huobi.pro/ws")
tradeStr="""{"sub": "market.ethusdt.kline.1min","id": "id10"}"""
ws.send(tradeStr)
while(1):
        compressData=ws.recv()
        result=gzip.decompress(compressData).decode('utf-8')
        if result[:7] == '{"ping"':
            ts=result[8:21]
            pong='{"pong":'+ts+'}'
            ws.send(pong)
            ws.send(tradeStr)
        else:
            print(result)