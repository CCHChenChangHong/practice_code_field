from websocket import create_connection
import time
if __name__ == '__main__':
    while(1):
        try:
            ws = create_connection("wss://api.bitfinex.com/ws/2")
            break
        except Exception as error:
            print('连接WS失败,重新连接...')
            print("错误原因：",error)
            time.sleep(3)
