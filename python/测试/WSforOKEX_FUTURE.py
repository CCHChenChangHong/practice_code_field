from websocket import create_connection
import time

if __name__ == '__main__':
    while(1):
        try:
            ws = create_connection("wss://real.okex.com:10440/websocket/okexapi")
            break
        except Exception as e:
            print('connect ws error,retry...',e)
            time.sleep(2)

    # 订阅 KLine 数据
    tradeStr="""{'event':'addChannel','channel':'ok_sub_futureusd_btc_ticker_this_week'}"""

    ws.send(tradeStr)
    while(1):
        compressData=ws.recv()
        