# build in python3.5
# 作者 ： 陈常鸿
# 用于REST模仿websocket的功能，定时向服务器发送请求，并接收消息
# 功能 ：
# 创建链接create_connetion()
# 发送请求send()
# 接收数据recv()
import time
from vnpy.api.okex.OkcoinFutureAPI import OKexFutureByCCH

class restshop:
    def __init__(self):
        self.exchangeName=''
        self.channel=''
        self.symbol=''
        self.contract_type=''
        self.timeType=''
        self.size=''
        self.since=''
        self.apiKey=''
        self.secretKey=''
        self.__activity=False
        self.__sleepTime=1
        self.api=None

    #-------------------------------------------------------------------
    def create_connection(self,exchangeName):
        '''封装好交易所的请求地址，传入交易所的名字即可连接'''
        self.exchangeName=exchangeName
        ExchangeCheck=['OKEX',
                    'HUOBI',
                    'BITIFINEX',
                    'GATEIO']
        if exchangeName in ExchangeCheck:
            self.api=OKexFutureByCCH(self.apiKey,self.secretKey)
            pass                             # link to the exchange account to check
        else:
            print("交易所名字输入错误，引擎无法识别该名字")

    #-------------------------------------------------------------------
    def send(self,require):
        '''判断要发送什么请求'''
        self.channel=require['channel']
        if self.channel=='OKEX_FUTURE_TICKER':
            self.symbol=require['symbol']
            self.contractType=require['contractType']

        elif self.channel=='OKEX_FUTURE_DEPTH':
            self.symbol=require['symbol']
            self.contractType=require['contractType']
            self.size=require['size']

        elif self.channel=='OKEX_FUTURE_TRADES':
            self.symbol=require['symbol']
            self.contractType=require['contractType']
        
        elif self.channel=='OKEX_FUTURE_INDEX':
            self.symbol=require['symbol']
        
        elif self.channel=='OKEX_FUTURE_KLINE':
            self.symbol=require['symbol']
            self.contractType=require['contractType']
            self.since=require['since']
            self.size=require['size']
            self.timeType=require['timeType']
        
        #self.apiKey=require['apiKey']
        #self.secretKey=require['secretKey']

        
    #-------------------------------------------------------------------
    def recv(self):
        '''接收信息，并返回信息'''
        if self.channel=='OKEX_FUTURE_TICKER':
            #return self.api.future_ticker(self.symbol,self.contractType)
            print(self.api.future_ticker(self.symbol,self.contractType))

        elif self.channel=='OKEX_FUTURE_DEPTH':
            return self.api.future_depth(self.symbol,self.contractType,self.size)

        elif self.channel=='OKEX_FUTURE_TRADES':
            return self.api.future_trades(self.symbol,self.contractType)

        elif self.channel=='OKEX_FUTURE_INDEX':
            return self.api.future_index(self.symbol)
        
        elif self.channel=='OKEX_FUTURE_KLINE':
            return self.api.future_kline(self.symbol,self.timeType,self.contractType)
            
    #--------------------------------------------------------------------
    def run_forever(self):
        '''持续运行，不断向服务器发送请求'''
        self.__activity=True
        while self.__activity:
            print("持续型REST运行中")
            self.recv()
            time.sleep(self.__sleepTime)
    
    #--------------------------------------------------------------------
    def close(self):
        '''关闭持续REST'''
        print("关闭REST持续查询功能")
        self.__activity=False
    
    #--------------------------------------------------------------------
    def RESTERROR(self):
        '''内置的错误方法'''
        pass