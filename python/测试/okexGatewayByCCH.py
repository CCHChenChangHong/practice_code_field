# encoding: UTF-8

'''
vnpy.api.okex的gateway接入
只有期货接口，不做现货
'''

import os
import json
from datetime import datetime
from time import sleep
from copy import copy
from threading import Condition
from queue import Queue
from threading import Thread
from time import sleep
import traceback

from vnpy.api.okex import WsSpotApi, WsFuturesApi, SPOT_SYMBOL_PAIRS, CONTRACT_SYMBOL, CONTRACT_TYPE, SPOT_CURRENCY
from vnpy.api.okex.okexData import SPOT_TRADE_SIZE_DICT, SPOT_REST_ERROR_DICT, SPORT_WS_ERROR_DICT, FUTURES_ERROR_DICT

from vnpy.api.okex.OkcoinFutureAPI import OKEX_FUTURE_HOST,OKCoinFuture
from vnpy.trader.vtGateway import *
from vnpy.trader.vtFunction import getJsonPath
from vnpy.trader.vtConstant import EXCHANGE_OKEX, DIRECTION_NET, PRODUCT_SPOT, DIRECTION_LONG, DIRECTION_SHORT, PRICETYPE_LIMITPRICE, PRICETYPE_MARKETPRICE, OFFSET_OPEN, OFFSET_CLOSE
from vnpy.trader.vtConstant import STATUS_CANCELING,STATUS_CANCELLED, STATUS_NOTTRADED, STATUS_PARTTRADED, STATUS_ALLTRADED, STATUS_UNKNOWN, STATUS_REJECTED, PRODUCT_FUTURES
from vnpy.trader.vtObject import VtErrorData

# 价格类型映射
# 买卖类型： 限价单（buy/sell） 市价单（buy_market/sell_market）
priceTypeMap = {}
priceTypeMap['buy'] = (DIRECTION_LONG, PRICETYPE_LIMITPRICE)
priceTypeMap['buy_market'] = (DIRECTION_LONG, PRICETYPE_MARKETPRICE)
priceTypeMap['sell'] = (DIRECTION_SHORT, PRICETYPE_LIMITPRICE)
priceTypeMap['sell_market'] = (DIRECTION_SHORT, PRICETYPE_MARKETPRICE)
priceTypeMapReverse = {v: k for k, v in priceTypeMap.items()}

priceContractOffsetTypeMap = {}
priceContractOffsetTypeMap['1'] = (DIRECTION_LONG , OFFSET_OPEN)
priceContractOffsetTypeMap['2'] = (DIRECTION_SHORT , OFFSET_OPEN )
priceContractOffsetTypeMap['3'] = (DIRECTION_SHORT , OFFSET_CLOSE )
priceContractOffsetTypeMap['4'] = (DIRECTION_LONG , OFFSET_CLOSE)
priceContractTypeMapReverse = {v: k for k, v in priceContractOffsetTypeMap.items()}

# 委托状态印射
statusMap = {}
statusMap[-1] = STATUS_CANCELLED    # 撤单
statusMap[0] = STATUS_NOTTRADED     # 未成交
statusMap[1] = STATUS_PARTTRADED    # 部分成交
statusMap[2] = STATUS_ALLTRADED     # 全部成交
statusMap[3] = STATUS_UNKNOWN
statusMap[4] = STATUS_UNKNOWN       # 未知状态
statusMap[5] = STATUS_CANCELING       # 撤销中

EVENT_OKEX_INDEX_FUTURE = "eFuture_Index_OKEX"

########################################################################
class OkexGateway(VtGateway):
    """OKEX交易接口"""
    # ----------------------------------------------------------------------
    def __init__(self, eventEngine, gatewayName='OKEX'):
        """Constructor"""
        super(OkexGateway, self).__init__(eventEngine, gatewayName)

        self.api_futures = OkexFuturesApi(self)     # 期货交易接口

        self.apiKey = EMPTY_STRING
        self.secretKey = EMPTY_STRING
        self.leverage = 0

        self.use_spot_symbol_pairs = set()          # 使用现货合约对（从配置文件读取，减少运算量）
        self.auto_subscribe_symbol_pairs = set()    # 自动订阅现货合约对清单
        self.auto_subscribe_future_symbols = set()  # 自动订阅期货合约清单

        self.futures_connected = False              # 期货交易接口连接状态

        self.qryCount = 0                           # 查询触发倒计时
        self.qryTrigger = 2                         # 查询触发点
        self.hbCount = 0                            # 心跳触发倒计时
        self.hbTrigger = 30                         # 心跳触发点

        # gateway 配置文件
        self.fileName = self.gatewayName + '_connect.json'
        self.filePath = getJsonPath(self.fileName, __file__)

        # 消息调试
        self.log_message = False

        self.qryFunctionList = []
    # ----------------------------------------------------------------------
    def connect(self):
        """连接"""
        # 载入json文件
        try:
            f = open(self.filePath, 'r')
        except IOError:
            self.writeError(u'OkexGatewayByCCH.connect:读取连接配置{}出错，请检查'.format(self.filePath))
            return

        # 解析json文件
        setting = json.load(f)
        try:
            self.apiKey = str(setting['apiKey'])
            self.secretKey = str(setting['secretKey'])
            trace = setting['trace']
            self.leverage = setting.get('leverage', 1)
            futures_connect = setting['futures_connect']
            self.log_message = setting.get('log_message', False)

            # 若限定使用的合约对
            if "symbol_pairs" in setting.keys():
                self.use_spot_symbol_pairs = set(setting["symbol_pairs"])

            # 若希望连接后自动订阅
            if 'auto_subscribe' in setting.keys():
                auto_subscribe = set(setting['auto_subscribe'])
                for symbol in auto_subscribe:
                    if '_' in symbol and ':' not in symbol:
                        self.auto_subscribe_symbol_pairs.add(symbol)
                    elif ':' in symbol:
                        self.auto_subscribe_future_symbols.add(symbol)

            self.qryEnabled = setting.get('qryEnabled', True)
        except KeyError:
            self.writeError(u'OkexGatewayByCCH.connect:连接配置缺少字段，请检查')
            return

        if futures_connect:
            self.api_futures.active = True
            self.api_futures.connect(self.apiKey, self.secretKey, trace)
            self.writeLog(u'connect okex REST contract api')

            for future_symbol in self.auto_subscribe_future_symbols:
                self.writeLog(u'添加订阅期货合约:{}'.format(future_symbol))
                self.api_futures.registered_symbols.add(future_symbol)

        log = VtLogData()
        log.gatewayName = self.gatewayName
        log.logContent = u'接口初始化成功'
        self.onLog(log)

        # 启动查询
        self.initQuery()
        self.startQuery()

    def checkStatus(self):
        return  self.futures_connected

    # ----------------------------------------------------------------------
    def subscribe(self, subscribeReq):
        """
        订阅行情
        :param subscribeReq: VtSubscribeReq,
        :return:
        """
        try:
            symbol_pair_gateway = subscribeReq.symbol
            arr = symbol_pair_gateway.split('.')
            # 提取品种对 eth_usdt
            symbol_pair = arr[0]

            if symbol_pair in SPOT_SYMBOL_PAIRS:
                if self.api_futures and self.futures_connected:
                    self.api_futures.subscribe(subscribeReq)
                else:
                    self.writeError(u'期货接口未创建/未连接，无法调用subscribe')

        except Exception as ex:
            self.writeError(u'OkexGatewayByCCH.subscribe 异常,请检查日志:{}'.format(str(ex)))
            self.writeLog(u'OkexGatewayByCCH.subscribe Exception :{},{}'.format(str(ex), traceback.format_exc()))

    # ----------------------------------------------------------------------
    def sendOrder(self, orderReq):
        """发单"""
        # btc_usdt.OKEX => btc_usdt
        order_req_symbol = orderReq.symbol
        order_req_symbol = order_req_symbol.replace('.{}'.format(EXCHANGE_OKEX),'')

        if order_req_symbol in SPOT_SYMBOL_PAIRS:
            if self.api_futures and self.futures_connected:
                return self.api_futures.futureSendOrder(orderReq)
            else:
                self.writeError(u'期货接口未创建/连接，无法调用sendOrder')
                return ''

    # ----------------------------------------------------------------------
    def cancelOrder(self, cancelOrderReq):
        """撤单"""
        # btc_usdt.OKEX => btc_usdt
        order_req_symbol = cancelOrderReq.symbol
        order_req_symbol = order_req_symbol.replace('.{}'.format(EXCHANGE_OKEX), '')
        if order_req_symbol in SPOT_SYMBOL_PAIRS:
            if self.futures_connected:
                self.api_futures.futureCancel(cancelOrderReq)
            else:
                self.writeError(u'期货接口未创建/连接，无法调用cancelOrder')

    # ----------------------------------------------------------------------
    def qryAccount(self):
        """查询账户资金"""
        if self.futures_connected:
            self.api_futures.futureUserInfo()

    # ----------------------------------------------------------------------
    def qryOrderInfo(self):

        if self.futures_connected:
            self.api_futures.futureAllUnfinishedOrderInfo()

    # ----------------------------------------------------------------------
    def qryPosition(self):
        """查询持仓"""
        if self.futures_connected:
            pass
            #self.api_futures.subscribeFuturePositions()

    # ----------------------------------------------------------------------
    def close(self):
        """关闭"""
        if self.futures_connected:
            self.api_futures.active = False
            self.api_futures.close()

    # ----------------------------------------------------------------------
    def initQuery(self):
        """初始化连续查询"""
        if self.qryEnabled:
            # 需要循环的查询函数列表
            self.qryFunctionList = [self.qryAccount, self.qryOrderInfo,self.qryPosition]
            #self.qryFunctionList = [self.qryOrderInfo]
            # self.qryFunctionList = []

            self.qryCount = 0  # 查询触发倒计时
            self.qryTrigger = 2  # 查询触发点
            self.qryNextFunction = 0  # 上次运行的查询函数索引

            self.startQuery()

    # ----------------------------------------------------------------------
    def query(self, event):
        """注册到事件处理引擎上的查询函数"""
        self.qryCount += 1

        if self.qryCount > self.qryTrigger:
            # 清空倒计时
            self.qryCount = 0

            # 执行查询函数
            function = self.qryFunctionList[self.qryNextFunction]
            function()

            # 计算下次查询函数的索引，如果超过了列表长度，则重新设为0
            self.qryNextFunction += 1
            if self.qryNextFunction == len(self.qryFunctionList):
                self.qryNextFunction = 0

    def heartbeat(self,event):
        """
        心跳
        :return:
        """
        self.hbCount += 1

        if self.hbCount < self.hbTrigger:
            return

        # 清空倒计时
        self.hbCount = 0
        # 发送心跳请求

        if self.api_futures.active and self.futures_connected:
            self.api_futures.sendHeartBeat()

    # ----------------------------------------------------------------------
    def startQuery(self):
        """启动连续查询"""
        self.eventEngine.register(EVENT_TIMER, self.query)
        self.eventEngine.register(EVENT_TIMER, self.heartbeat)

    # ----------------------------------------------------------------------
    def setQryEnabled(self, qryEnabled):
        """设置是否要启动循环查询"""
        self.qryEnabled = qryEnabled

    #----------------------------------------------------------------------
    def onFutureIndexPush(self, push_dic):
        """
        合约指数更新事件
        :param push_dic:
        :return:
        """
        event1 = Event(type_=EVENT_OKEX_INDEX_FUTURE)
        event1.dict_['data'] = push_dic
        self.eventEngine.put(event1)

########################################################################
class OkexFuturesApi(WsFuturesApi):
    """okex的合约API实现"""
    # ----------------------------------------------------------------------
    def __init__(self, gateway):
        """Constructor"""
        super(OkexFuturesApi, self).__init__()    # 这里作了现货同样的处理--陈常鸿

        self.gateway = gateway  # gateway对象
        self.gatewayName = gateway.gatewayName  # gateway对象名称

        self.active = False  # 若为True则会在断线后自动重连

        self.cbDict = {}
        self.tickDict = {}
        self.orderDict = {}
        self.localOrderDict = {}  # 本地缓存的order_dict，key 是 localNo.gatewayName

        self.channelSymbolMap = {}
        self.localNo = 1  # 本地委托号
        self.localNoQueue = Queue()  # 未收到系统委托号的本地委托号队列
        self.localNoDict = {}  # key为本地委托号，value为系统委托号
        self.orderIdDict = {}  # key为系统委托号，value为本地委托号
        self.cancelDict = {}  # key为本地委托号，value为撤单请求

        self.recordOrderId_BefVolume = {}  # 记录的之前处理的量

        self.tradeID = 0

        self.registered_symbols = set([])
        self.queryed_pos_symbols = set([])
        self._use_leverage = "10"           # 缺省使用的杠杆比率

        self.bids_depth_dict = {}
        self.asks_depth_dict = {}

        self.contract_name_dict = {}

        self.contractIdToSymbol = {}

    # ----------------------------------------------------------------------
    def setLeverage(self, __leverage):
        """
        设置杠杆比率
        :param __leverage:
        :return:
        """
        self._use_leverage = __leverage

    # ----------------------------------------------------------------------
    def onMessage(self, rest, evt):
        """
        信息推送的处理
        :param rest:
        :param evt:
        :return:
        """
        # str => json
        rest_data = self.readData(evt)

        if self.gateway.log_message:
            self.gateway.writeLog(u'FutureApi.onMessage:{}'.format(rest_data))

        # 返回dict  REST没有心跳
        if isinstance(rest_data, dict):
            self.writeLog(u'其他数据:{}'.format(rest_data))

        # 返回list
        if isinstance(rest_data, list):
            for data in rest_data:
                channel_value = data['channel'] if 'channel' in data else None
                if channel_value == None:
                    continue

                # 登录请求恢复
                if channel_value == 'login':
                    if 'data' in data:
                        login_data = data['data']
                        result = login_data['result'] if 'result' in login_data else False
                        if result:
                            self.writeLog(u'登录成功: {}'.format(datetime.now()))
                            self.gateway.futures_connected = True
                        else:
                            self.gateway.writeError(u'登录失败')
                            self.writeLog(u'登录失败: {},data:{}'.format(datetime.now(), data))
                    continue


                # 其他回调/数据推送
                callback = self.cbDict.get(channel_value)
                if callback:
                    try:
                        callback(data)
                    except Exception as ex:
                        self.gateway.writeError(u'回调{}发生异常'.format(channel_value))
                        self.writeLog(u'onMessage call back {} exception：{},{}'.format(channel_value, str(ex),
                                                                               traceback.format_exc()))
                else:
                    self.gateway.writeError(u'okFutureApi:出现无回调处理的channel:{}'.format(channel_value))
                    self.writeLog(u'unkonw msg:{}'.format(data))

    # ----------------------------------------------------------------------
    def onError(self, ws, evt):
        """重载WsFutureApi.onError错误Event推送"""
        error = VtErrorData()
        error.gatewayName = self.gatewayName
        error.errorMsg = str(evt)
        self.gateway.onError(error)

    # #----------------------------------------------------------------------
    def onErrorMsg(self, data):
        """错误信息处理"""
        error = VtErrorData()
        error.gatewayName = self.gatewayName
        if 'data' in data and 'error_code' in data['data']:
            error_code = str(data["data"]["error_code"])
            error.errorID = error_code
            error.errorMsg = u'FutureApi Error:{}'.format(FUTURES_ERROR_DICT.get(error_code))
            self.gateway.onError(error)

    # ----------------------------------------------------------------------
    def reconnect(self):
        """
        重连
        :return:
        """
        while not self.gateway.futures_connected:
            self.writeLog(u'okex Api_contract 等待10秒后重新连接')
            self.connect(self.apiKey, self.secretKey, self.trace)

            sleep(10)
            if not self.gateway.futures_connected:
                self.reconnect()

    # ----------------------------------------------------------------------
    def onClose(self, ws):
        """接口断开"""
        # 如果尚未连上，则忽略该次断开提示
        if not self.gateway.futures_connected:
            return

        self.gateway.futures_connected = False
        self.writeLog(u'服务器连接断开')

        # 重新连接
        if self.active:
            t = Thread(target=self.reconnect)
            t.start()

    # ----------------------------------------------------------------------
    def dealSymbolFunc(self, symbol):
        """
        分解委托单symbol
        :param symbol:
        :return:
        """
        arr = symbol.split('.')
        symbol_pair = arr[0]
        l = symbol_pair.split(':')
        if len(l) !=3:
            self.gateway.writeError(u'合约代码{}错误:'.format(symbol))
            raise ValueError(u'合约代码{}错误:'.format(symbol))
        symbol, contract_type, leverage = l[0], l[1], l[2]
        if contract_type not in CONTRACT_TYPE:
            self.gateway.writeError(u'合约类型错误:{}'.format(contract_type))
            raise ValueError(u'合约类型{}不在:{}中'.format(contract_type,CONTRACT_TYPE))
        symbol = symbol.replace("_usd", "")

        return (symbol_pair, symbol, contract_type, leverage)

    # ----------------------------------------------------------------------
    def subscribe(self, subscribeReq):
        """
        订阅行情
        :param subscribeReq:
        :return:
        """
        try:
            # 分解出 合约对/合约/合约类型/杠杆倍数
            (symbol_pair, symbol, contract_type, leverage) = self.dealSymbolFunc(subscribeReq.symbol)
        except Exception as ex:
            self.writeLog(u'订阅合约行情异常:{}'.format(str(ex)))
            return

        if subscribeReq.symbol not in self.registered_symbols:
            # 登记合约对（btc_usd   ltc_usd   eth_usd   etc_usd   bch_usd）
            self.registered_symbols.add(subscribeReq.symbol)
            # 订阅行情
        self.subscribeSingleSymbol(symbol, contract_type, leverage)

    # ----------------------------------------------------------------------
    def subscribeSingleSymbol(self, symbol, contractType, leverage):
        """
        订阅行情
        :param symbol: 合约，如btc，eth，etc等
        :param contract_type: 合约类型，当周，下周，季度
        :param leverage:杠杆倍数
        :return:
        """
        self.gateway.writeLog(u'FuturesApi.subscribeSingleSymbol:symbol:{},cotract_type:{},leverage:{}'.format(symbol, contractType, leverage))
        if symbol in CONTRACT_SYMBOL:
            # 订阅tick行情
            self.subsribeFutureTicker(symbol, contractType)
            # 订阅深度行情
            self.subscribeFutureDepth(symbol, contractType)
            # 查询该合约的委托清单
            self.futureOrderInfo(symbol + "_usd", -1, contractType, status="1", current_page=1, page_length=50)
            # 订阅该合约得市场成交
            #self.subscribeFutureTrades(symbol+"_usd",contractType)
            # self.cbDict["ok_sub_futureusd_%s_trade_%s" % (symbol+'_usd', contractType)] = self.onFutureSub

    # ----------------------------------------------------------------------
    def futureAllOrders(self):
        """
        发出查询所有合约委托查询
        :return:
        """
        for symbol in self.registered_symbols:
            # 根据已订阅的合约清单，逐一合约发出
            try:
                (symbol_pair, symbol, contractType, leverage) = self.dealSymbolFunc(symbol)
                self.futureOrderInfo(symbol + "_usd", -1, contractType, status="1", current_page=1, page_length=50)
            except Exception as ex:
                self.writeLog(u'dealSymbolFunc :{}异常:{}'.format(symbol,str(ex)))
                continue

        for orderId in self.orderIdDict.keys():
            # 根据缓存的委托单id，逐一发出委托查询
            order = self.orderDict.get(orderId, None)
            if order != None:
                try:
                    symbol_pair, symbol, contractType, leverage = self.dealSymbolFunc(order.symbol)
                    self.futureOrderInfo(symbol + "_usd", orderId, contractType, status="1", current_page=1,
                                         page_length=50)
                except Exception as ex:
                    self.writeLog(u'发出委托单查询异常:{}'.format(str(ex)))

    # ----------------------------------------------------------------------
    def onOpen(self, ws):
        """连接成功"""
        self.gateway.futures_connected = True
        self.writeLog(u'服务器OKEX期货连接成功')

        self.initCallback()

        for symbol in CONTRACT_SYMBOL:
            self.channelSymbolMap[
                "ok_sub_futureusd_{0}_index".format(symbol)] = symbol + "_usd:%s:" + self._use_leverage # + "." + EXCHANGE_OKEX

            for use_contract_type in CONTRACT_TYPE:
                use_symbol_name = symbol + "_usd:{0}:{1}".format(use_contract_type, self._use_leverage)
                # Ticker数据
                self.channelSymbolMap["ok_sub_futureusd_{0}_ticker_{1}".format(symbol, use_contract_type)] = use_symbol_name
                # 盘口的深度
                self.channelSymbolMap["ok_sub_future_{0}_depth_{1}_usd".format(symbol, use_contract_type)] = use_symbol_name
                # 所有人的交易数据
                self.channelSymbolMap["ok_sub_futureusd_{0}_trade_{1}".format(symbol, use_contract_type)] = use_symbol_name

                contract = VtContractData()
                contract.gatewayName = self.gatewayName
                contract.symbol = use_symbol_name  + "." + EXCHANGE_OKEX
                contract.exchange = EXCHANGE_OKEX
                contract.vtSymbol = contract.symbol
                contract.name = u'期货{0}_{1}_{2}'.format(symbol, use_contract_type, self._use_leverage)
                contract.size = 0.00001
                contract.priceTick = 0.00001
                contract.productClass = PRODUCT_FUTURES
                self.gateway.onContract(contract)

                # print contract.vtSymbol , contract.name

                quanyi_vtSymbol = symbol + "_usd_future_qy"  + "."+ EXCHANGE_OKEX
                contract = VtContractData()
                contract.gatewayName = self.gatewayName
                contract.symbol = quanyi_vtSymbol
                contract.exchange = EXCHANGE_OKEX
                contract.vtSymbol = contract.symbol
                contract.name = u'期货权益{0}'.format(symbol)
                contract.size = 0.00001
                contract.priceTick = 0.00001
                contract.productClass = PRODUCT_FUTURES
                self.gateway.onContract(contract)

        self.login()
        # 连接后查询账户和委托数据
        self.futureUserInfo()
        self.futureAllUnfinishedOrderInfo()
        self.futureAllIndexSymbol()

        for symbol in self.registered_symbols:
            try:
                self.writeLog(u'okex future_api 重新订阅:{0}'.format(symbol))   # 为谁format？加了{}--陈常鸿
                # 分解出 合约对/合约/合约类型/杠杆倍数
                (symbol_pair, symbol, contract_type, leverage) = self.dealSymbolFunc(symbol)
                self.subscribeSingleSymbol(symbol, contract_type, leverage)
            except Exception as ex:
                self.writeLog(u'订阅合约行情异常:{0},{1}'.format(str(ex),traceback.format_exc()))
                continue

    # ----------------------------------------------------------------------
    def writeLog(self, content):
        """快速记录日志"""
        log = VtLogData()
        log.gatewayName = self.gatewayName
        log.logContent = content
        self.gateway.onLog(log)

    # ----------------------------------------------------------------------
    def LoopforceGetContractDict(self, unFishedSet):
        """
        递归强制获取合约信息
        :param unFishedSet:
        :return:
        """
        if len(unFishedSet) > 0:
            new_unfishedSet = set([])
            for symbol, use_contract_type in unFishedSet:
                t_contract_name = self.getContractName(symbol, use_contract_type)
                if t_contract_name != None:
                    self.contract_name_dict[t_contract_name] = {"symbol": symbol, "contract_type": use_contract_type}
                else:
                    new_unfishedSet.add((symbol, use_contract_type))

            self.LoopforceGetContractDict(new_unfishedSet)

    # ----------------------------------------------------------------------
    def initCallback(self):
        """初始化回调函数"""
        # USD_CONTRACT
        unfinished_contract_set = set([])
        for symbol in CONTRACT_SYMBOL:
            self.cbDict["ok_sub_futureusd_{0}_index".format(symbol)] = self.onFutureIndexInfo
            for use_contract_type in CONTRACT_TYPE:
                self.cbDict["ok_sub_futureusd_{0}_ticker_{1}".format(symbol, use_contract_type)] = self.onTicker
                self.cbDict["ok_sub_future_{0}_depth_{1}_usd".format(symbol, use_contract_type)] = self.onDepth
                self.cbDict["ok_sub_futureusd_{0}_trade_{1}".format(symbol, use_contract_type)] = self.onTrade

                t_contract_name = self.getContractName(symbol, use_contract_type)
                if t_contract_name != None:
                    self.contract_name_dict[t_contract_name] = {"symbol": symbol, "contract_type": use_contract_type}
                else:
                    unfinished_contract_set.add((symbol, use_contract_type))

        self.LoopforceGetContractDict(unfinished_contract_set)

        # print self.contract_name_dict.keys()

        self.cbDict['ok_futureusd_trade'] = self.onFutureOrder
        self.cbDict['ok_futureusd_cancel_order'] = self.onFutureOrderCancel

        self.cbDict['ok_futureusd_userinfo'] = self.onFutureUserInfo
        self.cbDict['ok_futureusd_orderinfo'] = self.onFutureOrderInfo

        self.cbDict['ok_sub_futureusd_trades'] = self.onFutureSubTrades
        self.cbDict['ok_sub_futureusd_userinfo'] = self.onFutureSubUserinfo

        self.cbDict['ok_sub_futureusd_positions'] = self.onFutureSubPositions

    # ----------------------------------------------------------------------
    '''
    推送 币币交易指数过去
    websocket.send("{'event':'addChannel','channel':'ok_sub_futureusd_X_index'}");

        ① X值为：btc, ltc, eth, etc, bch
    # Request
    {'event':'addChannel','channel':'ok_sub_futureusd_btc_index'}
    # Response
    [
        {
            "data": {
                "timestamp": "1490341322021",
                "futureIndex": "998.0"
            },
            "channel": "ok_sub_futureusd_btc_index"
        }
    ]
    返回值说明
    
    futureIndex(string): 指数
    timestamp(string): 时间戳
    '''

    def onFutureIndexInfo(self, rest_data):
        """
        指数合约推送
        :param rest_data:
        :return:
        """
        data = rest_data.get('data')

        if data is None:
            return

        channel = rest_data['channel']

        # 获取日期/时间
        t_date, t_time,_ = self.generateDateTime(float(data["timestamp"]))
        # 获取指数值
        float_index = float(data["futureIndex"])

        # 根据channnel，获取合约
        symbol_pattern = self.channelSymbolMap[channel]
        for use_contract_type in CONTRACT_TYPE:
            # 合约，
            symbol = symbol_pattern % (use_contract_type)
            push_dic = {"symbol": symbol, "date": t_date, "time": t_time, "index": float_index}
            # 调用gateway，推送event事件
            self.gateway.onFutureIndexPush(push_dic)

    # ----------------------------------------------------------------------
    """
    期货行情推送 例子
    {
        "data": {
            "limitHigh": "1030.3",
            "vol": 276406,
            "last": 998.05,
            "sell": 998.05,
            "buy": 997.61,
            "unitAmount": 100,
            "hold_amount": 180178,
            "contractId": 20170324034,
            "high": 1049.18,
            "low": 973.15,
            "limitLow": "968.1"
        },
        "channel": "ok_sub_futureusd_btc_ticker_this_week"
    }    
    """

    def onTicker(self, rest_data):
        """
        期货行情推送
        :param rest_data:
        :return:
        """
        data = rest_data.get('data', {})
        if len(data) == 0:
            return

        channel = rest_data.get('channel')
        if channel is None:
            return

        try:
            symbol = self.channelSymbolMap[channel]
            # 首次到达tick，记录在tickDict缓存
            if symbol not in self.tickDict:
                tick = VtTickData()
                tick.exchange = EXCHANGE_OKEX
                tick.symbol =  '.'.join([symbol, tick.exchange])
                tick.vtSymbol = tick.symbol

                tick.gatewayName = self.gatewayName
                self.tickDict[symbol] = tick

                # 创建symbol对应得深度行情字典缓存
                self.bids_depth_dict[symbol] = {}
                self.asks_depth_dict[symbol] = {}
            else:
                tick = self.tickDict[symbol]

            # 更新
            tick.highPrice = float(data['high'])
            tick.lowPrice = float(data['low'])
            tick.lastPrice = float(data['last'])

            if symbol.startswith('btc'):
                tick.volume = float(str(data['vol']).replace(',', ''))
                tick.volume=(tick.volume/tick.lastPrice)*100
            else:
                tick.volume = float(str(data['vol']).replace(',', ''))
                tick.volume=(tick.volume/tick.lastPrice)*10


            self.contractIdToSymbol[str(data["contractId"])] = tick.symbol
            # 待深度行情推送onTick
        except Exception as ex:
            self.gateway.writeError(u'ContractApi.onTicker exception:{}'.format(str(ex)))
            self.gateway.writeLog(rest_data)
            self.gateway.writeLog(u'ContractApi.onTicker exception:{},{}'.format(str(ex), traceback.format_exc()))

    # ----------------------------------------------------------------------
    """
    {
        "data": {
            "timestamp": 1490337551299,
            "asks": [
                [
                    "996.72",
                    "20.0",
                    "2.0065",
                    "85.654",
                    "852.0"
                ]
            ],
            "bids": [
                [
                    "991.67",
                    "6.0",
                    "0.605",
                    "0.605",
                    "6.0"
                ]
        },
        "channel": "ok_sub_futureusd_btc_depth_this_week"
    }

    timestamp(long): 服务器时间戳
    asks(array):卖单深度 数组索引(string) 0 价格, 1 量(张), 2 量(币) 3, 累计量(币) 4,累积量(张)
    bids(array):买单深度 数组索引(string) 0 价格, 1 量(张), 2 量(币) 3, 累计量(币) 4,累积量(张)
    使用描述:
        1，第一次返回全量数据
        2，根据接下来数据对第一次返回数据进行，如下操作
        删除（量为0时）
        修改（价格相同量不同）
        增加（价格不存在）
    {u'binary': 0, u'data': {u'timestamp': 1515745170254L, u'bids': [[33.824, 6, 1.7
738, 1.7738, 6], [33.781, 6, 1.7761, 7.6905, 26], [33.775, 6, 1.7764, 9.4669, 32
], [33.741, 0, 11.2622, 20.1331, 68], [33.692, 6, 1.7808, 991.3629, 3341], [33.6
35, 7, 2.0811, 1213.9536, 4090], [33.633, 0, 2.3786, 1223.1365, 4121], [33.629,
0, 1.1894, 1224.3259, 4125], [33.62, 14, 4.1641, 1218.1177, 4104], [33.617, 0, 1
.4873, 1225.8132, 4130], [33.614, 7, 2.0824, 1221.6875, 4116], [33.601, 0, 1.785
6, 1344.853, 4530], [33.591, 4, 1.1907, 1343.4074, 4525], [33.581, 16, 4.7645, 1
351.7451, 4553], [33.579, 6, 1.7868, 1353.5319, 4559], [33.552, 6, 1.7882, 1360.
0871, 4581], [33.543, 0, 1.4906, 1386.5663, 4670], [33.54, 136, 40.5485, 1423.88
44, 4795], [33.539, 9, 2.6834, 1426.5678, 4804], [33.53, 50, 14.912, 1441.4798,
4854], [33.525, 5, 1.4914, 1442.9712, 4859], [33.522, 5, 1.4915, 1444.4627, 4864
], [33.511, 10, 2.984, 1448.6402, 4878], [33.502, 0, 2.6864, 1409.2357, 4746], [
33.479, 4, 1.1947, 2353.4177, 7909], [25.245, 0, 9.9029, 21190.5178, 68253], [25
.235, 0, 10.3031, 21200.8209, 68279], [25.225, 0, 4.3607, 21205.1816, 68290], [2
5.215, 0, 3.9658, 21209.1474, 68300], [25.205, 0, 3.9674, 21213.1148, 68310]], u
'asks': [[43.493, 0, 3.2189, 21250.6634, 74286], [43.473, 0, 4.8305, 21247.4445,
 74272], [43.463, 0, 5.5219, 21242.614, 74251], [43.453, 0, 5.5232, 21237.0921,
74227], [43.433, 0, 3.6838, 21231.5689, 74203], [34.238, 500, 146.0365, 5495.506
5, 18746], [34.18, 5, 1.4628, 3523.3267, 12000], [34.159, 6, 1.7564, 3521.8639,
11995], [34.137, 5, 1.4646, 3020.1076, 10282], [34.107, 6, 1.7591, 2013.2213, 68
46], [34.105, 4, 1.1728, 2011.4622, 6840], [34.09, 4, 1.1733, 1345.1823, 4568],
[34.072, 500, 146.748, 1324.3485, 4497], [34.07, 6, 1.761, 1177.6005, 3997], [34
.068, 683, 200.4813, 1175.8395, 3991], [34.063, 0, 1.1742, 944.5044, 3203], [34.
039, 15, 4.4067, 975.3582, 3308], [34.025, 0, 1.4695, 943.3302, 3199], [34.024,
7, 2.0573, 970.9515, 3293], [34.014, 0, 1.4699, 941.8607, 3194], [33.992, 5, 1.4
709, 815.0705, 2763], [33.977, 0, 1.4715, 784.8013, 2660], [33.976, 0, 1.4716, 7
83.3298, 2655], [33.971, 0, 2.3549, 781.8582, 2650], [33.968, 0, 2.0607, 779.503
3, 2642], [33.965, 0, 1.1776, 777.4426, 2635], [33.96, 0, 2.0612, 776.265, 2631]
, [33.953, 200, 58.9049, 811.8338, 2752], [33.909, 13, 3.8337, 294.4836, 997], [
33.908, 0, 7.0779, 315.7585, 1069], [33.905, 27, 7.9634, 286.5209, 970], [33.899
, 8, 2.3599, 261.4503, 885], [33.895, 9, 2.6552, 259.0904, 877], [33.894, 0, 4.4
255, 273.2863, 925], [33.893, 13, 3.8356, 256.4352, 868], [33.891, 0, 4.1308, 26
8.8608, 910], [33.884, 6, 1.7707, 252.5996, 855], [33.878, 13, 3.8372, 250.8289,
 849], [33.825, 0, 17.7383, 17.7383, 60]]}, u'channel': u'ok_sub_future_etc_dept
h_this_week_usd'}
    """
    def onDepth(self, rest_data):
        """
        期货深度行情推送
        :param rest_data:
        :return:
        okex期货的深度数据原生返回是张数，需要转换为个数。转换公式（btc把10改为100，其他币种都是乘10）：张数/成交价*10=个数
        """
        channel = rest_data.get('channel')
        data = rest_data.get('data', {})
        if channel is None or len(data) == 0:
            return

        symbol = self.channelSymbolMap.get(channel)
        if symbol is None:
            return

        try:
            if symbol not in self.tickDict:
                tick = VtTickData()
                tick.symbol = symbol
                tick.vtSymbol = symbol
                tick.gatewayName = self.gatewayName
                self.tickDict[symbol] = tick

                self.bids_depth_dict[symbol] = {}
                self.asks_depth_dict[symbol] = {}
            else:
                tick = self.tickDict[symbol]

            tick_bids_depth = self.bids_depth_dict[symbol]
            tick_asks_depth = self.asks_depth_dict[symbol]

            # 更新bids得价格深度
            for inf in data.get('bids', []):
                price1, vol1, vol2, acc_vol1, acc_vol2 = inf
                if abs(float(vol1)) < 0.00001 and price1 in tick_bids_depth:
                    del tick_bids_depth[price1]
                else:
                    tick_bids_depth[price1] = float(vol1)
            volume_rate = 100 if symbol.startswith('btc') else 10

            try:
                # 根据bidPrice价格排序
                arr = sorted(tick_bids_depth.items(), key=lambda x: x[0])

                # 取后五个
                tick.bidPrice1, tick.bidVolume1 = arr[-1]
                tick.bidVolume1 = (tick.bidVolume1 / tick.bidPrice1) * volume_rate
                tick.bidPrice2, tick.bidVolume2 = arr[-2]
                tick.bidVolume2 = (tick.bidVolume2 / tick.bidPrice2) * volume_rate
                tick.bidPrice3, tick.bidVolume3 = arr[-3]
                tick.bidVolume3 = (tick.bidVolume3 / tick.bidPrice3) * volume_rate
                tick.bidPrice4, tick.bidVolume4 = arr[-4]
                tick.bidVolume4 = (tick.bidVolume4 / tick.bidPrice4) * volume_rate
                tick.bidPrice5, tick.bidVolume5 = arr[-5]
                tick.bidVolume5 = (tick.bidVolume5 / tick.bidPrice5) * volume_rate
            except Exception as ex:
                self.writeLog(u'ContractApi.onDepth exception:{0},{1}'.format(str(ex), traceback.format_exc()))

            for inf in data.get('asks', []):
                price1, vol1, vol2, acc_vol1, acc_vol2 = inf
                if abs(float(vol1)) < 0.00001 and price1 in tick_asks_depth:
                    del tick_asks_depth[price1]
                else:
                    tick_asks_depth[price1] = float(vol1)
            try:
                # 根据ask价格排序
                arr = sorted(tick_asks_depth.items(),  key=lambda x: x[0])
                # 取前五个
                tick.askPrice1, tick.askVolume1 = arr[0]
                tick.askVolume1 = (tick.askVolume1 / tick.askPrice1) * volume_rate
                tick.askPrice2, tick.askVolume2 = arr[1]
                tick.askVolume2 = (tick.askVolume2 / tick.askPrice2) * volume_rate
                tick.askPrice3, tick.askVolume3 = arr[2]
                tick.askVolume3 = (tick.askVolume3 / tick.askPrice3) * volume_rate
                tick.askPrice4, tick.askVolume4 = arr[3]
                tick.askVolume4 = (tick.askVolume4 / tick.askPrice4) * volume_rate
                tick.askPrice5, tick.askVolume5 = arr[4]
                tick.askVolume5 = (tick.askVolume5 / tick.askPrice5) * volume_rate

            except Exception as ex:
                self.writeLog(u'ContractApi.onDepth exception:{0},{1}'.format(str(ex), traceback.format_exc()))

            tick.date, tick.time ,tick.datetime= self.generateDateTime(data['timestamp'])

            # 推送onTick事件
            newtick = copy(tick)
            self.gateway.onTick(newtick)

        except Exception as ex:
            self.writeLog(u'ContractApi.onDepth exception:{0},{1}'.format(str(ex), traceback.format_exc()))

    # ----------------------------------------------------------------------
    """
        [
            {
                "data":{
                    "result":true,
                    "order_id":5017287829
                },
                "channel":"ok_futureusd_trade"
            }
        ]

        real data
        {u'binary': 0, u'data': {u'order_id': 230874269033472L, u'result': True}, u'channel': u'ok_futureusd_trade'}
    """

    def onTrade(self, rest_data):
        """
        委托全部成交回报
        :param rest_data:
        :return:
        """
        self.writeLog(u'onTrade {0}'.format(rest_data))

    # ----------------------------------------------------------------------

    """
        {
            "data":{
                "result":true,
                "order_id":"5017402127"
            },
            "channel":"ok_futureusd_order"
        }
    """
    def onFutureOrder(self, rest_data):
        """
        委托下单请求响应，
        :param rest_data: 出错代码，或者委托成功得order_id
        :return:
        """

        data = rest_data.get('data', {})
        error_code = data.get('error_code')
        if error_code is not None:
            self.gateway.writeError(u'onFutureOrder委托返回错误:{0}'.format(FUTURES_ERROR_DICT.get(str(error_code))), error_id=error_code)
            self.gateway.writeLog(rest_data)
            localNo = self.localNoQueue.get_nowait()
            if localNo is None:
                return

            self.gateway.writeLog(u'onFutureOrder移除本地localNo:{}'.format(localNo))
            vtOrderId = '.'.join([self.gatewayName,str(localNo)])
            order = self.localOrderDict.get(vtOrderId)
            if order:
                order.orderID=localNo
                dt = datetime.now()
                order.symbol= '.'.join([order.symbol,self.gatewayName])
                order.vtsymbol=order.symbol
                order.totalVolume=order.totalVolume
                order.cancelTime = dt.strftime("%H:%M:%S.%f")
                order.status = STATUS_REJECTED
                self.gateway.writeLog(u'onFutureOrder发出OnOrder，拒单,vtOrderId={}'.format(vtOrderId))
                # 发送期货委托单（拒单消息）到vtGateway
                self.gateway.onOrder(order)
            return
        ok_order_id = data.get('order_id')

        if ok_order_id is None:
            self.gateway.writeError(u'FuturesApi.onFutureOrder 委托返回中，没有orderid')
            self.gateway.writeLog(rest_data)
            return

        ok_order_id = str(ok_order_id)

        # 从本地编号Queue中，FIFO，提取最早的localNo
        localNo = self.localNoQueue.get_nowait()
        if localNo is None:
            self.gateway.writeError(u'FuturesApi.onSportOrder，未找到本地LocalNo，检查日志')
            self.gateway.writeLog(rest_data)
            return

        self.gateway.writeLog(u'FuturesApi.onSportOrder,绑定 local:{}  <==> ok_order_id:{}'.format(localNo, ok_order_id))
        self.localNoDict[localNo] = ok_order_id
        self.orderIdDict[ok_order_id] = localNo

    # ----------------------------------------------------------------------
    """
            {
                "data":{
                    "result":true,
                    "order_id":"5017402127"
                },
                "channel":"ok_futureusd_cancel_order"
            }
        """
    def onFutureOrderCancel(self, rest_data):
        """
        委托撤单的响应"
        :param rest_data: 
        :return: 
        """""
        data = rest_data.get('data', {})

        if 'error_code' in data:
            error_code = data.get('error_code', 0)
            self.gateway.writeError(u'OKEXGatewayCCH.onFutureOrderCancel 委托返回错误:{}'.format(FUTURES_ERROR_DICT.get(error_code)), error_id=error_code)
            self.gateway.writeLog(rest_data)
            return

        ok_order_id = data.get('order_id')
        if ok_order_id is None:
            self.gateway.writeError(u'OKEXGatewayCCH.onFutureOrderCancel 委托返回中，没有orderid')
            self.gateway.writeLog(rest_data)
            return

        ok_order_id = str(ok_order_id)
        # 获取本地委托流水号
        localNo = self.orderIdDict[ok_order_id]

        # 发送onOrder事件
        order = self.orderDict[ok_order_id]
        dt = datetime.now()
        order.cancelTime = dt.strftime("%H:%M:%S.%f")
        order.status = STATUS_CANCELLED
        # 发送现货委托单（撤单消息）到vtGateway
        self.gateway.onOrder(order)

        # 删除本地委托号与orderid的绑定
        #del self.orderDict[orderId]
        #del self.orderIdDict[orderId]
        #del self.localNoDict[localNo]


    '''
    逐仓返回：
    [{
        "channel": "ok_futureusd_userinfo",
        "data": {
            "info": {
                "btc": {
                    "balance": 0.00000673,
                    "contracts": [{
                        "available": 0.00000673,
                        "balance": 0,
                        "bond": 0,
                        "contract_id": 20150327013,
                        "contract_type": "quarter",
                        "freeze": 0,
                        "profit": 0,
                        "unprofit": 0
                    }],
                    "rights": 0.00000673
                },
                "ltc": {
                    "balance": 0.00007773,
                    "contracts": [{
                        "available": 16.5915,
                        "balance": 0,
                        "bond": 0.70871722,
                        "contract_type": "this_week",
                        "contractid": 20150130115,
                        "freeze": 0,
                        "profit": 17.30020414,
                        "unprofit": -1.8707
                    }, {
                        "available": 0.00007773,
                        "balance": 0.03188496,
                        "bond": 0,
                        "contract_type": "quarter",
                        "contractid": 20150327116,
                        "freeze": 0,
                        "profit": -0.03188496,
                        "unprofit": 0
                    }],
                    "rights": 0.00007773
                }
            },
            "result": true
        }
    }]
   
    
    逐仓信息
    balance(double):账户余额
    available(double):合约可用
    balance(double):合约余额
    bond(double):固定保证金
    contract_id(long):合约ID
    contract_type(string):合约类别
    freeze(double):冻结
    profit(double):已实现盈亏
    unprofit(double):未实现盈亏
    rights(double):账户权益
    
    全仓返回：
    [{
        "channel": "ok_futureusd_userinfo",
        "data": {
            "info": {
                "btc": {
                    "account_rights": 1,
                    "keep_deposit": 0,
                    "profit_real": 3.33,
                    "profit_unreal": 0,
                    "risk_rate": 10000
                },
                "ltc": {
                    "account_rights": 2,
                    "keep_deposit": 2.22,
                    "profit_real": 3.33,
                    "profit_unreal": 2,
                    "risk_rate": 10000
                }
            },
            "result": true
        }
    }]
    balance(double): 账户余额
    symbol(string)：币种
    keep_deposit(double)：保证金
    profit_real(double)：已实现盈亏
    unit_amount(int)：合约价值
    
    实际日志（逐仓模式)
  {u'info': {
     u'ltc': {u'contracts': [], u'balance': 0, u'rights': 0}, 
     u'bch': {u'contracts': [], u'balance': 0, u'rights': 0}, 
     u'eos': {u'contracts': [], u'balance': 0, u'rights': 0}, 
     u'etc': {u'contracts': [], u'balance': 4.98, u'rights': 4.98}, 
     u'btg': {u'contracts': [], u'balance': 0, u'rights': 0}, 
     u'btc': {u'contracts': [], u'balance': 0, u'rights': 0}, 
     u'eth': {u'contracts': [], u'balance': 0, u'rights': 0}, 
     u'xrp': {u'contracts': [], u'balance': 0, u'rights': 0}
     },u'result': True}
    # 逐仓模式，持有期货合约
    u'etc': {u'contracts': 
              [{u'available': 4.92279753, u'contract_id': 201802160040063L, u'profit': -0.01702246, u'unprofit': 0.0005, 
                u'freeze': 0, u'contract_type': u'this_week', u'balance': 0.05720247, u'bond': 0.04018001}], 
            u'balance': 4.92279753, u'rights': 4.96357399},
 
  {'eos': {'risk_rate': 10000, 'account_rights': 0, 'profit_real': 0, 'keep_deposit': 0, 'profit_unreal': 0}, 
  'btc': {'risk_rate': 10000, 'account_rights': 0.01, 'profit_real': 0, 'keep_deposit': 0, 'profit_unreal': 0}, 
  'ltc': {'risk_rate': 10000, 'account_rights': 0, 'profit_real': 0, 'keep_deposit': 0, 'profit_unreal': 0}, 
  'etc': {'risk_rate': 10000, 'account_rights': 0, 'profit_real': 0, 'keep_deposit': 0, 'profit_unreal': 0}, 
  'xrp': {'risk_rate': 10000, 'account_rights': 0, 'profit_real': 0, 'keep_deposit': 0, 'profit_unreal': 0}, 
  'eth': {'risk_rate': 10000, 'account_rights': 0, 'profit_real': 0, 'keep_deposit': 0, 'profit_unreal': 0}, 
  'btg': {'risk_rate': 10000, 'account_rights': 0, 'profit_real': 0, 'keep_deposit': 0, 'profit_unreal': 0}, 
  'bch': {'risk_rate': 10000, 'account_rights': 0, 'profit_real': 0, 'keep_deposit': 0, 'profit_unreal': 0}}
    '''

    # ----------------------------------------------------------------------
    def onFutureUserInfo(self, rest_data):
        """
        合约账户信息推送（账户权益/已实现盈亏/未实现盈亏/可用/已用/冻结）
        :param rest_data:
        :return:
        """
        data = rest_data.get('data', {})
        if len(data) == 0:
            self.writeLog(u'FuturesApi.onFutureUserInfo:not data in:{0}'.format(rest_data))
            return

        info = data.get("info", {})
        if len(data) == 0:
            self.writeLog(u'FuturesApi.onFutureUserInfo:not info in:{0}'.format(rest_data))
            return

        for symbol, s_inf in info.items():
            if "account_rights" in s_inf.keys():
                # 说明是 全仓返回
                account = VtAccountData()
                account.gatewayName = self.gatewayName
                account.accountID = u'[全仓]{0}'.format(symbol)
                account.vtAccountID = account.accountID
                account.balance = float(s_inf.get('account_rights',0.0))
                account.closeProfit = float(s_inf.get('profit_real',0.0))
                account.positionProfit = float(s_inf.get('profit_unreal',0.0))
                account.margin = float(s_inf.get('keep_deposit',0.0))
                self.gateway.onAccount(account)

                # 如果该合约账号的净值大于0,则通过rest接口，逐一合约类型获取持仓
                if account.balance > 0:
                    for contractType in CONTRACT_TYPE:
                        self.query_future_position(symbol=symbol,contractType=contractType,leverage=self._use_leverage)
            else:
                # 说明是逐仓返回
                t_contracts = s_inf.get('contracts',[])
                t_balance = float(s_inf.get('balance',0.0))
                t_rights = float(s_inf.get('rights',0.0))

                #if t_balance > 0 or t_rights > 0:
                account = VtAccountData()
                account.gatewayName = self.gatewayName + u'_合约'
                account.accountID = u'[逐仓]{0}'.format(symbol)
                account.vtAccountID = account.accountID
                account.balance = t_rights
                account.available = t_balance
                for contract in t_contracts:
                    # 保证金
                    account.margin += contract.get('bond',0.0)
                    # 平仓盈亏
                    account.closeProfit += contract.get('profit',0.0)
                    # 持仓盈亏
                    account.positionProfit += contract.get('unprofit',0.0)

                    if account.balance > 0 or account.available > 0:
                        for contractType in CONTRACT_TYPE:
                            self.query_future_position_4fix(symbol=symbol, contractType=contractType)

                self.gateway.onAccount(account)

    def query_future_position(self, symbol, contractType, leverage):
        """全仓模式下，查询持仓信息"""
        qry_symbol = '{}_usd'.format(symbol) + ':' + contractType
        if qry_symbol in self.queryed_pos_symbols:
            sleep(0.1)
        else:
            self.queryed_pos_symbols.add(qry_symbol)
        resp = self.get_future_position(symbol='{0}_usd'.format(symbol), contractType=contractType)
        result = resp.get('result', False)
        holdings = resp.get('holding', [])

        # 100美金（btc）；10美金（其他）/每份合约
        volume_rate = 100 if symbol.startswith('btc') else 10

        if result and len(holdings) > 0:
            for holding in holdings:
                pos = VtPositionData()
                pos.gatewayName = self.gatewayName
                contract_symbol = holding.get('symbol', '{0}_usd'.format(symbol)) + ':' + contractType + ':' + str(
                    holding.get('lever_rate', leverage))
                # 如果此合约不在已订阅清单中，重新订阅
                if contract_symbol not in self.registered_symbols:
                    v = VtSubscribeReq()
                    v.symbol = contract_symbol
                    self.subscribe(v)

                pos.symbol = '.'.join([contract_symbol, EXCHANGE_OKEX])
                pos.vtSymbol = pos.symbol
                tick = self.tickDict.get(contract_symbol, None)

                if holding.get('buy_amount', 0) > 0:
                    # 持有多仓
                    pos.direction = DIRECTION_LONG
                    pos.vtPositionName = pos.symbol + pos.direction
                    pos.price = holding.get('buy_price_avg', 0.0)
                    price = tick.lastPrice if tick is not None else pos.price
                    if price == 0.0:
                        continue

                    pos.position = volume_rate * holding.get('buy_amount') / price
                    if holding.get('buy_available') > 0:
                        pos.frozen = pos.position - (volume_rate * holding.get('buy_available') / price)
                    else:
                        pos.frozen = 0

                    pos.positionProfit = holding.get('buy_profit_real', 0.0)
                    self.gateway.onPosition(pos)
                if holding.get('sell_amount', 0) > 0:
                    sell_pos = copy(pos)                   # 原为copy.copy(pos)--陈常鸿
                    sell_pos.direction = DIRECTION_SHORT
                    sell_pos.vtPositionName = sell_pos.symbol + sell_pos.direction
                    sell_pos.price = holding.get('sell_price_avg', 0.0)
                    price = tick.lastPrice if tick is not None else sell_pos.price
                    if price == 0:
                        continue
                    sell_pos.position = volume_rate * holding.get('sell_amount') / price
                    if holding.get('sell_available', 0) > 0:
                        sell_pos.frozen = sell_pos.position - volume_rate * holding.get('sell_available') / price
                    else:
                        sell_pos.frozen = 0
                    sell_pos.positionProfit = holding.get('sell_profit_real', 0.0)
                    self.gateway.onPosition(sell_pos)

    def query_future_position_4fix(self,symbol, contractType,leverate=1):
        """
        :param symbol:
        :param contractType:
        :param leverate: 默认返回10倍杠杆持仓 type=1 返回全部持仓数据
        :return:
        """
        qry_symbol = '{0}_usd'.format(symbol) + ':' + contractType
        if qry_symbol in self.queryed_pos_symbols:
            sleep(0.1)
        else:
            self.queryed_pos_symbols.add(qry_symbol)
        resp = self.get_future_position_4fix(symbol='{0}_usd'.format(symbol), contractType=contractType,type1=leverate)
        result = resp.get('result', False)
        holdings = resp.get('holding', [])

        # 100美金（btc）；10美金（其他）/每份合约
        volume_rate = 100 if symbol.startswith('btc') else 10

        if result and len(holdings) > 0:
            for holding in holdings:
                pos = VtPositionData()
                pos.gatewayName = self.gatewayName
                contract_symbol = holding.get('symbol', '{0}_usd'.format(symbol)) + ':' + contractType + ':' + str(
                    holding.get('lever_rate', 10))
                # 如果此合约不在已订阅清单中，重新订阅
                if contract_symbol not in self.registered_symbols:
                    v = VtSubscribeReq()
                    v.symbol = contract_symbol
                    self.subscribe(v)

                pos.symbol = '.'.join([contract_symbol, EXCHANGE_OKEX])
                pos.vtSymbol = pos.symbol
                tick = self.tickDict.get(contract_symbol, None)

                if holding.get('buy_amount', 0) > 0:
                    # 持有多仓
                    pos.direction = DIRECTION_LONG
                    pos.vtPositionName = pos.symbol + pos.direction
                    pos.price = holding.get('buy_price_avg', 0.0)
                    price = tick.lastPrice if tick is not None else pos.price
                    if price == 0.0:
                        continue

                    pos.position = volume_rate * holding.get('buy_amount') / price
                    if holding.get('buy_available') > 0:
                        pos.frozen = pos.position - (volume_rate * holding.get('buy_available') / price)
                    else:
                        pos.frozen = 0

                    pos.positionProfit = holding.get('buy_profit_real', 0.0)
                    self.gateway.onPosition(pos)
                if holding.get('sell_amount', 0) > 0:
                    sell_pos = copy(pos)                       # 原为copy.copy(pos)--陈常鸿
                    sell_pos.direction = DIRECTION_SHORT
                    sell_pos.vtPositionName = sell_pos.symbol + sell_pos.direction
                    sell_pos.price = holding.get('sell_price_avg', 0.0)
                    price = tick.lastPrice if tick is not None else sell_pos.price
                    if price == 0:
                        continue
                    sell_pos.position = volume_rate * holding.get('sell_amount') / price
                    if holding.get('sell_available', 0) > 0:
                        sell_pos.frozen = sell_pos.position - volume_rate * holding.get('sell_available') / price
                    else:
                        sell_pos.frozen = 0
                    sell_pos.positionProfit = holding.get('sell_profit_real', 0.0)
                    self.gateway.onPosition(sell_pos)
    # ----------------------------------------------------------------------
    """ 所有委托查询回报 ws_data
    {
        "data":{
            "result":true,
            "orders":[
                {
                    "fee":0,
                    "amount":1,
                    "price":2,
                    "contract_name":"LTC0331",
                    "symbol":"ltc_usd",
                    "create_date":1490347972000,
                    "status":-1,
                    "lever_rate":10,
                    "deal_amount":0,
                    "price_avg":0,
                    "type":1,
                    "order_id":5017402127,
                    "unit_amount":10
                }
            ]
        },
        "channel":"ok_futureusd_orderinfo"
        }
    """
    def onFutureOrderInfo(self, rest_data):
        """
        所有委托信息查询响应
        :param rest_data:
        :return:
        """
        error_code = rest_data.get('error_code')
        if error_code is not None:
            self.gateway.writeError(u'期货委托查询报错:{0}'.format(FUTURES_ERROR_DICT.get(error_code)), error_id=error_code)
            self.gateway.writeLog(u'FutureApi.onFutureOrderInfo:{0}'.format(rest_data))
            return

        data = rest_data.get("data", {})

        for order_data in data.get('orders', []):
            orderId = str(order_data['order_id'])
            localNo = str(self.localNo)

            if orderId not in self.orderIdDict:
                # orderId 不在本地缓存中，需要增加一个绑定关系
                while str(self.localNo) in self.localNoDict:
                    self.localNo += 1
                localNo = str(self.localNo)
                # 绑定localNo 与 orderId
                self.localNoDict[localNo] = orderId
                self.orderIdDict[orderId] = localNo
                self.writeLog(u'onFutureOrderInfo add orderid: local:{0}<=>okex:{1}'.format(localNo,orderId))
            else:
                # orderid在本地缓存中，
                localNo = self.orderIdDict[orderId]
                # 检验 localNo是否在本地缓存，没有则补充
                if localNo not in self.localNoDict:
                    self.localNoDict[localNo] = orderId
                    self.writeLog(u'onFutureOrderInfo update orderid: local:{0}<=>okex:{1}'.format(localNo, orderId))


            # order新增或更新在orderDict
            if orderId not in self.orderDict:
                order = VtOrderData()
                order.gatewayName = self.gatewayName
                contract_name = order_data["contract_name"]
                dic_info = self.contract_name_dict[contract_name]

                use_contract_type = dic_info["contract_type"]
                order.symbol = '.'.join([order_data["symbol"] + ":" + use_contract_type + ":" + str(self._use_leverage), EXCHANGE_OKEX])

                order.vtSymbol = order.symbol
                order.orderID = self.orderIdDict[orderId]                # 更新orderId为本地的序列号
                order.vtOrderID = '.'.join([self.gatewayName, order.orderID])

                order.price = order_data['price']
                volume_rate = 100 if order.symbol.startswith('btc') else 10
                order.totalVolume = order_data['amount'] * volume_rate
                order.direction, offset = priceContractOffsetTypeMap[str(order_data['type'])]
                order.orderTime = datetime.now().strftime("%H:%M:%S.%f")
                self.orderDict[orderId] = order
                self.gateway.writeLog(u'新增本地orderDict缓存,okex orderId:{0},order.orderid:{1}'.format(orderId, order.orderID))
            else:
                order = self.orderDict[orderId]
                volume_rate = 100 if order.symbol.startswith('btc') else 10

            order.tradedVolume = order_data['deal_amount'] * volume_rate
            order.status = statusMap[int(order_data['status'])]
            # 推送期货委托单到vtGatway.OnOrder中
            self.gateway.onOrder(copy(order))

    '''
    [{
            "data":{
                amount:1
                contract_id:20170331115,
                contract_name:"LTC0331",
                contract_type:"this_week",
                create_date:1490583736324,
                create_date_str:"2017-03-27 11:02:16",
                deal_amount:0,
                fee:0,
                lever_rate:20,
                orderid:5058491146,
                price:0.145,
                price_avg:0,
                status:0,
                system_type:0,
                type:1,
                unit_amount:10,
                user_id:101
            },
            "channel":"ok_sub_futureusd_trades"
        }
    ]
实际数据
    ok_sub_futureusd_trades
{u'binary': 0, u'data': {u'orderid': 230874269033472L, u'contract_name': u'ETC0216', u'fee': 0.0, u'
user_id': 6548935, u'contract_id': 201802160040063L, u'price': 24.555, u'create_date_str': u'2018-02
-10 18:34:21', u'amount': 1.0, u'status': 0, u'system_type': 0, u'unit_amount': 10.0, u'price_avg':
0.0, u'contract_type': u'this_week', u'create_date': 1518258861775L, u'lever_rate': 10.0, u'type': 1
, u'deal_amount': 0.0}, u'channel': u'ok_sub_futureusd_trades'}
ok_sub_futureusd_trades
{u'binary': 0, u'data': {u'orderid': 230874269033472L, u'contract_name': u'ETC0216', u'fee': 0.0, u'
user_id': 6548935, u'contract_id': 201802160040063L, u'price': 24.555, u'create_date_str': u'2018-02
-10 18:34:21', u'amount': 1.0, u'status': 0, u'system_type': 0, u'unit_amount': 10.0, u'price_avg':
0.0, u'contract_type': u'this_week', u'create_date': 1518258861775L, u'lever_rate': 10.0, u'type': 1
, u'deal_amount': 0.0}, u'channel': u'ok_sub_futureusd_trades'}
    '''

    # ----------------------------------------------------------------------
    def onFutureSubTrades(self, rest_data):
        """
        交易信息回报
        :param rest_data:
        :return:
        """
        error_code = rest_data.get('error_code')
        if error_code is not None:
            self.gateway.writeError(u'期货交易报错:{0}'.format(FUTURES_ERROR_DICT.get(error_code)), error_id=error_code)
            self.gateway.writeLog(u'FutureApi.onFutureSubTrades:{0}'.format(rest_data))
            return

        data = rest_data.get("data")
        if data is None:
            self.gateway.writeError(u'期货交易报错:No data')
            self.gateway.writeLog(u'FutureApi.onFutureSubTrades: not data {0}'.format(rest_data))
            return

        orderId = str(data["orderid"])                          # okex的委托编号
        use_contract_type = data["contract_type"]               # 合约类型

        localNo = self.orderIdDict.get(orderId, None)           # 本地委托编号

        if localNo == None:
            self.gateway.writeError(u'期货交易回报，非本地发出:orderid={0}'.format(orderId))
            self.gateway.writeLog(u'FutureApi.onFutureSubTrades: not localNo,bypassing this trade {0}'.format(rest_data))
            return

        # 委托信息,如果不在本地缓存，创建一个
        if orderId not in self.orderDict:
            order = VtOrderData()
            order.gatewayName = self.gatewayName
            contract_name = data.get("contract_name")
            contract_info = self.contract_name_dict.get(contract_name)
            if contract_info is None:
                self.gateway.writeError(u'期货交易回报，找不到合约的登记信息')
                self.gateway.writeLog(u'no contract_name:{0} in self.contract_name_dict'.format(contract_name))
                return

            symbol = contract_info["symbol"] + "_usd"                                        # 自动添加_usd结尾
            future_symbol = ':'.join([symbol, use_contract_type, str(self._use_leverage)])   # 满足格式
            order.symbol = '.'.join([future_symbol, EXCHANGE_OKEX])                          # 合约.OKE
            order.vtSymbol = order.symbol
            order.orderID = localNo
            order.vtOrderID = '.'.join([self.gatewayName, order.orderID])
            order.price = float(data['price'])
            volume_rate = 100 if order.symbol.startswith('btc') else 10
            order.totalVolume = float(data['amount'])* volume_rate
            order.direction, order.offset = priceContractOffsetTypeMap[str(data['type'])]
            order.tradedVolume = float(data['deal_amount']) * volume_rate
            order.status = statusMap[data['status']]
            self.orderDict[orderId] = order
            self.gateway.writeLog(u'新增order,orderid:{0},symbol:{1},data:{2}'.format(order.orderID,order.symbol,data))
        else:
            # 更新成交数量/状态
            order = self.orderDict[orderId]
            volume_rate = 100 if order.symbol.startswith('btc') else 10
            self.gateway.writeLog(u'orderid:{0},tradedVolume:{1}=>{2},status:{3}=>{4}'
                                  .format(order.orderID,
                                          order.tradedVolume, float(data['deal_amount']*volume_rate),
                                          order.status, statusMap[data['status']]))
            order.tradedVolume = float(data['deal_amount']) * volume_rate
            order.status = statusMap[data['status']]

        # 发出期货委托事件
        self.gateway.onOrder(copy(order))

        # 判断成交数量是否有变化，有，则发出onTrade事件
        bef_volume = self.recordOrderId_BefVolume.get(orderId, 0.0)
        now_volume = float(data['deal_amount']) - bef_volume

        if now_volume > 0.000001:
            self.recordOrderId_BefVolume[orderId] = float(data['deal_amount'])
            trade = VtTradeData()
            trade.gatewayName = self.gatewayName
            trade.symbol = order.symbol
            trade.vtSymbol = order.symbol
            self.tradeID += 1
            trade.tradeID = str(self.tradeID)
            trade.vtTradeID = '.'.join([self.gatewayName, trade.tradeID])
            trade.orderID = localNo
            trade.vtOrderID = '.'.join([self.gatewayName, trade.orderID])

            trade.price = float(data['price'])
            volume_rate = 100 if trade.symbol.startswith('btc') else 10
            trade.volume = float(now_volume) * volume_rate

            trade.direction, trade.offset = priceContractOffsetTypeMap[str(data['type'])]

            trade.tradeTime = (data["create_date_str"].split(' '))[1]  # u'2018-02-10 18:34:21

            self.gateway.onTrade(trade)

    def onFutureSubDeals(self,rest_data):
        """市场上所有得合约成交"""
        pass

    '''
    # Response    
    OKEX的合约是OKEX推出的以BTC/LTC等币种进行结算的虚拟合约产品，每一张合约分别代表100美元的BTC，或10美元的其他币种（LTC,ETH等）
    
逐仓信息
 [{
        "data":{
            "balance":10.16491751,
            "symbol":"ltc_usd",
            "contracts":[
                {
                    "bond":0.50922987,
                    "balance":0.50922987,
                    "profit":0,
                    "freeze":1.72413792,
                    "contract_id":20170331115,
                    "available":6.51526374
                },
                {
                    "bond":0,
                    "balance":0,
                    "profit":0,
                    "freeze":1.64942529,
                    "contract_id":20170407135,
                    "available":6.51526374
                },
                {
                    "bond":0,
                    "balance":0,
                    "profit":0,
                    "freeze":0.27609056,
                    "contract_id":20170630116,
                    "available":6.51526374
                }
            ]
        },
        "channel":"ok_sub_futureusd_userinfo"
    }]
全仓信息
[{
    "data":{
        "balance":0.2567337,
        "symbol":"btc_usd",
        "keep_deposit":0.0025,
        "profit_real":-0.00139596,
        "unit_amount":100
    },
    "channel":"ok_sub_futureusd_userinfo"
}]

信息
{u'binary': 0, u'data': {u'contracts': [{u'available': 4.96199982, u'contract_id': 201802160040063L,
 u'profit': -0.01800018, u'freeze': 0.0, u'balance': 0.01800018, u'bond': 0.0}], u'symbol': u'etc_us
d', u'balance': 4.96199982}, u'channel': u'ok_sub_futureusd_userinfo'}
    '''

    # ----------------------------------------------------------------------
    def onFutureSubUserinfo(self, rest_data):
        """
        子账户持仓信息回报
        :param rest_data:
        :return:
        """
        u_inf = rest_data['data']
        self.gateway.writeLog(u'onFutureSubUserinfo:{0}'.format(rest_data))
        if "account_rights" in u_inf.keys():
            # 说明是 全仓返回
            pass
            # account = VtAccountData()
            # account.gatewayName = self.gatewayName
            # account.accountID = self.gatewayName
            # account.vtAccountID = account.accountID
            # account.balance = float(u_inf["account_rights"])
            # self.gateway.onAccount(account)
        else:
            pass
            # 感觉这里不是很有用。。。  遂放弃

            # 说明是逐仓返回

            # t_contracts = u_inf["contracts"]
            # t_symbol = u_inf["symbol"].replace('_usd',"")
            # for one_contract in t_contracts:
            #     account = VtAccountData()
            # account.gatewayName = self.gatewayName
            # account.accountID = self.gatewayName

            # pos = VtPositionData()
            # pos.gatewayName = self.gatewayName
            # #pos.symbol = t_symbol + "_usd:%s:%s.%s" % ( one_contract["contract_type"], self._use_leverage,EXCHANGE_OKEX)
            # pos.symbol = self.contractIdToSymbol[one_contract["contract_id"]]
            # pos.vtSymbol = pos.symbol
            # pos.direction = DIRECTION_NET
            # pos.frozen = float(one_contract["freeze"])
            # pos.position = pos.frozen + float(one_contract["balance"])

            # self.gateway.onPosition(pos)

    def get_future_position(self,symbol,contractType):
        """
        全仓用户持仓查询
        https://github.com/okcoin-okex/API-docs-OKEx.com/blob/master/API-For-Futures-CN/%E5%90%88%E7%BA%A6%E4%BA%A4%E6%98%93REST%20API.md

        :return:
        """
        try:
            future_rest = OKCoinFuture(url=OKEX_FUTURE_HOST, apikey=self.gateway.apiKey, secretkey=self.gateway.secretKey)
            resp = future_rest.future_position(symbol, contractType)
            result = json.loads(resp)
        except Exception as ex:
            self.writeLog(u'get_future_position异常:{0},{1}'.format(str(ex),traceback.format_exc()))
            result = {'result':False}
        return result

    def get_future_position_4fix(self,symbol,contractType,type1):
        """
        逐仓用户持仓查询
        https://github.com/okcoin-okex/API-docs-OKEx.com/blob/master/API-For-Futures-CN/%E5%90%88%E7%BA%A6%E4%BA%A4%E6%98%93REST%20API.md

        :return:
        """
        try:
            future_rest = OKCoinFuture(url = OKEX_FUTURE_HOST,apikey=self.gateway.apiKey,secretkey=self.gateway.secretKey)
            resp = future_rest.future_position_4fix(symbol,contractType,type1)
            result = json.loads(resp)
        except Exception as ex:
            self.writeLog(u'get_future_position_4fix异常:{0},{1}'.format(str(ex), traceback.format_exc()))
            result = {'result': False}
        return result

    '''
    [{
        "data":{
            "positions":[
                {
                    "position":"1",
                    "contract_name":"BTC0630",
                    "costprice":"994.89453079",
                    "bondfreez":"0.0025",
                    "avgprice":"994.89453079",
                    "contract_id":20170630013,
                    "position_id":27782857,
                    "eveningup":"0",
                    "hold_amount":"0",
                    "margin":0,
                    "realized":0
                },
                {
                    "position":"2",
                    "contract_name":"BTC0630",
                    "costprice":"1073.56",
                    "bondfreez":"0.0025",
                    "avgprice":"1073.56",
                    "contract_id":20170630013,
                    "position_id":27782857,
                    "eveningup":"0",
                    "hold_amount":"0",
                    "margin":0,
                    "realized":0
                }
            ],
            "symbol":"btc_usd",
            "user_id":101
        },
        "channel":"ok_sub_futureusd_positions"
}]
逐仓返回
[{
    "data":{
        "positions":[
            {
                "position":"1",
                "profitreal":"0.0",
                "contract_name":"LTC0407",
                "costprice":"0.0",
                "bondfreez":"1.64942529",
                "forcedprice":"0.0",
                "avgprice":"0.0",
                "lever_rate":10,
                "fixmargin":0,
                "contract_id":20170407135,
                "balance":"0.0",
                "position_id":27864057,
                "eveningup":"0.0",
                "hold_amount":"0.0"
            },
            {
                "position":"2",
                "profitreal":"0.0",
                "contract_name":"LTC0407",
                "costprice":"0.0",
                "bondfreez":"1.64942529",
                "forcedprice":"0.0",
                "avgprice":"0.0",
                "lever_rate":10,
                "fixmargin":0,
                "contract_id":20170407135,
                "balance":"0.0",
                "position_id":27864057,
                "eveningup":"0.0",
                "hold_amount":"0.0"
            }
        "symbol":"ltc_usd",
        "user_id":101
    }]

    实际返回的数据
    
    {u'binary': 0, u'data': 
    {u'positions': 
    [{u'contract_name': u'ETC0216', u'balance': 0.01606774, u'contract_id': 201802160040063L, u'fixmargin': 0.0,
     u'position_id': 73790127, u'avgprice': 24.46, u'eveningup': 0.0, u'profitreal': -0.01606774, 
     u'hold_amount': 0.0, u'costprice': 24.46, u'position': 1, u'lever_rate': 10, u'bondfreez': 0.04085301, u'forcedprice': 0.0}, 
     
    {u'contract_name': u'ETC0216', u'balance': 0.01606774, u'contract_id': 201802160040063L, u'fixmargin': 0.0,
     u'position_id': 73790127,u'avgprice': 0.0, u'eveningup': 0.0, u'profitreal': -0.01606774, 
     u'hold_amount': 0.0, u'costprice':0.0, u'position': 2, u'lever_rate': 10, u'bondfreez': 0.04085301, u'forcedprice': 0.0}, 
     {u'contract_name': u'ETC0216', u'balance': 0.01606774, u'contract_id': 201802160040063L, u'fixmargin': 0.0, 
     u'position_id': 73790127, u'avgprice': 0.0, u'eveningup': 0.0, u'profitreal': -0.01606774, u'hold_amount': 0.0, 
     u'costprice': 0.0, u'position': 1, u'lever_rate': 20, u'bondfreez': 0.04085301, u'forcedprice': 0.0}, 
     {u'contract_name': u'ETC0216', u'balance': 0.01606774, u'contract_id': 201802160040063L, u'fixmargin': 
     0.0, u'position_id': 73790127, u'avgprice': 0.0, u'eveningup': 0.0, u'profitreal': -0.01606774, 
     u'hold_amount': 0.0, u'costprice': 0.0, u'position': 2, u'lever_rate': 20, u'bondfreez': 0.04085301, u'forcedprice': 0.0}],
      u'symbol': u'etc_usd', u'user_id': 6548935}, u'channel': u'ok_sub_futureusd_positions'}
        '''

    # ----------------------------------------------------------------------
    def onFutureSubPositions(self, rest_data):
        """期货子账号仓位推送"""
        # 这个逐仓才是实际的仓位 ！！

        data = rest_data.get("data")
        if data is None:
            self.gateway.writeError(u'onFutureSubPositions: no data in :{0}'.format(rest_data))
            return

        symbol = data.get('symbol')
        user_id = data.get('user_id')

        positions = data["positions"]
        for inf in positions:
            if 'fixmargin' in inf.keys():
                # 逐仓模式
                contract_name = inf["contract_name"]
                position_leverage = str(inf["lever_rate"])

                # print contract_name , position_leverage , self._use_leverage
                if int(position_leverage) == int(self._use_leverage):
                    dic_inf = self.contract_name_dict[contract_name]
                    use_contract_type = dic_inf["contract_type"]

                    # print dic_inf
                    pos = VtPositionData()
                    pos.gatewayName = self.gatewayName

                    if int(inf["position"]) == 1:
                        pos.direction = DIRECTION_LONG
                    else:
                        pos.direction = DIRECTION_SHORT
                    contract_symbol = symbol + ":" + use_contract_type + ":" + position_leverage
                    pos.symbol = contract_symbol  + "." + EXCHANGE_OKEX
                    pos.vtSymbol = pos.symbol
                    pos.vtPositionName = pos.symbol + "." + pos.direction

                    # 如果此合约不在已订阅清单中，重新订阅
                    if contract_symbol not in self.registered_symbols:
                        v = VtSubscribeReq()
                        v.symbol = contract_symbol
                        self.subscribe(v)

                    # 获取实时价格
                    tick = self.tickDict.get(symbol + ":" + use_contract_type + ":" +position_leverage, None)
                    pos.price = float(inf.get('avgprice', 0.0))
                    price = pos.price if tick is None else tick.lastPrice
                    if price == 0.0:
                        continue

                    volume_rate = 100 if symbol.startswith('btc') else 10
                    pos.frozen = volume_rate * float(inf.get("hold_amount",0.0)) - float(inf.get('eveningup',0.0)) / price
                    pos.position = volume_rate * float(inf.get("hold_amount",0.0)) / price
                    pos.positionProfit = float(inf.get("profitreal",0.0))

                    # print inf , pos.symbol
                    self.gateway.onPosition(pos)
            else:
                # 全仓模式
                # 通过合约名称，获取合约类型
                contract_name = inf["contract_name"]
                dic_inf = self.contract_name_dict[contract_name]
                use_contract_type = dic_inf.get("contract_type")

                #
                pos = VtPositionData()
                pos.gatewayName = self.gatewayName

                # 持仓合约的方向
                pos.direction = DIRECTION_LONG if int(inf.get("position",0)) == 1 else DIRECTION_SHORT

                pos.symbol = symbol + ":" + use_contract_type + ":" + str(self._use_leverage)  + "." + EXCHANGE_OKEX
                pos.vtSymbol = pos.symbol
                pos.vtPositionName = pos.symbol + "." + pos.direction

                # 获取实时价格
                tick = self.tickDict.get(symbol + ":" + use_contract_type + ":" + str(self._use_leverage),None)
                pos.price = float(inf.get('costprice', 0.0))
                price = pos.price if tick is None else tick.lastPrice
                if price == 0.0:
                    continue
                # 计算持仓量/冻结量： 100(或10) * hold_amount/ 价格
                volume_rate = 100 if symbol.startswith('btc') else 10
                pos.frozen = volume_rate * (float(inf.get('hold_amount',0.0)) - float(inf.get('eveningup',0.0)))/price
                pos.position = volume_rate * float(inf.get('hold_amount',0.0)) / price
                pos.positionProfit = float(inf.get('realized',0.0))

                # print inf , pos.symbol
                self.gateway.onPosition(pos)

    # ----------------------------------------------------------------------
    def futureSendOrder(self, req):
        """
        发委托单
        :param req:
        :return:
        """
        try:
            (symbol_pair, symbol, contract_type, leverage) = self.dealSymbolFunc(req.symbol)
        except Exception as ex:
            self.gateway.writeError(u'请求合约代码格式错误:{0}'.format(req.symbol))
            self.writeLog(u'futureSendOrder 请求合约代码格式错误:{0},exception:{1},{2}'.format(req.symbol,str(ex),traceback.format_exc()))
            return ''

        # print symbol_pair(例如:btc_usd) ,  symbol(btc), contract_type(this_week) , leverage(10)
        type_ = priceContractTypeMapReverse[(req.direction, req.offset)]
        self.writeLog(u'futureSendOrder:{0},{1},{2},{3},{4}'.format(symbol_pair,  symbol, contract_type, leverage, type_))
        # 本地委托号加1，并将对应字符串保存到队列中，返回基于本地委托号的vtOrderID
        self.localNo += 1
        self.localNoQueue.put(str(self.localNo))
        vtOrderID = '.'.join([self.gatewayName, str(self.localNo)])
        self.writeLog(u'futureSendOrder:创建本地订单:orderId:{0}'.format(vtOrderID))

        order = VtOrderData()
        order.gatewayName = self.gatewayName
        order.symbol = symbol
        order.vtSymbol = order.symbol
        order.orderID = self.localNo
        order.vtOrderID = vtOrderID
        order.price = req.price
        order.totalVolume = req.volume
        order.direction = req.direction
        order.offset = req.offset
        order.status = STATUS_NOTTRADED
        dt = datetime.now()
        order.orderTime = dt.strftime("%H:%M:%S.%f")
        self.localOrderDict[vtOrderID] = order

        self.writeLog(u'futureSendOrder 发送:symbol:{0},合约类型:{1},交易类型:{2},价格:{3},委托量:{4}'.
                      format(symbol + "_usd", contract_type , type_ , str(req.price), str(req.volume)))
        try:
            volume_rate = 100 if symbol.startswith('btc') else 10
            orginal_volume = req.volume
            req.volume = 1 if round((req.volume * req.price)/volume_rate) == 0 else round((req.volume * req.price)/volume_rate)
            self.writeLog(u'转换为ok合约数:{0}=>{1}'.format(orginal_volume, req.volume))
            self.futureTrade(symbol + "_usd", contract_type, type_, str(req.price), str(req.volume),
                             _lever_rate=self._use_leverage)

            return vtOrderID
        except Exception as ex:
            self.gateway.writeError(u'futureSendOrder发送委托失败:{0}'.format(str(ex)))
            self.writeLog(u'futureSendOrder发送委托失败.{1}'.format(traceback.format_exc()))
            return ''
    # ----------------------------------------------------------------------
    def futureCancel(self, req):
        """
        发出撤单请求
        :param req:
        :return:
        """
        try:
            (symbol_pair, symbol, contract_type, leverage) = self.dealSymbolFunc(req.symbol)
        except Exception as ex:
            self.writeLog(u'futureCancel 异常:{0}'.format(str(ex)))
            return

        localNo = req.orderID

        if localNo in self.localNoDict:
            # 找出okex的委托流水号
            orderID = self.localNoDict[localNo]
            # 调用接口发出撤单请求
            self.futureCancelOrder(symbol + "_usd", orderID, contract_type)
        else:
            # 如果在系统委托号返回前客户就发送了撤单请求，则保存
            # 在cancelDict字典中，等待返回后执行撤单任务
            self.cancelDict[localNo] = req

    # ----------------------------------------------------------------------
    def generateDateTime(self, s):
        """生成时间"""
        dt = datetime.fromtimestamp(float(s) / 1e3)
        time = dt.strftime("%H:%M:%S.%f")
        date = dt.strftime("%Y%m%d")
        return date, time,dt