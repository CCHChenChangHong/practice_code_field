# 只要同一目录下，只要有__init__.py文件
# 那么加载同一目录下的其他文件就不需要用全部路径加载
# 比如文件结构：
# hasaki:
# ----__init__.py
# ----a.py
# ----b.py
# 那么a.py中调用b.py里面的变量只需要：
from b import xxxx
# 以上