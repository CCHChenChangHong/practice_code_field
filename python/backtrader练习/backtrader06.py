import datetime
import os.path
import sys
import pandas as pd
# Import the backtrader platform
import backtrader as bt
# ---------------------------------------------------------------
#                    带有买单的策略
# ----——————————-------------------------------------------------

# 创建一个策略
class TestStrategy(bt.Strategy):

    def log(self, txt, dt=None):
        ''' Logging function for this strategy'''
        dt = dt or self.datas[0].datetime.date(0)
        print('{}, {}'.format(dt.isoformat(), txt))

    def __init__(self):
        # 以收盘线为参考
        self.dataclose = self.datas[0].close

    def next(self):
        self.log('Close, {}'.format(self.dataclose[0]))
        if self.dataclose[0]<self.dataclose[-1]:
            if self.dataclose[-1]<self.dataclose[-2]:
                self.log("买进{}".format(self.dataclose[0]))
                self.buy()

if __name__ == '__main__':
    cerebro = bt.Cerebro()

    # Add a strategy
    cerebro.addstrategy(TestStrategy)

    path='/home/hashaki/pycharm-work/testKline.csv'
    dataframe = pd.read_csv(path, index_col=0, parse_dates=True)

    dataframe['openinterest'] = 0
    data = bt.feeds.PandasData(dataname=dataframe)


    # Add the Data Feed to Cerebro
    cerebro.adddata(data)

    # Set our desired cash start
    cerebro.broker.setcash(100000.0)

    # Print out the starting conditions
    print('投资组合启动:{}'.format(cerebro.broker.getvalue()))

    # Run over everything
    cerebro.run()

    # Print out the final result
    print('投资组合关闭:{}'.format(cerebro.broker.getvalue()))