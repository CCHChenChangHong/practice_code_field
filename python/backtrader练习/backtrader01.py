import backtrader as bt

if __name__=='__main__':
    cerebro = bt.Cerebro()
    print('投资组合启动值: %.2f' % cerebro.broker.getvalue())

    cerebro.run()

    print('投资组合结束值: %.2f' % cerebro.broker.getvalue())