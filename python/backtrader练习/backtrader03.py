import datetime
import os.path
import sys
import backtrader as bt
#-------------------------------------------------------
#              data feed
#-------------------------------------------------------
if __name__=='__main__':
    cerebro = bt.Cerebro()
    cerebro.broker.set_cash(100000.0)

    # 数据在samples的子文件夹中。需要找到脚本的位置，因为它可以从任何地方被调用
    modpath = os.path.dirname(os.path.abspath(sys.argv[0]))
    datapath = os.path.join(modpath, '/home/下载/backtrader-master/datas/orcl-1995-2014.txt')

    # 创建Data Feed
    data = bt.feeds.YahooFinanceCSVData(
        dataname=datapath,
        # Do not pass values before this date
        fromdate=datetime.datetime(2000, 1, 1),
        # Do not pass values after this date
        todate=datetime.datetime(2000, 12, 31),
        reverse=False)

    # 添加 Data Feed 到 Cerebro
    cerebro.adddata(data)
    print('投资组合启动值: %.2f' % cerebro.broker.getvalue())

    cerebro.run()

    print('投资组合结束值: %.2f' % cerebro.broker.getvalue())
