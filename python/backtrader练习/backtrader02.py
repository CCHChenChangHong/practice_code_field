import backtrader as bt
#-------------------------------------------------------
#              设置初始金额
#-------------------------------------------------------
if __name__=='__main__':
    cerebro = bt.Cerebro()
    cerebro.broker.set_cash(100000.0)
    print('投资组合启动值: %.2f' % cerebro.broker.getvalue())

    cerebro.run()

    print('投资组合结束值: %.2f' % cerebro.broker.getvalue())
