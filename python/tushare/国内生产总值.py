import tushare as ts

ts.get_gdp_year()

'''

    year :统计年度
    gdp :国内生产总值(亿元)
    pc_gdp :人均国内生产总值(元)
    gnp :国民生产总值(亿元)
    pi :第一产业(亿元)
    si :第二产业(亿元)
    industry :工业(亿元)
    cons_industry :建筑业(亿元)
    ti :第三产业(亿元)
    trans_industry :交通运输仓储邮电通信业(亿元)
    lbdy :批发零售贸易及餐饮业(亿元)

'''