import tushare as ts

ts.get_gdp_quarter()

'''

    quarter :季度
    gdp :国内生产总值(亿元)
    gdp_yoy :国内生产总值同比增长(%)
    pi :第一产业增加值(亿元)
    pi_yoy:第一产业增加值同比增长(%)
    si :第二产业增加值(亿元)
    si_yoy :第二产业增加值同比增长(%)
    ti :第三产业增加值(亿元)
    ti_yoy :第三产业增加值同比增长(%)


'''
