from datetime import datetime
from datetime import timedelta

(datetime.now()+timedelta(microseconds=-10*200)).strftime('%Y-%m-%d %H:%M:%S')
(datetime.now()+timedelta(seconds=-10*20)).strftime('%Y-%m-%d %H:%M')
(datetime.now()+timedelta(minutes=-5*20)).strftime('%Y-%m-%d %H:%M') # '2018-11-06 13:37'
(datetime.now()+timedelta(hours=-1*2)).strftime('%Y-%m-%d %H:%M')
(datetime.now()+timedelta(days=-1*2)).strftime('%Y-%m-%d %H:%M')
(datetime.now()+timedelta(weeks=-1*2)).strftime('%Y-%m-%d %H:%M')  #'2018-10-23 15:23'

datetime.now().strftime('%Y-%m-%d %H:%M')
# '2018-11-06 15:21'