from datetime import datetime

datetime.strptime('20200520','%Y%m%d').day
>>> 20

# 获取小时
datetime.strptime('20200608 16:45:00','%Y%m%d %H:%M:%S').hour
>>>16

# 获取分钟
datetime.strptime('20200608 16:45:00','%Y%m%d %H:%M:%S').minute
>>>45