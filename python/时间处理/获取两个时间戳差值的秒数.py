import sys
import os
import datetime
sys.path.append(os.getcwd())
import pandas as pd


data_path='C:/Program Files/wequant/resources/extraResources/Miniconda3/wequant_engine_ctp/testData.csv'

# 从CSV读取初始化和历史数据
temp=pd.read_csv(data_path)
temp['start_time']=pd.to_datetime(temp['start_time'])
temp['end_time']=pd.to_datetime(temp['end_time'])
# {'symbol': 'czce_ap000', 'start_time': Timestamp('2019-05-07 09:00:00'), 
# 'end_time': Timestamp('2019-05-07 15:00:00'), 'open': 8795.0, 'high': 8814.0, 'low': 8733.0, 'close': 8784.0, 'volume': 176054.0, 'amount': 1466331148.0, 
# 'openInterest': 228076.0}
dataCursor=temp.to_dict(orient='records')

for i in dataCursor:
    fre=i['end_time']-i['start_time']
    print(type(fre.seconds),fre.seconds)     # <class 'int'> 21600