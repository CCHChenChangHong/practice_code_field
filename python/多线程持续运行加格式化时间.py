# 多线程持久运行，加格式化时间
import time
from multiprocessing import Process

def run_forever():
    while 1:
        print(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())) # 若是毫秒级数据则先把时间戳除以1000，把时间戳传进localtime()里
        time.sleep(1)

if __name__ == '__main__':
    p = Process(target=run_forever)
    p.start()
    print('start a process.')
    time.sleep(10)
    if p.is_alive:  # stop a process gracefully
        p.terminate()
        print('stop process')
        p.join()
