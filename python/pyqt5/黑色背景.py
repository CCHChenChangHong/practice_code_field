from PyQt5 import QtCore, QtGui, QtWidgets
import qdarkstyle
import sys

if __name__=='__main__':
    app=QtWidgets.QApplication(sys.argv)    # 必须要有App的初始化,不然没法启动
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())   # 黑色背景
    w = QtWidgets.QWidget()
    w.resize(250, 150)
    w.move(300, 300)
    w.setWindowTitle('Simple')
    w.show()
    sys.exit(app.exec_())