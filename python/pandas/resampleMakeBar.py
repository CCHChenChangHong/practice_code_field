import pandas as pd

path='/home/hashaki/pycharm-work/testKline.csv'
dataframe = pd.read_csv(path, index_col=0, parse_dates=True)


ohlc_dict ={'open':'first','high':'max','low':'min','close': 'last','vol': 'sum'}

#a5=dataframe.resample('5min', how=ohlc_dict, closed='left', label='left')
#a15=dataframe.resample('15min', how=ohlc_dict, closed='left', label='left')
#a30=dataframe.resample('30min', how=ohlc_dict, closed='left', label='left')
#h=dataframe.resample('60min', how=ohlc_dict, closed='left', label='left')
d=dataframe.resample('D', how=ohlc_dict, closed='left', label='left')
print(d)