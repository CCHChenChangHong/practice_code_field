# DateOffset可以按照日历的方式来加减日期
import pandas as pd

ts = pd.Timestamp('2016-10-30 00:00:00')
>>>
Timestamp('2016-10-30 00:00:00')

ts+pd.DateOffset(1)
>>>
Timestamp('2016-10-31 00:00:00')