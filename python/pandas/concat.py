
import pandas as pd

data1=[{'a':1,'b':2,'c':3},{'a':20,'b':30,'c':40}]
data2=[{'a':3,'b':2,'c':1},{'a':40,'b':30,'c':20}]

# 转成df
df1=pd.DataFrame(data1)
df2=pd.DataFrame(data2)

df_concat=pd.concat([df1,df2])

print(df_concat)

>>>
a   b   c
0   1   2   3
1  20  30  40
0   3   2   1
1  40  30  20