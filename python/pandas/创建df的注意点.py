import pandas as pd

a=[1,2,3]
b=['a','b','c']

# 注意点，使用列表去创建dataframe的时候，列表在DataFrame里是表现成行，而不是列
print(pd.DataFrame([a,b],['#','#']))

>>>
   0  1  2
#  1  2  3
#  a  b  c  -->b=['a','b','c']是表现成行