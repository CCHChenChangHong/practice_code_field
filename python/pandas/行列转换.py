import pandas as pd

a=[1,2,3]
b=['a','b','c']

pd.DataFrame([a,b],colums=['w','y'])
>>>ValueError: 2 columns passed, passed data had 3 columns

result=pd.DataFrame([a,b]).transpose()
result.columns=['w','y']
>>>w  y
0  1  a
1  2  b
2  3  c