# 转换某一列的数据结构
import pandas as pd
import pymongo
import sys
import os
from datetime import datetime
sys.path.append(os.getcwd())

a=pymongo.MongoClient("192.168.101.88",27017)
collection=a['crypto_1hour']['bitfinex_eos_usd']

file='C:\hashaki\work/run/test.csv'

data_read=pd.read_csv(file)
data_read['start_time']=pd.to_datetime(data_read['start_time'])
temp=data_read.to_dict(orient='records')
'''
[{'_id': '5c171503c2548c0721902ddc', 'amount': 0.0, 'close': 1.9652, 'end_time': '2018-12-17 02:00:00', 'frequency': '1hour', 
'high': 1.9767, 'low': 1.9524, 'open': 1.975, 'openInterest': 0.0, 'start_time': '2018-12-17 01:00:00', 'symbol': 'bitfinex_eos_usd', 'volume': 148281.89352397}]
'''

print(type(temp[0]['start_time']))