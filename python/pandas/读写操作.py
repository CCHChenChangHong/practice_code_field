# 使用pandas保存CSV,因为pandas 读取CSV一定有行索引，所以使用内置csv库读取csv数据
import pandas as pd
import pymongo
import csv
import sys
import os
sys.path.append(os.getcwd())
'''
数据库名称列表
['admin', 'config', 'crypto_12hour', 'crypto_15min', 'crypto_1day', 'crypto_1hour', 'crypto_1min', 'crypto_1month', 
'crypto_1week', 'crypto_30min', 'crypto_3hour', 'crypto_4hour', 'crypto_5min', 'crypto_6hour', 'future_15min', 'future_1day',
 'future_1hour', 'future_1min', 'future_1month', 'future_1week', 'future_30min', 'future_5min', 'futuresf_15min', 'futuresf_1day', 
 'futuresf_1hour', 'futuresf_1min', 'futuresf_1month', 'futuresf_1week', 'futuresf_30min', 'futuresf_5min', 'local', 'symParameter', 'update_record']
'''
'''
表的形式
'bitfinex_eos_usd'
'''
'''
单条数据的字段
{'_id': ObjectId('5c171503c2548c0721902ddc'), 'start_time': datetime.datetime(2018, 12, 17, 1, 0),
 'open': 1.975, 'close': 1.9652, 'high': 1.9767, 'low': 1.9524, 'volume': 148281.89352397, 'end_time': datetime.datetime(2018, 12, 17, 2, 0), 
 'symbol': 'bitfinex_eos_usd', 'frequency': '1hour', 'amount': 0.0, 'openInterest': 0.0}
'''
a=pymongo.MongoClient("192.168.101.88",27017)
collection=a['crypto_1hour']['bitfinex_eos_usd']

file='C:\hashaki\work/run/test.csv'
data=collection.find_one()
temp=[]
temp.append(data)
data_change=pd.DataFrame(temp)
data_change.to_csv(file,index=False)

with open(file,'r') as file_data:
    data=csv.DictReader(file_data)
    for i in data:
        print(i['close'])            # 是一个数值字符
        print(type(i['close']))      # str