#build in python3.6
# 作者：陈常鸿
# python实现各种二叉树

# BinTree by List
#------------------------------------------------------------------------------------
tree=['A',['B',None,None],
      ['C',['D',['F',None,None]]],
      ['G',None,None]]
print(tree[2])


# 构造函数二叉树
#-----------------------------------------------------------------------------------
def BinTree(data,left=None,right=None):
      return [data,left,right]

def is_empty_BinTree(btree):
    return btree is None

def root(btree):
    return btree[0]

def left(btree):
    return btree[1]

def right(btree):
    return btree[2]

def set_root(btree,data):
    btree[0]=data

def set_left(btree,left):
    btree[1]=left

def set_right(btree,right):
    btree[2]=right

# 基于上述构造函数，可以做出任意复杂的二叉树
t1=BinTree(2,BinTree(4),BinTree(8))
print(t1)
set_left(left(t1),BinTree(5))
print(t1)

# BinTree by tupe
#---------------------------------------------------------------------------------------
TupeTree=('*',(3,None,None),
          ('+',(2,None,None),(5,None,None)))

print(TupeTree)

# 元组构造函数表达树
#---------------------------------------------------------------------------------------
def make_sum(a,b):
    return ('+',a,b)

def make_prod(a,b):
    return ('*',a,b)

def make_diff(a,b):
    return ('-',a,b)

def make_div(a,b):
    return ('/',a,b)

# 构造任意复杂的字符串表达式
print(make_sum(make_prod('a',5),make_diff('b',7)))

# 优先列表
#------------------------------------------------------------------------------------------
class PrioQue:
    def __init__(self,elist=[]):
        self.__elems=list(elist)
        self.__elems.sort(reverse=True)

    def enqueue(self,e):
        i=len(self._elems)-1
        while i>=0:
            if self._elems[i]<=e:
                i=+1
            else:
                break
        self.__elems.insert(i+1,e)
