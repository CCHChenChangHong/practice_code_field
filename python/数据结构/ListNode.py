# build in python 3.6
# 作者：陈常鸿
# 链表的各种操作

# 一个简单的链表结点类
class LNode:
    def __init__(self,elem,next_=None):
        self.elem=elem
        self.next=next_

p=LNode(1)
q=p
for i in range(2,11):
    q.next=LNode(i)
    q=q.next

q=p
while q is not None:
    print(q.elem)
    q=q.next

#-------------------------------------------------------------------------
class LList:
    def __init__(self):
        self.__head=None

    def is_empty(self):
        return self.__head is None

    def prepend(self,elem):
        self.__head=LNode(elem,self.__head)

    def pop(self):
        if self.__head is None:   # 无结点，引发异常
            raise ValueError
        e=self.__head.elem
        self.__head=self.__head.next
