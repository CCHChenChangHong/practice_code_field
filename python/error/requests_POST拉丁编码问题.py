# 源码
uid=self.ctaEngine.mainEngine.settingData['userId']
url=self.ctaEngine.mainEngine.settingData['LOG_HOST']
token=self.ctaEngine.mainEngine.settingData['token']
md5=self.ctaEngine.mainEngine.settingData['uidMd5']
fileId=self.ctaEngine.mainEngine.settingData['strategyId']
now_time=datetime.now().strftime("%Y-%m-%d %H:%M:%S")

headers={
                "Content-Type":"application/json ;charset=utf-8",
                "UID":md5,
                "Authorization":token
}

body=json.dumps({'uid':uid,'fileID':fileId,'moduleType':4,'uploadType':1,'uploadData':log_msg})
requests.post(url,headers=headers,data=body).json()

'''
错误：
requests.post(url,headers=headers,data=body).json()
  File "/usr/local/lib/python3.6/site-packages/requests/api.py", line 112, in post
    return request('post', url, data=data, json=json, **kwargs)
  File "/usr/local/lib/python3.6/site-packages/requests/api.py", line 58, in request
    return session.request(method=method, url=url, **kwargs)
  File "/usr/local/lib/python3.6/site-packages/requests/sessions.py", line 518, in request
    resp = self.send(prep, **send_kwargs)
  File "/usr/local/lib/python3.6/site-packages/requests/sessions.py", line 639, in send
    r = adapter.send(request, **kwargs)
  File "/usr/local/lib/python3.6/site-packages/requests/adapters.py", line 438, in send
    timeout=timeout
  File "/usr/local/lib/python3.6/site-packages/requests/packages/urllib3/connectionpool.py", line 600, in urlopen
    chunked=chunked)
  File "/usr/local/lib/python3.6/site-packages/requests/packages/urllib3/connectionpool.py", line 356, in _make_request
    conn.request(method, url, **httplib_request_kw)
  File "/usr/local/lib/python3.6/http/client.py", line 1239, in request
    self._send_request(method, url, body, headers, encode_chunked)
  File "/usr/local/lib/python3.6/http/client.py", line 1280, in _send_request
    self.putheader(hdr, value)
  File "/usr/local/lib/python3.6/http/client.py", line 1212, in putheader
    values[i] = one_value.encode('latin-1')
UnicodeEncodeError: 'latin-1' codec can't encode characters in position 0-2: ordinal not in range(256)
'''




# 解决方法
uid=self.ctaEngine.mainEngine.settingData['userId']
url=self.ctaEngine.mainEngine.settingData['LOG_HOST']
token=self.ctaEngine.mainEngine.settingData['token'].encode("utf-8")
md5=self.ctaEngine.mainEngine.settingData['uidMd5'].encode("utf-8")
fileId=self.ctaEngine.mainEngine.settingData['strategyId']
now_time=datetime.now().strftime("%Y-%m-%d %H:%M:%S")