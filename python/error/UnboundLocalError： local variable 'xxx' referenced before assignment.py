# UnboundLocalError： local variable 'xxx' referenced before assignment
# 原因：在函数外部已经定义了变量n，在函数内部对该变量进行运算，运行时会遇到了这样的错误：
# 主要是因为没有让解释器清楚变量是全局变量还是局部变量。


# 解决方法：换个变量名就好了