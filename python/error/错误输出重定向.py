'''
用于重定向输出
'''
import sys
import traceback
path='C:/Users/cchyp/Desktop/stdout.txt'
path_trace='C:/Users/cchyp/Desktop/stdout_trace.txt'
out=sys.stdout

a=1
try:
    b=a+'b'
except Exception as error:
    with open(path,'w+') as f:
        sys.stdout=f
        traceback.print_exc(file=open(path_trace,'w+')) # 完整的错误信息
        print(error) # 这个不会在终端输出,这个只有错误原因，位置信息没有

sys.stdout=out
print('hsaki')    # 终端输出