# python positional argument follows keyword argument 

#关键字参数必须跟随在位置参数后面! 因为python函数在解析参数时, 是按照顺序来的, 位置参数是必须先满足, 才能考虑其他可变参数.

# 例子
def hasaki(a,b=None,c)  # 这就会报错
def hasaki(a,c,b=None)  # 要这样