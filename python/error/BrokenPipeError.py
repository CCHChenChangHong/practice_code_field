# socket链接问题，客户端与服务端通信的时候，客户端发去信息，而在服务端返回信息前，通信断了
# 产生原因：
'''
1，因为接收缓存接收太多信息，满了之后，socket出现问题，导致brokenpipe(使用python websocket库自测，并不是客户端的问题)

2, broken pipe最直接的意思是：写入端出现的时候，另一端却休息或退出了，因此造成没有及时取走管道中的数据，从而系统异常退出

3，发生broken pipe错误时，进程收到SIGPIPE信号，默认动作是进程终止。

4，broken pipe经常发生socket关闭之后（或者其他的描述符关闭之后）的write操作中。

5，client端用户在杀死进程时，接口的TCP请求尚未完成（未完成的原因是处理时间长）。 
导致server端write数据时，收到SIGPIPE信号，抛出Broken pipe异常。 
但由于已经杀死了进程，并不会对用户产生任何影响。

python 官网解析：
A subclass of ConnectionError, raised when trying to write on a pipe while the other end has been closed, 
or trying to write on a socket which has been shutdown for writing. Corresponds to errno EPIPE and ESHUTDOWN.
https://docs.python.org/3/library/exceptions.html
'''
