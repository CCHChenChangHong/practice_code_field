# build in python 3.5.2
# 作者：陈常鸿
# 在Dual Thrust交易系统中，对于震荡区间的定义非常关键，这也是该交易系统的核心和精髓。Dual Thrust系统使用Range
# 教程：https://gitee.com/CCHChenChangHong/changhong_quantizing_machine/wikis/%E4%BA%A4%E6%98%93%E7%B3%BB%E7%BB%9F%EF%BC%8C%E4%BB%8EDual%20Thrust%E4%B8%AD%E5%AD%A6%E2%80%9C%E8%B6%8B%E5%8A%BF%E2%80%9D?sort_id=653571
import requests
import matplotlib.pyplot as plt
import numpy as np

K_line=lambda url,token,sec,hour:requests.get(url+token+'?group_sec='+str(sec)+'&range_hour='+str(hour)).json()
result=K_line('https://data.gateio.io/api2/1/candlestick2/','btc_usdt',60,24)['data']
high=[]
low=[]
time=[]
# (时间，开盘，收盘，最高，最低)
for i in result:
    high.append([float(i[3])])
    low.append([float(i[4])])
    time.append([float(i[0])])

highest=np.asarray(high)
lowest=np.asarray(low)
mean=(highest+lowest)/2

ticker=lambda url,token:requests.get(url+token).json()
new_ticker=ticker('https://data.gateio.io/api2/1/ticker/','btc_usdt')['last']     # 获取最新成交价

R=max(np.max(highest-mean),np.max(mean-lowest))              # 计算出范围
sell_line=mean+0.8*R                             # 0.8是个系数，买线与卖线的数值不一样，是不对称的
buy_line=mean-2*R

plt.figure()
plt.plot(time,lowest,color='green',label='low_line')
plt.plot(time,highest,color='red',label='high_line')
plt.plot(time,buy_line,color='orange',label='buy_line')
plt.plot(time,sell_line,color='blue',label='sell_line')
plt.title('Dual Thrust')
plt.legend()
plt.show()