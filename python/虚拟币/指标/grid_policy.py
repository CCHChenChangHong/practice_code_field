# build in python 3.5.2
# 作者：陈常鸿
# 网格交易，动态调仓策略 教程：https://gitee.com/CCHChenChangHong/changhong_quantizing_machine/wikis/%E7%BD%91%E6%A0%BC%E4%BA%A4%E6%98%93%E2%80%94%E5%8A%A8%E6%80%81%E8%B0%83%E4%BB%93%E7%AD%96%E7%95%A5?sort_id=653555
import requests
import matplotlib.pyplot as plt
import numpy as np
import time as T
K_line=lambda url,token,sec,hour:requests.get(url+token+'?group_sec='+str(sec)+'&range_hour='+str(hour)).json()
result=K_line('https://data.gateio.io/api2/1/candlestick2/','btc_usdt',60,12)['data']
high=[]
low=[]
time=[]
# (时间，开盘，收盘，最高，最低)
for i in result:
    high.append([float(i[3])])
    low.append([float(i[4])])
    time.append([float(i[0])])

mean=np.sum(np.asarray(high),np.asarray(low))/2
sell_point=np.max(mean)                # 设置买进卖出的价格点
buy_point=np.min(mean)
buy_lastTime=0.0                       # 记录上次买进的价格
keep=False                             # 是否持仓

ticker=lambda url,token:requests.get(url+token).json()
new_ticker=ticker('https://data.gateio.io/api2/1/ticker/','btc_usdt')['last']     # 获取最新成交价

while True:
    if new_ticker>buy_point and keep is True:
        # 卖出去
        pass
    elif new_ticker<sell_point:
        # 买买买
        pass

    T.sleep(20)                       # 不能操之过急