# build in python3.5.2
# 作者：陈常鸿
# 由于虚拟币交易为24h T+0交易，感觉开盘价和闭盘价没有多大作用，只留下最高价线和最低价线
# 均线，输入均线对应的均值步数即可出图
# 均线的解释：https://xueqiu.com/8287840120/71346849?page=6
import requests
import matplotlib.pyplot as plt
import numpy as np

K_line=lambda url,token,sec,hour:requests.get(url+token+'?group_sec='+str(sec)+'&range_hour='+str(hour)).json()
result=K_line('https://data.gateio.io/api2/1/candlestick2/','btc_usdt',60,1)['data']
high=[]
low=[]
time=[]
n=0
# (时间，开盘，收盘，最高，最低)
for i in result:
    high.append([float(i[3])])
    low.append([float(i[4])])
    time.append(n)
    n += 1

X=np.asarray(time)
highest=np.asarray(high)
lowest=np.asarray(low)
mean=np.add(lowest,highest)/2   # numpy的矩阵可以直接除2，list结构不可以直接除

mean_line=[]
N=int(input("输入n均线的n :"))
for j in range(1,n+1):
    if j<(N+1):
        mean_line.append(np.sum(mean[:j])/j)
    else:
        mean_line.append(np.sum(mean[j-N:j])/N)

plt.figure()
plt.plot(X,mean,color='blue', label='high')
plt.plot(X,mean_line,color='orange',label='mean-line')
plt.legend()
plt.show()