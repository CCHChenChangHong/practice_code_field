# build in python3.5.2
# 作者：陈常鸿
# 由于虚拟币交易为24h T+0交易，感觉开盘价和闭盘价没有多大作用，只留下最高价线和最低价线
# 多空心态测试，测试是否有多空偏好，如果两幅图都觉得是多或者空，那么就存在偏好，这时不要交易
import requests
import matplotlib.pyplot as plt
import numpy as np

K_line=lambda url,token,sec,hour:requests.get(url+token+'?group_sec='+str(sec)+'&range_hour='+str(hour)).json()
result=K_line('https://data.gateio.io/api2/1/candlestick2/','btc_usdt',60,12)['data']
high=[]
low=[]
time=[]
n=0
# (时间，开盘，收盘，最高，最低)
for i in result:
    high.append([float(i[3])])
    low.append([float(i[4])])
    time.append(n)
    n += 1

X=np.asarray(time)
highest=np.asarray(high)
lowest=np.asarray(low)
optimistic=(lowest+highest)/2   # numpy的矩阵可以直接除2，list结构不可以直接除
mean=np.sum(optimistic)/n
pressimistic=[]

for j in optimistic:
    if mean-j>0:
        pressimistic.append(mean+np.abs(mean-j))
    else:
        pressimistic.append(mean-np.abs(mean-j))

plt.subplot(2,1,1)
plt.plot(X,optimistic,color='blue', label='optimistic')
plt.legend()
plt.subplot(2,1,2)
plt.plot(X,pressimistic,color='orange',label='pressimistic')
plt.legend()
plt.show()