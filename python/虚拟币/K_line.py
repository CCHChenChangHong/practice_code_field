# build in python3.5.2
# 作者：陈常鸿

import requests
import matplotlib.pyplot as plt
import matplotlib.finance as mpf
K_line=lambda url,token,sec,hour:requests.get(url+token+'?group_sec='+str(sec)+'&range_hour='+str(hour)).json()
result=K_line('https://data.gateio.io/api2/1/candlestick2/','btc_usdt',60,4)['data']
l=[]
# (时间，开盘，收盘，最高，最低)
for i in result:
    tup=(float(i[0]),float(i[5]),float(i[2]),float(i[3]),float(i[4]))
    l.append(tup)
# 出图
fig, ax = plt.subplots(figsize=(20,15))
fig.subplots_adjust(bottom=0.5)
mpf.candlestick_ohlc(ax,l,colorup='r',colordown='g',alpha=1)
plt.show()