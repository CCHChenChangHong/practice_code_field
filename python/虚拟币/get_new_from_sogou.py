# python3.5.2
__author__='hashaki'
import requests
from bs4 import BeautifulSoup
# http://search.sina.com.cn/?q=区块链&c=news&from=channel&ie=utf-8
# 搜狗新闻(全文)：http://news.sogou.com/news?ie=utf8&p=40230447&interV=kKIOkrELjboLmLkEkrkTkKIMkrELjboImLkEk74TkKILmrELjb8TkKIKmrELjbkI_1864569649&query=区块链&
# 微信公众号：http://weixin.sogou.com/weixin?p=42341200&query=区块链&type=2&ie=utf8
headers = {'Accept': '*/*',
               'Accept-Language': 'en-US,en;q=0.8',
               'Cache-Control': 'max-age=0',
               'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36',
               'Connection': 'keep-alive',
               'Referer': 'http://www.sogou.com/'
               }

url_head='http://www.sina.com.cn/mid/search.shtml?range=all&c=news&q='
url_tail='&from=home&ie=utf-8'
url_content='区块链'       # 搜索字眼

webdata=requests.get('http://news.sogou.com/news?ie=utf8&p=40230447&interV=kKIOkrELjboLmLkEkrkTkKIMkrELjboImLkEk74TkKILmrELjb8TkKIKmrELjbkI_1864569649&query=区块链&',headers=headers).text
soup=BeautifulSoup(webdata,'lxml')
link=soup.select("h3 > a")
for i in link:
    print(i.get('href'))
