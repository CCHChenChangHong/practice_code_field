# build in python3.5.2
# 作者：陈常鸿
# 由于虚拟币交易为24h T+0交易，感觉开盘价和闭盘价没有多大作用，只留下最高价线和最低价线
import requests
import matplotlib.pyplot as plt

K_line=lambda url,token,sec,hour:requests.get(url+token+'?group_sec='+str(sec)+'&range_hour='+str(hour)).json()
result=K_line('https://data.gateio.io/api2/1/candlestick2/','btc_usdt',60,4)['data']
high=[]
low=[]
time=[]
# (时间，交易量，开盘，收盘，最高，最低)
for i in result:
    high.append([float(i[3])])
    low.append([float(i[4])])
    time.append([float(i[0])])
# 出图
plt.figure(figsize=(20,20))
plt.plot(time,high,color='red',label='high')
plt.plot(time,low,color='green',label='low')
plt.legend()
plt.show()
