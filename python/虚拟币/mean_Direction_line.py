# build in python3.5.2
# 作者：陈常鸿
# 由于虚拟币交易为24h T+0交易，感觉开盘价和闭盘价没有多大作用，只留下最高价线和最低价线
# 求单位时间内最高价和最低价的均值，均值做成直线，最高值均值为蓝色线，最低价均值为黑色线，其实两均值差值不大
import requests
import matplotlib.pyplot as plt
import numpy as np

K_line=lambda url,token,sec,hour:requests.get(url+token+'?group_sec='+str(sec)+'&range_hour='+str(hour)).json()
result=K_line('https://data.gateio.io/api2/1/candlestick2/','btc_usdt',60,1)['data']
high=[]
low=[]
time=[]
n=0
# (时间，开盘，收盘，最高，最低)
for i in result:
    high.append([float(i[3])])
    low.append([float(i[4])])
    time.append(n)
    n += 1

X=np.asarray(time)
highest=np.asarray(high)
lowest=np.asarray(low)

# 求平均，这里的n是价格总条数
mean_high=[]
_high=np.sum(highest)/n
mean_low=[]
_low=np.sum(lowest)/n

# 均值是一条直线
for j in range(n):
    mean_high.append(_high)
    mean_low.append(_low)

plt.figure()
plt.plot(X,highest,color='red', label='high')
plt.plot(X,lowest,color='green', label='low')
plt.plot(X,mean_high,color='blue',label='mean-high')
plt.plot(X,mean_low,color='black',label='mean-low')
plt.legend()
plt.show()