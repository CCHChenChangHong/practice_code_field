# build in python
# 使用matplotlib动态显示图像，没做到生成新的点就把最前面那个点删掉，所以坐标会一直增大
import matplotlib.pyplot as plt
import math

plt.close()  # clf() # 清图  cla() # 清坐标轴 close() # 关窗口
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.axis("equal")  # 设置图像显示的时候XY轴比例
plt.grid(True)  # 添加网格
plt.ion()  # interactive mode on
x=0
y=math.sin(x)
print('开始仿真')
try:
    for t in range(180):
        x+=1
        y=math.sin(x)
        ax.scatter(x, y, c='b', marker='.')  # 散点图
        # ax.lines.pop(0)  # 删除轨迹
        # 不能用show()出图，show()是静态图，pause()就可以出图
        plt.pause(0.01)


except Exception as error:
    print(error)