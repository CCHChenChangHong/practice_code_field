# build in python3.5.2
# 作者：陈常鸿
# 获取gateio上比特币所有价格，交易量数据
# 用pandas 储存
import requests
from pandas.core.frame import DataFrame

path_save='F:/btc_price_history/'
rate_file_name='btc_rate.txt'
amount_file_name='btc_amount.txt'

trades=lambda url,token,TID:requests.get(url+token+TID).json()

TID=1

while True:
    rate = []
    amount = []
    a = trades('https://data.gateio.io/api2/1/tradeHistory/', 'btc_usdt', '/' + str(TID))
    for i in a['data']:
        rate.append(i['rate'])
        amount.append(i['amount'])
        TID = i['tradeID']
        print('TID', TID)
    rate_data=DataFrame(rate)
    amount_data=DataFrame(amount)
    rate_data.to_csv(path_save+rate_file_name,mode='a',header=False)
    amount_data.to_csv(path_save+amount_file_name,mode='a',header=False)
    TID=str(int(TID)+1)