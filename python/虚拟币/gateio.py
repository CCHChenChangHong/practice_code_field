# build in python3.5.2
# 作者：陈常鸿
# gateio.io里虚拟币的行情获取
# 深度API有bug

import requests
import json
from bs4 import BeautifulSoup

# 全币种行情
# 第二参数：币符号_usdt
#baseVolume: 交易量
#high24hr:24小时最高价
#highestBid:买方最高价
#last:最新成交价
#low24hr:24小时最低价
#lowestAsk:卖方最低价
#percentChange:涨跌百分比
#quoteVolume: 兑换货币交易量
allTicker=lambda url,token:requests.get(url).json()[token]
allTicker('https://data.gateio.io/api2/1/tickers',"btc_usdt")

# 单币种行情
# 每10秒钟更新
# 第二参数：代币符号_usdt
#baseVolume: 交易量
#high24hr:24小时最高价
#highestBid:买方最高价
#last:最新成交价
#low24hr:24小时最低价
#lowestAsk:卖方最低价
#percentChange:涨跌百分比
#quoteVolume: 兑换货币交易量
ticker=lambda url,token:requests.get(url+token).json()
ticker('https://data.gateio.io/api2/1/ticker/','btc_usdt')

# 市场深度
# 返回系统支持的所有交易对的市场深度（委托挂单），其中 asks 是委卖单, bids 是委买单。
# 第二参数为代币符号_usdt，第三参数为深度大小size=n
# asks : 卖方深度
# bids : 买方深度
depth=lambda url,token:requests.get(url+token).json()
depth('https://data.gateio.io/api2/1/orderBook/','btc_usdt')

# 历史成交
# 返回从[TID]往后的最多1000历史成交记录,往后的意思是往最新的方向
# 第二参数为代币符号_usdt
# amount: 成交币种数量
# date: 订单时间
# rate: 币种单价
# total: 订单总额
# tradeID: tradeID
# type: 买卖类型, buy买 sell卖
trades=lambda url,token,TID:requests.get(url+token+TID).json()
trades('https://data.gateio.io/api2/1/tradeHistory/','btc_usdt','/TID')

# K线
#返回市场最近时间段内的K先数据
#time: 时间戳
#volume: 交易量
#close: 收盘价
#high: 最高价
#low: 最低价
#open: 开盘价
K_line=lambda url,token,sec,hour:requests.get(url+token+'?group_sec='+str(sec)+'&range_hour='+str(hour)).json()
K_line('https://data.gateio.io/api2/1/candlestick2/','btc_usdt',60,12)