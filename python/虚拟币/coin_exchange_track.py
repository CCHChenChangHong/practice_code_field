# build in python3.5.2
# 作者：陈常鸿
# 虚拟币钱包地址转账追踪
# ETH大额追踪检测网址：http://www.searchain.io/specialtrans?type=eth_single_record_count&title=ETH大额交易监测（24h)&title_sub=单笔交易数量 > 1,000
# Token大额交易监测（24h)：http://www.searchain.io/specialtrans?type=token_single_record_price&title=Token大额交易监测（24h)&title_sub=单笔交易价值 > %24 10,000
# 特殊地址检测：http://www.searchain.io/specialtrans?type=special_address_record&title=特殊地址交易监测（24h)&title_sub=

import requests
import matplotlib.pyplot as plt

# ETH大额追踪http://scvelk.searchain.io/open/address/oneday_trans_record?type=eth_single_record_count&size=10&page=1
# Token大额交易监测（24h):http://scvelk.searchain.io/open/address/oneday_trans_record?type=token_single_record_count&size=10&page=1
#                        :http://scvelk.searchain.io/open/address/oneday_trans_record?type=token_single_record_price&size=10&page=1
# 特殊地址：http://scvelk.searchain.io/open/address/oneday_trans_record?type=special_address_record&size=10&page=1
# value:转账数量
# market_value:value*单价
# from:出账方地址
# to:收账方地址
# symbol：币种
# trans_time:时间戳
# trans_hash:交易hash
webdata=requests.post('http://scvelk.searchain.io/open/address/oneday_trans_record?type=eth_single_record_count&size=10&page=1').json()

exchange_address={'0x8fd3121013a07c57f0d69646e86e7a4880b467b7':'Airswap',
'0xbbc79794599b19274850492394004087cbf89710':'Bancor',
'0xc6725ae749677f21e4d8f85f41cfb6de49b9db29':'Bancor',
'0x77a77eca75445841875ebb67a33d0a97dc34d924':'Bancor',
'0xcf1cc6ed5b653def7417e3fa93992c3ffe49139b':'Bancor',
'0xcc1aac4513f751effc94e259daf8a37b76f9db75':'Bancor',
'0x4f8af8e8734d02a21d73e2a4fb29941f35cbbeac':'Bancor',
'0xc39e562defc6ddd1f44ee698cf9303092b86051d':'Bancor',
'0xf73c3c65bde10bf26c2e1763104e609a41702efe':'Bibox',
'0xd4dcd2459bb78d7a645aa7e196857d421b10d93f':'BigOne',
'0xf7793d27a1b76cdf14db7c83e82c772cf7c92910':'Bilaxy',
'0x3f5ce5fbfe3e9af3971dd833d26ba9b5c936f0be':'Binance',
'0xd551234ae421e3bcba99a0da6d736074f22192ff':'Binance',
'0x564286362092d8e7936f0549571a803b203aaced':'Binance',
'0x0681d8db095565fe8a346fa0277bffde9c0edbbf':'Binance',
'0xcafb10ee663f465f9d10588ac44ed20ed608c11e':'Bitfinex',
'0x1151314c646ce4e0efd76d1af4760ae66a9fe30f':'Bitfinex',
'0x7727e5113d1d161373623e5f49fd568b4f543a9e':'Bitfinex',
'0x4fdd5eb2fb260149a3903859043e962ab89d8ed4':'Bitfinex',
'0x876eabf441b2ee5b5b0554fd502a8e0600950cfa':'Bitfinex',
'0xfbb1b73c4f0bda4f67dca266ce6ef42f520fbb98':'Bitfinex',
'0x91337a300e0361bddb2e377dd4e88ccb7796663d':'Bittrex',
'0x96fc4553a00c117c5b0bed950dd625d1c16dc894':'BTC-e',
'0x8958618332df62af93053cb9c535e26462c959b0':'Changelly',
'0x0bb9fc3ba7bcf6e5d6f6fc15123ff8d5f96cee00':'Cobinhood',
'0x9539e0b14021a43cde41d9d45dc34969be9c7cb0':'Cobinhood',
'0xb6ba1931e4e74fd080587688f6db10e830f810d5':'Coinbene',
'0x4b01721f0244e7c5b5f63c20942850e447f5a5ee':'Coindelta',
'0x81cffe15763c2daec1649681f70fabc2c57bcd55':'Coinexchange.io',
'0xa193c943980a9340f306b3d59deb183dc501b35f':'CoinFlux',
'0x0d6b5a54f940bf3d52e438cab785981aaefdf40c':'Coinhako',
'0x4aea7cf559f67cedcad07e12ae6bc00f07e8cf65':'Coss.io',
'0x8d12a197cb00d4747a1fe03395095ce2a5cc6819':'Etherdelta',
'0x9f0673e07d8f70e01747ba3ebef51ef06ace2388':'Etherdelta',
'0x915d7915f2b469bb654a7d903a5d4417cb8ea7df':'Fcoin',
'0x0d0707963952f2fba59dd06f2b425ace40b492fe':'Gate.io',
'0x6fc82a5fe25a5cdb58bc74600a40a69c065263f8':'Gemini',
'0xd24400ae8bfebb18ca49be86258a3c749cf46853':'Gemini',
'0x9c67e141c0472115aa1b98bd0088418be68fd249':'HitBTC',
'0x59a5208b32e627891c389ebafc644145224006e8':'HitBTC',
'0x46705dfff24256421a05d056c29e81bdc09723b8':'Huobipro',
'0xe93381fb4c4f14bda253907b18fad305d799241a':'Huobipro',
'0xfa4b5be3f2f84f56703c42eb22142744e95a2c58':'Huobipro',
'0xab5c66752a9e8167967685f1450532fb96d5d24f':'Huobipro',
'0x6748f50f686bfbca6fe8ad62b22228b87f31ff2b':'Huobipro',
'0xfdb16996831753d5331ff813c29a93c76834a0ad':'Huobipro',
'0xeee28d484628d41a82d01e21d12e2e78d69920da':'Huobipro',
'0x2a0c0dbecc7e4d658f48e01e3fa353f44050c208':'IDEX',
'0x2910543af39aba0cd09dbb2d50200b3e800a63d2':'kraken',
'0x0a869d79a7052c7f1b55a8ebabbea3420f0d1e13':'kraken',
'0xe853c56864a2ebe4576a807d26fdc4a0ada51919':'Kraken',
'0x267be1c1d684f78cb4f6a176c4911b741e4ffdc0':'Kraken',
'0xa2a8f158aed54ce9a73d41eeec23bf3a51b5654d':'Kraken',
'0x3eb01b3391ea15ce752d01cf3d3f09dec596f650':'Kyber',
'0x8271b2e8cbe29396e9563229030c89679b9470db':'Liqui.io',
'0x6cc5f688a315f3dc28a7781717a9a798a59fda7b':'OKEX',
'0x209c4784ab1e8183cf58ca33cb740efbf3fc18ef':'Poloniex',
'0x32be343b94f860124dc4fee278fdcbd38c102d88':'Poloniex',
'0x70faa28a6b8d6829a4b1e649d26ec9a2a39ba413':'Shapeshift',
'0x120a270bbc009644e35f0bb6ab13f95b8199c4ad':'Shapeshift',
'0x9e6316f44baeeee5d41a1070516cc5fa47baf227':'Shapeshift',
'0x3613ef1125a078ef96ffc898c4ec28d73c5b8c52':'Tidex',
'0x390de26d772d2e2005c6d1d24afc902bae37a4bb':'Upbit',
'0xf5bec430576ff1b82e44ddb5a1c93f6f9d0884f3':'YoBit'}                            # 交易所钱包地址

exchange=[]
amount=[]
for i in webdata['data']['lists']:
    print('最近24小时大额转账记录：','转出地址：',i['from'],'接受地址:',i['to'],'转账量：',i['value'])
    for j in exchange_address:
        if i['to']==j:
            amount.append(i['value'])
            exchange.append(exchange_address[j])
print("转到交易所：",exchange,'\n','转入量：',amount)
# 出图
_, ax = plt.subplots()
ax.scatter(exchange,amount, lw = 2, color = '#539caf', alpha = 1)
ax.set_title('track')
ax.set_xlabel('address')
ax.set_ylabel('exchange')