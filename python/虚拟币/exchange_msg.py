# python3.5.2
# 作者：陈常鸿
# 交易所公告
import requests
from bs4 import BeautifulSoup
class Exchage:
    def __init__(self):
        self.gate_url='https://gateio.io'
        self.gate_msg_url='https://gateio.io/articlelist/ann'

    def get_msg_url(self):
        webdata = requests.get(self.gate_msg_url).text
        soup = BeautifulSoup(webdata, 'lxml')
        link = soup.select("div.entry > a")
        first_msg_url = self.gate_url + link[0].get('href')
        self.firstMsgURL=first_msg_url
        print(first_msg_url)

    def get_msg(self):
        webdata=requests.get(self.firstMsgURL).text
        soup=BeautifulSoup(webdata,'lxml')
        content=soup.select("div.dtl-title > h2")
        print(content)
