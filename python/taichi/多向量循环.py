import taichi as ti
ti.init()

x = ti.var(ti.i32, (3,2))  # 数据类型为32位整型，张量形状为3x2

@ti.kernel
def my_kernel():
  for i, j in x:  # 自动根据 x 的大小决定区间
    if (i+j)>1:
        x[i, j] = i + j
        print('hasaki',i+j)
    else:
        print('-----',i+j)


my_kernel()
print(x.to_numpy())  # 输出 np.array([[0, 1], [1, 2], [2, 3]])
