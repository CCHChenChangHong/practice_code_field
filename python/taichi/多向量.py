import taichi as ti
ti.init()

x = ti.var(ti.i32, (3,2))  # 数据类型为32位整型，张量形状为3x2

x[1, 0] = 12
print(x[1, 0])  # 输出12

arr = x.to_numpy()  # 返回一个 np.ndarray 对象
print(arr.shape)  # 输出 (3, 2)
print(arr)  # 输出 np.array([[0, 0], [12, 0], [0, 0]])