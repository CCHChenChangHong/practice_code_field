import taichi as ti

ti.init()

x = ti.var(ti.i32, 4)  # 数据类型为32位整型，数组长度为4

x[1] = 12
x[2] = 24
x[3] = 36
print(x[1],x[2],x[3])  # 输出12

@ti.kernel
def test():
    for i in range(4):
        x[i]=i

test()
print(x.to_numpy())  # 转换成numpy数组


# 0维向量
y = ti.var(ti.i32, ())  # 形状参数设定为 () 就是 0 维张量了   这个药放在前面才不会报错
# 报错-AssertionError: No new variables can be declared after kernel invocations or Python-scope tensor accesses.

y[None]= 233
print(y[None])  # 输出233
