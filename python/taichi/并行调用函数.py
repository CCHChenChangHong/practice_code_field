import taichi as ti

@ti.kernel
def test():
    for i in range(10):
        print('outside',i)
        hasaki(i)

@ti.func
def hasaki(i):
    for j in range(5):
        print('inside : ',i,'num : ',j)

if __name__=='__main__':
    test()