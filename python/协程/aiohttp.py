# 相关资料：https://www.cnblogs.com/ssyfj/p/9222342.html
#        ：https://www.cnblogs.com/shenh/p/9090586.html
# 官方git资料：https://github.com/aio-libs/aiohttp
import time,asyncio,aiohttp


url = 'https://www.baidu.com/'
async def hello(url,semaphore):
    async with semaphore:
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:   # session.post(url,headers=headers,params=params)
                return await response.read()


async def run():
    semaphore = asyncio.Semaphore(500) # 限制并发量为500  linux上限为1000，window为500
    to_get = [hello(url.format(),semaphore) for _ in range(1000)] #总共1000任务
    await asyncio.wait(to_get)


if __name__ == '__main__':
#    now=lambda :time.time()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run())
    loop.close()