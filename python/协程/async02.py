import asyncio
import datetime

async def hashaki(x,y):
    print("hashaki",x,y)
    await asyncio.sleep(1)
    return x+y

async def other(x,y):
    c=await hashaki(x,y)
    print("other",x,y,c)

loop=asyncio.get_event_loop()
loop.run_until_complete(other(2,2))
loop.close()