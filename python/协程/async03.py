import time
import asyncio

# 定义异步函数
async def hello():
    asyncio.sleep(1)
    print('hasaki : %s' % time.time())

def run():
    for i in range(5):
        loop.run_until_complete(hello())

loop = asyncio.get_event_loop()
if __name__ =='__main__':
    run()

'''
hasaki:1527595104.8338501
hasaki:1527595104.8338501
hasaki:1527595104.8338501
hasaki:1527595104.8338501
hasaki:1527595104.8338501
'''

###########################另一种，阻塞的情况###################################
import time
import asyncio

# 定义异步函数
async def hello():
    await asyncio.sleep(1)              # 加上了await之后，其他协程也要等待这个睡眠的时间
    print('hasaki : %s' % time.time())

def run():
    for i in range(5):
        loop.run_until_complete(hello())

loop = asyncio.get_event_loop()
if __name__ =='__main__':
    run()

'''
hasaki:1527595104
hasaki:1527595105
hasaki:1527595106
hasaki:1527595107
hasaki:1527595108
'''