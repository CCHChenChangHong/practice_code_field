import asyncio
 
import time
 
now = lambda: time.time()
 
async def do_some_work(x):
    print('等待时间', x)
 
    await asyncio.sleep(x)     # 挂起执行别的东西
    return '在{}秒后完成'.format(x)
 
start = now()
 
coroutine1 = do_some_work(1)  # 任务一
coroutine2 = do_some_work(2)  # 任务二
coroutine3 = do_some_work(4)  # 任务三
 
tasks = [
    asyncio.ensure_future(coroutine1),
    asyncio.ensure_future(coroutine2),
    asyncio.ensure_future(coroutine3)
]
 
loop = asyncio.get_event_loop()      # 总循环
loop.run_until_complete(asyncio.wait(tasks))    # 开始运行
 
for task in tasks:
    print('Task ret: ', task.result())
 
print('总时间: ', now() - start)