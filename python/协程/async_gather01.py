import asyncio
from datetime import datetime

async def hashaki(name,num):
    f=1
    for i in range(2,num+1):
        print("hashaki 1",name,'循环代数',i,'时间',datetime.now())
        await asyncio.sleep(1)
        f*=i
    print("hashaki 2",name,'结束代数',num,'f的值',f,'时间',datetime.now())

loop=asyncio.get_event_loop()
loop.run_until_complete(asyncio.gather(hashaki('a',2),hashaki('b',3),hashaki('c',4)))
loop.close()

'''
hashaki 1 b 循环代数 2 时间 2018-11-28 08:04:26.480684
hashaki 1 a 循环代数 2 时间 2018-11-28 08:04:26.480684
hashaki 1 c 循环代数 2 时间 2018-11-28 08:04:26.480684
hashaki 1 b 循环代数 3 时间 2018-11-28 08:04:27.480932
hashaki 2 a 结束代数 2 f的值 2 时间 2018-11-28 08:04:27.480932
hashaki 1 c 循环代数 3 时间 2018-11-28 08:04:27.480932
hashaki 2 b 结束代数 3 f的值 6 时间 2018-11-28 08:04:28.481500
hashaki 1 c 循环代数 4 时间 2018-11-28 08:04:28.481500
hashaki 2 c 结束代数 4 f的值 24 时间 2018-11-28 08:04:29.483667
'''