import asyncio
import datetime

async def hashaki(future):
    await asyncio.sleep(1)
    future.set_result("hashaki")

loop=asyncio.get_event_loop()
future=asyncio.Future()
asyncio.ensure_future(hashaki(future))
loop.run_until_complete(future)
print(future.result())
loop.close()