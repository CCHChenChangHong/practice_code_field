import sys
import os

# 使用os._exit(0),和sys.exit(0)都可以使编译器推出，但是sys.exit可以引发SystemExit异常
# 可以捕捉异常处理，但是os._exit就不会引起异常而是直接退出编译器，所以sys.exit会更优雅一点

sys.exit(0)
os._exit(0)