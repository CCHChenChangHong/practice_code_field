# 获取运行环境的编码
import sys
print(sys.stdout.encoding)   # 'ANSI_X3.4-1968'

# 修改运行环境的编码
import codecs
sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
