import platform as p
print(p.python_version())

>>> import sys
>>> print(sys.version)
3.6.3 (v3.6.3:2c5fed8, Oct  3 2017, 18:11:49) [MSC v.1900 64 bit (AMD64)]
>>> print(sys.version_info)
sys.version_info(major=3, minor=6, micro=3, releaselevel='final', serial=0)
>>> print(sys.version_info.major)
3