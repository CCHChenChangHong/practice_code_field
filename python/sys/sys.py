# build in python 3.5
# write by hashaki first edit on 20181023 last change on 20181023

import sys

print(sys.platform)             # 查看系统
print(sys.argv[0])              # 当前运行的文件，hashaki.py

for i in range(100):
    if i==10:
        sys.exit(1)             # 中途退出，0是正常退出，其他为异常
    print(i)

# 获取指定模块搜索路径的字符串集合，可以将写好的模块放在得到的某个路径下，就可以在程序中import时正确找到。
print(sys.path)
sys.path.append("/home/hashaki/桌面/")

# sys.modules是一个全局字典，该字典是python启动后就加载在内存中。每当程序员导入新的模块，sys.modules将自动记录该模块。
# 当第二次再导入该模块时，python会直接到字典中查找，从而加快了程序运行的速度。它拥有字典所拥有的一切方法。
# 貌似没什么用。。。。。
print(sys.modules.keys())
print(sys.modules.values())
print(sys.modules['os'])

# 内存测量
sys.getsizeof("hashaki")    # 56