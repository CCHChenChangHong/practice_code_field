'''
一只公鸡5元，一只母鸡3元，而3只小鸡1元，100元买100只鸡，这100只鸡中公鸡，母鸡，小鸡各多少？(有三种答案)
公鸡:4 母鸡:18 小鸡:78
公鸡:8 母鸡:11 小鸡:81
公鸡:12 母鸡:4 小鸡:84
'''
for i in range(1,21):
    for j in range(1,34):
        for k in range(100):
            num=i+j+k
            if num!=100:
                continue     
            money=15*i+9*j+k
            if money!=300:
                continue
            print('公鸡:{} 母鸡:{} 小鸡:{}'.format(i,j,k))