# bytes转字符串类型
# 下面这个bytes类型本来是一行的，为了好看，手动换了行
b_=b'92fb5c39a70d1e0f5bb765acbabf4ddbd5125fb9\trefs/tags/release-0.9.1-bitmex\n
332850f199e07e8822ffdcd51eeb5567fbf72956\trefs/tags/release-0.9.1-bitmex^{}\n
4faa9fc594d75fbf92b51bebe6656d92457c5a9f\trefs/tags/v0.9.1-bitmex\n
ca9025e427c070cb2d1d37f8c8c246d89b0612a9\trefs/tags/v0.9.1-bitmex^{}\n'

# bytes转字符串,结果尽管看起来像列表，但实际上是字符串呀
str(b_,'utf-8')
>>>
92fb5c39a70d1e0f5bb765acbabf4ddbd5125fb9       refs/tags/release-0.9.1-bitmex
332850f199e07e8822ffdcd51eeb5567fbf72956        refs/tags/release-0.9.1-bitmex^{}
4faa9fc594d75fbf92b51bebe6656d92457c5a9f        refs/tags/v0.9.1-bitmex
ca9025e427c070cb2d1d37f8c8c246d89b0612a9        refs/tags/v0.9.1-bitmex^{}