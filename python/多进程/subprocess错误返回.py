# 三种方法
'''
try:
    next(iter(engine_run.stderr.readline,'b'))
    print(' *********engine subprocess error********* ： {}'.format([i for i in iter(engine_run.stderr.readline,'b')]))
    sleep(1)
    engine_run.kill()
    sleep(1)
    quit()
except StopIteration:
    pass
'''
'''
if len(engine_run.stderr.read())>0:
    print(' *********engine subprocess error********* ： {}'.format(engine_run.stderr.read()))
    sleep(1)
    engine_run.kill()
    sleep(1)
    quit()
'''
while 1:
    try:
        out,err=engine_run.communicate(timeout=30)
    except TimeoutError:
        engine_run.kill()
        out,err=engine_run.communicate()