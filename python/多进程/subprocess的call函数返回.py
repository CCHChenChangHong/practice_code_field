# call当然是等待子进程结束过后再继续执行主进程
# 如果子进程有错误，但不会影响到主进程，因为错误也是一种结束
# 所以要检查subprocess.call的返回代码returncode
import subprocess
order1='go version'    # 使用子进程获取golang的版本号
order2='go get'        # 强制报错
returncode1=subprocess.call(order1,shell=True)
returncode2=subprocess.call(order2,shell=True)
if returncode1!=0:
    print("子进程没有正确运行，返回的returncode1是：{}  returncode1的类型是：{}",format(returncode1,type(returncode1)))
# 子进程正确运行
# go version go1.11.4 windows/amd64
# type returncode : int  returncode1==0
# 子进程错运行
# can't load package: package .: no Go files in C:\Users\cchyp
# returncode2==1