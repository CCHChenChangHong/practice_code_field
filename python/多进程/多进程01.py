import multiprocessing as mp
import time
def hashaki(q):
    while 1:
        q.put("hashaki")
        time.sleep(1)

if __name__=='__main__':
    q=mp.Queue()
    p = mp.Process(target=hashaki, args=(q,))
    p.start()
    # 要在队列中不断put，才能不断get
    while 1:
        print(q.get())
    p.join()