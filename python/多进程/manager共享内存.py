# 共享内存的方式有Pipe,queue,Manger三种方式
# Manager共享内存的方式是复制一份数据,也就是当开多个进程的时候,数据内存会依然增加
from multiprocessing import Manager,Process

def func(m_list):
    m_list.pop()


if __name__=='__main__':
    m=Manager()
    m_list=m.list(["gg","gugu","hasaki"])      # 核心部分,创建一个用于进程通信的数据
    print('origin data : \n{}'.format(m_list))
    p=Process(target=func,args=(m_list,))
    p.start()
    p.join()
    print('later data : \n{}'.format(m_list))

'''
origin data :
['gg', 'gugu', 'hasaki']
later data :
['gg', 'gugu']
'''