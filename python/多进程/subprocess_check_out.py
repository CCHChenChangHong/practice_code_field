# call 和 check_call正常运行时都是返回状态码，而check_out则会返回子进程终端的打印
import subprocess

git_address='git.coding.net/xxxx/wequant-quant-engine.git'                  # 引擎git
git_order='git ls-remote --tags https://xxx:xxx@{}'.format(git_address)
git=subprocess.check_output(git_order,shell=True)
print('下载引擎结束',type(git))
>>>下载引擎结束 <class 'bytes'>