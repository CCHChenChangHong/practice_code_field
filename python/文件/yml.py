from ruamel import yaml

with open('./docker-compose.yml', encoding="utf-8") as f:
    content = yaml.load(f, Loader=yaml.RoundTripLoader)
image:str= "registry.cn-guangzhou.aliyuncs.com/hasakichen/base_python3:v2.1.1"
volumes:str="/home/hasaki/testDockerFile:/home"
command:list=['sh','-c','python3 /home/main.py\n']
for i in range(int(taskNum)):
    i+=1
    data={'la_python_'+str(i):{'image':image,'volumes':[volumes],'command':['sh','-c','python3 /home/main.py']}}
    content['services'].update(data)
with open('./docker-compose-demo.yml', 'w', encoding="gbk") as nf:
    yaml.dump(content, nf, Dumper=yaml.RoundTripDumper)