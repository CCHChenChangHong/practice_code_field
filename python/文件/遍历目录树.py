# write by hasaki
# 功能：获取文件目录树
# 方法：遍历目录树，生成另一个相同的目录树
import os

file_path='C:/hashaki/工作交接'

for i,j,k in os.walk(file_path):
    #print('这是第一个结果',i,'结果的类型是',type(i))            # 只显示文件夹的名，文件名没有显示，结果是字符串
    #print('这是第二个结果',j,'结果的类型是',type(j))            # 只显示文件夹的名，文件名没有显示,结果是个列表
    print('这是第三个结果',k,'结果的类型是',type(k))             # 没有文件夹的名字，只有文件名(当前文件夹下所有文件名)，结果是个列表


'''
这是第一个结果 C:/hashaki/工作交接 结果的类型是 <class 'str'>
这是第一个结果 C:/hashaki/工作交接\工作交接 结果的类型是 <class 'str'>
这是第一个结果 C:/hashaki/工作交接\工作交接\bianyipyc 结果的类型是 <class 'str'>
这是第一个结果 C:/hashaki/工作交接\工作交接\bianyipyc\.idea 结果的类型是 <class 'str'>
这是第一个结果 C:/hashaki/工作交接\工作交接\bianyipyc\.idea\inspectionProfiles 结果的类型是 <class 'str'>
这是第一个结果 C:/hashaki/工作交接\工作交接\bianyipyc\build 结果的类型是 <class 'str'>
这是第一个结果 C:/hashaki/工作交接\工作交接\bianyipyc\build\lib 结果的类型是 <class 'str'>



这是第二个结果 ['工作交接'] 结果的类型是 <class 'list'>
这是第二个结果 ['bianyipyc'] 结果的类型是 <class 'list'>
这是第二个结果 ['.idea', 'build'] 结果的类型是 <class 'list'>
这是第二个结果 ['inspectionProfiles'] 结果的类型是 <class 'list'>
这是第二个结果 [] 结果的类型是 <class 'list'>
这是第二个结果 ['lib'] 结果的类型是 <class 'list'>
这是第二个结果 [] 结果的类型是 <class 'list'>



这是第三个结果 [] 结果的类型是 <class 'list'>
这是第三个结果 ['python开发规范.txt', '交接.txt', '策略加密打包02.pptx'] 结果的类型是 <class 'list'>
这是第三个结果 ['be_test.pyc', 'main.py', 'packages.py', 'setup.py', 'text_pyc.py'] 结果的类型是 <class 'list'>
这是第三个结果 ['bianyipyc.iml', 'misc.xml', 'modules.xml', 'workspace.xml'] 结果的类型是 <class 'list'>
这是第三个结果 [] 结果的类型是 <class 'list'>
这是第三个结果 [] 结果的类型是 <class 'list'>
这是第三个结果 ['main.py'] 结果的类型是 <class 'list'>


'''
