# 保存成json的时候，会由自动换行格式化的操作
with open(self.save_json_path,'r',encoding='utf-8') as json_file:
    json_data=json.load(json_file)

# 更新数据
json_data.append(data)

# 写入，就是写入的时候，dump里面加入一个indent参数，表示缩进行数
with open(self.save_json_path,'w',encoding='utf-8') as json_file:
    json.dump(json_data,json_file,ensure_ascii=False,indent=4)