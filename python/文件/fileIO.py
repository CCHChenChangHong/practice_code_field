
import os

path=os.path.dirname(__file__)+'/'

print(path)  # /home/hashaki/下载/vnpy_crypto/examples/

file=path+'balabala.txt'

# mode模式：r，文件必须已存在，而且只读
#          w，文件已存在则改写，文件不存在则新建
#          a，文件已存在则追加，文件不存在则新建
#          r+，文件必须已存在，可以读取和写入，可以调用f所有方法
#          w+，文件可读可写，可以调用f所有的方法，文件已存在则改写，文件不存在则新建
#          a+，如上差不多，如果f对应下一个操作是写入，则f.seek没有效果，如果是读取则f.seek可以正常工作
with open(file,mode='w') as f:
    f.write("hashaki")
    print(f.tell())   # 7

# f的方法
# f.close() 调用后就不能再调用f了
# f.closed()是一个只读属性，当已经调用close()后，返回真
# f.flush()请求把f的缓冲区写到操作系统
# f.fileno()返回一个数字，是操作系统级别的文件标识符
# f.read()
# f.readline()读取一行直到'\n'
# f.seek()
# f.tell() 返回以字节为单位的整数偏移量

