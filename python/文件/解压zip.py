import zipfile

def unpackZip(unzipFile):
    '''unzipFile是压缩文件的地址'''
    zf = zipfile.ZipFile(unzipFile)
    dest_dir=unzipFile.replace('/src.zip','')     # 把压缩文件src.zip解压到当前文件夹
    try:
        zf.extractall(path=dest_dir)
    except RuntimeError as e:
        print(e)
    zf.close()


