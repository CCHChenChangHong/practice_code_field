# write by hasaki
# 把代码文件进行压缩打包，win平台打包成zip，linux平台打包成tar.gz
import os
import sys
import zipfile
import tarfile
import platform

file_path='C:/Users/cchyp/Desktop/工作交接'
zip_path='C:/Users/cchyp/Desktop/工作交接.zip'
def make_zip(dirname,zipfilename):
    '''打包成zip'''
    z = zipfile.ZipFile(zipfilename,'w',zipfile.ZIP_DEFLATED) #参数一：文件夹名
    for dirpath, dirnames, filenames in os.walk(dirname):
        fpath = dirpath.replace(dirname,'') #这一句很重要，不replace的话，就从根目录开始复制
        fpath = fpath and fpath + os.sep or ''#这句话理解我也点郁闷，实现当前文件夹以及包含的所有文件的压缩
        for filename in filenames:
            z.write(os.path.join(dirpath, filename),fpath+filename)
            print ('压缩成功')
    z.close()

def make_tar():
    '''打包成tar'''
    pass

def run():
    '''跑起来'''
    if 'Linux' in platform.platform():
        make_tar()   
    
    elif 'Window' in platform.platform():
        print('源文件：',file_path,'打包后的文件：',zip_path)
        make_zip(file_path,zip_path)
    
    else:
        raise Exception('既不是window系统也不是linux系统')

if __name__=='__main__':
    run()