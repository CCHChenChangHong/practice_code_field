import json
path='C:\hashaki\work/hasaki.json'

json_data=[
    {
        "name": "bitmex_xbt_ma_5min",
        "className": "BitmexMAStrategy",
        "market": "crypto",
        "exchage": "BITMEX",
        "vtSymbol": "XBTUSD",
        "symbol":"bitmex_xbtusd",
        "frequency": "5min",
        "barNum": 250000,
        "n1":10,
        "n2":3,
        "fixedSize": 10
    }
]

with open(path,'w',encoding='utf-8') as json_file:
    json.dump(json_data,json_file,ensure_ascii=False)
