# 冒号是参数类型的提示，->是返回的类型，都是提示而已，就算传错也不会报错的，用于代码重构和分析

def twoSum(num1: int, num2: int=100) -> int:
    sum = num1 + num2
    return sum
    

if __name__ == "__main__":
    print(twoSum.__annotations__)
    print(twoSum(1,2))
    print(twoSum(1))
    print(twoSum('I love ','hasaki'))

'''
{'num1': <class 'int'>, 'num2': <class 'int'>, 'return': <class 'int'>}
3
101
I love hasaki
'''