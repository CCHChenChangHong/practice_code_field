import functools

# 示例函数
def hasaki(string):
    print(string)

def hasaki2(func)
    @functools.wraps(func)
    def wrapper(string):
        print(string)
        return func(string)  # 这个返回值不是必要的，如果没有这个返回，那么被装饰的函数将不会执行本身的代码
    return wrapper

@hasaki2
def hasaki2excample(string):
    print('hasaki2excample')

# partial用法,已经有了一个默认值
hasaki_partial=functools.partial(hasaki,'hasaki')
>>>hasaki_partial()
>>>hasaki

# wraps用法
>>>hasaki2excample('hasaki2')
>>>hasaki2
>>>hasaki2excample
# 注释掉hasaki2函数钟的wrapper里面的返回时
>>>hasaki2    # 此时不再打印hasaki2excample里面的hasaki2excample字符串