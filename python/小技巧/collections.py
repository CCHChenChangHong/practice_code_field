# collections各类集合的使用
# https://www.liaoxuefeng.com/wiki/001374738125095c955c1e6d8bb493182103fac9270762a000/001411031239400f7181f65f33a4623bc42276a605debf6000

import collections

# namedtuple
a=collections.namedtuple('hashaki',['a','b'])
ha=a(810,814)
print(ha.a,ha.b)
print(isinstance(ha,a))
print(isinstance(ha,tuple))

# deque适合用于队列和栈
a=collections.deque([1,2,3])
a.append(4)
a.appendleft(0)
print(a)
a.pipleft()
print(a)

# 使用dict时，如果引用的Key不存在，就会抛出KeyError。如果希望key不存在时，返回一个默认值，就可以用defaultdict
a=defaultdict(list)
b=defaultdict(str)
print(a[1],'\t',b[1])   # []    ''
c=a['hashaki']
c.append(910)    # {'hashaki':910}

# 使用dict时，Key是无序的。在对dict做迭代时，我们无法确定Key的顺序
a=collections.OrderedDict([('a',1),('b',2),('c',3)])
d=dict([('a',1),('b',2),('c',3)])
print(a,'\n',d)

# ------------------------------------------------------------------
from collections import OrderedDict

a=OrderedDict()
a['a']=1
a['b']=2
a['c']=3

#a.clear()  # 清空
'''
for i in a.items():
    print(i)
'''
for i in list(a.items()):
    print(i[1])     # ('a',1),('b',2),('c',3)

#print(a.keys())
# -------------------------------------------------------------------------


# Counter是一个简单的计数器，例如，统计字符出现的个数
a=collections.Counter()
for i in 'hashaki':
    c[i]=c[i]+1

print(c)