import cProfile

# ncalls:函数被调用的次数
# totime:函数总运行时间， 不含调用函数 运行的时间
# percall：函数运行一次的平均时间，等于totime/ncalls
# cumtime:函数运行总时间，包含调用时间
# percall:
# filename:函数名，行号，函数所在文件名
def haha():
    print("hashaki")

if __name__=='__main__':
    cProfile("haha()")
    