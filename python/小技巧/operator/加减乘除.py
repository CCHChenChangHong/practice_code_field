from operator import add,sub,mul,floordiv,truediv,mod,eq

a=5
b=2
print(add(a,b)) # a+b
print(sub(a,b)) # a-b
print(mul(a,b)) # a*b
print(floordiv(a,b)) # a/b整除
print(truediv(a,b)) # a/b浮点
print(mod(a,b)) # a%b
print(eq(a,b)) # a==b