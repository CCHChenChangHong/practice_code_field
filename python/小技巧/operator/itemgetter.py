from operator import itemgetter
a=[('a',1,),('d',4),('b',2),('c',3)]

for i in sorted(a,key=itemgetter(1)):
    print(i)
#('a', 1)
#('b', 2)
#('c', 3)
#('d', 4)