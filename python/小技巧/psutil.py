import psutil

# https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000/001511052957192bb91a56a2339485c8a8c79812b400d49000
print(psutil.cpu_count(),'\n',psutil.cpu_count(logical=False))
print(psutil.cpu_times())
print(psutil.virtual_memory(),'\n',psutil.swap_memory())         # 获取内存信息
print(psutil.disk_partitions(),'\n',psutil.disk_usage('/'),'\n',psutil.disk_io_counters())   # 获取磁盘信息
print(psutil.net_io_counters(),'\n',psutil.net_if_addrs(),'n',psutil.net_if_stats())         #获取网络信息
print(psutil.pids())    # 获取进程信息