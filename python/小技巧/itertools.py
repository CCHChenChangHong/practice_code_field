# python内置循环器
# https://www.liaoxuefeng.com/wiki/001374738125095c955c1e6d8bb493182103fac9270762a000/001415616001996f6b32d80b6454caca3d33c965a07611f000
import itertools

# 自然数无限
a=itertools.count(1)
for i in a:
    print(i)

# 无限循环
cs=itertools.cycle('YPL')
for i in cs:
    print(i)

# 重复循环指定次，第二参数可以不指定
ns=itertools.repeat('LOVE',10)
for i in ns:
    print(i)

# 先连接再迭代
c=itertools.chain('YPL','LOVE','CCH')
for i in c:
    print(i)