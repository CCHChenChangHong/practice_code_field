# all类似且关系,在可迭代对象中，有一个不符合都不行，any类似or，只要满足一个即可
a=[1,2,3,4,5]
if all(x>3 for x in a):
    print("hashaki")
else:
    print("No all")

if any(x>3 for x in a):
    print("hashaki")
else:
    print("NO any")

# # 结果为：No all  /   hashaki