# 使用del对象，内存不会马上被回收,所以得使用gc来释放内存
import gc

gc.collect()