# make in python3.5
# hashaki practice
# HOW TO USE MONGO-DB IN PYTHON
from pymongo import MongoClient
client=MongoClient()

# 查看数据库
print(client.database_names())
print(client.list_database_names())   # 返回['db1','db2']

# 选择数据库,hashaki是数据库名字，hatable是数据集名字
db=client['hashaki']
collection=db.hatable

# 查看表
print(db.list_collection_names())

# 查询数据
result=collection.find_one({"name":"YPL"})  #单个查找
result=collection.find_one()  # 第一条数据

results=collection.find({"type":"1Min"})
print([i for i in results])

# 插入数据
data={"name":"CCH","date":20180914}
collection.insert(data)

many_data=[{"name":"AAA","date":20180909},{"name":"BBB","date":20181010}]
collection.insert_many(many_data)

# 修改
result=collection.update_one({'name':'xfy23'},{'$inc':{'age':12}})
results=collection.update_many({},{'$inc':{'age':22}})

# 删除
result=collection.delete_one({'name':'xfy23'})
result=collection.delete_many({'sex':'n'})