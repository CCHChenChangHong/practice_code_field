from pymongo import MongoClient
import pandas as pd
client=MongoClient()

# 选择数据库,hashaki是数据库名字，hatable是数据集名字
db=client['hashaki']
collection=db.hatable


# 读取CSV
filePath='/home/hashaki/pycharm-work/testKline.csv'
dataFromPD=pd.read_csv(filePath)
data={}
# 写入数据库
for i,j,k,l,m,n in zip(dataFromPD['Unnamed: 0'],dataFromPD['vol'],dataFromPD['close'],dataFromPD['high'],dataFromPD['low'],dataFromPD['open']):
    data['time']=i
    data['vol']=j
    data['close']=k
    data['high']=l
    data['low']=m
    data['open']=n
    collection.insert_one(data)
    data={}


