import redis

r = redis.Redis(host='localhost', port=6379, decode_responses=True)  
r.set('name', 'hasaki')  # 设置 name 对应的值

print(r.get('name'))
# >>> hasaki

# 保存复杂数据结构进redis(投机取巧)
import json
pythonListData:list=[{'a':1},{'b':2}]
data:str=json.dumps(pythonListData)  # 序列化list结构
r.set('data',data)      # 上传json数据
