# wirte by hasaki
# first edit on 2018/12/19
# last change on 2018/12/19
# 把本地mongoDB的数据转换成CSV
import pandas as pd
import pymongo
from datetime import datetime,timedelta

import sys
import os
sys.path.append(os.getcwd())
'''
数据库名称列表
['admin', 'config', 'crypto_12hour', 'crypto_15min', 'crypto_1day', 'crypto_1hour', 'crypto_1min', 'crypto_1month', 
'crypto_1week', 'crypto_30min', 'crypto_3hour', 'crypto_4hour', 'crypto_5min', 'crypto_6hour', 'future_15min', 'future_1day',
 'future_1hour', 'future_1min', 'future_1month', 'future_1week', 'future_30min', 'future_5min', 'futuresf_15min', 'futuresf_1day', 
 'futuresf_1hour', 'futuresf_1min', 'futuresf_1month', 'futuresf_1week', 'futuresf_30min', 'futuresf_5min', 'local', 'symParameter', 'update_record']
'''
'''
表的形式
'bitfinex_eos_usd'
'''
'''
单条数据的字段
{'_id': ObjectId('5c171503c2548c0721902ddc'), 'start_time': datetime.datetime(2018, 12, 17, 1, 0),
 'open': 1.975, 'close': 1.9652, 'high': 1.9767, 'low': 1.9524, 'volume': 148281.89352397, 'end_time': datetime.datetime(2018, 12, 17, 2, 0), 
 'symbol': 'bitfinex_eos_usd', 'frequency': '1hour', 'amount': 0.0, 'openInterest': 0.0}
'''
a=pymongo.MongoClient("192.168.101.88",27017)
collection=a['crypto_1hour']['bitfinex_eos_usd']

data_file='C:\hashaki\work/run/testData.csv'
init_file='C:\hashaki\work/run/testInit.csv'

strategyStartDate = datetime.strptime('20180101', '%Y%m%d')
dataStartDate = strategyStartDate - timedelta(10)
dataEndDate = datetime.strptime('20181212', '%Y%m%d')
dataEndDate.replace(hour=23, minute=59)

flt1 = {'start_time': {'$gte': dataStartDate,
                                '$lt': strategyStartDate}}
flt2 = {'start_time': {'$gte':strategyStartDate,
                                '$lte': dataEndDate}}

cursor = collection.find(flt1).sort('start_time', pymongo.ASCENDING)
init_list=[]
for d in cursor:
    init_list.append(d)
temp_init=pd.DataFrame(init_list)
temp_init.to_csv(init_file,index=False)

# 读取回测数据
cursor = collection.find(flt2).sort('start_time', pymongo.ASCENDING)

data_list=[]
for d in cursor:
    data_list.append(d)
temp_data=pd.DataFrame(data_list)
temp_data.to_csv(data_file,index=False)
