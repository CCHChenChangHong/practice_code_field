from pymongo import MongoClient
import pandas as pd
client=MongoClient()

# 选择数据库,hashaki是数据库名字，hatable是数据集名字
db=client['hashaki']
collection=db.hatable

# 正则处理字符
result=collection.find({"time": {'$regex': '^2018-09-30.*'}}) # 这种用法的返回值可以用迭代 用FOR循环读出
#result=collection.find({"time": {'$text': {'$search': '2018-09-30'}}})  # 这个返回的是一个数据库光标
#print(type(result))
for i in result:
    print(i)