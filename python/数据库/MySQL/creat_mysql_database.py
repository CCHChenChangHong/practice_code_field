# build in python3.5.2
# 创建一个mysql数据库
import pymysql

# connect the database
# the argvs based on the database you set.
# Generally speaking, you should change the No. of the port 3306 , because it's easy to be  attack
# localhost = 127.0.0.1
conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='******')
curs = conn.cursor()

# create a database named hashaki
# Ensure the program can run multiple times,we should use try...exception
try:
    curs.execute('create database hashaki')
except:
    print('Database hashaki exists!')

conn.select_db('hashaki')


conn.commit()
curs.close()
conn.close()