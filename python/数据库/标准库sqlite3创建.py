# python内置的关系型数据库
import sqlite3

# 创建了一个数据库文件在
conn = sqlite3.connect("test.db")

c = conn.cursor()

# 创建表
c.execute('''CREATE TABLE category
      (id int primary key, sort int, name text)''')
c.execute('''CREATE TABLE book
      (id int primary key, 
       sort int, 
       name text, 
       price real, 
       category int,
       FOREIGN KEY (category) REFERENCES category(id))''')

#保存改变
conn.commit()

# 关闭数据库
conn.close()