pragma solidity ^0.5.9;

/*
<address>.balance (uint256)：
Address的余额，以wei为单位。
<address>.transfer(uint256 amount)：
发送给定数量的ether，以wei为单位，到某个地址。失败时抛出异常。
<address>.send(uint256 amount) returns (bool):
发送给定数量的ether，以wei为单位，到某个地址。失败时返回false。
<address>.call(...) returns (bool)：
发起底层的call调用。失败时返回false。
<address>.callcode(...) returns (bool)：
发起底层的callcode调用，失败时返回false。
<address>.delegatecall(...) returns (bool)：
发起底层的delegatecall调用，失败时返回false
*/