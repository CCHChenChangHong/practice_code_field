pragma solidity ^0.5.9;

/*
区块和交易的属性（Block And Transaction Properties）
block.blockhash(uint blockNumber) returns (bytes32)，给定区块号的哈希值，只支持最近256个区块，且不包含当前区块。
block.coinbase (address) 当前块矿工的地址。
block.difficulty (uint)当前块的难度。
block.gaslimit (uint)当前块的gaslimit。
block.number (uint)当前区块的块号。
block.timestamp (uint)当前块的时间戳。
msg.data (bytes)完整的调用数据（calldata）。
msg.gas (uint)当前还剩的gas。
msg.sender (address)当前调用发起人的地址。
msg.sig (bytes4)调用数据的前四个字节（函数标识符）。
msg.value (uint)这个消息所附带的货币量，单位为wei。
now (uint)当前块的时间戳，等同于block.timestamp
tx.gasprice (uint) 交易的gas价格。
tx.origin (address)交易的发送者（完整的调用链）
msg的所有成员值，如msg.sender,msg.value的值可以因为每一次外部函数调用，或库函数调用发生变化（因为msg就是和调用相关的全局变量）。

如果你想在库函数中，用msg.sender实现访问控制，你需要将msg.sender做为参数（就是说不能使用默认的msg.value，因为它可能被更改）。
*/
