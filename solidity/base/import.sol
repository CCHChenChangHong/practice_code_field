// solidity练习，加载库

// 全局引入
import "filename";

// 自定义名命空间引入
import * as hasaki from "filename";

// 关于路径,非.头的路径会被认为是绝对路径，所以要引用同目录下的文件使用
import "./x" as x;