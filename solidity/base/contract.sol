// 合约结构
pragma solidity ^0.5.9;

// 一个简单的合约结构
contract simpleStorage{

    uint valueStore; //state variable

}

// 函数,函数返回不用return，而在函数括号外定义，有点像go
contract simpleFunc{
    // 一个简单的函数
    function hasaki(uint a) returns (uint result) {
        result=a+1;
    }
}
