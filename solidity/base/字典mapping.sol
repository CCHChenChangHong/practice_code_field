pragma solidity ^0.5.9;

contract MappingExcample{
    mapping (address =>uint ) public balance;
    function hasaki(uint z) public returns (address){
        balance[msg.sender] = z;         // =这里还是要两边空格
        return msg.sender;
    }
}